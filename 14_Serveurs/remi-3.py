#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import remi.gui as gui
from remi import start, App

class MonAppliGUI(App):
    ''' Objet de création d'un exemple d'interface REMI '''
    def __init__(self, *args):
        ''' Initialisations héritées de l'objet App avec les paramètres '''
        super(MonAppliGUI, self).__init__(*args)

    def main(self):
        ''' Création de la fenêtre principale '''
        # Création des éléments de de l'espace de travail
        content = gui.Label('Bonjour tout le monde !', width='80%', height='50%', style={"white-space":"pre"}) # Créé le texte à afficher dans l'espace de travail

        return content

if __name__ == "__main__":
    # Démarrage de l'interface
    start(MonAppliGUI, standalone=True)
