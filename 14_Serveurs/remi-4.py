#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import remi.gui as gui
from remi import start, App

class MonAppliGUI(App):
    ''' Objet de création d'un exemple d'interface REMI '''
    def __init__(self, *args):
        ''' Initialisations héritées de l'objet App avec les paramètres '''
        super(MonAppliGUI, self).__init__(*args)

    def main(self):
        ''' Création de la fenêtre principale '''
        # Création des éléments de de l'espace de travail
        espacetravail = gui.VBox(width=500, height=100) # Fixe la taille de l'espace de travail
        self.content = gui.Label('Bonjour tout le monde !', width='80%', height='50%', style={"white-space":"pre"}) # Créé le texte à afficher dans l'espace de travail
        self.button_ok = gui.Button('OK', width=200, height=30) # Crée un bouton pour valider une action
        self.button_choix = gui.Button('Choix', width=200, height=30) # Crée un bouton pour valider une action

        # Configure l'action sur l'utilisation du bouton
        self.button_ok.onclick.do(self.action_du_bouton, 'OK')
        self.button_choix.onclick.do(self.action_du_bouton, 'Choix', 'Vous avec cliquez sur le bouton «Choix»')

        # Ajoute les objets de la GUI à l'espace de travail
        espacetravail.append(self.content)
        espacetravail.append(self.button_choix)
        espacetravail.append(self.button_ok)

        return espacetravail

    def action_du_bouton(self, widget, type='', message=''):
        ''' Fonction de traitement de l'action du bouton '''
        if type == 'Choix':
            self.content.set_text(message)
        if type == 'OK':
            self.content.set_text('Cliquer sur le bouton «Choix»')

if __name__ == "__main__":
    # Démarrage de l'interface
    start(MonAppliGUI, address='127.0.0.1', port=5000)
