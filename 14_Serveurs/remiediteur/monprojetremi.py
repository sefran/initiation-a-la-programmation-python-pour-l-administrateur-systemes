
# -*- coding: utf-8 -*-

from remi.gui import *
from remi import start, App


class MonAppliGUI(App):
    def __init__(self, *args, **kwargs):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        if not 'editing_mode' in kwargs.keys():
            super(MonAppliGUI, self).__init__(*args, static_file_path={'my_res':'./res/'})

    def idle(self):
        #idle function called every update cycle
        pass
    
    def main(self):
        return MonAppliGUI.construct_ui(self)
        
    @staticmethod
    def construct_ui(self):
        #DON'T MAKE CHANGES HERE, THIS METHOD GETS OVERWRITTEN WHEN SAVING IN THE EDITOR
        espacedetravail = Container()
        espacedetravail.attr_class = "Container"
        espacedetravail.attr_editor_newclass = False
        espacedetravail.css_height = "330.0px"
        espacedetravail.css_left = "105.0px"
        espacedetravail.css_position = "absolute"
        espacedetravail.css_top = "45.0px"
        espacedetravail.css_width = "540.0px"
        espacedetravail.variable_name = "espacedetravail"
        content = Label()
        content.attr_class = "Label"
        content.attr_editor_newclass = False
        content.css_height = "30px"
        content.css_left = "210.0px"
        content.css_position = "absolute"
        content.css_top = "150.0px"
        content.css_width = "100px"
        content.text = "Bonjour à tout le monde !"
        content.variable_name = "content"
        espacedetravail.append(content,'content')
        button_ok = Button()
        button_ok.attr_class = "Button"
        button_ok.attr_editor_newclass = False
        button_ok.css_height = "30px"
        button_ok.css_left = "210.0px"
        button_ok.css_position = "absolute"
        button_ok.css_top = "210.0px"
        button_ok.css_width = "100px"
        button_ok.text = "OK"
        button_ok.variable_name = "button_ok"
        espacedetravail.append(button_ok,'button_ok')
        espacedetravail.children['button_ok'].onclick.do(self.onclick_button_ok)
        

        self.espacedetravail = espacedetravail
        return self.espacedetravail
    
    def onclick_button_ok(self, emitter):
        self.espacedetravail.children['content'].set_text('Click sur OK')



#Configuration
configuration = {'config_project_name': 'MonAppliGUI', 'config_address': '127.0.0.1', 'config_port': 5000, 'config_multiple_instance': True, 'config_enable_file_cache': True, 'config_start_browser': True, 'config_resourcepath': './res/'}

if __name__ == "__main__":
    # start(MyApp,address='127.0.0.1', port=8081, multiple_instance=False,enable_file_cache=True, update_interval=0.1, start_browser=True)
    start(MonAppliGUI, address=configuration['config_address'], port=configuration['config_port'], 
                        multiple_instance=configuration['config_multiple_instance'], 
                        enable_file_cache=configuration['config_enable_file_cache'],
                        start_browser=configuration['config_start_browser'])
