#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import remi.gui as gui
from remi import start, App

class MonAppliGUI(App):
    ''' Objet de création d'un exemple d'interface REMI '''
    def __init__(self, *args):
        ''' Initialisations héritées de l'objet App avec les paramètres '''
        super(MonAppliGUI, self).__init__(*args)

    def main(self):
        ''' Création de la fenêtre principale '''
        # Création des éléments de de l'espace de travail
        espacetravail = gui.VBox(width=500, height=100) # Fixe la taille de l'espace de travail
        self.content = gui.Label('Bonjour tout le monde !', width='80%', height='50%', style={"white-space":"pre"}) # Créé le texte à afficher dans l'espace de travail
        self.button = gui.Button('OK', width=200, height=30) # Crée un bouton pour valider une action

        # Configure l'action sur l'utilisation du bouton
        self.button.onclick.do(self.action_du_bouton)

        # Ajoute les objets de la GUI à l'espace de travail
        espacetravail.append(self.content)
        espacetravail.append(self.button)

        return espacetravail

    def action_du_bouton(self, widget):
        ''' Fonction de traitement de l'action du bouton '''
        self.content.set_text('Click sur le bouton fait')
        self.button.set_text('Salut')


if __name__ == "__main__":
    # Démarrage de l'interface
    start(MonAppliGUI, debug=True, address='0.0.0.0', port=0)
