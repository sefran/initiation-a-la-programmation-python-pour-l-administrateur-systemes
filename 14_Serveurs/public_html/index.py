#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import cgi

formulaire = cgi.FieldStorage()
print("Content-type: text/html; charset=utf-8\n")
print(formulaire.getvalue("nom"))

html = """<!DOCTYPE html>
<head>
    <title>Mon site</title>
</head>
<body>
    <form action="index.py" method="post">
        <input type="text" name="nom" value="Votre nom SVP" />
        <input type="submit" name="send" value="Envoyer l'information au serveur">
    </form>
</body>
</html>
"""
print(html)

