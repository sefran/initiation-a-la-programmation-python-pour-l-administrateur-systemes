#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask

application = Flask(__name__)

@application.route('/')
def bonjour():
    return 'Bonjour de la part de Flask'
