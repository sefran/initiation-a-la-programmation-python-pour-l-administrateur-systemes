#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import http.client

connexion = http.client.HTTPConnection('utilisateur.documentation.domaine-perso.fr')
connexion.request('GET', '/initiation_developpement_python_pour_administrateur/')
resultat = connexion.getresponse()
print(resultat.status, resultat.reason)
connexion.close()
