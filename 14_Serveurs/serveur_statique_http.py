#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import http.server
import socketserver

PORT = 10000

os.chdir(os.path.expanduser('./public_html'))

with socketserver.TCPServer(('', PORT), http.server.SimpleHTTPRequestHandler) as httpd:
    print('Serveur http sur port ', PORT)
    httpd.serve_forever()
