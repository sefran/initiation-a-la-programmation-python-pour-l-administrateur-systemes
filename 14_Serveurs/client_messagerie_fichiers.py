#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import smtplib
import mimetypes
from email.message import EmailMessage

monemail = EmailMessage()
monemail.set_content('Message du client Python')
monemail['Subject'] = 'Objet du message du client Python'
monemail['From'] = 'prenom.nom@domaine-perso.fr'
monemail['To'] = 'utilisateur@localhost'

ctype, encodage = mimetypes.guess_type('./client_messagerie.py')
if ctype is None or encodage is not None:
    ctype = 'application/octet-stream'
typeprincipal, soustype = ctype.split('/', 1)
with open('./client_messagerie.py', 'rb') as fichier:
    monemail.add_attachment(fichier.read(), maintype=typeprincipal, subtype=soustype, filename='client_messagerie.py')

serveursmtp = smtplib.SMTP('localhost')
serveursmtp.send_message(monemail)
serveursmtp.quit()
