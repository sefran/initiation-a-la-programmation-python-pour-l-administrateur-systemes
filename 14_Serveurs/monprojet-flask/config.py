class Config():
    SECRET_KEY = 'ma-clé-secrète'
    LESS_BIN = '/usr/bin/lessc'
    ASSETS_DEBUG = False
    ASSETS_AUTO_BUILD = True
