import json
from app import app
from app.auth import User, utilisateurs
from flask import request, jsonify, render_template, redirect, url_for
from flask_login import login_required, login_user, logout_user, current_user

@app.route('/')
@app.route('/index')
def racine():
    if current_user.is_authenticated:
        return render_template('index.html', user=current_user)
    else:
        return redirect(url_for('connexion'))

@app.route('/apropos/')
def apropos():
    return render_template('about.html')

@app.route('/connexion', methods=['GET', 'POST'])
def connexion():
    if request.method == 'GET':
        return '''<form action='connexion' method='POST'>
                   <input type="text" name="identifiant" id="identifiant" placeholder="identifiant"/>
                   <input type="motdepasse" name="motdepasse" id="motdepasse" placeholder="motdepasse">
                   <input type="submit" name="submit">
               </form>'''
    identifiant = request.form['identifiant']
    if identifiant in utilisateurs:
        if request.form['motdepasse'] == utilisateurs[identifiant]['motdepasse']:
            utilisateur = User()
            utilisateur.id = identifiant
            login_user((utilisateur))
            return redirect(url_for('racine'))
        else:
            return 'Mauvais mot de passe'
    else:
        return 'Mauvais identifiant'

@app.route('/deconnexion')
def deconnexion():
    logout_user()
    return redirect(url_for('racine'))

@app.route('/utilisateur/')
@app.route('/utilisateur/<nom>')
def utilisateur(nom=''):
    if nom != '':
        return render_template('utilisateur.html', name=nom)
    else:
        return 'vous n\'avez pas saisi votre nom!'

@login_required
@app.route('/donnees', methods=['GET'])
def recherche_donnee():
    print('recherche_donnee()')
    nom = request.args.get('nom')
    print(nom)
    with open('app/donnees.txt', 'r') as fichier:
        donnees = fichier.read()
        if donnees:
            enregistrements = json.loads(donnees)
            for enregistrement in enregistrements:
                if enregistrement['nom'] == nom:
                    return jsonify(enregistrement)
        return jsonify({'erreur': 'donnée non trouvée'})
    return nom

@login_required
@app.route('/donnees', methods=['PUT'])
def cree_donnee():
    print('cree_donnee()')
    nouveau = json.loads(request.data)
    print(nouveau)
    with open('app/donnees.txt', 'r') as fichier:
        donnees = fichier.read()
        if not donnees:
            enregistrements = [nouveau]
        else:
            enregistrements = json.loads(donnees)
            enregistrements.append(nouveau)
    with open('app/donnees.txt', 'w') as fichier:
        fichier.write(json.dumps(enregistrements, indent=2))
    return jsonify(nouveau)

@login_required
@app.route('/donnees', methods=['POST'])
def maj_donnee():
    print('maj_donnee()')
    enregistrement = json.loads(request.data)
    misajours = []
    with open('app/donnees.txt', 'r') as fichier:
        donnees = fichier.read()
        enregistrements = json.loads(donnees)
    for element in enregistrements:
        if element['nom']  == enregistrement['nom']:
            element['courriel'] = enregistrement['courriel']
        misajours.append(element)
    with open('app/donnees.txt', 'w') as fichier:
        fichier.write(json.dumps(misajours, indent=2))
    return jsonify(enregistrement)

@login_required
@app.route('/donnees', methods=['DELETE'])
def supprime_donnee():
    print('supprime_donnee')
    enregistrement = json.loads(request.data)
    misajours = []
    with open('app/donnees.txt', 'r') as fichier:
        donnees = fichier.read()
        enregistrements = json.loads(donnees)
        for element in enregistrements:
            if element['nom']  == enregistrement['nom']:
                continue
            misajours.append(element)
    with open('app/donnees.txt', 'w') as fichier:
        fichier.write(json.dumps(misajours, indent=2))
    return jsonify(enregistrement)
