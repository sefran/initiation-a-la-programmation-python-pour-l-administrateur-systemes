from flask import Flask
from config import Config
from flask_assets import Environment, Bundle
from flask_login import LoginManager
from app.auth import User, utilisateurs


app = Flask(__name__, instance_relative_config=False)
app.config.from_object(Config)

assets = Environment(app)
style_bundle = Bundle(
    'css/less/main.less',
    filters='less,cssmin',
    output='css/styles.min.css',
    extra={'rel': 'stylesheet/css'}
)
assets.register('main_styles', style_bundle)
style_bundle.build()

gestionnaire_de_connexion = LoginManager()
gestionnaire_de_connexion.init_app(app)

@gestionnaire_de_connexion.user_loader
def user_loader(identifiant):
    if identifiant not in utilisateurs:
        return
    utilisateur = User()
    utilisateur.id = identifiant
    return utilisateur

@gestionnaire_de_connexion.request_loader
def request_loader(requête):
    identifiant = requête.form.get('identifiant')
    if identifiant not in utilisateurs:
        return
    utilisateur = User()
    utilisateur.id = identifiant
    return utilisateur

@gestionnaire_de_connexion.unauthorized_handler
def unauthorized_handler():
    return 'Non autorisé'

from app import appli
