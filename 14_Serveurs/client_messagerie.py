#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import smtplib
from email.message import EmailMessage

monemail = EmailMessage()
monemail.set_content('Message du client Python')
monemail['Subject'] = 'Objet du message du client Python'
monemail['From'] = 'prenom.nom@domaine-perso.fr'
monemail['To'] = 'utilisateur@localhost'

serveursmtp = smtplib.SMTP('localhost')
serveursmtp.send_message(monemail)
serveursmtp.quit()
