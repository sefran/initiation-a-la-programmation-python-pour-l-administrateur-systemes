#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import ftplib

with ftplib.FTP('ftp.fr.debian.org') as serveurftp:
    try:
        serveurftp.login()

        # Envoie de la commande FTP PWD qui affiche le répertoire courant
        répertoire_courant = serveurftp.sendcmd('PWD')
        print(ftplib.parse257(répertoire_courant))

        # La même chose avec la propriété pwd()
        répertoire_courant = serveurftp.pwd()
        print(répertoire_courant)

        # Change de répertoire
        serveurftp.cwd('debian')
        répertoire_courant = serveurftp.pwd()
        print(répertoire_courant)

    except ftplib.all_errors as e:
        print('Erreur FTP :', e)
