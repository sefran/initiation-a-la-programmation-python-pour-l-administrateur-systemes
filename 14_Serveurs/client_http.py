#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import http.client

connexion = http.client.HTTPConnection('utilisateur.documentation.domaine-perso.fr')
connexion.request('GET', '/initiation_developpement_python_pour_administrateur/')
resultat = connexion.getresponse()

if resultat.status == 200: # Imprime les 8 premières ligne du retour HTML
    for ligne in range(8):
        print(resultat.readline())

connexion.close()
