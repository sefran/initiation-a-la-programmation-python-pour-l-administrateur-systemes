#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import http.server

PORT = 10000
ref_serveur = ('', PORT)

os.chdir(os.path.expanduser('./public_html'))

entree = http.server.CGIHTTPRequestHandler
entree.cgi_directories = ['/']
print("Serveur actif sur le port :", PORT)

httpd = http.server.HTTPServer(ref_serveur, entree)
httpd.serve_forever()
