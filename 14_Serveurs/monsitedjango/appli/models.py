from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

class Administratif(models.Model):
    utilisateur = models.OneToOneField(User, on_delete=models.PROTECT) # Lien avec le modèle User
    datenaissance = models.DateField()
    SEXES = (
        ('M', 'Masculin'),
        ('F', 'Féminin'),
        ('H', 'Hermaphrodite'),
        ('I', 'Itersexuation'),
        )
    sexe = models.CharField('Sexe', max_length=100, choices = SEXES)
    ville = models.CharField('Ville', max_length=180)
    message_codepostal = 'Le code postal doit-être de la forme 00000'
    codepostal_regex = RegexValidator(
            regex = r'^[0-9]{5}$',
            message = message_codepostal,
        )
    codepostal = models.CharField('Code postal', validators=[codepostal_regex], max_length=12)
    addresse = models.TextField(blank=True)
    message_téléphone = 'Le numéro de téléphone saisi doit être de la forme : 0000000000'
    téléphone_regex = RegexValidator(
            regex = r'^(0|\+33|0033)[1-9][0-9]{8}$',
            message = message_téléphone,
        )
    telephone = models.CharField(validators=[téléphone_regex], max_length=60, null=True, blank=True)

    def __str__(self):
        return "Administratif de {0}".format(self.utilisateur.username)
