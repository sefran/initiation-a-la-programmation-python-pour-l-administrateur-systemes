from django import forms

class FormulaireDeConnexion(forms.Form):
    utilisateur = forms.CharField(label='Utilisateur : ', max_length=30)
    motdepasse = forms.CharField(label='Mot de passe : ', widget=forms.PasswordInput)
