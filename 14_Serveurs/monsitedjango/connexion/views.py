from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from connexion.formulaire import FormulaireDeConnexion

def connexion(request):

    mauvaisutilisateur = False
    mauvaismotdepasse = False
    utilisateurexiste = False

    if request.method == 'POST':
        formulaire = FormulaireDeConnexion(request.POST)
        if formulaire.is_valid():
            nomutilisateur = formulaire.cleaned_data['utilisateur']
            motdepasseutilisateur = formulaire.cleaned_data['motdepasse']
            try: # Teste si l'utilisateur est dans la base de données du modèle
                User.objects.get(username=nomutilisateur)
                utilisateurexiste = True
            except User.DoesNotExist:
                mauvaisutilisateur = True
            if utilisateurexiste:
                utilisateur = authenticate(username=nomutilisateur, password=motdepasseutilisateur)  # Teste si le nom d'utilisateur et le mot de passe correspondent
                if utilisateur:
                    login(request, utilisateur)
                    return redirect('/admin/')
                else:
                    mauvaismotdepasse = True
    else:
        formulaire = FormulaireDeConnexion()

    return render(request, 'connexion.html', locals())


def deconnexion(request):
    logout(request)
    return redirect('/connexion')

