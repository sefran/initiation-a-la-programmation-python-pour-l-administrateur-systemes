class Pere():
    def __new__(cls, *args, **kwargs):
        newclass = super(Pere, cls).__new__(cls)
        if 'paramètrePère' in kwargs.keys():
            if kwargs['paramètrePère']:
                setattr(newclass, 'mapropriété_conditionnellepère', None)
                def ma_Methode_Conditionnelle_Père():
                    return newclass.mapropriété_conditionnellepère
                setattr(newclass, ma_Methode_Conditionnelle_Père.__name__, ma_Methode_Conditionnelle_Père)
            del kwargs['paramètrePère']
        if 'banieméthodes' in kwargs and 'ma_Methode_Père' in kwargs['banieméthodes']:
            setattr(newclass, 'mapropriété_père', 'mapropriété_père')
        else:
            setattr(newclass, 'mapropriété_père', 'mapropriété_père')
            def ma_Methode_Père(self):
                return newclass.mapropriété_père
            setattr(newclass, ma_Methode_Père.__name__, ma_Methode_Père)
        return newclass
    def __init__(self, *args, **kwargs):
        pass


class Enfant(Pere):
    def __new__(cls, *args, **kwargs):
        newclass = super(Enfant, cls).__new__(cls, *args, **kwargs)
        if 'paramètreEnfant' in kwargs.keys():
            if kwargs['paramètreEnfant']:
                setattr(newclass, 'mapropriété_conditionnelleenfant', None)
                def ma_Methode_Conditionnelle_Enfant():
                    return newclass.mapropriété_conditionnelleenfant
                setattr(newclass, ma_Methode_Conditionnelle_Enfant.__name__, ma_Methode_Conditionnelle_Enfant)
            del kwargs['paramètreEnfant']
        return newclass
    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.mapropriété_enfant = None
    def ma_Methode_Enfant(self):
        pass


class PetitEnfant(Enfant):
    def __new__(cls, *args, **kwargs):
        newclass = super(PetitEnfant, cls).__new__(cls, *args, **kwargs)
        if 'paramètrePetitEnfant' in kwargs.keys():
            if kwargs['paramètrePetitEnfant']:
                setattr(newclass, 'mapropriété_conditionnellepetitenfant', None)
                def ma_Methode_Conditionnelle_PetitEnfant():
                    return newclass.mapropriété_conditionnellepetitenfant
                setattr(newclass, ma_Methode_Conditionnelle_PetitEnfant.__name__, ma_Methode_Conditionnelle_PetitEnfant)
            del kwargs['paramètrePetitEnfant']
        return newclass
    def __init__(self, **kwargs):
        self.mapropriétépetitenfant = None
        super().__init__()
    def ma_Methode_PetitEnfant(self):
        pass

if __name__ == '__main__':
    class Vide():
        pass
    vide = Vide()
    vide_list = set(dir(vide))
    print('Pere()')
    a = Pere()
    a_list = set(dir(a))
    print(str(a_list - vide_list))
    print('Pere(paramètrePère=True)')
    a = Pere(paramètrePère=True)
    a_list = set(dir(a))
    print(str(a_list - vide_list))
    print('Enfant()')
    b = Enfant()
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(paramètrePère=True)')
    b = Enfant(paramètrePère=True)
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(banieméthodes=[\'ma_Methode_Père\'])')
    b = Enfant(banieméthodes=['ma_Methode_Père'])
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(paramètreEnfant=True)')
    b = Enfant(paramètreEnfant=True)
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(paramètreEnfant=True, paramètrePère=True)')
    b = Enfant(paramètreEnfant=True, paramètrePère=True)
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=[\'ma_Methode_Père\'])')
    b = Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=['ma_Methode_Père'])
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('PetitEnfant()')
    c = PetitEnfant()
    c_list = set(dir(c))
    print(str(c_list - vide_list))
    print('PetitEnfant(paramètrePère=True)')
    c = PetitEnfant(paramètrePère=True)
    c_list = set(dir(c))
    print(str(c_list - vide_list))
    print('PetitEnfant(banieméthodes=[\'ma_Methode_Père\']')
    c = PetitEnfant(banieméthodes=['ma_Methode_Père'])
    c_list = set(dir(c))
    print(str(c_list - vide_list))
    print('PetitEnfant(paramètreEnfant=True)')
    c = Enfant(paramètreEnfant=True)
    c_list = set(dir(c))
    print(str(c_list - vide_list))
    print('PetitEnfant(paramètrePetitEnfant=True)')
    c = PetitEnfant(paramètrePetitEnfant=True)
    c_list = set(dir(c))
    print(str(c_list - vide_list))
