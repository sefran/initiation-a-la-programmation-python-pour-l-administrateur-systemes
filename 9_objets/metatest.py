#! /usr/bin/env python3
# -*- coding: utf8 -*-

class MetaTest(type):
    """ Classe Meta de test """
    def __new__(cls, name, bases, namespace, **kwargs):
        """ Set class MetaTest """
        print('new cls: %s' % cls)
        print('new name: %s' % name)
        if bases:
            print('new bases: %s' % bases)
        print('new namespace: %s' % namespace)
        print('new kwargs: %s' % kwargs)

        setattribute = False
        setmethod = False
        if 'settest' in kwargs.keys():
            if kwargs['settest']:
                setattribute = True
                setmethod = True
            del kwargs['settest']
        if 'setattribute' in kwargs.keys():
            if kwargs['setattribute']:
                setattribute = True
            del kwargs['setattribute']
        if 'setmethod' in kwargs.keys():
            if kwargs['setmethod']:
                setmethod = True
            del kwargs['setmethod']

        newclass = super(MetaTest, cls).__new__(cls, name, bases, namespace, **kwargs)

        if setattribute:
            setattr(newclass, 'my_property', None)

        if setmethod:
            def info(self):
                return 'My method info'
            setattr(newclass, info.__name__, info)

        if setmethod and setattribute:
            setattr(newclass, 'my_method', None)
            def set_method(self, value):
                self.my_method = value
            def get_method(self):
                return self.my_method
            setattr(newclass, set_method.__name__, set_method)
            setattr(newclass, get_method.__name__, get_method)

        return newclass

    def __init__(cls, name, bases, namespace, **kwargs):
        """ Initialize metaclass MetaTest """
        super().__init__(name, bases, namespace)
        print('init cls: %s' % cls)
        print('init name: %s' % name)
        if bases:
            print('init bases: %s' % bases)
        print('init namespace: %s' % namespace)
        print('init kwargs: %s' % kwargs)


class A(metaclass=MetaTest):
    pass

class B(metaclass=MetaTest, settest=False):
    pass

class C(metaclass=MetaTest, settest=True):
    pass

class D(metaclass=MetaTest, setattribute=False):
    pass

class E(metaclass=MetaTest, setattribute=True):
    pass

class F(metaclass=MetaTest, setmethod=False):
    pass

class G(metaclass=MetaTest, setmethod=True):
    pass

print('A: %s' % dir(A))
print('B: %s' % dir(B))
print('C: %s' % dir(C))
print('D: %s' % dir(D))
print('E: %s' % dir(E))
print('F: %s' % dir(F))
print('G: %s' % dir(G))

a = A()
b = B()
c = C()
d = D()
e = E()
f = F()
g = G()

print('a: %s' % dir(a))
print('b: %s' % dir(b))
print('c: %s' % dir(c))
print('c.my_property: %s' % c.my_property)
c.my_property = 'Property value'
print('c.my_property: %s' % c.my_property)
print('c.get_method(): %s' % c.get_method())
c.set_method('Method value')
print('c.get_method(): %s' % c.get_method())
print('d: %s' % dir(d))
print('e: %s' % dir(e))
print('e.my_property: %s' % e.my_property)
e.my_property = 'Property value'
print('e.my_property: %s' % e.my_property)
print('f: %s' % dir(f))
print('g: %s' % dir(g))
print('g.info(): %s' % g.info())
