# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`terre` -- Module terrestre
################################

.. module:: mondes.terre
   :platform: Linux
   :synopsis: Ce module gère les classes de la terre.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
"""

__title__ = "Module des classes terrestres"
__author__ = "Formateur PYTHON"
__version__ = '1.0.0'
__release_life_cycle__ = 'rc'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'

class Nuages():
    """  """
    def __init__(self, nuages=['aucun'], *args, **kwargs):
        """ Initialisation de l'objet """
        self.__types_nuages = ['aucun', 'cirrus', 'homogenitus', 'cirrocumulus', 'cirrostratus', 'altocumulus', 'nimbostratus', 'altostratus', 'stratocumulus', 'stratus', 'cumulus', 'cumulonimbus', 'mammatus', 'pyrocumulonimbus']
        self.nuages = nuages

    def get_types_nuages(self):
        """ Retourne la liste des nuages """
        return self.__types_nuages

    def get_nuages(self):
        """ Retourne les nuages """
        return self.nuages

    def add_nuage(self, nuage='aucun'):
        """ ajoute un nuage à la liste """
        if nuage != 'aucun':
            if nuage in self.__types_nuages:
                self.nuages.append(nuage)
                if 'aucun' in self.nuages:
                    self.delNuage('aucun')
            else:
                print(f'{nuage} n\'est pas un nuage répertorié')

    def add_nuages(self, nuages=[]):
        """ ajoute une liste de nuages à la liste """
        for nuage in nuages:
            if nuage != 'aucun':
                if nuage in self.__types_nuages:
                    self.nuages.append(nuage)
                    if 'aucun' in self.nuages:
                        self.delNuage('aucun')
                else:
                    print(f'{nuage} n\'est pas un nuage répertorié')

    def del_nuage(self, nuage):
        """ supprime un type de nuage """
        if nuage in self.nuages:
            self.nuages.remove(nuage)
            if self.nuages == []:
                self.nuages.append('aucun')

    def erase_nuage(self, nuage):
        """ Initialise la liste des nuages """
        self.nuages = ['aucun']


class Atmosphere(Nuages):
    """ L'atmosphère """
    def __init__(self, atmosphere, *args, **kwargs):
        """ Initialisation de l'objet """
        super(Atmosphere, self).__init__(*args, **kwargs)
        self.__types_atmosphères = ['aucune', 'troposphère', 'stratosphère', 'mésosphère', 'thermosphère', 'exosphère']
        self.atmosphère = atmosphere
        parent = Nuages
        class nouveau_parent(parent.__base__):
            pass
        parent_liste = dir(parent)
        nouveau_parent_liste = dir(nouveau_parent)
        méthodes = set(parent_liste) - set(nouveau_parent_liste)
        if atmosphere in self.__types_atmosphères:
            if atmosphere == 'troposphère':
                for méthode in méthodes:
                    setattr(nouveau_parent, méthode, parent.__getattribute__(parent, méthode))
        else:
            self.atmosphère = 'aucune'
        Atmosphere.__bases__ = (nouveau_parent, )

    def get_types_atmospheres(self):
        """ Retourne la liste des atmosphères) """
        return self.__types_atmosphères


class Geomorphologie():
    """ Le paysage """
    def __init__(self, geomorphologie='urbaine', *args, **kwargs):
        """ Initialisation de l'objet """
        self.__types_geomorphologies = ['fosse', 'dorsale', 'haut-fond', 'sous-marin', 'eau', 'plage', 'plaine', 'plateau', 'vallée', 'colline', 'fjord', 'gorge', 'montagne', 'polaire', 'urbaine']
        if geomorphologie in self.__types_geomorphologies:
            self.geomorphologie = geomorphologie
        else:
            self.geomorphologie = self.getTypesGeomorphologies()[-1]
            print(f'La geomorphologie «{geomorphologie}» n\'est pas dans la liste')

    def get_types_geomorphologies(self):
        """ Retourne la liste des geomorphologies """
        return self.__types_geomorphologies

    def add_type_geomorphologie(self, geomorphologie):
        """ ajoute un geomorphologie à la liste """
        if geomorphologie not in self.__types_geomorphologies:
            self.__types_geomorphologies.append(geomorphologie)

    def del_type_geomorphologie(self, geomorphologie):
        """ supprime un geomorphologie """
        if geomorphologie in self.__types_geomorphologies:
            self.__types_geomorphologies.remove(geomorphologie)


class Hydrotopologie():
    """ L'hydro topoligie """
    def __init__(self, hydrotopologie=['aucune'], *args, **kwargs):
        """ Initialisation de l'objet """
        self.__types_hydro_topologies = ['aucune', 'océan', 'mer', 'bras de mer', 'estuaire', 'étang', 'lac', 'fleuve', 'rivière', 'torrent']
        self.hydro_topologie = hydrotopologie

    def get_types_hydrotopologies(self):
        """ Retourne la liste des hydro_topologies """
        return self.__types_hydro_topologies

    def add_type_hydrotopologie(self, hydro_topologie):
        """ ajoute une hydro_topologie à la liste """
        if hydro_topologie not in self.__types_hydro_topologies:
            self.__types_hydro_topologies.append(hydro_topologie)

    def del_type_hydrotopologie(self, hydro_topologie):
        """ supprime un hydro_topologie """
        if hydro_topologie in self.__types_hydro_topologies:
            if hydro_topologie != 'aucune':
                self.__types_hydro_topologies.remove(hydro_topologie)

    def add_hydrotopologie(self, hydro_topologie):
        """ ajoute une hydro_topologie à la liste """
        if hydro_topologie not in self.hydro_topologie:
            self.hydro_topologie.append(hydro_topologie)
            if 'aucune' in self.hydro_topologie:
                self.delHydroTopologie('aucune')

    def del_hydrotopologie(self, hydro_topologie):
        """ supprime un hydro_topologie """
        if hydro_topologie in self.hydro_topologie:
            self.hydro_topologie.remove(hydro_topologie)
            if self.hydro_topologie == []:
                self.hydro_topologie.append('aucune')

    def erase_hydrotopologie(self, hydro_topologie):
        """ Initialise l'hydro_topologie """
        self.hydro_topologie = ['aucune']


class Vegetation():
    """ La végétation """
    def __init__(self, vegetation=['aucune'], *args, **kwargs):
        """ Initialisation de l'objet """
        self.__types_vegetations = ['aucune', 'milieu marin', 'algues', 'marais salés', 'roselières', 'mangrove', 'végétation aquatique', 'pelouses sèches', 'mésophiles', 'prairies', 'landes sèches', 'landes humides', 'tourbières', 'fourés secs', 'fourés humides', 'haies', 'forêts sèches', 'forêts humides']
        self.végétation = vegetation

    def get_types_vegetations(self):
        """ Retourne la liste des vegetations """
        return self.__types_vegetations

    def add_type_vegetation(self, vegetation):
        """ ajoute une vegetation à la liste """
        if vegetation not in self.__types_vegetations:
            self.__types_vegetations.append(vegetation)

    def del_type_vegetation(self, vegetation):
        """ supprime un vegetation """
        if vegetation in self.__types_vegetations:
            if vegetation != 'aucune':
                self.__types_vegetations.remove(vegetation)

    def add_vegetation(self, vegetation):
        """ ajoute une vegetation à la liste """
        if vegetation not in self.végétation:
            self.végétation.append(vegetation)
            if 'aucune' in self.végétation:
                self.delVegetation('aucune')

    def del_vegetation(self, vegetation):
        """ supprime un vegetation """
        if vegetation in self.végétation:
            self.végétation.remove(vegetation)
            if self.végétation == []:
                self.végétation.append('aucune')

    def erase_vegetation(self):
        """ Initialise la vegetation """
        self.végétation = ['aucune']


class Urbanisation():
    """ Les habitations """
    def __init__(self, urbanisation=['aucune'], *args, **kwargs):
        """ Initialisation de l'objet """
        self.__type_urbanisation = ['aucune', 'habitation', 'bourgade', 'village', 'ville', 'mégapôle']
        self.urbanisation = urbanisation

    def get_types_urbanisations(self):
        """ Retourne la liste des types d'urbanisation """
        return self.__types_terrains

    def add_type_urbanisation(self, urbanisation):
        """ ajoute un urbanisation aux types"""
        if urbanisation not in self.__types_urbanisation:
            self.__types_urbanisation.append(urbanisation)

    def del_type_urbanisation(self, urbanisation):
        """ supprime un urbanisation aux types """
        if urbanisation in self.__types_urbanisation:
            if urbanisation != 'aucune':
                self.__types_urbanisation.remove(urbanisation)

    def add_urbanisation(self, urbanisation):
        """ ajoute une urbanisation """
        if urbanisation != 'aucune':
            self.urbanisation.append(urbanisation)
            if 'aucune' in self.urbanisation:
                self.urbanisation.remove('aucune')

    def del_urbanisation(self, urbanisation):
        """ supprime une urbanisation """
        if urbanisation in self.urbanisation:
            self.urbanisation.remove(urbanisation)
            if self.urbanisation == []:
                self.urbanisation.append('aucune')

    def erase_urbanisation(self):
        """ Initialise urbanisation """
        self.urbanisation = ['aucune']
