# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`batiments` -- Module des bâtiments
########################################

.. module:: construtions.batiments
   :platform: Linux
   :synopsis: Ce module gère les classes de la terre.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
"""

__title__ = "Module des classes batiments"
__author__ = "Formateur PYTHON"
__version__ = '1.0.0'
__release_life_cycle__ = 'rc'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'

class FabriqueBatiments():
    """ Construit des bâtiments """
    def __init__(self, batiments=['aucun'], *args, **kwargs):
        """ Initialisation de l'objet """
        self.__types_bâtiments = ['aucun', 'cabane', 'maison', 'ferme', 'entreprise', 'industrie', 'immeuble', 'building']
        self.bâtiments = batiments

    def get_types_de_batiments(self):
        """ Retourne la liste des types de bâtiments """
        return self.__types_bâtiments

    def get_batiments(self):
        """ Retourne la liste des bâtiments à construire """
        return self.bâtiments

    def construit_batiments(self, bâtiments=['aucun']):
        """ Construit des bâtiments """
        for bâtiment in bâtiments:
            if bâtiment in self.__types_bâtiments:
                if bâtiment != 'aucun':
                    self.bâtiments.append(bâtiment)
                else:
                    print("Pas de bâtiment demandé à construire")
            else:
                print(f'Bâtiment «{bâtiment}» non dans la liste des bâtiments')
        return self.bâtiments

    def add_batiment(self, bâtiment):
        """ Construit un bâtiment supplémentaire """
        if bâtiment in self.__types_bâtiments:
            if bâtiment != 'aucun':
                self.bâtiments.append(bâtiment)

    def del_batiment(self, bâtiment):
        """ Détruit un bâtiment de la liste des bâtiments contruits """
        if bâtiment in self.__types_bâtiments:
            if bâtiment != 'aucun':
                self.bâtiments.remove(bâtiment)


if __name__ == '__main__':
    contruit_bâtiment = FabriqueBatiments()
    print(f'Bâtiments contruit : {contruit_bâtiment.get_batiments()}')
    print(f'Liste des bâtiments constructibles : {contruit_bâtiment.get_types_de_batiments()}')
    contruit_bâtiment.construit_batiments([])
    print(f'Bâtiments contruit : {contruit_bâtiment.get_batiments()}')
    contruit_bâtiment.construit_batiments(['aucun'])
    print(f'Bâtiments contruit : {contruit_bâtiment.get_batiments()}')
    contruit_bâtiment.construit_batiments(['cabane', 'maison', 'ferme', 'entreprise', 'industrie', 'immeuble', 'bidon'])
    print(f'Bâtiments contruit : {contruit_bâtiment.get_batiments()}')
    contruit_bâtiment.add_batiment('building')
    print(f'Bâtiments contruit : {contruit_bâtiment.get_batiments()}')
    for bâtiment in ['aucun', 'bidon', 'entreprise', 'industrie', 'immeuble', 'building'] :
        contruit_bâtiment.del_batiment(bâtiment)
    print(f'Bâtiments contruit : {contruit_bâtiment.get_batiments()}')

