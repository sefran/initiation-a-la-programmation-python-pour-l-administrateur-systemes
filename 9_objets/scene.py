# -*- coding: utf-8 -*-

"""
.. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
:mod:`scene` -- Module scene de paysage
#######################################

.. module:: scene
   :platform: Linux
   :synopsis: Ce module gère les scènes de paysages.
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
"""

__title__ = "Module des classes scène de paysages"
__author__ = "Formateur PYTHON"
__version__ = '1.0.0'
__release_life_cycle__ = 'rc'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'

from mondes.terre import Atmosphere, Geomorphologie, Hydrotopologie, Vegetation, Urbanisation
from mondes.constructions.batiments import FabriqueBatiments

class Decor(Atmosphere, Geomorphologie, Hydrotopologie, Vegetation, Urbanisation):
    """ Le décor de la scène paysage """
    def __init__(self, *args, **kwargs):
        """ Initialisation de l'objet """
        super(Decor, self).__init__(*args, **kwargs)
        self.fabriqueBatiments = FabriqueBatiments(*args, **kwargs)

if __name__ == '__main__':
    paysage = Decor(atmosphere='troposphère', geomorphologie='plaine', hydrotopologie=['lac', 'rivière'], vegetation=['prairies', 'végétation aquatique'], urbanisation=['habitation', 'bourgade', 'village'], nuages=True)
    print(f'L\'objet «{paysage}» à ses propriétés et méthodes : {dir(paysage)}')
    paysage = Decor(atmosphere='aucune', geomorphologie='plaine', hydrotopologie=['lac', 'rivière'], vegetation=['prairies', 'végétation aquatique'], urbanisation=['habitation', 'bourgade', 'village'], nuages=False)
    print(f'L\'objet «{paysage}» à ses propriétés et méthodes : {dir(paysage)}')
