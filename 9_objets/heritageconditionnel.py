class Pere():
    def __init__(self, **kwargs):
        self.mapropriété_père = 'mapropriété_père'
        if 'paramètrePère' in kwargs and kwargs['paramètrePère']:
            self.mapropriété_conditionnellepère = 'mapropriété_conditionnellepère'
            def ma_Methode_Conditionnelle_Père():
                return self.mapropriété_conditionnellepère
            self.ma_Methode_Conditionnelle_Père = ma_Methode_Conditionnelle_Père
    def ma_Methode_Père(self):
        return self.mapropriété_père


class Enfant(Pere):
    def __init__(self, **kwargs):
        Pere.__init__(Pere, **kwargs)
        if 'paramètreEnfant' in kwargs and kwargs['paramètreEnfant']:
            self.mapropriété_conditionnelleenfant = 'mapropriété_conditionnelleenfant'
            def ma_Methode_Conditionnelle_Enfant():
                return self.mapropriété_conditionnelleenfant
            self.ma_Methode_Conditionnelle_Enfant = ma_Methode_Conditionnelle_Enfant
        class new_parent(Pere.__base__):
            pass
        parent_list = dir(Pere)
        new_parent_list = dir(new_parent)
        if 'paramètrePère' in kwargs and kwargs['paramètrePère']:
            methodes = set(parent_list) - set(new_parent_list)
        else:
            methodes = set(parent_list) - set(new_parent_list) - {'mapropriété_conditionnellepère', 'ma_Methode_Conditionnelle_Père'}
        if 'banieméthodes' in kwargs and kwargs['banieméthodes'] != []:
            for methode in methodes:
                if methode not in kwargs['banieméthodes']:
                    setattr(new_parent, methode, Pere.__getattribute__(Pere, methode))
        else:
            for methode in methodes:
                setattr(new_parent, methode, Pere.__getattribute__(Pere, methode))
        Enfant.__bases__ = (new_parent, )
        self.mapropriété_enfant = 'mapropriété_enfant'
    def ma_Methode_Enfant(self):
        return self.mapropriété_enfant


class PetitEnfant(Enfant):
    def __init__(self, paramètrePetitEnfant=False, paramètreEnfant=False, options=[], paramètrePère=False):
        super().__init__(paramètreEnfant=paramètreEnfant, banieméthodes=options, paramètrePère=paramètrePère)
        self.mapropriétépetitenfant = 'mapropriétépetitenfant'
        if paramètrePetitEnfant:
            self.mapropriété_conditionnellepetitenfant = 'mapropriété_conditionnellepetitenfant'
            def ma_Methode_Conditionnelle_PetitEnfant():
                return self.mapropriété_conditionnellepetitenfant
            self.ma_Methode_Conditionnelle_PetitEnfant = ma_Methode_Conditionnelle_PetitEnfant
    def ma_Methode_PetitEnfant(self):
        return self.mapropriétépetitenfant

if __name__ == '__main__':
    class Vide():
        pass
    vide = Vide()
    vide_list = set(dir(vide))
    print('Pere()')
    a = Pere()
    a_list = set(dir(a))
    print(str(a_list - vide_list))
    print('Pere(paramètrePère=True)')
    a = Pere(paramètrePère=True)
    a_list = set(dir(a))
    print(str(a_list - vide_list))
    print('Enfant()')
    b = Enfant()
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(paramètrePère=True)')
    b = Enfant(paramètrePère=True)
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(banieméthodes=[\'ma_Methode_Père\'])')
    b = Enfant(banieméthodes=['ma_Methode_Père'])
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(paramètreEnfant=True)')
    b = Enfant(paramètreEnfant=True)
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(paramètreEnfant=True, paramètrePère=True)')
    b = Enfant(paramètreEnfant=True, paramètrePère=True)
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=[\'ma_Methode_Père\'])')
    b = Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=['ma_Methode_Père'])
    b_list = set(dir(b))
    print(str(b_list - vide_list))
    print('PetitEnfant()')
    c = PetitEnfant()
    c_list = set(dir(c))
    print(str(c_list - vide_list))
    print('PetitEnfant(paramètrePère=True)')
    c = PetitEnfant(paramètrePère=True)
    c_list = set(dir(c))
    print(str(c_list - vide_list))
    print('PetitEnfant(options=[\'ma_Methode_Père\']')
    c = PetitEnfant(options=['ma_Methode_Père'])
    c_list = set(dir(c))
    print(str(c_list - vide_list))
    print('PetitEnfant(paramètreEnfant=True)')
    c = Enfant(paramètreEnfant=True)
    c_list = set(dir(c))
    print(str(c_list - vide_list))
    print('PetitEnfant(paramètrePetitEnfant=True)')
    c = PetitEnfant(paramètrePetitEnfant=True)
    c_list = set(dir(c))
    print(str(c_list - vide_list))
