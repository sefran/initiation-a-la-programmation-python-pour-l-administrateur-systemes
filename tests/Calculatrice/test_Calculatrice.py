# -*- coding: utf-8 -*-

import unittest
from Unittest.Calculatrice import Calculatrice

class Calculatricetest(unittest.TestCase):
    """ Tests de la classe Calculatrice  """
    def setUp(self):
        """ Traitements de début d'éxécution """
        print('\nClasse Calculatrice')
        self.objet = Calculatrice()

    def tearDown(self):
        """ Traitements de fin d'éxécution """
        self.objet.efface()
        print('\nFin du test')

    def test_simple_ajoute(self):
        """ Tests sur la somme """
        print('\nTest de la somme')
        self.objet.valeur1(2)
        self.objet.valeur2(3)
        self.assertAlmostEqual(self.objet.ajoute(), 5)

    def test_simple_divise(self):
        """ Tests sur la division """
        print('\nTest de la division')
        self.objet.valeur1(10)
        self.objet.valeur2(2)
        self.assertAlmostEqual(self.objet.divise(), 5)

if __name__ == '__main__':
    unittest.main()
