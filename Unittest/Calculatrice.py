class Calculatrice:
    """ Fait des opérations entre deux valeurs """
    def __init__(self):
        """Initialisation de la Classe"""
        self.efface()

    def valeur1(self, première_valeur):
        """ Affecte la première valeur """
        self.__a = première_valeur

    def valeur2(self, deuxième_valeur):
        """ Affecte la deuxième valeur """
        self.__b = deuxième_valeur

    def efface(self):
        """ Efface les valeurs """
        self.__a = None
        self.__b = None

    def ajoute(self):
        """ Ajoute les valeurs """
        return self.__a + self.__b

    def divise(self):
        """ Divise les valeurs """
        return self.__a / self.__b

if __name__ == '__main__':
    calcule = Calculatrice()
    calcule.valeur1(4)
    calcule.valeur2(2)
    print("Valeur des opérandes : 4 et 2")
    print("Addition : ", calcule.ajoute())
    print("Division : ", calcule.divise())
