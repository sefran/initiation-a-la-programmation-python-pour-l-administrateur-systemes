# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2021, Prénom NOM
# This file is distributed under the same license as the Documentation sur
# l'initiation à la programmation Python pour l'administrateur systèmes
# package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2021.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Documentation sur l'initiation à la programmation "
"Python pour l'administrateur systèmes 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-06 13:51+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.9.1\n"

#: ../../sources-documents/index.rst:9
msgid "Contenu"
msgstr ""

#: ../../sources-documents/cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst:4
msgid "Initiation à la programmation Python pour l'administrateur systèmes"
msgstr ""

#: ../../sources-documents/index.rst:16
msgid "Modules"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:1 of
msgid ""
"Cette classe docstring montre comment utiliser sphinx et la syntaxe rst. "
"La première ligne est une brève explication de la classe avec ses "
"paramètres et ce que renvoi l’objet. Cela doit être complété par une "
"description plus précise des méthodes et des attributs dans le code de la"
" classe dans le code . La seule méthode ici est :func:`mafonction`."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:8 of
msgid "**paramètres**, **types**, **retour** et **type de retours**::"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:17 of
msgid ""
"Documentez des sections **Exemple** en utilisant la syntaxe des doubles "
"points ``:`` ::"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:25 of
msgid "qui apparaît comme suit :"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple of
msgid "Exemple"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:29 of
msgid "suivi d'une ligne vierge !"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:31 of
msgid ""
"Des sections spéciales telles que **Voir aussi**, **Avertissements**, "
"**Notes** avec la syntaxe sphinx (*directives de paragraphe*)::"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:40 of
msgid "Il existe de nombreux autres champs Info mais ils peuvent être redondants:"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:42 of
msgid "param, parameter, arg, argument, key, keyword: Description d'un paramètre."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:44 of
msgid "type: Type de paramètre."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:45 of
msgid ""
"raises, raise, except, exception: Quand une exception spécifique est "
"levée."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:47 of
msgid "var, ivar, cvar: Description d'une variable."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:48 of
msgid "returns, return: Description de la valeur de retour."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:49 of
msgid "rtype: Type de retour."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:52 of
msgid ""
"Il existe de nombreuses autres directives telles que : versionadded, "
"versionchanged, rubric, centered, ...  Voir la documentation sphinx pour "
"plus de détails."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple:56 of
msgid "Voici ci-dessous les résultats pour :func:`mafonction` docstring."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:1 of
msgid "Retourne (arg1 / arg2) + arg3"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:3 of
msgid ""
"Ceci est une explication plus précise, qui peut inclure des mathématiques"
" avec la syntaxe latex :math:`\\alpha`. Ensuite, vous devez fournir une "
"sous-section facultative (juste pour être cohérent et avoir une "
"documentation uniforme. Rien ne vous empêche de changer l'ordre):"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:9 of
msgid "paramètres utilisés ``:param <nom>: <description>``"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:10 of
msgid "type des paramètres ``:type <nom>: <description>``"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:11 of
msgid "retours de la méthode ``:returns: <description>``"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:12 of
msgid "exemples (doctest)"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:13 of
msgid "utilisation de voir aussi ``.. seealso:: texte``"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:14 of
msgid "utilisation des notes ``.. note:: texte``"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:15 of
msgid "utilisation des alertes ``.. warning:: texte``"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:16 of
msgid "liste des restes à faire ``.. todo:: text``"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:21 of
msgid "**Avantages**:"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:19 of
msgid "Utilise les balises sphinx."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:20 of
msgid "Belle sortie HTML avec les directives Seealso, Note, Warning."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:25 of
msgid "**Désavantages**:"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:24 of
msgid ""
"En regardant simplement la docstring, les sections de paramètres, de "
"types et de retours n'apparaissent pas bien dans le code."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction of
msgid "Paramètres"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:27
#: 8_Documentation.mon_module.ClasseExemple.mafonction:28
#: 8_Documentation.mon_module.ClasseExemple.mafonction:29 of
msgid "la première valeur"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction of
msgid "Renvoie"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:33 of
msgid "arg1/arg2 +arg3"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction of
msgid "Type renvoyé"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction of
msgid "Example"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:43 of
msgid "il peut être utile de souligner une caractéristique importante"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:44 of
msgid ":class:`AutreClasseExemple`"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:45 of
msgid "arg2 doit être différent de zéro."
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:46 of
msgid "À faire"
msgstr ""

#: 8_Documentation.mon_module.ClasseExemple.mafonction:46 of
msgid "vérifier que arg2 est non nul."
msgstr ""

#~ msgid "Contenu :"
#~ msgstr ""

#~ msgid "Index"
#~ msgstr ""

#~ msgid ":ref:`genindex`"
#~ msgstr ""

#~ msgid "Index des modules"
#~ msgstr ""

#~ msgid ":ref:`modindex`"
#~ msgstr ""

