.. |date| date::

:Date: |date|
:Revision: 1.0
:Author: Prénom NOM <prénom.nom@fai.fr>
:Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
:Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

.. toctree::
   :maxdepth: 2
   :caption: Contenu

.. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst


.. only:: html

  .. image:: ./badges/obsolescence.svg
     :alt: Obsolescence du code Python
     :align: left
     :width: 200px

  .. image:: ./badges/pylint.svg
     :alt: Cliquez pour voir le rapport
     :align: left
     :width: 200px
     :target: ./pylint/index.html

.. image:: classes/classes.png
   :alt: UML des classes de calculatrice.py
   :align: left
   :width: 200px

.. image:: classes/Calculatrice.png
   :alt: UML de Calculatrice
   :align: left
   :width: 200px


----

Modules
*******

.. automodule:: Unittest.Calculatrice
  :members: 
