#! /usr/bin/env python3
# -*- coding: utf8 -*-

import re

courriel = "nom.prenom@fai.service.fr"
expressionreg = r"^[a-z0-9._\-]+@[a-z0-9\._\-]+\.(fr|org|net|com)"

if re.match(expressionreg, courriel):
    print("Vrai")
    print(re.match(expressionreg, courriel))
else:
    print("Faux")
