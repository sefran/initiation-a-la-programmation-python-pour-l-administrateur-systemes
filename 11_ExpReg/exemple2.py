#! /usr/bin/env python3
# -*- coding: utf8 -*-

import re

courriel = "nom.prénom@fai.fr"
expressionreg = r"^[a-z0-9._-]+@"

if re.match(expressionreg, courriel):
    print("Vrai")
    print(re.match(expressionreg, courriel))
else:
    print("Faux")
