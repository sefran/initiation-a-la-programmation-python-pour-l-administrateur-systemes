#! /usr/bin/env python3
# -*- coding: utf8 -*-

import argparse, logging

# Récupère l'argument de la ligne de commande du paramètre log et le met dans la variable loglevel
params = argparse.ArgumentParser()
params.add_argument('--log')
args = params.parse_args()
loglevel = args.log

# Défini le niveau de journalisation
if loglevel:
    numeric_level = getattr(logging, loglevel.upper())
else:
    numeric_level = logging.DEBUG

# Teste si le paramètre est valide
if not isinstance(numeric_level, int):
    raise ValueError('Niveau de journalisation invalide : %s' % loglevel)

# Configure le niveau de journalisation et le fichier où journaliser
logging.basicConfig(filename='niveau.log', filemode='w', level=numeric_level)

# Messages de tests
logging.error('Message erreur')
logging.warning('Message alerte')
logging.info('Message information')
logging.debug('Message debug')
