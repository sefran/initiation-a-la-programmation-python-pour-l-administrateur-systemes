# Tester Docker

Après vous être reconnecter sous Ubuntu, vérifiez dans un terminal que docker fonctionne bien en exécutant la commande docker `docker run hello-world` ci-dessous.

```console
utilisateur@MachineUbuntu:~$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete
Digest: sha256:ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
  1. The Docker client contacted the Docker daemon.
  2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
  3. The Docker daemon created a new container from that image which runs the executable that produces the output you are currently reading.
  4. The Docker daemon streamed that output to the Docker client, which sent it to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
  $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID: https://hub.docker.com/

For more examples and ideas, visit: https://docs.docker.com/get-started/

utilisateur@MachineUbuntu:~$ docker ps -a
CONTAINER ID   IMAGE         COMMAND    CREATED          STATUS                      PORTS   NAMES
dcd0d025b44b   hello-world   "/hello"   19 seconds ago   Exited (0) 16 seconds ago           elegant_torvalds
```

Nous sommes maintenant prêts à installer GitLab.
