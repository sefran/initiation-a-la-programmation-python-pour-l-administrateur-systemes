# Configurer et tester le Runner

Activation du runner dans docker

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
Unable to find image 'gitlab/gitlab-runner:latest' locally
latest: Pulling from gitlab/gitlab-runner
a31c7b29f4ad: Pull complete
d843a3e4344f: Pull complete
cf545e7bed9f: Pull complete
c863409f4294: Pull complete
ba06fc4b920b: Pull complete
Digest: sha256:79692bb4b239cb2c1a70d7726e633ec918a6af117b68da5eac55a00a85f38812
Status: Downloaded newer image for gitlab/gitlab-runner:latest

Runtime platform arch=amd64 os=linux pid=7 revision=8925d9a0 version=14.2.0
Running in system-mode.

Enter the Gitlab instance URL (for example, https://gitlab.com/):
```

Pour activer le runner :

![Projets GitLab](images/gitlab_22.png)![Détail du projet](images/gitlab_23.png)![Menu «Paramètres», sous menu «Intégration et livraison» du projet](images/gitlab_24.png)

Choisir l’option «**Exécuteurs**» et click sur le bouton «**Étendre**».

![Option de configuration des exécuteurs du projet](images/gitlab_25.png)

Aller dans «**Spécific runners**» dans l’option Exécuteurs.

![Section «Specific runner» de configuration des exécuteurs du projet](images/gitlab_26.png)

Informations pour déclarer le runner pour le projet.

![Section «Set up a specific runner manually»](images/gitlab_27.png)
```console
Enter the GitLab instance URL (for example, https://gitlab.com/): http://gitlab.domaine-perso.fr/
Enter the registration token: 9FfDsP_9Z2cXWi1Axwig
Enter a description for the runner: [75d626bde768]: Runner Developpement Python 3
Enter tags for the runner (comma-separated): runner
Registering runner... succeeded runner=Tzzfs5xc
Enter an executor: kubernetes, custom, docker-ssh, shell, docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox: docker
Enter the default Docker image (for example, ruby:2.6): python:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo chmod o+r /etc/gitlab-runner/config.toml
```

Changez dans «**/etc/gitlab-runner/config.toml**» :

```docker
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner Developpement Python 3"
  url = "http://gitlab.domaine-perso.fr/"
  token = "9FfDsP_9Z2cXWi1Axwig"
  executor = "docker"
  pull_policy = "if-not-present"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

Vous pouvez démarrer le Runner

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run -d --restart always --name gitlab-runner -v /etc/gitlab-runner:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
c9f30b11275ac803ebb17209441c7e0b6351c60d9f0ddadc17c8b0a7ae9cbb96
```

Autorisez le registre pour la machine ubuntu

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/docker/certs.d/MachineUbuntu\:5000/ca.crt /usr/local/share/ca-certificates/MachineUbuntu.crt
utilisateur@MachineUbuntu:~/**\ **repertoire_de_developpement**\ $ sudo update-ca-certificates
```

Si tout se passe bien vous obtenez le message :

```console
Updatting certificates in /etc/ssl/certs...
1 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
```

Dans «**Specific runners**»  de l’option «**Exécuteurs**» du sous menu «**Intégration et livraison**» du menu «**Paramètres**» du projet apparaît le runner en exécution

![Runner d'exécution du projet dans la section «Available specific runner» de l'option Exécuteurs d'Intégration et livraison du menu paramètre du projet](images/gitlab_28.png)

Mettre en pause le runner avec le bouton «**Pause**».

Cliquez sur l’icone ![L'icône éditer du runner du projet](images/gitlab_29.png) pour éditer les options du runner, et sélectionnez «**Indique si l’exécuteur peut choisir des tâches sans étiquettes (tags)**» :

![Fenêtre de configuration du runner d'un projet](images/gitlab_30.png)

Modifier aussi le temps «**Durée maximale d’exécution de la tâche**» avec «**30m**»

Relancer l’exécution du runner pour valider les modifications.

![Éxécution du runner](images/gitlab_31.png)

## Tester le fonctionnement du runner

Éditer le fichier «**.gitlab-ci.yml**» dans **repertoire_de_developpement**.

```yaml
travail-de-construction:
  stage: build
  script:
    - echo "Bonjour, $GITLAB_USER_LOGIN !"

travail-de-tests-1:
  stage: test
  script:
    - echo "Ce travail teste quelque chose"

travail-de-tests-2:
  stage: test
  script:
    - echo "Ce travail teste quelque chose, mais prend plus de temps que travail-de-test-1."
    - echo "Une fois les commandes echo terminées, il exécute la commande de veille pendant 20 secondes"
    - echo "qui simule un test qui dure 20 secondes de plus que travail-de-test-1."
    - sleep 20

deploiement-production:
  stage: deploy
  script:
    - echo "Ce travail déploie quelque chose de la branche $CI_COMMIT_BRANCH."
```

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Test du runner dans Gitlab"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```

![Runner en cours d'exécution dans la fenêtre de détails du projet](images/gitlab_32.png)

On peut voir l’activité en cours du runner avec l’icône : ![Icône d'activité du runner](images/gitlab_33.png)

Dans le sous menu «**Pipelines**» du menu «**Intégration et livraison**» du projet on peut voir les taches d’exécution du runner :

![Sous menu Pipeline avec des taches en cours d'exécutions](images/gitlab_34.png)

On voit ici la tache «**Travail-de-construction**» en cours dans la phase de «**Build**» de l’exécuteur.

![Tâche «Build» du runner en cours](images/gitlab_35.png)

Si on clique sur cette icône on voit les opérations en cours de la tache :

![icône de progression](images/gitlab_36.png)

Une fois la tache réussi, l’exécuteur passe dans la phase d’exécution des tests.

![Exécution des tests dans le Pipeline](images/gitlab_37.png)

On peut voir le résultat en cliquant sur les icônes des taches de tests.

![Résultat travail-de-test-1](images/gitlab_38.png)![Résultat travail-de-test-2](images/gitlab_39.png)

Puis après l’exécuteur passe dans la phase «**Deploy**».

![Pipelines exécutions OK](images/gitlab_40.png)![Résultat de la tache déploiement-production](images/gitlab_41.png)

Test du déploiement docker :

```yaml
default:
  image: python:latest
```

Pour plus d’informations sur Gitlab et son utilisation [https://github.com/SocialGouv/tutoriel-gitlab](https://github.com/SocialGouv/tutoriel-gitlab), [https://makina-corpus.com/blog/metier/2019/gitlab-astuces-projets](https://makina-corpus.com/blog/metier/2019/gitlab-astuces-projets).
