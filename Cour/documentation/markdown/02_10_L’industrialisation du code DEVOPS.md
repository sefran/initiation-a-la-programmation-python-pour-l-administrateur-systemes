# L’industrialisation du code DEVOPS

- Gitlabs
- Github/Azure
- BitbucketInstaller l’infrastructure DEV/OPS

Nous allons installer GitLab avec Docker. De plus, nous utiliserons Ubuntu 21.04 comme système d’exploitation principal.

Prérequis:

- Serveur Ubuntu 21.04
- Min 4 Go de RAM
- Privilèges root

Qu’allons nous faire?

1. Configurer le DNS local
2. Installer Docker
3. Tester Docker
4. Installer Gitlab
5. Configurer et tester Gitlab
6. Autorisations pour Docker et le Runner
7. Configurer et tester le Runner
8. Tester les Pages de Gitlab

## Configurer le DNS local

Vous avez besoin d’un nom de domaine avec un enregistrement A valide pointant vers votre serveur GitLab.

### Installer une interface réseau virtuelle

Éditer «**/etc/systemd/network/10-virtualeth0.netdev**»

```basic
[NetDev]
Name = virtualeth0
Kind = dummy
```

Éditer «**/etc/systemd/network/10-virtualeth0.network**»

```basic
[Match]
Name = virtualeth0

[Network]
Address = 10.10.10.1/24
Address = fd00::/8
```

```console
utilisateur@MachineUbuntu:~$ sudo systemctl start systemd-networkd
utilisateur@MachineUbuntu:~$ sudo systemctl enable systemd-networkd
utilisateur@MachineUbuntu:~$ ip a
…
3: virtualeth0: <BROADCAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
  link/ether 9a:3c:56:42:f5:c9 brd ff:ff:ff:ff:ff:ff
  inet 10.10.10.1/24 brd 10.10.10.255 scope global virtualeth0
    valid_lft forever preferred_lft forever
  inet6 fd00::/8 scope global
    valid_lft forever preferred_lft forever
  inet6 fe80::983c:56ff:fe42:f5c9/64 scope link
    valid_lft forever preferred_lft forever
…
```

### Configuration du client dhcp adaptée au DNS local

Pour pouvoir ajouter le serveur DNS local à «**/etc/resolv.conf**» il faut renseigner l’option «**prepend**» qui permet l’ajout du serveur DNS local **en début de** la liste des **serveurs DNS** fournit automatiquement par DHCP.

Éditer «**/etc/dhcp/dhclient.conf**»

```properties
prepend domaine-perso.fr 10.10.10.1 fd00::
```

Vérifier les DNS présents :

```console
utilisateur@MachineUbuntu:~$ nmcli dev show | grep DNS
IP4.DNS[1]: yyy.yyy.yyy.yyy
IP4.DNS[1]: yyy.yyy.yyy.yyy
IP6.DNS[1]: yyyy:yyyy:yyyy::yyyy
IP6.DNS[2]: yyyy:yyyy:yyyy::yyyy
IP6.DNS[3]: yyyy:yyyy:yyyy::yyyy
utilisateur@MachineUbuntu:~$ resolvectl dns
Global:
Link 2 (enp0sxx):
Link 3 (wlx803xxxxx): yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 4 (wlo1): yyy.yyy.yyy.yyy
Link 6 (virtualeth0):
```

### Définir le domaine local de la machine Ubuntu

```console
utilisateur@MachineUbuntu:~$ sudo hostnamectl set-hostname MachineUbuntu.domaine-perso.fr --static
utilisateur@MachineUbuntu:~$ hostname -d
domaine-perso.fr
```

### Installer les applications de base

```console
utilisateur@MachineUbuntu:~$ sudo apt install bind9 bind9utils bind9-dnsutils bind9-doc bind9-host net-tools
utilisateur@MachineUbuntu:~$ sudo systemctl status named
utilisateur@MachineUbuntu:~$ sudo systemctl enable named
```

### Configuration du DNS local

Éditer «**/etc/bind/named.conf.options**»

```c
options {
  directory "/var/cache/bind";

  // Pour des raisons de sécurité.
  // Cache la version du serveur DNS pour les clients.
  version "Pas pour les crackers";

  listen-on { 127.0.0.1; 10.10.10.1; };
  listen-on-v6 { ::1; fd00::; };

  allow-query { 127.0.0.1; 10.10.10.1; ::1; fd00::; };

  // Optionnel - Comportement par défaut de BIND en récursions.
  recursion yes;

  // Récursions autorisées seulement pour les interfaces clients
  allow-recursion { 127.0.0.1; 10.10.10.0/24; ::1; fd00::/8; };

  dnssec-validation auto;

  // Activer la journalisation des requêtes DNS
  querylog yes;

};
```

Vérifier la validité de la configuration, et redémarrer le serveur DNS si la configuration est OK.

```console
utilisateur@MachineUbuntu:~$ sudo named-checkconf
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
```

Ajout du server DNS local à la liste des serveurs DNS de systemd-resolved.

Éditer «**/etc/systemd/resolved.conf**»

```properties
DNS=10.10.10.1 fd00::
```

```console
utilisateur@MachineUbuntu:~$ sudo systemctl restart systemd-resolved
utilisateur@MachineUbuntu:~$ nmcli general reload
```

### Tests du serveur DNS

#### Vérifications du serveur

```console
utilisateur@MachineUbuntu:~$ sudo rndc status
version: BIND 9.16.8-Ubuntu (Stable Release) <id:539f9f0> (Pas pour les crackers)
running on MachineUbuntu.domaine-perso.fr: Linux x86_64 5.11.0-31-generic #33-Ubuntu SMP Wed Aug 11 13:19:04 UTC 2021
boot time: Thu, 26 Aug 2021 06:13:19 GMT
last configured: Thu, 26 Aug 2021 06:13:19 GMT
configuration file: /etc/bind/named.conf
CPUs found: 4
worker threads: 4
UDP listeners per interface: 4
number of zones: 102 (97 automatic)
debug level: 0
xfers running: 0
xfers deferred: 0
soa queries in progress: 0
query logging is ON
recursive clients: 0/900/1000
tcp clients: 0/150
TCP high-water: 0
server is up and running
```

Vérifier le fonctionnement de bind sur le port 53

```console
utilisateur@MachineUbuntu:~$ sudo lsof -i:53
COMMAND PID USER FD TYPE DEVICE SIZE/OFF NODE NAME
named 5624 bind 37u IPv4 54315 0t0 UDP localhost:domain
named 5624 bind 38u IPv4 54316 0t0 UDP localhost:domain
named 5624 bind 39u IPv4 54317 0t0 UDP localhost:domain
named 5624 bind 40u IPv4 54318 0t0 UDP localhost:domain
named 5624 bind 42u IPv4 51987 0t0 TCP localhost:domain (LISTEN)
named 5624 bind 43u IPv4 54319 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 44u IPv4 54320 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 45u IPv4 54321 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 46u IPv4 54322 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 47u IPv4 51988 0t0 TCP MachineUbuntu.domaine-perso.fr:domain (LISTEN)
named 5624 bind 48u IPv6 54323 0t0 UDP ip6-localhost:domain
named 5624 bind 49u IPv6 54324 0t0 UDP ip6-localhost:domain
named 5624 bind 50u IPv6 54325 0t0 UDP ip6-localhost:domain
named 5624 bind 51u IPv6 54326 0t0 UDP ip6-localhost:domain
named 5624 bind 52u IPv6 51989 0t0 TCP ip6-localhost:domain (LISTEN)
named 5624 bind 53u IPv6 54327 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 54u IPv6 54328 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 55u IPv6 54329 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 56u IPv6 54330 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 58u IPv6 54331 0t0 TCP MachineUbuntu.domaine-perso.fr:domain (LISTEN)
systemd-r 5799 systemd-resolve 12u IPv4 52844 0t0 UDP localhost:domain
systemd-r 5799 systemd-resolve 13u IPv4 52845 0t0 TCP localhost:domain (LISTEN)
```

Vérifier l’écoute réseau sur le port 53

```console
utilisateur@MachineUbuntu:~$ sudo netstat -alnp | grep -i :53
tcp  0 0 127.0.0.53:53 0.0.0.0:* LISTEN 5799/systemd-resol
tcp  0 0 127.0.0.1:53  0.0.0.0:* LISTEN 5624/named
tcp  0 0 10.10.10.1:53 0.0.0.0:* LISTEN 5624/named
tcp6 0 0 fd00:::53     :::*      LISTEN 5624/named
tcp6 0 0 ::1:53        :::*      LISTEN 5624/named
udp  0 0 127.0.0.53:53 0.0.0.0:*        5799/systemd-resol
udp  0 0 127.0.0.1:53  0.0.0.0:*        5624/named
udp  0 0 10.10.10.1:53 0.0.0.0:*        5624/named
udp  0 0 0.0.0.0:5353  0.0.0.0:*        771/avahi-daemon: r
udp6 0 0 fd00:::53     :::*             5624/named
udp6 0 0 ::1:53        :::*             5624/named
udp6 0 0 :::5353       :::*             771/avahi-daemon: r
```

Vérifier que le système Ubuntu écoute le serveur DNS

```console
utilisateur@MachineUbuntu:~$ resolvectl dns
Global: 10.10.10.1 fd00::
Link 2 (enp0sxx): yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 3 (virtualeth0):
Link 4 (wlx803xxxxx): yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 5 (wlox): yyy.yyy.yyy.yyy
utilisateur@MachineUbuntu:~$ dig MachineUbuntu +noall +answer
MachineUbuntu.                  0 IN A 127.0.1.1
utilisateur@MachineUbuntu:~$ dig MachineUbuntu.domaine-perso.fr +noall +answer
MachineUbuntu.domaine-perso.fr. 0 IN A 10.10.10.1
MachineUbuntu.domaine-perso.fr. 0 IN A aaa.aaa.aaa.aaa
MachineUbuntu.domaine-perso.fr. 0 IN A bbb.bbb.bbb.bbb
…
utilisateur@MachineUbuntu:~$ dig bidon +noall +answer
utilisateur@MachineUbuntu:~$ dig bidon.domaine-perso.fr +noall +answer
```

Si UFW est activé, ouvrir le port DNS sur UFW.

```console
utilisateur@MachineUbuntu:~$ sudo ufw allow from 192.168.0.0/16 to any port 53 proto udp
```

Éditer «**/etc/bind/named.conf.local**» pour définir **la zone DNS**

```c
zone "domaine-perso.fr" {
  type master;
  file "/etc/bind/db.domaine-perso.fr";
};
zone "10.10.10.in-addr.arpa" {
  type master;
  file "/etc/bind/db.10.10.10";
};
zone "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa." {
  type master;
  file "/etc/bind/db.fd00";
};
```

```console
utilisateur@MachineUbuntu:~$ sudo named-checkconf
```

Éditer «**/etc/bind/db.domaine-perso.fr**» pour définir **les alias DNS**

```text
$TTL 15m
@             IN SOA     @ root (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

              IN NS      @
              IN A       10.10.10.10
              IN AAAA    fd00::a
              IN MX      2 courriel
; domaine vers adresse IP
gitlab        IN A       10.10.10.1
gitlab        IN AAAA    fd00::
courriel      IN A       10.10.10.2
courriel      IN AAAA    fd00::2
documentation IN A       10.10.10.3
documentation IN AAAA    fd00::3
*             IN A       10.10.10.10
*             IN AAAA    fd00::a
```

Éditer «**/etc/bind/db.10.10.10**» pour définir **les alias inverse DNS**

```text
$TTL 15m
@             IN SOA     gitlab.domaine-perso.fr. root.domaine-perso.fr. (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

               IN NS     gitlab.domaine-perso.fr.

; IP vers nom de domaine DNS
1             IN PTR     gitlab.domaine-perso.fr.
2             IN PTR     courriel.domaine-perso.fr.
3             IN PTR     documentation.domaine-perso.fr.
10            IN PTR     domaine-perso.fr.
```

Éditer «**/etc/bind/db.fd00**» pour définir **les alias inverse DNS**

```text
$TTL 15m
@             IN SOA     gitlab.domaine-perso.fr. root.domaine-perso.fr. (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

               IN NS     gitlab.domaine-perso.fr.

; IPv6 vers nom de domaine DNS
0             IN PTR     gitlab.domaine-perso.fr.
2             IN PTR     courriel.domaine-perso.fr.
3             IN PTR     documentation.domaine-perso.fr.
a             IN PTR     domaine-perso.fr.
```

```console
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
```

#### Vérifier la résolution DNS

```console
utilisateur@MachineUbuntu:~$ dig ANY domaine-perso.fr +noall +answer
domaine-perso.fr. 6444 IN SOA domaine-perso.fr. root.domaine-perso.fr. 2021082512 3600 900 604800 3600
domaine-perso.fr. 6444 IN NS domaine-perso.fr.
domaine-perso.fr. 6444 IN A 10.10.10.10
domaine-perso.fr. 6444 IN AAAA fd00::a
domaine-perso.fr. 6444 IN MX 2 courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig ANY gitlab.domaine-perso.fr +noall +answer
gitlab.domaine-perso.fr. 6444 IN A 10.10.10.1
gitlab.domaine-perso.fr. 6444 IN AAAA fd00::
utilisateur@MachineUbuntu:~$ dig ANY courriel.domaine-perso.fr +noall +answer
courriel.domaine-perso.fr. 6444 IN A 10.10.10.2
courriel.domaine-perso.fr. 6444 IN AAAA fd00::2
utilisateur@MachineUbuntu:~$ dig ANY documentation.domaine-perso.fr +noall +answer
documentation.domaine-perso.fr. 6444 IN A 10.10.10.3
documentation.domaine-perso.fr. 6444 IN AAAA fd00::3
utilisateur@MachineUbuntu:~$ dig ANY bidon.domaine-perso.fr +noall +answer
bidon.domaine-perso.fr. 6444 IN A 10.10.10.10
bidon.domaine-perso.fr. 6444 IN AAAA fd00::a
```

#### Vérifier la résolution externe

```console
utilisateur@MachineUbuntu:~$ dig google.com +noall +answer
google.com. 16 IN A 216.58.223.110
google.com. 32 IN AAAA 2a00:…::200e
…
```

#### Vérifier la résolution inverse

Vous pouvez utiliser la commande `host` ou `dig -x`

```console
utilisateur@MachineUbuntu:~$ host 10.10.10.1
1.10.10.10.in-addr-arpa domain name pointer gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.1 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00:: +noall +answer
a.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.2 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::2 +noall +answer
2.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.3 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR documentation.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::3 +noall +answer
3.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR documentation.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.10 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::a +noall +answer
a.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR domaine-perso.fr.
```

### Paramétrer définitivement votre DNS pour gitlab

Éditer «**/etc/bind/db.domaine-perso.fr**» pour définir **les alias DNS** définitifs

```text
…
               IN NS     @
               IN A      10.10.10.1
               IN AAAA   fd00::
               IN MX     1 courriel
; domaine vers adresse IP
gitlab         IN A      10.10.10.1
gitlab         IN AAAA   fd00::
courriel       IN A      10.10.10.1
courriel       IN AAAA   fd00::1
*              IN A      10.10.10.1
*              IN AAAA   fd00::
```

Éditer «**/etc/bind/db.10.10.10**» pour définir **les alias inverse DNS**

```text
$TTL 15m
@              IN SOA    gitlab.domaine-perso.fr. root.domaine-perso.fr. (
           2021082512    ; n° série
                   1h    ; intervalle de rafraîchissement esclave
                  15m    ; intervalle de réessaie pour l’esclave
                   1w    ; temps d’expiration de la copie esclave
                   1h )  ; temps de cache NXDOMAIN

                IN NS    gitlab.domaine-perso.fr.

; IP vers nom de domaine DNS
1               IN PTR   gitlab.domaine-perso.fr.
1               IN PTR   courriel.domaine-perso.fr.
1               IN PTR   domaine-perso.fr.
```

Éditer «**/etc/bind/db.fd00**» pour définir **les alias inverse DNS**

```text
$TTL 15m
@              IN SOA    gitlab.domaine-perso.fr. root.domaine-perso.fr. (
           2021082512    ; n° série
                   1h    ; intervalle de rafraîchissement esclave
                  15m    ; intervalle de réessaie pour l’esclave
                   1w    ; temps d’expiration de la copie esclave
                   1h )  ; temps de cache NXDOMAIN

                IN NS   gitlab.domaine-perso.fr.

; IPv6 vers nom de domaine DNS
0               IN PTR   gitlab.domaine-perso.fr.
0               IN PTR   domaine-perso.fr.
1               IN PTR   courriel.domaine-perso.fr.
```

```console
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.1 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR gitlab.domaine-perso.fr.
1.10.10.10.in-addr.arpa. 900 IN PTR domaine-perso.fr.
1.10.10.10.in-addr.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00:: +noall +answer
0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR gitlab.domaine-perso.fr.
0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::1 +noall +answer
1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR courriel.domaine-perso.fr.
```

## Installer Docker

Installer les applications de base :

```console
utilisateur@MachineUbuntu:~$ sudo apt install docker.io curl openssh-server ca-certificates postfix mailutils
```

![Fenêtre d'information Postfix à valider](images/postfix_1.png)![Type de serveur de messagerie Local](images/postfix_2.png)![Nom de courrier courriel.domaine-perso.fr](images/postfix_3.png)

Autorisez le compte utilisateur à utiliser docker :

```console
utilisateur@MachineUbuntu:~$ sudo usermod -aG docker $USER
```

Démarrez le service docker et ajoutez-le au démarrage du système :

```console
utilisateur@MachineUbuntu:~$ sudo systemctl start docker
```

Vérifiez le bon fonctionnement du service docker à l’aide de la commande `systemctl` ci-dessous.

```console
utilisateur@MachineUbuntu:~$ systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2020-10-09 11:07:10 CEST; 47s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 6241 (dockerd)
      Tasks: 12
     Memory: 38.6M
     CGroup: /system.slice/docker.service
             └─6241 /usr/bin/dockerd -H fd://
--containerd=/run/containerd/containerd.sock
q
```

Activez le service au démarrage.

```console
utilisateur@MachineUbuntu:~$ sudo systemctl enable docker
utilisateur@MachineUbuntu:~$ ip a
…
4: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
  link/ether 02:42:a3:0c:9c:fb brd ff:ff:ff:ff:ff:ff
  inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
    valid_lft forever preferred_lft forever
  inet6 fa80::42:a3ff:fe0c:9cfb/64 scope link
    valid_lft forever preferred_lft forever
…
```

Éditer «**/etc/bind/named.conf.option**» pour ajouter l’interface de docker

```c
options {
    directory "/var/cache/bind";

    // Pour des raisons de sécurité.
    // Cache la version du serveur DNS pour les clients.
    version "Pas pour les crackers";*

    listen-on { 127.0.0.1; 10.10.10.1; 172.17.0.1; };
    listen-on-v6 { ::1; fd00::; fe80::42:a3ff:fe0c:9cfb; };

    // Optionnel - Comportement par défaut de BIND en récursions.
    recursion yes;

    allow-query { 127.0.0.1; 10.10.10.1; ::1; fd00::; 172.17.0.0/16; fe80::42:a3ff:fe0c:9cfb; };

    // Récursions autorisées seulement pour les interfaces clients
    allow-recursion { 127.0.0.1; 10.10.10.0/24; ::1; fd00::/8; 172.17.0.0/16; fe80::42:a3ff:fe0c:9cfb; };

    dnssec-validation auto;

    // Activer la journalisation des requêtes DNS
    querylog yes;
};
```

```console
utilisateur@MachineUbuntu:~$ sudo named-checkconf
```

Redémarrer votre Ubuntu pour valider les modifications

```console
utilisateur@MachineUbuntu:~$ reboot
```

## Tester Docker

Après vous être reconnecter sous Ubuntu, vérifiez dans un terminal que docker fonctionne bien en exécutant la commande docker `docker run hello-world` ci-dessous.

```console
utilisateur@MachineUbuntu:~$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete
Digest: sha256:ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
  1. The Docker client contacted the Docker daemon.
  2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
  3. The Docker daemon created a new container from that image which runs the executable that produces the output you are currently reading.
  4. The Docker daemon streamed that output to the Docker client, which sent it to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
  $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID: https://hub.docker.com/

For more examples and ideas, visit: https://docs.docker.com/get-started/

utilisateur@MachineUbuntu:~$ docker ps -a
CONTAINER ID   IMAGE         COMMAND    CREATED          STATUS                      PORTS   NAMES
dcd0d025b44b   hello-world   "/hello"   19 seconds ago   Exited (0) 16 seconds ago           elegant_torvalds
```

Nous sommes maintenant prêts à installer GitLab.

## Installer GitLab

GitLab est un gestionnaire de référentiels open source basé sur Rails (langage Rubis) développé par la société GitLab. Il s’agit d’un gestionnaire de révisions de code WEB basé sur git qui permet à votre équipe de collaborer sur le codage, le test et le déploiement d’applications. GitLab fournit plusieurs fonctionnalités, notamment les wikis, le suivi des problèmes, les révisions de code et les flux d’activité.

### Téléchargez le paquet d’installation GitLab pour Ubuntu et l’installer

Installation longue (prévoir une image VM ou USB ?)

[https://packages.gitlab.com/gitlab/gitlab-ce](https://packages.gitlab.com/gitlab/gitlab-ce) et choisissez la dernière version gitlab-ce pour ubuntu xenial

```console
utilisateur@MachineUbuntu:~/gitlab$ wget https://packages.gitlab.com/gitlab/gitlab-ce/packages/ubuntu/focal/gitlab-ce_14.1.3-ce.0_amd64.deb/download.deb
utilisateur@MachineUbuntu:~/gitlab$ sudo apt update ; sudo EXTERNAL_URL="http://gitlab.domaine-perso.fr" dpkg -i download.deb
```

### Paramétrer GitLab

```console
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-ctl show-config
utilisateur@MachineUbuntu:~/gitlab$ sudo chmod o+r /etc/gitlab/gitlab.rb
utilisateur@MachineUbuntu:~/gitlab$ sudo nano /etc/gitlab/gitlab.rb
```

```ruby
external_url "http://gitlab.domaine-perso.fr"
# Pour activer les fonctions artifacts (tester la qualité du code, déployer sur un serveur distant en SSH, etc.)
gitlab_rails['artifacts_enabled'] = true
# pour générer la doc et l’afficher avec Gitlab
pages_external_url "http://documentation.domaine-perso.fr"
```

```console
utilisateur@MachineUbuntu:~/gitlab$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/gitlab/trusted-certs/MachineUbuntu.key -out /etc/gitlab/trusted-certs/MachineUbuntu.crt
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-ctl reconfigure
```

## Configurer et tester GitLab

Saisissez dans un navigateur l’URL **gitlab.domaine-perso.fr**

![Initialisation du mot de passe de l'administrateur root de GitLab](images/gitlab_1.png)

Si vous n’avez pas la fenêtre d’initialisation du mot de passe :

```console
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-rake "gitlab:password:reset"
```

![Connection en root dans GitLab](images/gitlab_2.png)![Accueil projets GitLab](images/gitlab_3.png)![Préférences de GitLab aller vers «Localization»](images/gitlab_4.png)![Préférences language à «French» et First day of the week à «Monday»](images/gitlab_5.png)

Tapez la touche **F5** pour rafraîchir l’affichage de votre navigateur.

![Icône Paramètres de l'utilisateur](images/gitlab_6.png)![Profil de l'utilisateur](images/gitlab_7.png)![Compte de l'utilisateur](images/gitlab_8.png)![Icône  espace d'administration](images/gitlab_9.png)![Espace d'administration Paramètres et menu Général](images/gitlab_10.png)![Contrôle de visibilité d'accès](images/gitlab_11.png)

Intégrer le dépot git local dans Gitlab :

```console
utilisateur@MachineUbuntu:~/$ cd repertoire_de_developpement
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config credential.helper store
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git remote add origin http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push -u origin --all
Username for 'http://gitlab.domaine-perso.fr': utilisateur
Password for 'http://gitlab.domaine-perso.fr': motdepasse
Énumération des objets: 51, fait.
Décompte des objets: 100% (43/43), fait.
Compression par delta en utilisant jusqu’à 4 fils d’exécution
Compression des objets: 100% (43/43), fait.
Écriture des objets: 100% (51/51), 180.78 Kio \| 4.89 Mio/s, fait.
Total 51 (delta 3), réutilisés 0 (delta 0), réutilisés du pack 0 To http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
* [new branch] master → master
La branche 'master' est paramétrée pour suivre la branche distante 'master' depuis 'origin'.
```

![Projet local dans les projets de GitLab](images/gitlab_12.png)![Visualisation des détails du projet](images/gitlab_13.png)![Paramètres généraux du projet](images/gitlab_14.png)![Intégration des modifications des paramètres généraux dans la visualisation des détails du projet](images/gitlab_15.png)![Ajout des fichiers README.md, LICENCE, CHANGELOG et CONTRIBUTING](images/gitlab_16.png)

Vous pouvez maintenant récupérer les nouveaux fichiers d’information Gitlab (**CHANGELOG**, **CONTRIBUTING.md**, **LICENSE** et **README.md**) dans votre projet local :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git fetch
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git merge
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ssh-keygen -t rsa -b 2048 -C "Ma clé de chiffrement"
Generating public/private rsa key pair.
Enter file in which to save the key(/home/utilisateur/.ssh/id_rsa):
Created directory '/home/utilisateur/.ssh'.
Enter passphrase (empty for no passphrase): motdepasse
Enter same passphrase again: motdepasse
Your identification has been saved in /home/utilisateur/.ssh/id_rsa
Your public key has been saved in /home/utilisateur/.ssh/id_rsa.pub
The key fingerprint is: SHA256:n60tA2JwGV0tptwB48YrPT6hQQWrxGYhEVegfnO9GXM Ma clé de chiffrement
The key's randomart image is:
+---[RSA 2048]----+
|   +o+ooo+o..    |
|    = ..=..+ .   |
|   . = o+++ o    |
|  . +.oo+o..     |
|   . +o+ S E     |
|    . oo=.X o    |
|      ...=.o .   |
|          .oo    |
|           .o.   |
+----[SHA256]-----+
```

![Synthèse des projets GitLab](images/gitlab_17.png)![Demande d'ajout d'une clé SSH dans les détails du projet](images/gitlab_18.png)![Bouton Add SSH key](images/gitlab_19.png)![Fenêtre de définition des clés SSH](images/gitlab_20.png)

Copier le contenu du fichier «**/home/utilisateur/.ssh/id-rsa.pub**»

![Ajout de la clé SSH créée](images/gitlab_21.png)

## Autorisations pour Docker et le Runner

Cette étape consiste à créer un certificat pour autoriser Docker à interagir avec le registre et le Runner.

Pour le registre :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir -p /etc/docker/certs.d/MachineUbuntu:5000
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/gitlab/trusted-certs/MachineUbuntu.crt /etc/docker/certs.d/MachineUbuntu:5000/ca.crt
```

Pour le runner :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir -p /etc/gitlab-runner/certs
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/gitlab/trusted-certs/MachineUbuntu.crt /etc/gitlab-runner/certs/ca.crt
```

## Configurer et tester le Runner

Activation du runner dans docker

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
Unable to find image 'gitlab/gitlab-runner:latest' locally
latest: Pulling from gitlab/gitlab-runner
a31c7b29f4ad: Pull complete
d843a3e4344f: Pull complete
cf545e7bed9f: Pull complete
c863409f4294: Pull complete
ba06fc4b920b: Pull complete
Digest: sha256:79692bb4b239cb2c1a70d7726e633ec918a6af117b68da5eac55a00a85f38812
Status: Downloaded newer image for gitlab/gitlab-runner:latest

Runtime platform arch=amd64 os=linux pid=7 revision=8925d9a0 version=14.2.0
Running in system-mode.

Enter the Gitlab instance URL (for example, https://gitlab.com/):
```

Pour activer le runner :

![Projets GitLab](images/gitlab_22.png)![Détail du projet](images/gitlab_23.png)![Menu «Paramètres», sous menu «Intégration et livraison» du projet](images/gitlab_24.png)

Choisir l’option «**Exécuteurs**» et click sur le bouton «**Étendre**».

![Option de configuration des exécuteurs du projet](images/gitlab_25.png)

Aller dans «**Spécific runners**» dans l’option Exécuteurs.

![Section «Specific runner» de configuration des exécuteurs du projet](images/gitlab_26.png)

Informations pour déclarer le runner pour le projet.

![Section «Set up a specific runner manually»](images/gitlab_27.png)
```console
Enter the GitLab instance URL (for example, https://gitlab.com/): http://gitlab.domaine-perso.fr/
Enter the registration token: 9FfDsP_9Z2cXWi1Axwig
Enter a description for the runner: [75d626bde768]: Runner Developpement Python 3
Enter tags for the runner (comma-separated): runner
Registering runner... succeeded runner=Tzzfs5xc
Enter an executor: kubernetes, custom, docker-ssh, shell, docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox: docker
Enter the default Docker image (for example, ruby:2.6): python:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo chmod o+r /etc/gitlab-runner/config.toml
```

Changez dans «**/etc/gitlab-runner/config.toml**» :

```docker
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner Developpement Python 3"
  url = "http://gitlab.domaine-perso.fr/"
  token = "9FfDsP_9Z2cXWi1Axwig"
  executor = "docker"
  pull_policy = "if-not-present"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

Vous pouvez démarrer le Runner

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run -d --restart always --name gitlab-runner -v /etc/gitlab-runner:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
c9f30b11275ac803ebb17209441c7e0b6351c60d9f0ddadc17c8b0a7ae9cbb96
```

Autorisez le registre pour la machine ubuntu

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/docker/certs.d/MachineUbuntu\:5000/ca.crt /usr/local/share/ca-certificates/MachineUbuntu.crt
utilisateur@MachineUbuntu:~/**\ **repertoire_de_developpement**\ $ sudo update-ca-certificates
```

Si tout se passe bien vous obtenez le message :

```console
Updatting certificates in /etc/ssl/certs...
1 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
```

Dans «**Specific runners**»  de l’option «**Exécuteurs**» du sous menu «**Intégration et livraison**» du menu «**Paramètres**» du projet apparaît le runner en exécution

![Runner d'exécution du projet dans la section «Available specific runner» de l'option Exécuteurs d'Intégration et livraison du menu paramètre du projet](images/gitlab_28.png)

Mettre en pause le runner avec le bouton «**Pause**».

Cliquez sur l’icone ![L'icône éditer du runner du projet](images/gitlab_29.png) pour éditer les options du runner, et sélectionnez «**Indique si l’exécuteur peut choisir des tâches sans étiquettes (tags)**» :

![Fenêtre de configuration du runner d'un projet](images/gitlab_30.png)

Modifier aussi le temps «**Durée maximale d’exécution de la tâche**» avec «**30m**»

Relancer l’exécution du runner pour valider les modifications.

![Éxécution du runner](images/gitlab_31.png)

### Tester le fonctionnement du runner

Éditer le fichier «**.gitlab-ci.yml**» dans **repertoire_de_developpement**.

```yaml
travail-de-construction:
  stage: build
  script:
    - echo "Bonjour, $GITLAB_USER_LOGIN !"

travail-de-tests-1:
  stage: test
  script:
    - echo "Ce travail teste quelque chose"

travail-de-tests-2:
  stage: test
  script:
    - echo "Ce travail teste quelque chose, mais prend plus de temps que travail-de-test-1."
    - echo "Une fois les commandes echo terminées, il exécute la commande de veille pendant 20 secondes"
    - echo "qui simule un test qui dure 20 secondes de plus que travail-de-test-1."
    - sleep 20

deploiement-production:
  stage: deploy
  script:
    - echo "Ce travail déploie quelque chose de la branche $CI_COMMIT_BRANCH."
```

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Test du runner dans Gitlab"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```

![Runner en cours d'exécution dans la fenêtre de détails du projet](images/gitlab_32.png)

On peut voir l’activité en cours du runner avec l’icône : ![Icône d'activité du runner](images/gitlab_33.png)

Dans le sous menu «**Pipelines**» du menu «**Intégration et livraison**» du projet on peut voir les taches d’exécution du runner :

![Sous menu Pipeline avec des taches en cours d'exécutions](images/gitlab_34.png)

On voit ici la tache «**Travail-de-construction**» en cours dans la phase de «**Build**» de l’exécuteur.

![Tâche «Build» du runner en cours](images/gitlab_35.png)

Si on clique sur cette icône on voit les opérations en cours de la tache :

![icône de progression](images/gitlab_36.png)

Une fois la tache réussi, l’exécuteur passe dans la phase d’exécution des tests.

![Exécution des tests dans le Pipeline](images/gitlab_37.png)

On peut voir le résultat en cliquant sur les icônes des taches de tests.

![Résultat travail-de-test-1](images/gitlab_38.png)![Résultat travail-de-test-2](images/gitlab_39.png)

Puis après l’exécuteur passe dans la phase «**Deploy**».

![Pipelines exécutions OK](images/gitlab_40.png)![Résultat de la tache déploiement-production](images/gitlab_41.png)

Test du déploiement docker :

```yaml
default:
  image: python:latest
```

Pour plus d’informations sur Gitlab et son utilisation [https://github.com/SocialGouv/tutoriel-gitlab](https://github.com/SocialGouv/tutoriel-gitlab), [https://makina-corpus.com/blog/metier/2019/gitlab-astuces-projets](https://makina-corpus.com/blog/metier/2019/gitlab-astuces-projets).

## Tester les Pages GitLab

### Créer un projet de rendu de pages HTML

Créer un nouveau projet

![Création d'un nouveau projet](images/gitlab_42.png)

Création depuis un modèle

![Creation d'un projet depuis un modèle](images/gitlab_43.png)![Modèles de projets GitLab](images/gitlab_44.png)

Choisir «**Pages/Plain HTML**» comme modèle

![Projet Pages/Plain HTML](images/gitlab_45.png)

Renseignez :

- le nom du projet «**HTML**»
- La description du projet «**Test des GitLab Pages**»
- Le niveau de sécurité «**Public**»

![Renseignement du projet HTML](images/gitlab_46.png)![Détail du projet HTML](images/gitlab_47.png)

### Créer le «runner» pour ce projet

![menu «Paramètres», sous menu «Intégration et livraison» et option «Exécuteurs»](images/gitlab_48.png)![Paramètres du runner pour l'éxécuteur du projet HTML](images/gitlab_49.png)
```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
Runtime platform arch=amd64 os=linux pid=7 revision=8925d9a0 version=14.2.0
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/): http://gitlab.domaine-perso.fr/
Enter the registration token: 7YBLdSA9en4NMex5zyQy
Enter a description for the runner: [75d626bde768]: Runner Test Pages GitLab
Enter tags for the runner (comma-separated): runner
Registering runner... succeeded runner=Tzzfs5xc
Enter an executor: kubernetes, custom, docker-ssh, shell, docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox: docker
Enter the default Docker image (for example, ruby:2.6): alpine:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Changez dans «**/etc/gitlab-runner/config.toml**» :

```docker
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner Developpement Python 3"
  url = "http://gitlab.domaine-perso.fr/"
  token = "9FfDsP_9Z2cXWi1Axwig"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0

[[runners]]
  name = "Runner Test Pages GitLab"
  url = "http://gitlab.domaine-perso.fr/"
  token = "7YBLdSA9en4NMex5zyQy"
  executor = "docker"
  pull_policy = "if-not-present"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

Vous pouvez configurer et redémarrer le Runner

![Mettre en pause le runner](images/gitlab_50.png)![Éditer le runner](images/gitlab_51.png)![Édition de la configuration du runner du Projet HTML](images/gitlab_52.png)

Modifiez l’option «**Indique si l’exécuteur peut choisir des tâches sans étiquettes (tags)**» pour l’activer. Et préciser une durrée maximale d’exécution de «**30m**»

![Configuration du runner du Projet HTML](images/gitlab_53.png)

Enregirtrer les modifications et relancer le runner

![Reprendre l'exécution du runner](images/gitlab_54.png)

#### Déployer et tester le HTML dans une Pages GitLab

![Détail du projet HTM](images/gitlab_55.png)

Éditer le fichier «**gitlab-ci.yml**» avec GitLab en cliquant sur le bouton ![Bouton Configuration de l'intégration et de la livraison continues](images/gitlab_56.png)

![Édition du fichier gitlab-ci.yml](images/gitlab_57.png)

Renseigner le Message de commit «**Mise à jour du fichier .gitlab-ci.yml pour le lancement du runner**». Puis cliquer sur le bouton «**Commit changes**»

![Section de Message de commit](images/gitlab_58.png)![Détail du projet HTML avec le runner en cours de lancement](images/gitlab_59.png)

Cliquer sur la tache «**Pages**» sans annuler la tache ( l’icône Cancel de l’image )

![Taches pages du runner du projet HTML dans l'étape deploy](images/gitlab_60.png)![Bilan de l'exécution de la tache pages](images/gitlab_61.png)

Dans le menu «**Dépôt**» avec le sous menu «**Commits**» on peut voir la réussite de la tâche suite au commit.

![Commits de la mise à jour du fichier .gitlab-ci.yml  avec le runner OK](images/gitlab_62.png)

Maintenant il ne manque plus qu’a récupérer le site web de la page html.

Pour cela allons dans le menu «**Paramètres**»,  le sous menu «**Pages**» du projet.

![](images/gitlab_63.png)![](images/gitlab_64.png)

La présence du lien «[http://utilisateur.documentation.domaine-perso.fr/html](http://utilisateur.documentation.domaine-perso.fr/html)» nous confirme que GitLab fonctionne avec les Pages.

Un click sur ce lien et on vérifie l’accès au site web.

![](images/gitlab_65.png)

Supprimez le projet («**Paramètres/Général/Advenced/Delete project**»), et nettoyez le runner de test «**Runner Test Pages GitLab**» du fichier «**/etc/gitlab-runner/config.toml**».
