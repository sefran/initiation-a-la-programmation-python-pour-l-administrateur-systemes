# Autorisations pour Docker et le Runner

Cette étape consiste à créer un certificat pour autoriser Docker à interagir avec le registre et le Runner.

Pour le registre :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir -p /etc/docker/certs.d/MachineUbuntu:5000
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/gitlab/trusted-certs/MachineUbuntu.crt /etc/docker/certs.d/MachineUbuntu:5000/ca.crt
```

Pour le runner :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir -p /etc/gitlab-runner/certs
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/gitlab/trusted-certs/MachineUbuntu.crt /etc/gitlab-runner/certs/ca.crt
```
