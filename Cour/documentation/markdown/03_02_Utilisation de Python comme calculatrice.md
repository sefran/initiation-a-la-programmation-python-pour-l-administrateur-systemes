# Utilisation de Python comme calculatrice

L’interpréteur agit comme une simple calculatrice. Vous pouvez lui entrer une expression et il vous affiche la valeur.

Distribuer document codage_nombres.pdf.

La syntaxe des expressions est simple, les opérateurs `+`, `-`, `*` et `/` fonctionnent comme dans la plupart des langages (par exemple, Pascal ou C) ; les parenthèses peuvent être utilisées pour faire des regroupements. Par exemple :

```pycon
>>> 2 + 2
4
>>> 50 - 5 \ 6
20
>>> (50 - 5 * 6) / 4
5.0
>>> 8 / 5  # la division retourne toujours un nombre à virgule flottant
1.6
```

Les nombres entiers (comme 2, 4, 20) sont de type **int**, alors que les décimaux (comme 5.0, 1.6) sont de type **float**.

Les **divisions** `/` donnent toujours des **float**.

Utilisez **l’opérateur** `//` pour effectuer des **divisions entières**, afin d’obtenir un résultat entier.

Pour obtenir **le reste** d’une division entière, utilisez **l’opérateur** `%` :

```pycon
>>> 17 / 3 # la division classique renvoie un nombre à virgule flottante
5.666666666666667
>>> 17 // 3  # division entière, ne tient pas compte du reste
5
>>> 17 % 3  # l’opérateur % retourne le reste de la division
2
>>> 5 * 3 + 2  # le quotien * diviseur + reste
17
```

En Python, il est possible de calculer des puissances avec l’opérateur `**` :

```pycon
>>> 5 ** 2  # 5 au carré
25
>>> 2 ** 7  # 2 à la puissance 7
128
```

Les **nombres à virgule flottante** sont tout à fait admis (Python utilise le point «**.**» comme séparateur entre la partie entière et la partie décimale des nombres, c’est la convention anglo-saxonne), **les opérateurs avec des opérandes de types différents convertissent l’opérande de type entier en type virgule flottante** :

```pycon
>>> 4 * 3.75 - 1
14.0
```

En plus des **int** et des **float**, il existe les **Décimal** et les **Fraction** avec l’utilisation d’une bibliothèque.

Python gère aussi **les nombres complexes**, en utilisant le suffixe «**j**» ou «**J**» pour indiquer la partie imaginaire (tel que `3+5j`).

```pycon
>>> (3+5j) * (3-5j)
(34+0j)
>>> _.conjugate()
(34+0j)
>>> (34+0j).real
34.0
>>> 34 + 0j
(34+0j)
>>> _.imag
0.0
```
