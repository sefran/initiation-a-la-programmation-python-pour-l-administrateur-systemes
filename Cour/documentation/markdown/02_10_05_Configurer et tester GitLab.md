# Configurer et tester GitLab

Saisissez dans un navigateur l’URL **gitlab.domaine-perso.fr**

![Initialisation du mot de passe de l'administrateur root de GitLab](images/gitlab_1.png)

Si vous n’avez pas la fenêtre d’initialisation du mot de passe :

```console
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-rake "gitlab:password:reset"
```

![Connection en root dans GitLab](images/gitlab_2.png)![Accueil projets GitLab](images/gitlab_3.png)![Préférences de GitLab aller vers «Localization»](images/gitlab_4.png)![Préférences language à «French» et First day of the week à «Monday»](images/gitlab_5.png)

Tapez la touche **F5** pour rafraîchir l’affichage de votre navigateur.

![Icône Paramètres de l'utilisateur](images/gitlab_6.png)![Profil de l'utilisateur](images/gitlab_7.png)![Compte de l'utilisateur](images/gitlab_8.png)![Icône  espace d'administration](images/gitlab_9.png)![Espace d'administration Paramètres et menu Général](images/gitlab_10.png)![Contrôle de visibilité d'accès](images/gitlab_11.png)

Intégrer le dépot git local dans Gitlab :

```console
utilisateur@MachineUbuntu:~/$ cd repertoire_de_developpement
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config credential.helper store
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git remote add origin http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push -u origin --all
Username for 'http://gitlab.domaine-perso.fr': utilisateur
Password for 'http://gitlab.domaine-perso.fr': motdepasse
Énumération des objets: 51, fait.
Décompte des objets: 100% (43/43), fait.
Compression par delta en utilisant jusqu’à 4 fils d’exécution
Compression des objets: 100% (43/43), fait.
Écriture des objets: 100% (51/51), 180.78 Kio \| 4.89 Mio/s, fait.
Total 51 (delta 3), réutilisés 0 (delta 0), réutilisés du pack 0 To http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
* [new branch] master → master
La branche 'master' est paramétrée pour suivre la branche distante 'master' depuis 'origin'.
```

![Projet local dans les projets de GitLab](images/gitlab_12.png)![Visualisation des détails du projet](images/gitlab_13.png)![Paramètres généraux du projet](images/gitlab_14.png)![Intégration des modifications des paramètres généraux dans la visualisation des détails du projet](images/gitlab_15.png)![Ajout des fichiers README.md, LICENCE, CHANGELOG et CONTRIBUTING](images/gitlab_16.png)

Vous pouvez maintenant récupérer les nouveaux fichiers d’information Gitlab (**CHANGELOG**, **CONTRIBUTING.md**, **LICENSE** et **README.md**) dans votre projet local :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git fetch
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git merge
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ssh-keygen -t rsa -b 2048 -C "Ma clé de chiffrement"
Generating public/private rsa key pair.
Enter file in which to save the key(/home/utilisateur/.ssh/id_rsa):
Created directory '/home/utilisateur/.ssh'.
Enter passphrase (empty for no passphrase): motdepasse
Enter same passphrase again: motdepasse
Your identification has been saved in /home/utilisateur/.ssh/id_rsa
Your public key has been saved in /home/utilisateur/.ssh/id_rsa.pub
The key fingerprint is: SHA256:n60tA2JwGV0tptwB48YrPT6hQQWrxGYhEVegfnO9GXM Ma clé de chiffrement
The key's randomart image is:
+---[RSA 2048]----+
|   +o+ooo+o..    |
|    = ..=..+ .   |
|   . = o+++ o    |
|  . +.oo+o..     |
|   . +o+ S E     |
|    . oo=.X o    |
|      ...=.o .   |
|          .oo    |
|           .o.   |
+----[SHA256]-----+
```

![Synthèse des projets GitLab](images/gitlab_17.png)![Demande d'ajout d'une clé SSH dans les détails du projet](images/gitlab_18.png)![Bouton Add SSH key](images/gitlab_19.png)![Fenêtre de définition des clés SSH](images/gitlab_20.png)

Copier le contenu du fichier «**/home/utilisateur/.ssh/id-rsa.pub**»

![Ajout de la clé SSH créée](images/gitlab_21.png)
