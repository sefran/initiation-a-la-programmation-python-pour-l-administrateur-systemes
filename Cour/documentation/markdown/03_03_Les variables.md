# Les variables

## L’affectation dans le code

Le signe égal `=` est utilisé pour affecter une valeur à une variable.

Dans ce cas, aucun résultat n’est affiché avant l’invite suivante :

```pycon
>>> largeur = 20
>>> hauteur = 5 * 9
>>> largeur * hauteur
900
```

Si une variable n’est pas définie (si aucune valeur ne lui a été affectée), son utilisation produit une erreur :

```console
>>> n # Essaye d'accéder à une variable non définie
Traceback (most recent call last):
File "<input>", line 1, in <module>
 n # Essaye d'accéder à une variable non définie
NameError: name 'n' is not defined
```

En mode interactif, la dernière expression affichée est affectée à la variable `_`. Ainsi, lorsque vous utilisez Python comme calculatrice, cela vous permet de continuer des calculs facilement, par exemple :

```pycon
>>> taxe = 12.5 / 100
>>> prix = 100.50
>>> prix * taxe
12.5625
>>> prix + _
113.0625
>>> round(_, 2)
113.06
```

Cette variable doit être considérée comme une variable en lecture seule par l’utilisateur. N’affectez pas de valeur explicitement à `_`. Vous créeriez ainsi une variable locale indépendante, avec le même nom, qui masquerait la variable native et son fonctionnement magique.

## L’affectation au clavier

### La fonction input

```pycon
>>> nom = input('Saisissez votre nom : ')
Saisissez votre nom : PERSONNE
>>> 'Bonjour ' + nom
'Bonjour PERSONNE'
```

## L’affectation par variables passées à un script

### Passage d’arguments en ligne de commande

Python supporte complètement la création de programmes qui peuvent être lancés en ligne de commande, à l’aide d’arguments et de drapeaux longs ou cours pour spécifier diverses options.

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 4_Passage_paramètres ; cd 4_Passage_paramètres
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ nano litparams.py ; chmod u+x litparams.py
```

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

for arg in sys.argv:
    print(arg)
```

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ ./litparams.py -a --help bidon
```

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ nano litargs.py ; chmod u+x litargs.py
```

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

print('Nombre d\'arguments :', len(sys.argv), 'arguments.')
print('Liste des arguments :', str(sys.argv))
```

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/4_Passage_paramètres$ ./litargs.py -a --help bidon ; cd ..
```
