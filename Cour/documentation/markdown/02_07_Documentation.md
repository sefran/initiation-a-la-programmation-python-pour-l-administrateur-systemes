# Documentation

- **Documenter le code** (annotations des variables, docstring)
- **Génération de la documentation** (Sphinx, doxygen, docutil, pdoc3, pydoctor, etc.)
- **Syntaxes** (restructured text, markdown, asciidoc, mediawiki, html, etc.)

Fournir un lexique sur la syntaxe pour la doc.

Nous reviendrons plus tard dans ce cours sur l’utilisation de la documentation dans Python.

Mise en place du système de documentation du code, architecture et scripts (Sphinx).

Installer les logiciels de la documentation :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ source .env/bin/activate
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pip install sphinx sphinx-intl
```

Créer la documentations :

```console
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir docs; cd docs
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sphinx-quickstart
Bienvenue dans le kit de démarrage rapide de Sphinx 3.2.1.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
"source" and "build" directories within the root path.

> Séparer les répertoires build et source (y/n) [n]: y

The project name will occur in several places in the built documentation.

> Nom du projet: Documentation sur l’initiation à la programmation Python pour l’administrateur systèmes
> Nom(s) de l\'auteur: Prénom NOM
> version du projet []:

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.

> Langue du projet [en]: fr

Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/source/conf.py.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/source/index.rst.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/Makefile.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/make.bat.

Terminé : la structure initiale a été créée.

You should now populate your master file /home/utilisateur/repertoire_de_developpement/docs/source/index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
    make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ rmdir build ; mv source sources-documents
```

modifier les fichiers «**Makefile**» et «**make.bat**», dans lesquels il faudra adapter le contenu de la variable «**SOURCEDIR**».

**Makefile** :

```makefile
SOURCEDIR = sources-documents
BUILDDIR = documentation
```

**make.bat** :

```doscon
set SOURCEDIR=sources-documents
set BUILDDIR=documentation
```

Pour générer la doc sous Linux, c’est très simple, il suffit d’ouvrir un terminal dans le dossier du projet et de taper la commande suivante :

```console
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html ; cd ..
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ deactivate
```

Si vous n’avez pas la commande `make`, il vous faudra l’installer. Ça peut se faire avec la commande suivante si vous utilisez Debian, Ubuntu ou l’un de leurs dérivés :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install build-essential
```

Si vous êtes sous Windows et que vous utilisez Git Bash, il vous faudra utiliser la commande suivante pour générer votre documentation :

```console
$ ./make.bat html
```

Voir la documentation générée «**…/repertoire_de_developpement/docs/documentation/html/index.html**» avec un navigateur web.

## Sauvegarder la structure de documentation

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
Votre branche est à jour avec 'origin/master'.
Modifications qui seront validées :
(utilisez "git restore --staged <fichier>..." pour désindexer)
 nouveau fichier : docs/Makefile
 nouveau fichier : docs/documentation/doctrees/environment.pickle
 nouveau fichier : docs/documentation/doctrees/index.doctree
 nouveau fichier : docs/documentation/html/.buildinfo
 nouveau fichier : docs/documentation/html/_sources/index.rst.txt
 nouveau fichier : docs/documentation/html/_static/_stemmer.js
 nouveau fichier : docs/documentation/html/_static/alabaster.css
 nouveau fichier : docs/documentation/html/_static/basic.css
 nouveau fichier : docs/documentation/html/_static/custom.css
 nouveau fichier : docs/documentation/html/_static/doctools.js
 nouveau fichier : docs/documentation/html/_static/documentation_options.js
 nouveau fichier : docs/documentation/html/_static/file.png
 nouveau fichier : docs/documentation/html/_static/jquery-3.5.1.js
 nouveau fichier : docs/documentation/html/_static/jquery.js
 nouveau fichier : docs/documentation/html/_static/language_data.js
 nouveau fichier : docs/documentation/html/_static/minus.png
 nouveau fichier : docs/documentation/html/_static/plus.png
 nouveau fichier : docs/documentation/html/_static/pygments.css
 nouveau fichier : docs/documentation/html/_static/searchtools.js
 nouveau fichier : docs/documentation/html/_static/translations.js
 nouveau fichier : docs/documentation/html/_static/underscore-1.3.1.js
 nouveau fichier : docs/documentation/html/_static/underscore.js
 nouveau fichier : docs/documentation/html/genindex.html
 nouveau fichier : docs/documentation/html/index.html
 nouveau fichier : docs/documentation/html/objects.inv
 nouveau fichier : docs/documentation/html/search.html
 nouveau fichier : docs/documentation/html/searchindex.js
 nouveau fichier : docs/make.bat
 nouveau fichier : docs/sources-documents/conf.py
 nouveau fichier : docs/sources-documents/index.rst
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Structure de la documentation du projet"
[master 31c720b] Structure de la documentation du projet
31 files changed, 17692 insertions(+)
create mode 100644 docs/Makefile
create mode 100644 docs/documentation/doctrees/environment.pickle
create mode 100644 docs/documentation/doctrees/index.doctree
create mode 100644 docs/documentation/html/.buildinfo
create mode 100644 docs/documentation/html/_sources/index.rst.txt
create mode 100644 docs/documentation/html/_static/alabaster.css
create mode 100644 docs/documentation/html/_static/base-stemmer.js
create mode 100644 docs/documentation/html/_static/basic.css
create mode 100644 docs/documentation/html/_static/custom.css
create mode 100644 docs/documentation/html/_static/doctools.js
create mode 100644 docs/documentation/html/_static/documentation_options.js
create mode 100644 docs/documentation/html/_static/file.png
create mode 100644 docs/documentation/html/_static/french-stemmer.js
create mode 100644 docs/documentation/html/_static/jquery-3.5.1.js
create mode 100644 docs/documentation/html/_static/jquery.js
create mode 100644 docs/documentation/html/_static/language_data.js
create mode 100644 docs/documentation/html/_static/minus.png
create mode 100644 docs/documentation/html/_static/plus.png
create mode 100644 docs/documentation/html/_static/pygments.css
create mode 100644 docs/documentation/html/_static/searchtools.js
create mode 100644 docs/documentation/html/_static/translations.js
create mode 100644 docs/documentation/html/_static/underscore-1.12.0.js
create mode 100644 docs/documentation/html/_static/underscore.js
create mode 100644 docs/documentation/html/genindex.html
create mode 100644 docs/documentation/html/index.html
create mode 100644 docs/documentation/html/objects.inv
create mode 100644 docs/documentation/html/search.html
create mode 100644 docs/documentation/html/searchindex.js
create mode 100644 docs/make.bat
create mode 100644 docs/sources-documents/conf.py
create mode 100644 docs/sources-documents/index.rst
```
