# L’édition de code Python

L’édition de code est une question d’ergonomie personnelle.

Certains préfèrent la méthode manuelle pour tout contrôler de leur poste de travail (système et comprendre ce qu’ils utilisent et font). Pour ne pas s’enfermer dans un environnement de travail fournisseur logiciel et permettre l’interopérabilité. Ils se tourneront alors vers un éditeur de texte évolué avec des plugins plus ou moins automatisés pour garder le contrôle de leur poste de travail (**ingénieurs systèmes**).

D’autres adorent l’automatisation de leur production de développement et ne veulent se concentrer que sur le code. Ils se tourneront alors vers un «**I**ntegrated **D**eveloppement **E**nvironnement» le plus intégré que possible et standard (**développeurs**).

Et encore d’autres aiment s’enferment dans des technologies fournisseurs et se tournent vers des **R**apid **A**pplication **D**éveloppement (**informatique non cœur de métier**) qui ont le défaut de la non optimisation du code et d’être des usines à gaz.

- **Éditeurs de texte avec coloration syntaxique et plugins** (l’IDE c’est le système d’exploitation. Pour les geeks comme moi ;-p)
- **Idle** (IDE minimaliste natif de python)
- **Pyscripter** (IDE gratuit débutants pour Windows)
- **Eric** (IDE purement python)
- **Éclipse** (IDE professionnel industriel avec l’extension PyDev pour le développement Python)
- **Visual studio** (IDE/RAD .Net professionnel Windows avec l’extension PTVS=Python Tools for Visual Studio)
- **Boaconstructor** (RAD Python + wxPython)
- **Visual python** (RAD Python + Tkinter)

Voir pour une liste plus exhaustive des éditeurs [https://wiki.python.org/moin/PythonEditors](https://wiki.python.org/moin/PythonEditors) et pour les IDE voir [https://wiki.python.org/moin/IntegratedDevelopmentEnvironments](https://wiki.python.org/moin/IntegratedDevelopmentEnvironments)
<br/>

## Installer un éditeur de code

Exercice :

Le stagiaire installe l’éditeur de son choix.

Distribuer sous forme papier la procédure pour **Visual studio** voir
[https://docs.microsoft.com/fr-fr/visualstudio/python/installing-python-support-in-visual-studio?view=vs-2019](https://docs.microsoft.com/fr-fr/visualstudio/python/installing-python-support-in-visual-studio?view=vs-2019)

Présentation de Visual studio ?

Distribuer sous forme papier la procédure pour **éclipse avec PyDev** :

- installer [https://www.liclipse.com/download.html](https://www.liclipse.com/download.html)
- Pour éclipse seul voir
  [https://www.eclipse.org/downloads/packages/installer](https://www.eclipse.org/downloads/packages/installer)
- Pour le plugin voir
  [https://koor.fr/Python/Tutorial/python_ide_pydev.wp](https://koor.fr/Python/Tutorial/python_ide_pydev.wp)

Ou pour les manuels et les pros de l’éditeur texte (allergiques aux IDE et qui veulent contrôler ce qu’il y a sous le capot), installation de notepad++ par exemple [https://notepad-plus-plus.org/downloads/](https://notepad-plus-plus.org/downloads/).
