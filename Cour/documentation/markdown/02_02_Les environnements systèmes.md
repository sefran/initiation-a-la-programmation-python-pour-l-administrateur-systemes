# Les environnements systèmes

## EnvironnementUNIX/Linux sous Windows

### SSH et Telnet

Vous pouvez vous connecter à distance avec un terminal texte sur un serveur Unix/Linux [en telnet](https://www.pcastuces.com/pratique/astuces/4519.htm) (sans sécurité), ou de façon très sécurisé [en ssh](http://www.kevinsubileau.fr/informatique/astuces-tutoriels/windows-10-client-serveur-ssh-natif.html) en l’installant directement depuis Windows. Ceci est utile si l’on veut dans un environnement Unix/Linux administrer, développer ou faire des tests de qualifications avec une infrastructure proche de celle réelle de production (Modèle V ou DEVOPS).

### Serveur X

Vous pouvez vous connecter à un un client graphique Unix/Linux sous Windows en installant le serveur graphique [VcXsrv](https://sourceforge.net/projects/vcxsrv/). Utile si on veut tester des interfaces graphiques Unix/Linux à distance sous Windows ou se connecter en mode graphique sur une application en tant qu’utilisateur Unix/Linux.

### RDP

Unix/Linux supporte l’installation d’un client RDP sur vos serveurs pour se connecter avec une session terminal serveur Windows en tant que client utilisateur Unix graphique.

### Nomachine/freenx ou X2GO (optimisation X)

Un serveur graphique Windows Nomachine ou X2GO doit être installé sous Windows pour avoir une connexion client Unix/Linux graphique.

### VNC

Linux permet aussi le partage de session graphique active d’un utilisateur Unix/Linux. C’est alors l’utilisation d’un serveur Unix/Linux (TightVNC, X11Vnc ou Vino) avec un client VNC à installer sous Windows.

Phase de test de la maîtrise par les stagiaires du poste de travail, et réglages des problèmes techniques des postes de travail.

Réglages des problèmes techniques de début de cours.

## Linux sous Windows

Lorsque nous souhaitons utiliser Unix/Linux sous Windows pour administrer, développer, tester nous passons habituellement par une solution de virtualisation. Les logiciels [Hyper-V](https://docs.microsoft.com/fr-fr/virtualization/hyper-v-on-windows/about/) ou [VirtualBox](https://www.zebulon.fr/telechargements/utilitaires/systeme-utilitaires/virtualbox.html) permettent de virtualiser une distribution Unix/Linux de son choix avec Windows.
Néanmoins, Windows 10 permet d’accéder à Linux depuis Windows assez simplement.

### Activer le mode développeur de Windows 10

Avant toutes choses, il convient d’activer le mode développeur dans Windows. Cliquer sur le bouton **Démarrer**, aller dans **Paramètres** puis choisissez **Mise à jour et sécurité**.

![Mise à jour et sécurité](images/env_system_1.png)

Cliquer ensuite sur **Pour les développeurs** dans la colonne de gauche.

![Pour les développeurs](images/env_system_2.png)

Sélectionner le **Mode développeur**, une fenêtre vous demandera d’*activer le mode développeur* :

![Utiliser les fonctionnalités de développement](images/env_system_3.png)

Cliquer sur **Oui**. La recherche du package en mode développeur débute :

![Recherche du package en mode développeur](images/env_system_4.png)

Il vous sera ensuite demandé de redémarrer l’ordinateur. Après le redémarrage, le **package en mode développeur** est installé et les **outils à distance pour le Bureau** sont désormais activés.

### Installer le sous-système Windows pour Linux

Nous devons maintenant installer un sous-système Windows pour faire fonctionner Linux. Cliquer sur le bouton **Démarrer**, **Paramètres** puis **Applications** :

![Applications](images/env_system_5.png)

Dans la colonne de droite, cliquer sur **Programmes et fonctionnalités** dans la section Paramètres associés. Dans la colonne de gauche, cliquer
sur **Activer ou désactiver des fonctionnalités Windows**. Cochez l’option **Sous-système Windows pour Linux** puis cliquer sur le bouton **OK**.

![Sous-système Windows pour Linux](images/env_system_6.png)

Les fichiers vont être installés, puis Windows vous demande de redémarrer l’ordinateur. Cliquer sur le bouton **Redémarrer maintenant**.

### Choisir une distribution Linux pour Windows

Nous avons activé le mode développeur et installé le sous-système Windows pour Linux, nous devons maintenant installer une distribution Linux fonctionnant avec Windows 10. Pour lancer Linux, il nous suffira de taper la commande **Bash** dans le champ de recherche en bas à gauche :

![Lancement Bash](images/env_system_7.png)

La fenêtre bash.exe s’ouvre alors :

![Shell Bash Windows](images/env_system_8.png)

Nous n’avons pas encore installé Linux, mais nous avons le shell Unix Bash sous Windows.

Windows nous invite alors à installer Linux en indiquant un lien. Dans votre navigateur, saisissez alors l’URL [https://aka.ms/wslstore](https://aka.ms/wslstore).
Microsoft Store va alors s’ouvrir et vous demander de choisir votre distribution Linux compatible avec Windows.

Les distributions suivantes sont proposées Ubuntu, Debian, Fedora, openSUSE, SUSE Linux Enterprise Server, Kali Linux, etc.

![Distribution Ubuntu de Microsoft Store](images/env_system_9.png)

Nous choisissons Ubuntu pour ce cours (plus modèle en V avec une base Debian).

Après avoir cliqué sur Ubuntu, cliquer sur le bouton **Télécharger**. Après téléchargement et installation, cliquer sur le bouton **Lancer**.

### Installer Linux

Voilà maintenant votre système Linux prêt à être installé sous Windows.

Lancer à nouveau la commande **bash** dans le champ de recherche. Le premier lancement permettra d’installer définitivement Ubuntu sous Windows :

![Bash installation d'Ubuntu](images/env_system_10.png)

Vous devrez alors saisir un login de votre choix ainsi qu’un mot de passe :

![Fenêtre terminal sous Ubuntu dans Windows](images/env_system_11.png)

Ubuntu est maintenant prêt à être utilisé en ligne de commande, votre disque dur étant déjà monté.

## Environnements Windows sous MAC OS

### PowerShell

installer **PowerShell** :

Lancer un shell

```console
$ brew cask install powershell
```

Enfin, vérifiez que votre installation fonctionne correctement :

```console
$ pwsh
```

Quand de nouvelles versions de **PowerShell** sont publiées, mettez à jour les formules de **Homebrew** et mettez à niveau **PowerShell** :

```console
$ brew update
$ brew upgrade powershell –cask
```

## Environnements Windows sous Linux

### PowerShell

PowerShell pour Linux est distribué par Microsoft pour les référentiels de packages afin de faciliter l’installation et les mises à jour à partir de Windows.

```console
utilisateur@MachineUbuntu:~$ sudo snap install --classic powershell
utilisateur@MachineUbuntu:~$ sudo snap remove powershell
```

#### La méthode recommandée est la suivante pour une distribution Debian

**Inscrivez le référentiel de logiciels Microsoft pour Ubuntu**

*Téléchargements des clés de cryptages CPG des dépôts de Microsoft*

```console
utilisateur@MachineUbuntu:~$ wget -q
https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb
```

*Enregistrer ces clés Microsoft dans le répertoire d’installation de logiciels*

```console
utilisateur@MachineUbuntu:~$ sudo dpkg -i packages-microsoft-prod.deb
```

*Mise à jour de la liste des logiciels installables*

```console
utilisateur@MachineUbuntu:~$ sudo apt update
```

**Installer Powershell**

Après l’inscription du dépôt logiciel Microsoft, vous pouvez installer **PowerShell**

*Installation*

```console
utilisateur@MachineUbuntu:~$ sudo apt install -y powershell liblttng-ust0 liburcu6 liblttng-ust-ctl4
```

*Démarrer PowerShell*

```console
utilisateur@MachineUbuntu:~$ pwsh
PowerShell 7.1.3
Copyright (c) Microsoft Corporation.

https://aka.ms/powershell
Type 'help' to get help.

PS /home/utilisateur> exit
```

### Serveurs RDP (TSE)

Vous pouvez vous connecter en mode graphique sur un serveur Linux distant en TSE avec le protocole RDP en installant XRDP :

```console
utilisateur@MachineUbuntu:~$ sudo apt install gnome-session gnome-terminal
utilisateur@MachineUbuntu:~$ sudo apt -y install xrdp
utilisateur@MachineUbuntu:~$ sudo systemctl status xrdp
utilisateur@MachineUbuntu:~$ sudo adduser xrdp ssl-cert
```

Pensez à désinstaller le serveur graphique de vos serveurs Linux de production pour la sécurité (sous Ubuntu `sudo apt remove xserver-xorg-video-all` ou `sudo apt remove xserver-xorg-driver-all`)
