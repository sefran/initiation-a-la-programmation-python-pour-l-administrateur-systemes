# Conception et modélisations

Comment approchez-vous le développement d’une application ?

- **Structure** (UML : Visual paradigm, StarUML 3, PyUML pour éclipse,
  PlantUML pour visual studio, Pynsource, Graphor, Umbrello, le papier
  et le crayon)
- **Interactions** (Concurrences : SA-RT, logique avec l’algèbre de
  Boole, etc.)
- **Données** (Sql avec Merise, nosql, bases graphes, etc.)
- **Optimisations** (recettes, patrons de conception, etc.)

## Réalités des développeurs

- Développement logiciel (**approche produit**) : Modélisation
  conceptuelle vers code.
- Développeurs ingénieurs systèmes déploiements/exploitation/matériels
  (**approche opérationnelle**) : Fonctions vers code.
- Développeur WEB (**approche interfaces**) : Apparence/Ergonomie vers
  code.

## Bonnes pratiques

- Modèle (**données**) Vues (**interfaces utilisateurs ou
  applicatives**) Contrôleur (**opérations entre les données et les
  interfaces**)
- **K**eep **I**t **S**imple **S**tupid et «**modulaire**»
  (découpage en plus petit programmes ou en objets simples)
- **Respecter les standards des normes d’interopérabilités** (lire les
  normes), et ne pas réinventer la roue avec le code (voir la
  catastrophe des clients de messagerie avec maildir et l’obligation de
  passer par imap pour en avoir les fonctionnalités sur le client MUA).
- **Maintenabilité et compréhension du code pour les autres**
  (documentation, composants, déploiement, maintenance).

Lire [https://www.laurentbloch.org/Data/SI-Projets-extraits/livre008.html](https://www.laurentbloch.org/Data/SI-Projets-extraits/livre008.html)
pour ceux qui veulent aller plus loin sur le sujet.

Distribuer sous forme papier
