# Présentation et installation des outils du langage

Approche interactive avec les stagiaires pour créer le groupe.

Avez-vous déjà programmé ?

Avez-vous déjà programmé en Python ?

## Conception et modélisations

Comment approchez-vous le développement d’une application ?

- **Structure** (UML : Visual paradigm, StarUML 3, PyUML pour éclipse,
  PlantUML pour visual studio, Pynsource, Graphor, Umbrello, le papier
  et le crayon)
- **Interactions** (Concurrences : SA-RT, logique avec l’algèbre de
  Boole, etc.)
- **Données** (Sql avec Merise, nosql, bases graphes, etc.)
- **Optimisations** (recettes, patrons de conception, etc.)

### Réalités des développeurs

- Développement logiciel (**approche produit**) : Modélisation
  conceptuelle vers code.
- Développeurs ingénieurs systèmes déploiements/exploitation/matériels
  (**approche opérationnelle**) : Fonctions vers code.
- Développeur WEB (**approche interfaces**) : Apparence/Ergonomie vers
  code.

### Bonnes pratiques

- Modèle (**données**) Vues (**interfaces utilisateurs ou
  applicatives**) Contrôleur (**opérations entre les données et les
  interfaces**)
- **K**eep **I**t **S**imple **S**tupid et «**modulaire**»
  (découpage en plus petit programmes ou en objets simples)
- **Respecter les standards des normes d’interopérabilités** (lire les
  normes), et ne pas réinventer la roue avec le code (voir la
  catastrophe des clients de messagerie avec maildir et l’obligation de
  passer par imap pour en avoir les fonctionnalités sur le client MUA).
- **Maintenabilité et compréhension du code pour les autres**
  (documentation, composants, déploiement, maintenance).

Lire [https://www.laurentbloch.org/Data/SI-Projets-extraits/livre008.html](https://www.laurentbloch.org/Data/SI-Projets-extraits/livre008.html)
pour ceux qui veulent aller plus loin sur le sujet.

Distribuer sous forme papier

## Les environnements systèmes

### EnvironnementUNIX/Linux sous Windows

#### SSH et Telnet

Vous pouvez vous connecter à distance avec un terminal texte sur un serveur Unix/Linux [en telnet](https://www.pcastuces.com/pratique/astuces/4519.htm) (sans sécurité), ou de façon très sécurisé [en ssh](http://www.kevinsubileau.fr/informatique/astuces-tutoriels/windows-10-client-serveur-ssh-natif.html) en l’installant directement depuis Windows. Ceci est utile si l’on veut dans un environnement Unix/Linux administrer, développer ou faire des tests de qualifications avec une infrastructure proche de celle réelle de production (Modèle V ou DEVOPS).

#### Serveur X

Vous pouvez vous connecter à un un client graphique Unix/Linux sous Windows en installant le serveur graphique [VcXsrv](https://sourceforge.net/projects/vcxsrv/). Utile si on veut tester des interfaces graphiques Unix/Linux à distance sous Windows ou se connecter en mode graphique sur une application en tant qu’utilisateur Unix/Linux.

#### RDP

Unix/Linux supporte l’installation d’un client RDP sur vos serveurs pour se connecter avec une session terminal serveur Windows en tant que client utilisateur Unix graphique.

#### Nomachine/freenx ou X2GO (optimisation X)

Un serveur graphique Windows Nomachine ou X2GO doit être installé sous Windows pour avoir une connexion client Unix/Linux graphique.

#### VNC

Linux permet aussi le partage de session graphique active d’un utilisateur Unix/Linux. C’est alors l’utilisation d’un serveur Unix/Linux (TightVNC, X11Vnc ou Vino) avec un client VNC à installer sous Windows.

Phase de test de la maîtrise par les stagiaires du poste de travail, et réglages des problèmes techniques des postes de travail.

Réglages des problèmes techniques de début de cours.

### Linux sous Windows

Lorsque nous souhaitons utiliser Unix/Linux sous Windows pour administrer, développer, tester nous passons habituellement par une solution de virtualisation. Les logiciels [Hyper-V](https://docs.microsoft.com/fr-fr/virtualization/hyper-v-on-windows/about/) ou [VirtualBox](https://www.zebulon.fr/telechargements/utilitaires/systeme-utilitaires/virtualbox.html) permettent de virtualiser une distribution Unix/Linux de son choix avec Windows.
Néanmoins, Windows 10 permet d’accéder à Linux depuis Windows assez simplement.

#### Activer le mode développeur de Windows 10

Avant toutes choses, il convient d’activer le mode développeur dans Windows. Cliquer sur le bouton **Démarrer**, aller dans **Paramètres** puis choisissez **Mise à jour et sécurité**.

![Mise à jour et sécurité](images/env_system_1.png)

Cliquer ensuite sur **Pour les développeurs** dans la colonne de gauche.

![Pour les développeurs](images/env_system_2.png)

Sélectionner le **Mode développeur**, une fenêtre vous demandera d’*activer le mode développeur* :

![Utiliser les fonctionnalités de développement](images/env_system_3.png)

Cliquer sur **Oui**. La recherche du package en mode développeur débute :

![Recherche du package en mode développeur](images/env_system_4.png)

Il vous sera ensuite demandé de redémarrer l’ordinateur. Après le redémarrage, le **package en mode développeur** est installé et les **outils à distance pour le Bureau** sont désormais activés.

#### Installer le sous-système Windows pour Linux

Nous devons maintenant installer un sous-système Windows pour faire fonctionner Linux. Cliquer sur le bouton **Démarrer**, **Paramètres** puis **Applications** :

![Applications](images/env_system_5.png)

Dans la colonne de droite, cliquer sur **Programmes et fonctionnalités** dans la section Paramètres associés. Dans la colonne de gauche, cliquer
sur **Activer ou désactiver des fonctionnalités Windows**. Cochez l’option **Sous-système Windows pour Linux** puis cliquer sur le bouton **OK**.

![Sous-système Windows pour Linux](images/env_system_6.png)

Les fichiers vont être installés, puis Windows vous demande de redémarrer l’ordinateur. Cliquer sur le bouton **Redémarrer maintenant**.

#### Choisir une distribution Linux pour Windows

Nous avons activé le mode développeur et installé le sous-système Windows pour Linux, nous devons maintenant installer une distribution Linux fonctionnant avec Windows 10. Pour lancer Linux, il nous suffira de taper la commande **Bash** dans le champ de recherche en bas à gauche :

![Lancement Bash](images/env_system_7.png)

La fenêtre bash.exe s’ouvre alors :

![Shell Bash Windows](images/env_system_8.png)

Nous n’avons pas encore installé Linux, mais nous avons le shell Unix Bash sous Windows.

Windows nous invite alors à installer Linux en indiquant un lien. Dans votre navigateur, saisissez alors l’URL [https://aka.ms/wslstore](https://aka.ms/wslstore).
Microsoft Store va alors s’ouvrir et vous demander de choisir votre distribution Linux compatible avec Windows.

Les distributions suivantes sont proposées Ubuntu, Debian, Fedora, openSUSE, SUSE Linux Enterprise Server, Kali Linux, etc.

![Distribution Ubuntu de Microsoft Store](images/env_system_9.png)

Nous choisissons Ubuntu pour ce cours (plus modèle en V avec une base Debian).

Après avoir cliqué sur Ubuntu, cliquer sur le bouton **Télécharger**. Après téléchargement et installation, cliquer sur le bouton **Lancer**.

#### Installer Linux

Voilà maintenant votre système Linux prêt à être installé sous Windows.

Lancer à nouveau la commande **bash** dans le champ de recherche. Le premier lancement permettra d’installer définitivement Ubuntu sous Windows :

![Bash installation d'Ubuntu](images/env_system_10.png)

Vous devrez alors saisir un login de votre choix ainsi qu’un mot de passe :

![Fenêtre terminal sous Ubuntu dans Windows](images/env_system_11.png)

Ubuntu est maintenant prêt à être utilisé en ligne de commande, votre disque dur étant déjà monté.

### Environnements Windows sous MAC OS

#### PowerShell

installer **PowerShell** :

Lancer un shell

```console
$ brew cask install powershell
```

Enfin, vérifiez que votre installation fonctionne correctement :

```console
$ pwsh
```

Quand de nouvelles versions de **PowerShell** sont publiées, mettez à jour les formules de **Homebrew** et mettez à niveau **PowerShell** :

```console
$ brew update
$ brew upgrade powershell –cask
```

### Environnements Windows sous Linux

#### PowerShell

PowerShell pour Linux est distribué par Microsoft pour les référentiels de packages afin de faciliter l’installation et les mises à jour à partir de Windows.

```console
utilisateur@MachineUbuntu:~$ sudo snap install --classic powershell
utilisateur@MachineUbuntu:~$ sudo snap remove powershell
```

##### La méthode recommandée est la suivante pour une distribution Debian

**Inscrivez le référentiel de logiciels Microsoft pour Ubuntu**

*Téléchargements des clés de cryptages CPG des dépôts de Microsoft*

```console
utilisateur@MachineUbuntu:~$ wget -q
https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb
```

*Enregistrer ces clés Microsoft dans le répertoire d’installation de logiciels*

```console
utilisateur@MachineUbuntu:~$ sudo dpkg -i packages-microsoft-prod.deb
```

*Mise à jour de la liste des logiciels installables*

```console
utilisateur@MachineUbuntu:~$ sudo apt update
```

**Installer Powershell**

Après l’inscription du dépôt logiciel Microsoft, vous pouvez installer **PowerShell**

*Installation*

```console
utilisateur@MachineUbuntu:~$ sudo apt install -y powershell liblttng-ust0 liburcu6 liblttng-ust-ctl4
```

*Démarrer PowerShell*

```console
utilisateur@MachineUbuntu:~$ pwsh
PowerShell 7.1.3
Copyright (c) Microsoft Corporation.

https://aka.ms/powershell
Type 'help' to get help.

PS /home/utilisateur> exit
```

#### Serveurs RDP (TSE)

Vous pouvez vous connecter en mode graphique sur un serveur Linux distant en TSE avec le protocole RDP en installant XRDP :

```console
utilisateur@MachineUbuntu:~$ sudo apt install gnome-session gnome-terminal
utilisateur@MachineUbuntu:~$ sudo apt -y install xrdp
utilisateur@MachineUbuntu:~$ sudo systemctl status xrdp
utilisateur@MachineUbuntu:~$ sudo adduser xrdp ssl-cert
```

Pensez à désinstaller le serveur graphique de vos serveurs Linux de production pour la sécurité (sous Ubuntu `sudo apt remove xserver-xorg-video-all` ou `sudo apt remove xserver-xorg-driver-all`)

## L’édition de code Python

L’édition de code est une question d’ergonomie personnelle.

Certains préfèrent la méthode manuelle pour tout contrôler de leur poste de travail (système et comprendre ce qu’ils utilisent et font). Pour ne pas s’enfermer dans un environnement de travail fournisseur logiciel et permettre l’interopérabilité. Ils se tourneront alors vers un éditeur de texte évolué avec des plugins plus ou moins automatisés pour garder le contrôle de leur poste de travail (**ingénieurs systèmes**).

D’autres adorent l’automatisation de leur production de développement et ne veulent se concentrer que sur le code. Ils se tourneront alors vers un «**I**ntegrated **D**eveloppement **E**nvironnement» le plus intégré que possible et standard (**développeurs**).

Et encore d’autres aiment s’enferment dans des technologies fournisseurs et se tournent vers des **R**apid **A**pplication **D**éveloppement (**informatique non cœur de métier**) qui ont le défaut de la non optimisation du code et d’être des usines à gaz.

- **Éditeurs de texte avec coloration syntaxique et plugins** (l’IDE c’est le système d’exploitation. Pour les geeks comme moi ;-p)
- **Idle** (IDE minimaliste natif de python)
- **Pyscripter** (IDE gratuit débutants pour Windows)
- **Eric** (IDE purement python)
- **Éclipse** (IDE professionnel industriel avec l’extension PyDev pour le développement Python)
- **Visual studio** (IDE/RAD .Net professionnel Windows avec l’extension PTVS=Python Tools for Visual Studio)
- **Boaconstructor** (RAD Python + wxPython)
- **Visual python** (RAD Python + Tkinter)

Voir pour une liste plus exhaustive des éditeurs [https://wiki.python.org/moin/PythonEditors](https://wiki.python.org/moin/PythonEditors) et pour les IDE voir [https://wiki.python.org/moin/IntegratedDevelopmentEnvironments](https://wiki.python.org/moin/IntegratedDevelopmentEnvironments)
<br/>

### Installer un éditeur de code

Exercice :

Le stagiaire installe l’éditeur de son choix.

Distribuer sous forme papier la procédure pour **Visual studio** voir
[https://docs.microsoft.com/fr-fr/visualstudio/python/installing-python-support-in-visual-studio?view=vs-2019](https://docs.microsoft.com/fr-fr/visualstudio/python/installing-python-support-in-visual-studio?view=vs-2019)

Présentation de Visual studio ?

Distribuer sous forme papier la procédure pour **éclipse avec PyDev** :

- installer [https://www.liclipse.com/download.html](https://www.liclipse.com/download.html)
- Pour éclipse seul voir
  [https://www.eclipse.org/downloads/packages/installer](https://www.eclipse.org/downloads/packages/installer)
- Pour le plugin voir
  [https://koor.fr/Python/Tutorial/python_ide_pydev.wp](https://koor.fr/Python/Tutorial/python_ide_pydev.wp)

Ou pour les manuels et les pros de l’éditeur texte (allergiques aux IDE et qui veulent contrôler ce qu’il y a sous le capot), installation de notepad++ par exemple [https://notepad-plus-plus.org/downloads/](https://notepad-plus-plus.org/downloads/).

## Interpréteurs

Python est un langage de haut niveau, c’est à dire que l’on n’a pas à
tenir compte des contraintes du système d’exploitation, comme la gestion
du matériel ou de la mémoire avec le code par exemple.

Python est un langage interprété, c’est-à-dire que son code pour
s’exécuter n’a pas besoin d’être «compilé» (traduit dans le langage
machine) pour une architecture matérielle. Il s’exécute avec
l’interpréteur Python de l’architecture matérielle.

En tant que langage interprété, lorsque nous installons Python, nous
installons un interpréteur.

En réalité Python est un langage semi-interprété, l’interpréteur Python
va passer par une étape de compilation qui ne produira pas un code
adapté à la machine, mais un code intermédiaire. Souvent appelé byte
code, celui-ci sera le code réel interprété par l’interpréteur Python de
l’environnement matériel du système d’exploitation.

Il existe de nombreux interpréteurs Python écrits dans différents
langages qui fonctionnent sur différentes architectures matérielles et
systèmes d’exploitations.

- **Cpython** : L’interpréteur «classique» écrit en C
- **Pypy** : Un interpréteur écrit en… Python
- **Jython** : Un interpréteur écrit en Java qui permet d’accéder en
  Python aux bibliothèques d’objets Java
- **IronPython** : Un interpréteur écrit en .Net et intégré à Visual
  Studio
- **PythonNet** (.Net) : Un interpréteur distribué avec vos
  développements d’applications .Net
- **Rustpython** : Un interpréteur écrit en Rust, langage système bas
  niveau (comme le C, mais plus moderne et très à la mode actuellement)
- etc.

### Installer Python

Exercice :

Distribuer la procédure sous forme papier (Windows, MAC, Linux) [https://openclassrooms.com/fr/courses/4262331-demarrez-votre-projet-avec-python/4262506-installez-python](https://openclassrooms.com/fr/courses/4262331-demarrez-votre-projet-avec-python/4262506-installez-python)

Voir la doc [https://docs.python.org/fr/3/using/index.html](https://docs.python.org/fr/3/using/index.html)

### Mode Interactif

On peut essentiellement distinguer trois types d’interpréteurs
interactifs Python :

- **python** : l’interpréteur interactif classique et basique intégré à
  Python.
- **IPython** (intégré avec Jupyter Notebook, le mode ordinateur de
  présentations scientifiques ou d’Intelligence Artificielle) : Un
  interpréteur interactif adapté à l’affichage en temps réel de courbes
  et graphiques dessinés avec Matplotlib.
- **BPython** (le mode test de codes ou d’exposés pédagogiques de
  codes) : Un interpréteur interactif amélioré grâce à l’utilisation de
  la coloration syntaxique, la mise à disposition d’un historique des
  commandes, la complétion automatique, l’auto indentation, etc.

Suivant nos besoins d’utilisation de Python en mode interactif nous
pourrons être amenés à évoluer de l’interpréteur python classique vers
un des deux autres types (IPython ou BPython).

Exercice :

```console
utilisateur@MachineUbuntu:~$ python3
Python 3.9.4 (default, Apr 4 2021, 19:38:44)
[GCC 10.2.1 20210401] on linux
Type "help", "copyright", "credits" or "license" for more information.
```

```pycon
>>> help()
Welcome to Python 3.9's help utility!

If this is your first time using Python, you should definitely check out
the tutorial on the Internet at https://docs.python.org/3.8/tutorial/.

Enter the name of any module, keyword, or topic to get help on writing
Python programs and using Python modules. To quit this help utility and
return to the interpreter, just type "quit".

To get a list of available modules, keywords, symbols, or topics, type
"modules", "keywords", "symbols", or "topics". Each module also comes
with a one-line summary of what it does; to list the modules whose name
or summary contain a given string such as "spam", type "modules spam".

help> quit
You are now leaving help and returning to the Python interpreter.
If you want to ask for help on a particular object directly from the
interpreter, you can type "help(object)". Executing "help('string')"
has the same effect as typing a particular string at the help> prompt.
>>> help(quit)
Help on Quit in module _sitebuiltins object:

class Quit(builtins.object)
 | Quit(name, eof)
 |
 | Methods defined here:
 |
 | __call__(self, code=None)
 | Call self as a function.
 |
 | __init__(self, name, eof)
 | Initialize self. See help(type(self)) for accurate signature.
 |
 | __repr__(self)
 | Return repr(self).
 |
 |
 | Data descriptors defined here:
 |
 | __dict__
 | dictionary for instance variables (if defined)
 |
 | __weakref__
 | list of weak references to the object (if defined)
(END)
q
>>> quit()
```

### Les mots clé

[Distribuer Lexique Mots Clé](../InitiationALaProgrammationPythonPourLAdministrateurSystèmesLexiqueMotsClé.odt)

### Les fonctions de base de Python

[Distribuer Lexique Les Fonctions de base](../InitiationALaProgrammationPythonPourLAdministrateurSystèmesLesFonctionsDeBase.odt)

### Mode interprété

Exercice :

Créer le répertoire répertoire_de_développement :

```console
utilisateur@MachineUbuntu:~$ mkdir -p repertoire_de_developpement/1_Mode_interprété; cd repertoire_de_developpement/1_Mode_interprété
```

Créer dans ce répertoire le fichier **mon_1er_programme.py** avec l’éditeur de code choisi, et le modifier comme suit :

```python
#! /usr/bin/env python3
# -*- coding: utf8 -*-

print('Bonjour à toutes et tous !')
```

Le **shebang**, représenté par **#!**, c’est un en-tête d’un fichier texte qui indique au système d’exploitation (de type Unix) que ce fichier n’est pas un fichier binaire mais un script (ensemble de commandes) ; sur la même ligne est précisé l’interpréteur permettant d’exécuter ce script.

Exécuter le programme :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ python3 mon_1er_programme.py
```

Ou sur Unix le rendre exécutable (chmod u+x) et le lancer en ligne de commande comme une simple application :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ chmod u+x mon_1er_programme.py ; ./mon_1er_programme.py
```

#### Conversion Python 2 vers Python 3

```console
$ python2.7
Python 2.7.18 (default, Sep  5 2020, 11:17:26)
[GCC 10.2.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
```

```pycon
>>> type('chaine') # bits => encodée
<type 'str'>
>>> type(u'chaine') # unicode => décodée
<type 'unicode'>
```

```console
$ python3
Python 3.8.5 (default, Sep  5 2020, 10:50:12)
[GCC 10.2.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
```

```pycon
>>> type("chaine") # unicode => decodée
<class 'str'>
>>> type(b"chaine") # bits => encodée
<class 'bytes'>
```

Votre but, c’est de n’avoir dans votre code que des chaînes de type ‘unicode’.

En Python 3, c’est automatique. Toutes les chaînes sont de type ‘unicode’ (appelé ‘str’ dans cette version) par défaut. En Python 2 en revanche, il faut préfixer la chaîne par un u pour avoir de l’unicode.

Python 2 vient de prendre fin le 1<sub>er</sub> janvier 2020.

Donc si vous utilisez un interpréteur Python 2, dans votre code, TOUTES vos chaînes unicode doivent être déclarées ainsi :

```python
u"votre chaîne"
```

Si vous voulez, vous pouvez activer le comportement de Python 3 dans Python 2 en mettant ceci au début de CHACUN de vos modules pour vous aider à migrer vos scripts et programmes :

```python
from __future__ import unicode_literals
```

Ceci n’affecte que le fichier en cours, jamais les autres modules. On peut également le mettre au démarrage d’iPython.

Résumé pour migrer Python 2 :

1. Réglez votre éditeur sur UTF8.
2. Mettez # coding: utf8 au début de vos modules.
3. Préfixez toutes vos chaînes de **u** ou faites `from __future__ import unicode_literals` en début de chaque module.

Si vous ne faites pas cela, votre code marchera uniquement avec Python 2. Et un jour, quand Python 2 ne pourra plus être déployer, il ne marchera plus. Plus du tout.

Donner sous forme papier [http://sametmax.com/lencoding-en-python-une-bonne-fois-pour-toute/](http://sametmax.com/lencoding-en-python-une-bonne-fois-pour-toute/) si besoins de migrations de python2 vers python3

### Mode Compilé

La compilation en python existe, c’est «**Cython**» ou «LPython».

## Révision de code

Lorsque l’on développe un logiciel, ce dernier est voué à évoluer. On ne part malheureusement pas de l’idée pour aboutir immédiatement au programme fini.

Même si les spécifications sont précises, il y aura toujours de petits bugs à corriger et donc des lignes de codes seront modifiées, supprimées ou ajoutées. Mais que se passe-t-il lorsque plusieurs développeurs travaillent sur le même fichier ou programme, ou lorsqu’une correction n’en est pas une et qu’il faut revenir en arrière ?

C’est là qu’interviennent **les logiciels de gestion de versions concurrentes**, vision collective, **ou de révision de code**, vision individuelle.

- **Git** : le standard de fait en mode décentralisé.
- **Visualsource** : celui de Microsoft
- Bazaar
- Mercurial
- dinosaures (**rcs**, **svn**) etc.

### Installer un logiciel de révision de code sur le poste de développement

Exercice :

Distribuer procédure installation de git voir [https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/6113016-installez-git-sur-votre-ordinateur](https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/6113016-installez-git-sur-votre-ordinateur)

Documentation voir [https://git-scm.com/book/fr/v2](https://git-scm.com/book/fr/v2)

#### Installer git

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ cd .. ; sudo apt update; sudo apt upgrade
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install git
```

#### Configurer git

Récupérer le fichier [https://github.com/github/gitignore/blob/master/Python.gitignore](https://github.com/github/gitignore/blob/master/Python.gitignore) et le renommer en **.gitignore** dans le répertoire :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ wget https://raw.githubusercontent.com/github/gitignore/master/Python.gitignore ; mv Python.gitignore .gitignore
```

Ajouter en début de fichier de **.gitignore** :

```bash
# Ignore itself
.gitignore
```

Mettre en place la coloration syntaxique dans git :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global color.ui auto
```

Définir l’utilisateur de git avec son adresse courriel :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.name "Prénom NOM"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.email "utilisateur@domaine-perso.fr"
```

Configurer les paramètres de la sauvegarde des identifiants de connections aux dépôts distants :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global http.sslVerify false
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git  config --global http.postBuffer 524288000
```

Distribuer un lexique sur Git «commandes git».

Initialiser le dépôt git et ajouter le fichier Python «**mon_1er_programme.py**» :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git init
…
Dépôt Git vide initialisé dans /home/utilisateur/repertoire_de_developpement/.git/
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master

Aucun commit

Fichiers non suivis:
 (utilisez "git add <fichier>…" pour inclure dans ce qui sera validé)
 "1_Mode_interpr\303\251t\303\251/"

 aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)
 utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
 utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
 Sur la branche master

 Aucun commit

Modifications qui seront validées :
 (utilisez "git rm --cached <fichier>…" pour désindexer)
 nouveau fichier : "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout du fichier mon_1er_programme.py"
[master (commit racine) dd36b76] Ajout du fichier mon_1er_programme.py
 1 file changed, 4 insertions(+)
 create mode 100755 "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
rien à valider, la copie de travail est propre
```

## Environnement virtuel PYTHON 3

L’ensemble des paquets Python installés par votre distribution, dans votre système Linux, a été bien testé par les intégrateurs de la distribution. Il faut donc autant que possible installer les outils/bibliothèques Python avec les outils d’administration des paquets de votre système pour vos applications informatiques Python du poste de travail .

L’installation de paquets Python par l’intermédiaire d’outils tierces risque de casser cet écosystème système bien testé.

Lorsque l’on fait du développement le besoin d’ajouter des paquets d’outils/bibliothèques Python au delà de votre système d’exploitation est une nécessité. C’est votre projet de programmation Python qui l’impose.

Donc l’utilisation de ces outils/bibliothèques sont propre à vos projets de développements Python. Ils peuvent alors rentrer en conflit de versions avec les applications Python de votre système Linux, Mac, Windows ou autres.

Afin d’isoler ces ajouts d’outils/bibliothèques du système de votre environnement poste de travail, nous allons créer pour vos projets des environnements virtuels de développement Python, avec des outils et des bibliothèques propre à ces environnements.

### venv

C’est l’environnement virtuel standard de Python. Pour l’installer :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install -y python3-venv python3-pip
```

Création de l’environnement virtuel Python :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m venv .env
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ source .env/bin/activate
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ deactivate
```

L’application `pip` servira alors d’outil d’installation des outils et bibliothèque Python pour ces environnements virtuels.

### pipenv

Installation de pipenv :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install pipenv
```

Création de l’environnement virtuel Python :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pipenv shell
Creating a virtualenv for this project…
Using /usr/bin/python3 (3.9.4) to create virtualenv…
⠋created virtual environment CPython3.9.4.final.0-64 in 232ms creator
CPython3Posix(dest=/home/utilisateur/.local/share/virtualenvs/repertoire_de_developpement-hIqPJnF9, clear=False, no_vcs_ignore=False, global=False)
seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/utilisateur/.local/share/virtualenv)
added seed packages: pip==20.3.4, pkg_resources==0.0.0, setuptools==44.1.1, wheel==0.34.2
activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator

Virtualenv location: /home/utilisateur/.local/share/virtualenvs/repertoire_de_developpement-hIqPJnF9
Creating a Pipfile for this project…
Spawning environment shell (/bin/bash). Use 'exit' to leave.
(repertoire_de_developpement-hIqPJnF9) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ exit
```

## Documentation

- **Documenter le code** (annotations des variables, docstring)
- **Génération de la documentation** (Sphinx, doxygen, docutil, pdoc3, pydoctor, etc.)
- **Syntaxes** (restructured text, markdown, asciidoc, mediawiki, html, etc.)

Fournir un lexique sur la syntaxe pour la doc.

Nous reviendrons plus tard dans ce cours sur l’utilisation de la documentation dans Python.

Mise en place du système de documentation du code, architecture et scripts (Sphinx).

Installer les logiciels de la documentation :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ source .env/bin/activate
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pip install sphinx sphinx-intl
```

Créer la documentations :

```console
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir docs; cd docs
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sphinx-quickstart
Bienvenue dans le kit de démarrage rapide de Sphinx 3.2.1.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
"source" and "build" directories within the root path.

> Séparer les répertoires build et source (y/n) [n]: y

The project name will occur in several places in the built documentation.

> Nom du projet: Documentation sur l’initiation à la programmation Python pour l’administrateur systèmes
> Nom(s) de l\'auteur: Prénom NOM
> version du projet []:

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.

> Langue du projet [en]: fr

Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/source/conf.py.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/source/index.rst.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/Makefile.
Fichier en cours de création /home/utilisateur/repertoire_de_developpement/docs/make.bat.

Terminé : la structure initiale a été créée.

You should now populate your master file /home/utilisateur/repertoire_de_developpement/docs/source/index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
    make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ rmdir build ; mv source sources-documents
```

modifier les fichiers «**Makefile**» et «**make.bat**», dans lesquels il faudra adapter le contenu de la variable «**SOURCEDIR**».

**Makefile** :

```makefile
SOURCEDIR = sources-documents
BUILDDIR = documentation
```

**make.bat** :

```doscon
set SOURCEDIR=sources-documents
set BUILDDIR=documentation
```

Pour générer la doc sous Linux, c’est très simple, il suffit d’ouvrir un terminal dans le dossier du projet et de taper la commande suivante :

```console
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html ; cd ..
(.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ deactivate
```

Si vous n’avez pas la commande `make`, il vous faudra l’installer. Ça peut se faire avec la commande suivante si vous utilisez Debian, Ubuntu ou l’un de leurs dérivés :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install build-essential
```

Si vous êtes sous Windows et que vous utilisez Git Bash, il vous faudra utiliser la commande suivante pour générer votre documentation :

```console
$ ./make.bat html
```

Voir la documentation générée «**…/repertoire_de_developpement/docs/documentation/html/index.html**» avec un navigateur web.

### Sauvegarder la structure de documentation

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
Votre branche est à jour avec 'origin/master'.
Modifications qui seront validées :
(utilisez "git restore --staged <fichier>..." pour désindexer)
 nouveau fichier : docs/Makefile
 nouveau fichier : docs/documentation/doctrees/environment.pickle
 nouveau fichier : docs/documentation/doctrees/index.doctree
 nouveau fichier : docs/documentation/html/.buildinfo
 nouveau fichier : docs/documentation/html/_sources/index.rst.txt
 nouveau fichier : docs/documentation/html/_static/_stemmer.js
 nouveau fichier : docs/documentation/html/_static/alabaster.css
 nouveau fichier : docs/documentation/html/_static/basic.css
 nouveau fichier : docs/documentation/html/_static/custom.css
 nouveau fichier : docs/documentation/html/_static/doctools.js
 nouveau fichier : docs/documentation/html/_static/documentation_options.js
 nouveau fichier : docs/documentation/html/_static/file.png
 nouveau fichier : docs/documentation/html/_static/jquery-3.5.1.js
 nouveau fichier : docs/documentation/html/_static/jquery.js
 nouveau fichier : docs/documentation/html/_static/language_data.js
 nouveau fichier : docs/documentation/html/_static/minus.png
 nouveau fichier : docs/documentation/html/_static/plus.png
 nouveau fichier : docs/documentation/html/_static/pygments.css
 nouveau fichier : docs/documentation/html/_static/searchtools.js
 nouveau fichier : docs/documentation/html/_static/translations.js
 nouveau fichier : docs/documentation/html/_static/underscore-1.3.1.js
 nouveau fichier : docs/documentation/html/_static/underscore.js
 nouveau fichier : docs/documentation/html/genindex.html
 nouveau fichier : docs/documentation/html/index.html
 nouveau fichier : docs/documentation/html/objects.inv
 nouveau fichier : docs/documentation/html/search.html
 nouveau fichier : docs/documentation/html/searchindex.js
 nouveau fichier : docs/make.bat
 nouveau fichier : docs/sources-documents/conf.py
 nouveau fichier : docs/sources-documents/index.rst
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Structure de la documentation du projet"
[master 31c720b] Structure de la documentation du projet
31 files changed, 17692 insertions(+)
create mode 100644 docs/Makefile
create mode 100644 docs/documentation/doctrees/environment.pickle
create mode 100644 docs/documentation/doctrees/index.doctree
create mode 100644 docs/documentation/html/.buildinfo
create mode 100644 docs/documentation/html/_sources/index.rst.txt
create mode 100644 docs/documentation/html/_static/alabaster.css
create mode 100644 docs/documentation/html/_static/base-stemmer.js
create mode 100644 docs/documentation/html/_static/basic.css
create mode 100644 docs/documentation/html/_static/custom.css
create mode 100644 docs/documentation/html/_static/doctools.js
create mode 100644 docs/documentation/html/_static/documentation_options.js
create mode 100644 docs/documentation/html/_static/file.png
create mode 100644 docs/documentation/html/_static/french-stemmer.js
create mode 100644 docs/documentation/html/_static/jquery-3.5.1.js
create mode 100644 docs/documentation/html/_static/jquery.js
create mode 100644 docs/documentation/html/_static/language_data.js
create mode 100644 docs/documentation/html/_static/minus.png
create mode 100644 docs/documentation/html/_static/plus.png
create mode 100644 docs/documentation/html/_static/pygments.css
create mode 100644 docs/documentation/html/_static/searchtools.js
create mode 100644 docs/documentation/html/_static/translations.js
create mode 100644 docs/documentation/html/_static/underscore-1.12.0.js
create mode 100644 docs/documentation/html/_static/underscore.js
create mode 100644 docs/documentation/html/genindex.html
create mode 100644 docs/documentation/html/index.html
create mode 100644 docs/documentation/html/objects.inv
create mode 100644 docs/documentation/html/search.html
create mode 100644 docs/documentation/html/searchindex.js
create mode 100644 docs/make.bat
create mode 100644 docs/sources-documents/conf.py
create mode 100644 docs/sources-documents/index.rst
```

## Débogages

Si vous avez un bogue non banal, c’est là que les stratégies de débogage vont rentrer en ligne de compte. Le problème doit être isolé dans un petit nombre de lignes de code, hors frameworks ou code applicatif.

Pour déboguer un problème donné :

1. Faites échouer le code de façon fiable : trouvez un cas de test qui fait échouer le code à chaque fois.
2. Diviser et conquérir : une fois que vous avez un cas de test échouant, isolez le code coupable.
   > 1. Quel module.
   > 2. Quelle fonction.
   > 3. Quelle ligne de code.
   > 4. Isolez une petite erreur reproductible (permet de définir un cas de test à implémenter).
3. Changez une seule chose à chaque fois, l’archiver dans la révision de code, et ré-exécutez le cas de test d’échec.
4. Utilisez le débogueur (pour Python pdb) pour comprendre ce qui ne va pas.
5. Prenez des notes et soyez patient, ça peut prendre un moment.

Une fois que vous avez procédé à cette étape, isolez un petit bout de code reproduisant le bogue et corrigez celui-ci en utilisant ce bout de code, ajoutez le code de test dans votre suite de test (Unittest).

### Le débogueur Python pdb

#### Installation de pdb

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$sudo apt install python3-ipdb
```

#### Déboguer avec pdb

Les façons de lancer le débogueur :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 2_Debug ; cd 2_Debug
```

Créer le fichier «**error.py**» dans le dossier «**repertoire_de_developpement/2_Debug**»

```python
#! /usr/bin/env python3
# -*- coding: utf8 -*-

dividende = 5
nombres = [5, 4, 3, 2, 1, 0]
for diviseur in nombres:
  print('Valeur du rapport : %s' % (dividende/diviseur))
```

##### Postmortem

`pdb` est invoqué (exécuté) pour déboguer un script.

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ python3 -m pdb error.py
>/home/utilisateur/repertoire_de_developpement/2_Debug/error.py(4)<module>()
-> dividende = 5
(pdb) q
```

Pour arrêter le débogage (prompt `pdb`) tapez `q`.

##### Lancez le module avec le débogueur

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3 error.py
```

ou

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3
In [1]:%run error.py
```

pour sortir du débogueur (prompt `In [num]:%`) tapez `quit`.

##### Exécution pas à pas du débogueur

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3 -c '%run -d error.py'
```

ou

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ ipython3
In [1]: %run -d error.py
```

Continuez dans le code avec `n`(ext), next saute à la prochaine déclaration de code dans le contexte d’exécution courant  :

```console
ipdb> n
```

Placez un point d’arrêt à la ligne 7 en utilisant `b 7` :

```console
ipdb> b 7
```

Continuez l’exécution jusqu’au prochain point d’arrêt avec `c`(ontinue) :

```console
ipdb> c
```

Continuez dans le code avec `s`(tep), step va traverser les contextes d’exécution, c’est-à-dire permettre l’exploration à l’intérieur des appels de fonction :

```console
ipdb> s
```

Visualiser l’état d’une variable avec `print()` :

```console
ipdb> print(diviseur)
```

Arrêter le débogage :

```console
ipdb> q
```

Quitter le débogueur :

```console
In [3]: quit
```

##### Appeler le débogueur à l’intérieur du module

```python
import pdb; pdb.set_trace()
```

##### Les commandes du débogueur

| l (list)   | Liste le code à la position courante                                             |
|------------|----------------------------------------------------------------------------------|
| u(p)       | Monte à la pile d’appel                                                          |
| d(own)     | Descend à la pile d’appel                                                        |
| n(ext)     | Exécute la prochaine ligne (ne va pas à l’intérieur<br/>d’une nouvelle fonction) |
| s(tep)     | Exécute la prochaine déclaration (va à l’intérieur d’une<br/>nouvelle fonction)  |
| bt         | Affiche la pile d’appel                                                          |
| a          | Affiche les variables locales                                                    |
| !command   | Exécute la commande **Python** donnée (par opposition à<br/>une commande pdb)    |
```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/2_Debug$ cd ..
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout des exemples de débogages"
```

### La Gestion des Warnings d’exécution

Attention cet exemple fonctionne jusqu’à Python 3.9. Pour les versions postérieures les modules obsolètes hérités de Python2 ne sont plus pris en charge.

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 3_Interpreteur_alerts ; cd 3_Interpreteur_alerts
```

Créer le fichier «**monscript.py**» dans le dossier «**repertoire_de_developpement/3_Interpreteur_alerts**»

```python
#! /usr/bin/env python3
# -*- coding: utf8 -*-

import formatter

print('Bonjour %s' % 'Moi')
```

Exécution de python avec les Warnings :

```shell
python3 -Wd monscript.py
```

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/3_Interpreteur_alerts$ python3 -Wd monscript.py
monscript.py:4: DeprecationWarning: the formatter module is deprecated
  import formatter
Bonjour Moi
```

Pour l’activer par défaut pour toutes les alertes :

```bash
python3 -Wa
```

À chaque mise à jour de version de python, pour son code il est important de vérifier les warnings.

Ceux-ci nous informe de l’obsolescence des bibliothèques ou des fonctions de python que nous utilisons. Cela permet de préparer et corriger le code python de nos applications développées pour les migrations futures de vos systèmes informatiques et de leurs bibliothèques/frameworks.

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement/3_Interpreteur_alerts$ cd ..
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout des exemples de warnings d’exécution"
```

## Tests Unitaires

- **Tests unitaires en python** (Unittest et doctest)
- **Frameworks de tests** (Unittest, Robot, Pytest, Doctest, Nose2, Testify)

Les tests unitaires permettent de vérifier (tester) des éléments particuliers d’un programme.

Par exemple si un programme contient plusieurs parties de code autonome, les tests unitaires permettront de vérifier leurs présences, le fonctionnement de chacune des parties suivant un comportement attendu.

La mise en place de tests unitaires permet de s’assurer que la correction de bugs, ou le développement de nouvelles fonctions, n’entraînera pas de régressions au niveau du code.

Nous verrons ultérieurement au cours de cette formation le module Python Unittest

* Architecture des tests
* Les valeurs de retour des tests
* Les différents tests de Unittest
* Exécution de l’ensemble des tests

Mise en place de l’infrastructure, créer le répertoire tests :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir tests ; touch tests/README.md
```

Contenu du fichier «**README.md**»

```python
# Tests unitaires du code Python
```

### Sauvegarder la structure de tests

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
Votre branche est à jour avec 'origin/master'.
Modifications qui seront validées :
(utilisez "git restore --staged <fichier>..." pour désindexer)
 nouveau fichier : tests/README.md
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout de la structure de tests"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd ..
```

## L’industrialisation du code DEVOPS

- Gitlabs
- Github/Azure
- BitbucketInstaller l’infrastructure DEV/OPS

Nous allons installer GitLab avec Docker. De plus, nous utiliserons Ubuntu 21.04 comme système d’exploitation principal.

Prérequis:

- Serveur Ubuntu 21.04
- Min 4 Go de RAM
- Privilèges root

Qu’allons nous faire?

1. Configurer le DNS local
2. Installer Docker
3. Tester Docker
4. Installer Gitlab
5. Configurer et tester Gitlab
6. Autorisations pour Docker et le Runner
7. Configurer et tester le Runner
8. Tester les Pages de Gitlab

### Configurer le DNS local

Vous avez besoin d’un nom de domaine avec un enregistrement A valide pointant vers votre serveur GitLab.

#### Installer une interface réseau virtuelle

Éditer «**/etc/systemd/network/10-virtualeth0.netdev**»

```basic
[NetDev]
Name = virtualeth0
Kind = dummy
```

Éditer «**/etc/systemd/network/10-virtualeth0.network**»

```basic
[Match]
Name = virtualeth0

[Network]
Address = 10.10.10.1/24
Address = fd00::/8
```

```console
utilisateur@MachineUbuntu:~$ sudo systemctl start systemd-networkd
utilisateur@MachineUbuntu:~$ sudo systemctl enable systemd-networkd
utilisateur@MachineUbuntu:~$ ip a
…
3: virtualeth0: <BROADCAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
  link/ether 9a:3c:56:42:f5:c9 brd ff:ff:ff:ff:ff:ff
  inet 10.10.10.1/24 brd 10.10.10.255 scope global virtualeth0
    valid_lft forever preferred_lft forever
  inet6 fd00::/8 scope global
    valid_lft forever preferred_lft forever
  inet6 fe80::983c:56ff:fe42:f5c9/64 scope link
    valid_lft forever preferred_lft forever
…
```

#### Configuration du client dhcp adaptée au DNS local

Pour pouvoir ajouter le serveur DNS local à «**/etc/resolv.conf**» il faut renseigner l’option «**prepend**» qui permet l’ajout du serveur DNS local **en début de** la liste des **serveurs DNS** fournit automatiquement par DHCP.

Éditer «**/etc/dhcp/dhclient.conf**»

```properties
prepend domaine-perso.fr 10.10.10.1 fd00::
```

Vérifier les DNS présents :

```console
utilisateur@MachineUbuntu:~$ nmcli dev show | grep DNS
IP4.DNS[1]: yyy.yyy.yyy.yyy
IP4.DNS[1]: yyy.yyy.yyy.yyy
IP6.DNS[1]: yyyy:yyyy:yyyy::yyyy
IP6.DNS[2]: yyyy:yyyy:yyyy::yyyy
IP6.DNS[3]: yyyy:yyyy:yyyy::yyyy
utilisateur@MachineUbuntu:~$ resolvectl dns
Global:
Link 2 (enp0sxx):
Link 3 (wlx803xxxxx): yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 4 (wlo1): yyy.yyy.yyy.yyy
Link 6 (virtualeth0):
```

#### Définir le domaine local de la machine Ubuntu

```console
utilisateur@MachineUbuntu:~$ sudo hostnamectl set-hostname MachineUbuntu.domaine-perso.fr --static
utilisateur@MachineUbuntu:~$ hostname -d
domaine-perso.fr
```

#### Installer les applications de base

```console
utilisateur@MachineUbuntu:~$ sudo apt install bind9 bind9utils bind9-dnsutils bind9-doc bind9-host net-tools
utilisateur@MachineUbuntu:~$ sudo systemctl status named
utilisateur@MachineUbuntu:~$ sudo systemctl enable named
```

#### Configuration du DNS local

Éditer «**/etc/bind/named.conf.options**»

```c
options {
  directory "/var/cache/bind";

  // Pour des raisons de sécurité.
  // Cache la version du serveur DNS pour les clients.
  version "Pas pour les crackers";

  listen-on { 127.0.0.1; 10.10.10.1; };
  listen-on-v6 { ::1; fd00::; };

  allow-query { 127.0.0.1; 10.10.10.1; ::1; fd00::; };

  // Optionnel - Comportement par défaut de BIND en récursions.
  recursion yes;

  // Récursions autorisées seulement pour les interfaces clients
  allow-recursion { 127.0.0.1; 10.10.10.0/24; ::1; fd00::/8; };

  dnssec-validation auto;

  // Activer la journalisation des requêtes DNS
  querylog yes;

};
```

Vérifier la validité de la configuration, et redémarrer le serveur DNS si la configuration est OK.

```console
utilisateur@MachineUbuntu:~$ sudo named-checkconf
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
```

Ajout du server DNS local à la liste des serveurs DNS de systemd-resolved.

Éditer «**/etc/systemd/resolved.conf**»

```properties
DNS=10.10.10.1 fd00::
```

```console
utilisateur@MachineUbuntu:~$ sudo systemctl restart systemd-resolved
utilisateur@MachineUbuntu:~$ nmcli general reload
```

#### Tests du serveur DNS

##### Vérifications du serveur

```console
utilisateur@MachineUbuntu:~$ sudo rndc status
version: BIND 9.16.8-Ubuntu (Stable Release) <id:539f9f0> (Pas pour les crackers)
running on MachineUbuntu.domaine-perso.fr: Linux x86_64 5.11.0-31-generic #33-Ubuntu SMP Wed Aug 11 13:19:04 UTC 2021
boot time: Thu, 26 Aug 2021 06:13:19 GMT
last configured: Thu, 26 Aug 2021 06:13:19 GMT
configuration file: /etc/bind/named.conf
CPUs found: 4
worker threads: 4
UDP listeners per interface: 4
number of zones: 102 (97 automatic)
debug level: 0
xfers running: 0
xfers deferred: 0
soa queries in progress: 0
query logging is ON
recursive clients: 0/900/1000
tcp clients: 0/150
TCP high-water: 0
server is up and running
```

Vérifier le fonctionnement de bind sur le port 53

```console
utilisateur@MachineUbuntu:~$ sudo lsof -i:53
COMMAND PID USER FD TYPE DEVICE SIZE/OFF NODE NAME
named 5624 bind 37u IPv4 54315 0t0 UDP localhost:domain
named 5624 bind 38u IPv4 54316 0t0 UDP localhost:domain
named 5624 bind 39u IPv4 54317 0t0 UDP localhost:domain
named 5624 bind 40u IPv4 54318 0t0 UDP localhost:domain
named 5624 bind 42u IPv4 51987 0t0 TCP localhost:domain (LISTEN)
named 5624 bind 43u IPv4 54319 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 44u IPv4 54320 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 45u IPv4 54321 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 46u IPv4 54322 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 47u IPv4 51988 0t0 TCP MachineUbuntu.domaine-perso.fr:domain (LISTEN)
named 5624 bind 48u IPv6 54323 0t0 UDP ip6-localhost:domain
named 5624 bind 49u IPv6 54324 0t0 UDP ip6-localhost:domain
named 5624 bind 50u IPv6 54325 0t0 UDP ip6-localhost:domain
named 5624 bind 51u IPv6 54326 0t0 UDP ip6-localhost:domain
named 5624 bind 52u IPv6 51989 0t0 TCP ip6-localhost:domain (LISTEN)
named 5624 bind 53u IPv6 54327 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 54u IPv6 54328 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 55u IPv6 54329 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 56u IPv6 54330 0t0 UDP MachineUbuntu.domaine-perso.fr:domain
named 5624 bind 58u IPv6 54331 0t0 TCP MachineUbuntu.domaine-perso.fr:domain (LISTEN)
systemd-r 5799 systemd-resolve 12u IPv4 52844 0t0 UDP localhost:domain
systemd-r 5799 systemd-resolve 13u IPv4 52845 0t0 TCP localhost:domain (LISTEN)
```

Vérifier l’écoute réseau sur le port 53

```console
utilisateur@MachineUbuntu:~$ sudo netstat -alnp | grep -i :53
tcp  0 0 127.0.0.53:53 0.0.0.0:* LISTEN 5799/systemd-resol
tcp  0 0 127.0.0.1:53  0.0.0.0:* LISTEN 5624/named
tcp  0 0 10.10.10.1:53 0.0.0.0:* LISTEN 5624/named
tcp6 0 0 fd00:::53     :::*      LISTEN 5624/named
tcp6 0 0 ::1:53        :::*      LISTEN 5624/named
udp  0 0 127.0.0.53:53 0.0.0.0:*        5799/systemd-resol
udp  0 0 127.0.0.1:53  0.0.0.0:*        5624/named
udp  0 0 10.10.10.1:53 0.0.0.0:*        5624/named
udp  0 0 0.0.0.0:5353  0.0.0.0:*        771/avahi-daemon: r
udp6 0 0 fd00:::53     :::*             5624/named
udp6 0 0 ::1:53        :::*             5624/named
udp6 0 0 :::5353       :::*             771/avahi-daemon: r
```

Vérifier que le système Ubuntu écoute le serveur DNS

```console
utilisateur@MachineUbuntu:~$ resolvectl dns
Global: 10.10.10.1 fd00::
Link 2 (enp0sxx): yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 3 (virtualeth0):
Link 4 (wlx803xxxxx): yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyyy:yyyy:yyyy::yyyy yyy.yyy.yyy.yyy
Link 5 (wlox): yyy.yyy.yyy.yyy
utilisateur@MachineUbuntu:~$ dig MachineUbuntu +noall +answer
MachineUbuntu.                  0 IN A 127.0.1.1
utilisateur@MachineUbuntu:~$ dig MachineUbuntu.domaine-perso.fr +noall +answer
MachineUbuntu.domaine-perso.fr. 0 IN A 10.10.10.1
MachineUbuntu.domaine-perso.fr. 0 IN A aaa.aaa.aaa.aaa
MachineUbuntu.domaine-perso.fr. 0 IN A bbb.bbb.bbb.bbb
…
utilisateur@MachineUbuntu:~$ dig bidon +noall +answer
utilisateur@MachineUbuntu:~$ dig bidon.domaine-perso.fr +noall +answer
```

Si UFW est activé, ouvrir le port DNS sur UFW.

```console
utilisateur@MachineUbuntu:~$ sudo ufw allow from 192.168.0.0/16 to any port 53 proto udp
```

Éditer «**/etc/bind/named.conf.local**» pour définir **la zone DNS**

```c
zone "domaine-perso.fr" {
  type master;
  file "/etc/bind/db.domaine-perso.fr";
};
zone "10.10.10.in-addr.arpa" {
  type master;
  file "/etc/bind/db.10.10.10";
};
zone "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa." {
  type master;
  file "/etc/bind/db.fd00";
};
```

```console
utilisateur@MachineUbuntu:~$ sudo named-checkconf
```

Éditer «**/etc/bind/db.domaine-perso.fr**» pour définir **les alias DNS**

```text
$TTL 15m
@             IN SOA     @ root (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

              IN NS      @
              IN A       10.10.10.10
              IN AAAA    fd00::a
              IN MX      2 courriel
; domaine vers adresse IP
gitlab        IN A       10.10.10.1
gitlab        IN AAAA    fd00::
courriel      IN A       10.10.10.2
courriel      IN AAAA    fd00::2
documentation IN A       10.10.10.3
documentation IN AAAA    fd00::3
*             IN A       10.10.10.10
*             IN AAAA    fd00::a
```

Éditer «**/etc/bind/db.10.10.10**» pour définir **les alias inverse DNS**

```text
$TTL 15m
@             IN SOA     gitlab.domaine-perso.fr. root.domaine-perso.fr. (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

               IN NS     gitlab.domaine-perso.fr.

; IP vers nom de domaine DNS
1             IN PTR     gitlab.domaine-perso.fr.
2             IN PTR     courriel.domaine-perso.fr.
3             IN PTR     documentation.domaine-perso.fr.
10            IN PTR     domaine-perso.fr.
```

Éditer «**/etc/bind/db.fd00**» pour définir **les alias inverse DNS**

```text
$TTL 15m
@             IN SOA     gitlab.domaine-perso.fr. root.domaine-perso.fr. (
          2021082512     ; n° série
                  1h     ; intervalle de rafraîchissement esclave
                 15m     ; intervalle de réessaie pour l’esclave
                  1w     ; temps d’expiration de la copie esclave
                  1h )   ; temps de cache NXDOMAIN

               IN NS     gitlab.domaine-perso.fr.

; IPv6 vers nom de domaine DNS
0             IN PTR     gitlab.domaine-perso.fr.
2             IN PTR     courriel.domaine-perso.fr.
3             IN PTR     documentation.domaine-perso.fr.
a             IN PTR     domaine-perso.fr.
```

```console
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
```

##### Vérifier la résolution DNS

```console
utilisateur@MachineUbuntu:~$ dig ANY domaine-perso.fr +noall +answer
domaine-perso.fr. 6444 IN SOA domaine-perso.fr. root.domaine-perso.fr. 2021082512 3600 900 604800 3600
domaine-perso.fr. 6444 IN NS domaine-perso.fr.
domaine-perso.fr. 6444 IN A 10.10.10.10
domaine-perso.fr. 6444 IN AAAA fd00::a
domaine-perso.fr. 6444 IN MX 2 courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig ANY gitlab.domaine-perso.fr +noall +answer
gitlab.domaine-perso.fr. 6444 IN A 10.10.10.1
gitlab.domaine-perso.fr. 6444 IN AAAA fd00::
utilisateur@MachineUbuntu:~$ dig ANY courriel.domaine-perso.fr +noall +answer
courriel.domaine-perso.fr. 6444 IN A 10.10.10.2
courriel.domaine-perso.fr. 6444 IN AAAA fd00::2
utilisateur@MachineUbuntu:~$ dig ANY documentation.domaine-perso.fr +noall +answer
documentation.domaine-perso.fr. 6444 IN A 10.10.10.3
documentation.domaine-perso.fr. 6444 IN AAAA fd00::3
utilisateur@MachineUbuntu:~$ dig ANY bidon.domaine-perso.fr +noall +answer
bidon.domaine-perso.fr. 6444 IN A 10.10.10.10
bidon.domaine-perso.fr. 6444 IN AAAA fd00::a
```

##### Vérifier la résolution externe

```console
utilisateur@MachineUbuntu:~$ dig google.com +noall +answer
google.com. 16 IN A 216.58.223.110
google.com. 32 IN AAAA 2a00:…::200e
…
```

##### Vérifier la résolution inverse

Vous pouvez utiliser la commande `host` ou `dig -x`

```console
utilisateur@MachineUbuntu:~$ host 10.10.10.1
1.10.10.10.in-addr-arpa domain name pointer gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.1 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00:: +noall +answer
a.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR gitlab.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.2 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::2 +noall +answer
2.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.3 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR documentation.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::3 +noall +answer
3.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR documentation.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.10 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::a +noall +answer
a.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR domaine-perso.fr.
```

#### Paramétrer définitivement votre DNS pour gitlab

Éditer «**/etc/bind/db.domaine-perso.fr**» pour définir **les alias DNS** définitifs

```text
…
               IN NS     @
               IN A      10.10.10.1
               IN AAAA   fd00::
               IN MX     1 courriel
; domaine vers adresse IP
gitlab         IN A      10.10.10.1
gitlab         IN AAAA   fd00::
courriel       IN A      10.10.10.1
courriel       IN AAAA   fd00::1
*              IN A      10.10.10.1
*              IN AAAA   fd00::
```

Éditer «**/etc/bind/db.10.10.10**» pour définir **les alias inverse DNS**

```text
$TTL 15m
@              IN SOA    gitlab.domaine-perso.fr. root.domaine-perso.fr. (
           2021082512    ; n° série
                   1h    ; intervalle de rafraîchissement esclave
                  15m    ; intervalle de réessaie pour l’esclave
                   1w    ; temps d’expiration de la copie esclave
                   1h )  ; temps de cache NXDOMAIN

                IN NS    gitlab.domaine-perso.fr.

; IP vers nom de domaine DNS
1               IN PTR   gitlab.domaine-perso.fr.
1               IN PTR   courriel.domaine-perso.fr.
1               IN PTR   domaine-perso.fr.
```

Éditer «**/etc/bind/db.fd00**» pour définir **les alias inverse DNS**

```text
$TTL 15m
@              IN SOA    gitlab.domaine-perso.fr. root.domaine-perso.fr. (
           2021082512    ; n° série
                   1h    ; intervalle de rafraîchissement esclave
                  15m    ; intervalle de réessaie pour l’esclave
                   1w    ; temps d’expiration de la copie esclave
                   1h )  ; temps de cache NXDOMAIN

                IN NS   gitlab.domaine-perso.fr.

; IPv6 vers nom de domaine DNS
0               IN PTR   gitlab.domaine-perso.fr.
0               IN PTR   domaine-perso.fr.
1               IN PTR   courriel.domaine-perso.fr.
```

```console
utilisateur@MachineUbuntu:~$ sudo systemctl restart named
utilisateur@MachineUbuntu:~$ dig -x 10.10.10.1 +noall +answer
1.10.10.10.in-addr.arpa. 900 IN PTR gitlab.domaine-perso.fr.
1.10.10.10.in-addr.arpa. 900 IN PTR domaine-perso.fr.
1.10.10.10.in-addr.arpa. 900 IN PTR courriel.domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00:: +noall +answer
0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR gitlab.domaine-perso.fr.
0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR domaine-perso.fr.
utilisateur@MachineUbuntu:~$ dig -x fd00::1 +noall +answer
1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.d.f.ip6.arpa. 900 IN PTR courriel.domaine-perso.fr.
```

### Installer Docker

Installer les applications de base :

```console
utilisateur@MachineUbuntu:~$ sudo apt install docker.io curl openssh-server ca-certificates postfix mailutils
```

![Fenêtre d'information Postfix à valider](images/postfix_1.png)![Type de serveur de messagerie Local](images/postfix_2.png)![Nom de courrier courriel.domaine-perso.fr](images/postfix_3.png)

Autorisez le compte utilisateur à utiliser docker :

```console
utilisateur@MachineUbuntu:~$ sudo usermod -aG docker $USER
```

Démarrez le service docker et ajoutez-le au démarrage du système :

```console
utilisateur@MachineUbuntu:~$ sudo systemctl start docker
```

Vérifiez le bon fonctionnement du service docker à l’aide de la commande `systemctl` ci-dessous.

```console
utilisateur@MachineUbuntu:~$ systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Fri 2020-10-09 11:07:10 CEST; 47s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 6241 (dockerd)
      Tasks: 12
     Memory: 38.6M
     CGroup: /system.slice/docker.service
             └─6241 /usr/bin/dockerd -H fd://
--containerd=/run/containerd/containerd.sock
q
```

Activez le service au démarrage.

```console
utilisateur@MachineUbuntu:~$ sudo systemctl enable docker
utilisateur@MachineUbuntu:~$ ip a
…
4: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
  link/ether 02:42:a3:0c:9c:fb brd ff:ff:ff:ff:ff:ff
  inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
    valid_lft forever preferred_lft forever
  inet6 fa80::42:a3ff:fe0c:9cfb/64 scope link
    valid_lft forever preferred_lft forever
…
```

Éditer «**/etc/bind/named.conf.option**» pour ajouter l’interface de docker

```c
options {
    directory "/var/cache/bind";

    // Pour des raisons de sécurité.
    // Cache la version du serveur DNS pour les clients.
    version "Pas pour les crackers";*

    listen-on { 127.0.0.1; 10.10.10.1; 172.17.0.1; };
    listen-on-v6 { ::1; fd00::; fe80::42:a3ff:fe0c:9cfb; };

    // Optionnel - Comportement par défaut de BIND en récursions.
    recursion yes;

    allow-query { 127.0.0.1; 10.10.10.1; ::1; fd00::; 172.17.0.0/16; fe80::42:a3ff:fe0c:9cfb; };

    // Récursions autorisées seulement pour les interfaces clients
    allow-recursion { 127.0.0.1; 10.10.10.0/24; ::1; fd00::/8; 172.17.0.0/16; fe80::42:a3ff:fe0c:9cfb; };

    dnssec-validation auto;

    // Activer la journalisation des requêtes DNS
    querylog yes;
};
```

```console
utilisateur@MachineUbuntu:~$ sudo named-checkconf
```

Redémarrer votre Ubuntu pour valider les modifications

```console
utilisateur@MachineUbuntu:~$ reboot
```

### Tester Docker

Après vous être reconnecter sous Ubuntu, vérifiez dans un terminal que docker fonctionne bien en exécutant la commande docker `docker run hello-world` ci-dessous.

```console
utilisateur@MachineUbuntu:~$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete
Digest: sha256:ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
  1. The Docker client contacted the Docker daemon.
  2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
  3. The Docker daemon created a new container from that image which runs the executable that produces the output you are currently reading.
  4. The Docker daemon streamed that output to the Docker client, which sent it to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
  $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID: https://hub.docker.com/

For more examples and ideas, visit: https://docs.docker.com/get-started/

utilisateur@MachineUbuntu:~$ docker ps -a
CONTAINER ID   IMAGE         COMMAND    CREATED          STATUS                      PORTS   NAMES
dcd0d025b44b   hello-world   "/hello"   19 seconds ago   Exited (0) 16 seconds ago           elegant_torvalds
```

Nous sommes maintenant prêts à installer GitLab.

### Installer GitLab

GitLab est un gestionnaire de référentiels open source basé sur Rails (langage Rubis) développé par la société GitLab. Il s’agit d’un gestionnaire de révisions de code WEB basé sur git qui permet à votre équipe de collaborer sur le codage, le test et le déploiement d’applications. GitLab fournit plusieurs fonctionnalités, notamment les wikis, le suivi des problèmes, les révisions de code et les flux d’activité.

#### Téléchargez le paquet d’installation GitLab pour Ubuntu et l’installer

Installation longue (prévoir une image VM ou USB ?)

[https://packages.gitlab.com/gitlab/gitlab-ce](https://packages.gitlab.com/gitlab/gitlab-ce) et choisissez la dernière version gitlab-ce pour ubuntu xenial

```console
utilisateur@MachineUbuntu:~/gitlab$ wget https://packages.gitlab.com/gitlab/gitlab-ce/packages/ubuntu/focal/gitlab-ce_14.1.3-ce.0_amd64.deb/download.deb
utilisateur@MachineUbuntu:~/gitlab$ sudo apt update ; sudo EXTERNAL_URL="http://gitlab.domaine-perso.fr" dpkg -i download.deb
```

#### Paramétrer GitLab

```console
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-ctl show-config
utilisateur@MachineUbuntu:~/gitlab$ sudo chmod o+r /etc/gitlab/gitlab.rb
utilisateur@MachineUbuntu:~/gitlab$ sudo nano /etc/gitlab/gitlab.rb
```

```ruby
external_url "http://gitlab.domaine-perso.fr"
# Pour activer les fonctions artifacts (tester la qualité du code, déployer sur un serveur distant en SSH, etc.)
gitlab_rails['artifacts_enabled'] = true
# pour générer la doc et l’afficher avec Gitlab
pages_external_url "http://documentation.domaine-perso.fr"
```

```console
utilisateur@MachineUbuntu:~/gitlab$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/gitlab/trusted-certs/MachineUbuntu.key -out /etc/gitlab/trusted-certs/MachineUbuntu.crt
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-ctl reconfigure
```

### Configurer et tester GitLab

Saisissez dans un navigateur l’URL **gitlab.domaine-perso.fr**

![Initialisation du mot de passe de l'administrateur root de GitLab](images/gitlab_1.png)

Si vous n’avez pas la fenêtre d’initialisation du mot de passe :

```console
utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-rake "gitlab:password:reset"
```

![Connection en root dans GitLab](images/gitlab_2.png)![Accueil projets GitLab](images/gitlab_3.png)![Préférences de GitLab aller vers «Localization»](images/gitlab_4.png)![Préférences language à «French» et First day of the week à «Monday»](images/gitlab_5.png)

Tapez la touche **F5** pour rafraîchir l’affichage de votre navigateur.

![Icône Paramètres de l'utilisateur](images/gitlab_6.png)![Profil de l'utilisateur](images/gitlab_7.png)![Compte de l'utilisateur](images/gitlab_8.png)![Icône  espace d'administration](images/gitlab_9.png)![Espace d'administration Paramètres et menu Général](images/gitlab_10.png)![Contrôle de visibilité d'accès](images/gitlab_11.png)

Intégrer le dépot git local dans Gitlab :

```console
utilisateur@MachineUbuntu:~/$ cd repertoire_de_developpement
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config credential.helper store
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git remote add origin http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push -u origin --all
Username for 'http://gitlab.domaine-perso.fr': utilisateur
Password for 'http://gitlab.domaine-perso.fr': motdepasse
Énumération des objets: 51, fait.
Décompte des objets: 100% (43/43), fait.
Compression par delta en utilisant jusqu’à 4 fils d’exécution
Compression des objets: 100% (43/43), fait.
Écriture des objets: 100% (51/51), 180.78 Kio \| 4.89 Mio/s, fait.
Total 51 (delta 3), réutilisés 0 (delta 0), réutilisés du pack 0 To http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
* [new branch] master → master
La branche 'master' est paramétrée pour suivre la branche distante 'master' depuis 'origin'.
```

![Projet local dans les projets de GitLab](images/gitlab_12.png)![Visualisation des détails du projet](images/gitlab_13.png)![Paramètres généraux du projet](images/gitlab_14.png)![Intégration des modifications des paramètres généraux dans la visualisation des détails du projet](images/gitlab_15.png)![Ajout des fichiers README.md, LICENCE, CHANGELOG et CONTRIBUTING](images/gitlab_16.png)

Vous pouvez maintenant récupérer les nouveaux fichiers d’information Gitlab (**CHANGELOG**, **CONTRIBUTING.md**, **LICENSE** et **README.md**) dans votre projet local :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git fetch
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git merge
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ssh-keygen -t rsa -b 2048 -C "Ma clé de chiffrement"
Generating public/private rsa key pair.
Enter file in which to save the key(/home/utilisateur/.ssh/id_rsa):
Created directory '/home/utilisateur/.ssh'.
Enter passphrase (empty for no passphrase): motdepasse
Enter same passphrase again: motdepasse
Your identification has been saved in /home/utilisateur/.ssh/id_rsa
Your public key has been saved in /home/utilisateur/.ssh/id_rsa.pub
The key fingerprint is: SHA256:n60tA2JwGV0tptwB48YrPT6hQQWrxGYhEVegfnO9GXM Ma clé de chiffrement
The key's randomart image is:
+---[RSA 2048]----+
|   +o+ooo+o..    |
|    = ..=..+ .   |
|   . = o+++ o    |
|  . +.oo+o..     |
|   . +o+ S E     |
|    . oo=.X o    |
|      ...=.o .   |
|          .oo    |
|           .o.   |
+----[SHA256]-----+
```

![Synthèse des projets GitLab](images/gitlab_17.png)![Demande d'ajout d'une clé SSH dans les détails du projet](images/gitlab_18.png)![Bouton Add SSH key](images/gitlab_19.png)![Fenêtre de définition des clés SSH](images/gitlab_20.png)

Copier le contenu du fichier «**/home/utilisateur/.ssh/id-rsa.pub**»

![Ajout de la clé SSH créée](images/gitlab_21.png)

### Autorisations pour Docker et le Runner

Cette étape consiste à créer un certificat pour autoriser Docker à interagir avec le registre et le Runner.

Pour le registre :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir -p /etc/docker/certs.d/MachineUbuntu:5000
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/gitlab/trusted-certs/MachineUbuntu.crt /etc/docker/certs.d/MachineUbuntu:5000/ca.crt
```

Pour le runner :

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir -p /etc/gitlab-runner/certs
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/gitlab/trusted-certs/MachineUbuntu.crt /etc/gitlab-runner/certs/ca.crt
```

### Configurer et tester le Runner

Activation du runner dans docker

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
Unable to find image 'gitlab/gitlab-runner:latest' locally
latest: Pulling from gitlab/gitlab-runner
a31c7b29f4ad: Pull complete
d843a3e4344f: Pull complete
cf545e7bed9f: Pull complete
c863409f4294: Pull complete
ba06fc4b920b: Pull complete
Digest: sha256:79692bb4b239cb2c1a70d7726e633ec918a6af117b68da5eac55a00a85f38812
Status: Downloaded newer image for gitlab/gitlab-runner:latest

Runtime platform arch=amd64 os=linux pid=7 revision=8925d9a0 version=14.2.0
Running in system-mode.

Enter the Gitlab instance URL (for example, https://gitlab.com/):
```

Pour activer le runner :

![Projets GitLab](images/gitlab_22.png)![Détail du projet](images/gitlab_23.png)![Menu «Paramètres», sous menu «Intégration et livraison» du projet](images/gitlab_24.png)

Choisir l’option «**Exécuteurs**» et click sur le bouton «**Étendre**».

![Option de configuration des exécuteurs du projet](images/gitlab_25.png)

Aller dans «**Spécific runners**» dans l’option Exécuteurs.

![Section «Specific runner» de configuration des exécuteurs du projet](images/gitlab_26.png)

Informations pour déclarer le runner pour le projet.

![Section «Set up a specific runner manually»](images/gitlab_27.png)
```console
Enter the GitLab instance URL (for example, https://gitlab.com/): http://gitlab.domaine-perso.fr/
Enter the registration token: 9FfDsP_9Z2cXWi1Axwig
Enter a description for the runner: [75d626bde768]: Runner Developpement Python 3
Enter tags for the runner (comma-separated): runner
Registering runner... succeeded runner=Tzzfs5xc
Enter an executor: kubernetes, custom, docker-ssh, shell, docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox: docker
Enter the default Docker image (for example, ruby:2.6): python:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo chmod o+r /etc/gitlab-runner/config.toml
```

Changez dans «**/etc/gitlab-runner/config.toml**» :

```docker
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner Developpement Python 3"
  url = "http://gitlab.domaine-perso.fr/"
  token = "9FfDsP_9Z2cXWi1Axwig"
  executor = "docker"
  pull_policy = "if-not-present"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

Vous pouvez démarrer le Runner

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run -d --restart always --name gitlab-runner -v /etc/gitlab-runner:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
c9f30b11275ac803ebb17209441c7e0b6351c60d9f0ddadc17c8b0a7ae9cbb96
```

Autorisez le registre pour la machine ubuntu

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo ln -s /etc/docker/certs.d/MachineUbuntu\:5000/ca.crt /usr/local/share/ca-certificates/MachineUbuntu.crt
utilisateur@MachineUbuntu:~/**\ **repertoire_de_developpement**\ $ sudo update-ca-certificates
```

Si tout se passe bien vous obtenez le message :

```console
Updatting certificates in /etc/ssl/certs...
1 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
```

Dans «**Specific runners**»  de l’option «**Exécuteurs**» du sous menu «**Intégration et livraison**» du menu «**Paramètres**» du projet apparaît le runner en exécution

![Runner d'exécution du projet dans la section «Available specific runner» de l'option Exécuteurs d'Intégration et livraison du menu paramètre du projet](images/gitlab_28.png)

Mettre en pause le runner avec le bouton «**Pause**».

Cliquez sur l’icone ![L'icône éditer du runner du projet](images/gitlab_29.png) pour éditer les options du runner, et sélectionnez «**Indique si l’exécuteur peut choisir des tâches sans étiquettes (tags)**» :

![Fenêtre de configuration du runner d'un projet](images/gitlab_30.png)

Modifier aussi le temps «**Durée maximale d’exécution de la tâche**» avec «**30m**»

Relancer l’exécution du runner pour valider les modifications.

![Éxécution du runner](images/gitlab_31.png)

#### Tester le fonctionnement du runner

Éditer le fichier «**.gitlab-ci.yml**» dans **repertoire_de_developpement**.

```yaml
travail-de-construction:
  stage: build
  script:
    - echo "Bonjour, $GITLAB_USER_LOGIN !"

travail-de-tests-1:
  stage: test
  script:
    - echo "Ce travail teste quelque chose"

travail-de-tests-2:
  stage: test
  script:
    - echo "Ce travail teste quelque chose, mais prend plus de temps que travail-de-test-1."
    - echo "Une fois les commandes echo terminées, il exécute la commande de veille pendant 20 secondes"
    - echo "qui simule un test qui dure 20 secondes de plus que travail-de-test-1."
    - sleep 20

deploiement-production:
  stage: deploy
  script:
    - echo "Ce travail déploie quelque chose de la branche $CI_COMMIT_BRANCH."
```

```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Test du runner dans Gitlab"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
```

![Runner en cours d'exécution dans la fenêtre de détails du projet](images/gitlab_32.png)

On peut voir l’activité en cours du runner avec l’icône : ![Icône d'activité du runner](images/gitlab_33.png)

Dans le sous menu «**Pipelines**» du menu «**Intégration et livraison**» du projet on peut voir les taches d’exécution du runner :

![Sous menu Pipeline avec des taches en cours d'exécutions](images/gitlab_34.png)

On voit ici la tache «**Travail-de-construction**» en cours dans la phase de «**Build**» de l’exécuteur.

![Tâche «Build» du runner en cours](images/gitlab_35.png)

Si on clique sur cette icône on voit les opérations en cours de la tache :

![icône de progression](images/gitlab_36.png)

Une fois la tache réussi, l’exécuteur passe dans la phase d’exécution des tests.

![Exécution des tests dans le Pipeline](images/gitlab_37.png)

On peut voir le résultat en cliquant sur les icônes des taches de tests.

![Résultat travail-de-test-1](images/gitlab_38.png)![Résultat travail-de-test-2](images/gitlab_39.png)

Puis après l’exécuteur passe dans la phase «**Deploy**».

![Pipelines exécutions OK](images/gitlab_40.png)![Résultat de la tache déploiement-production](images/gitlab_41.png)

Test du déploiement docker :

```yaml
default:
  image: python:latest
```

Pour plus d’informations sur Gitlab et son utilisation [https://github.com/SocialGouv/tutoriel-gitlab](https://github.com/SocialGouv/tutoriel-gitlab), [https://makina-corpus.com/blog/metier/2019/gitlab-astuces-projets](https://makina-corpus.com/blog/metier/2019/gitlab-astuces-projets).

### Tester les Pages GitLab

#### Créer un projet de rendu de pages HTML

Créer un nouveau projet

![Création d'un nouveau projet](images/gitlab_42.png)

Création depuis un modèle

![Creation d'un projet depuis un modèle](images/gitlab_43.png)![Modèles de projets GitLab](images/gitlab_44.png)

Choisir «**Pages/Plain HTML**» comme modèle

![Projet Pages/Plain HTML](images/gitlab_45.png)

Renseignez :

- le nom du projet «**HTML**»
- La description du projet «**Test des GitLab Pages**»
- Le niveau de sécurité «**Public**»

![Renseignement du projet HTML](images/gitlab_46.png)![Détail du projet HTML](images/gitlab_47.png)

#### Créer le «runner» pour ce projet

![menu «Paramètres», sous menu «Intégration et livraison» et option «Exécuteurs»](images/gitlab_48.png)![Paramètres du runner pour l'éxécuteur du projet HTML](images/gitlab_49.png)
```console
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ docker run --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
Runtime platform arch=amd64 os=linux pid=7 revision=8925d9a0 version=14.2.0
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/): http://gitlab.domaine-perso.fr/
Enter the registration token: 7YBLdSA9en4NMex5zyQy
Enter a description for the runner: [75d626bde768]: Runner Test Pages GitLab
Enter tags for the runner (comma-separated): runner
Registering runner... succeeded runner=Tzzfs5xc
Enter an executor: kubernetes, custom, docker-ssh, shell, docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox: docker
Enter the default Docker image (for example, ruby:2.6): alpine:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Changez dans «**/etc/gitlab-runner/config.toml**» :

```docker
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Runner Developpement Python 3"
  url = "http://gitlab.domaine-perso.fr/"
  token = "9FfDsP_9Z2cXWi1Axwig"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0

[[runners]]
  name = "Runner Test Pages GitLab"
  url = "http://gitlab.domaine-perso.fr/"
  token = "7YBLdSA9en4NMex5zyQy"
  executor = "docker"
  pull_policy = "if-not-present"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
```

Vous pouvez configurer et redémarrer le Runner

![Mettre en pause le runner](images/gitlab_50.png)![Éditer le runner](images/gitlab_51.png)![Édition de la configuration du runner du Projet HTML](images/gitlab_52.png)

Modifiez l’option «**Indique si l’exécuteur peut choisir des tâches sans étiquettes (tags)**» pour l’activer. Et préciser une durrée maximale d’exécution de «**30m**»

![Configuration du runner du Projet HTML](images/gitlab_53.png)

Enregirtrer les modifications et relancer le runner

![Reprendre l'exécution du runner](images/gitlab_54.png)

##### Déployer et tester le HTML dans une Pages GitLab

![Détail du projet HTM](images/gitlab_55.png)

Éditer le fichier «**gitlab-ci.yml**» avec GitLab en cliquant sur le bouton ![Bouton Configuration de l'intégration et de la livraison continues](images/gitlab_56.png)

![Édition du fichier gitlab-ci.yml](images/gitlab_57.png)

Renseigner le Message de commit «**Mise à jour du fichier .gitlab-ci.yml pour le lancement du runner**». Puis cliquer sur le bouton «**Commit changes**»

![Section de Message de commit](images/gitlab_58.png)![Détail du projet HTML avec le runner en cours de lancement](images/gitlab_59.png)

Cliquer sur la tache «**Pages**» sans annuler la tache ( l’icône Cancel de l’image )

![Taches pages du runner du projet HTML dans l'étape deploy](images/gitlab_60.png)![Bilan de l'exécution de la tache pages](images/gitlab_61.png)

Dans le menu «**Dépôt**» avec le sous menu «**Commits**» on peut voir la réussite de la tâche suite au commit.

![Commits de la mise à jour du fichier .gitlab-ci.yml  avec le runner OK](images/gitlab_62.png)

Maintenant il ne manque plus qu’a récupérer le site web de la page html.

Pour cela allons dans le menu «**Paramètres**»,  le sous menu «**Pages**» du projet.

![](images/gitlab_63.png)![](images/gitlab_64.png)

La présence du lien «[http://utilisateur.documentation.domaine-perso.fr/html](http://utilisateur.documentation.domaine-perso.fr/html)» nous confirme que GitLab fonctionne avec les Pages.

Un click sur ce lien et on vérifie l’accès au site web.

![](images/gitlab_65.png)

Supprimez le projet («**Paramètres/Général/Advenced/Delete project**»), et nettoyez le runner de test «**Runner Test Pages GitLab**» du fichier «**/etc/gitlab-runner/config.toml**».
