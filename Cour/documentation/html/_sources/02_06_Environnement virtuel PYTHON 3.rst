.. role:: terminal(code)
  :language: console

Environnement virtuel PYTHON 3
******************************

L’ensemble des paquets Python installés par votre distribution, dans votre système Linux, a été bien testé par les intégrateurs de la distribution. Il faut donc autant que possible installer les outils/bibliothèques Python avec les outils d’administration des paquets de votre système pour vos applications informatiques Python du poste de travail .

L’installation de paquets Python par l’intermédiaire d’outils tierces risque de casser cet écosystème système bien testé.

Lorsque l’on fait du développement le besoin d’ajouter des paquets d’outils/bibliothèques Python au delà de votre système d’exploitation est une nécessité. C’est votre projet de programmation Python qui l’impose.

Donc l’utilisation de ces outils/bibliothèques sont propre à vos projets de développements Python. Ils peuvent alors rentrer en conflit de versions avec les applications Python de votre système Linux, Mac, Windows ou autres.

Afin d’isoler ces ajouts d’outils/bibliothèques du système de votre environnement poste de travail, nous allons créer pour vos projets des environnements virtuels de développement Python, avec des outils et des bibliothèques propre à ces environnements.

venv
====

C’est l’environnement virtuel standard de Python. Pour l’installer :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install -y python3-venv python3-pip

Création de l’environnement virtuel Python :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m venv .env
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ source .env/bin/activate
  (.env) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ deactivate

L’application :terminal:`pip` servira alors d’outil d’installation des outils et bibliothèque Python pour ces environnements virtuels.

pipenv
======

Installation de pipenv :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install pipenv

Création de l’environnement virtuel Python :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pipenv shell
  Creating a virtualenv for this project…
  Using /usr/bin/python3 (3.9.4) to create virtualenv…
  ⠋created virtual environment CPython3.9.4.final.0-64 in 232ms creator
  CPython3Posix(dest=/home/utilisateur/.local/share/virtualenvs/repertoire_de_developpement-hIqPJnF9, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/utilisateur/.local/share/virtualenv)
  added seed packages: pip==20.3.4, pkg_resources==0.0.0, setuptools==44.1.1, wheel==0.34.2
  activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator

  Virtualenv location: /home/utilisateur/.local/share/virtualenvs/repertoire_de_developpement-hIqPJnF9
  Creating a Pipfile for this project…
  Spawning environment shell (/bin/bash). Use 'exit' to leave.
  (repertoire_de_developpement-hIqPJnF9) utilisateur@MachineUbuntu:~/repertoire_de_developpement$ exit


.. only:: latex

  .. raw:: latex

    \newpage
