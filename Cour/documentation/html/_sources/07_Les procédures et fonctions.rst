Les procédures et fonctions
###########################

.. include:: 07_01_Définir une procédure-fonction.rst

.. only:: latex

  .. raw:: latex

    \newpage

.. include:: 07_02_Documenter les fonctions.rst

.. only:: latex

  .. raw:: latex

    \newpage

.. include:: 07_03_Création de bibliothèques de fonctions.rst
