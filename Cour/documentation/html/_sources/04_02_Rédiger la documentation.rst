.. role:: documentation(code)
  :language: rest

.. role:: python(code)
  :language: python

Rédiger la documentation
************************

Le fichier d’entrée de votre documentation **index.rst** (toctree) se trouve dans **~/repertoire_de_developpement/docs/sources-documents**.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install retext

Éditer le fichier index.rst et le modifier ainsi :

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. toctree::
     :caption: Contenu :
     :maxdepth: 2

  Initiation à la programmation Python pour l'administrateur systèmes
  ===================================================================


  Index
  =====

  * :ref:`genindex`

  Index des modules
  =================

  * :ref:`modindex`

  .. * :ref:`search\`

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html

.. image:: images/documentation_3.png
   :alt: Page de doc début
   :align: center
   :scale: 100%


Parties, chapitres, sections, paragraphes
=========================================

Par convention pour Python :

-  «**#**» : Parties
-  «**\***» : Chapitres
-  «**=**» : Sections
-  «**-**» : Sous-sections
-  «**^**» : Sous-sous-section
-  «**\"**» : Paragraphes

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. sectionauthor:: Prénom NOM <prenom.nom@fai.fr>

  .. codeauthor:: Geek DEVELOPPEUR <geek.developpeur@fai.fr>

  .. toctree::
     :caption: Contenu :
     :maxdepth: 5

  Programmation Python 3
  ######################

  Un texte d’introduction sur la partie Python 3.

  Initiation à la programmation Python pour l'administrateur systèmes
  *******************************************************************

  Un texte d’introduction pour mon chapitre.

  Section 1
  =========

  Un texte pour la section 1.

  Sous-section 1
  --------------

  Du texte pour la sous-section 1

      Du texte pour une sous-partie de la sous-section 1

          Du texte pour une sous sous partie de la sous-section 1

  Sous-section 2
  --------------

  Du texte pour la sous-section 2

  Exemple de titrages
  ###################

  Chapitre 1
  **********

  Section 1
  =========

  Sous-section 1
  --------------

  Sous-sous-section 1
  ^^^^^^^^^^^^^^^^^^^

  Paragraphe 1
  """"""""""""

  Sous-paragraphe 1
  +++++++++++++++++


.. code-block:: console

**utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$** make
html

.. image:: images/documentation_4.png
   :alt: Doc début avec parties, chapitres, sections et paragraphes
   :align: center
   :scale: 100%

.. image:: images/documentation_5.png
   :alt: Doc fin avec parties, chapitres, sections et paragraphes
   :align: center
   :scale: 100%


Mettre en forme du texte
========================

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. toctree::
     :caption: Contenu :
     :maxdepth: 2

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

  **Tout en gras.**

  Du texte \ **en gras**\ pour ma documentation.

  *Tout en italique.*

  Du texte \ *en italique*\ pour ma documentation.

  .. only:: html

      .. raw:: html

          Une phrase avec un <font color="Red">mot</font> en rouge.

  .. only:: latex

      .. raw:: latex

          Une phrase avec un \textcolor{red}{mot} en rouge.

  .. only:: odt

      Une phrase avec un mot non en rouge.


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf


.. image:: images/documentation_6.png
   :alt: Doc avec mise en forme
   :align: center
   :scale: 100%


Insertion de texte d’échappement
================================

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. toctree::
     :caption: Contenu :
     :maxdepth: 2

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

  Le caractère \\

  Du texte \
  sur une seule ligne.

  Du texte sans qu’il soit interprété ``\n, \r, \t, \\``.

  ``**Une phrase non en gras**``

  ``*Une phrase non en italique*``


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html


.. image:: images/documentation_7.png
   :alt: Doc avec caractères d'échappement
   :align: center
   :scale: 100%


Les listes
==========

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. toctree::
     :caption: Contenu :
     :maxdepth: 2

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

  Liste à puces

  * élément 1
  * élément 2
  * élément 3

  La liste numérotée

  1. élément 1
  2. élément 2
  3. élément 3

  La liste numérotée automatiquement

  #. élément 1
  #. élément 2
  #. élément 3


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html


.. image:: images/documentation_8.png
   :alt: Doc avec listes
   :align: center
   :scale: 100%


Les tableaux
============

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. toctree::
     :caption: Contenu :
     :maxdepth: 2

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

  +-----+-----------+
  |  A  |     B     |
  +=====+=====+=====+
  |  1  |  2  |  3  |
  +-----+-----+-----+

  ==== ==== ====
      A    B
  --------- ----
   A0   A1   B0
  ==== ==== ====
   01   02   03
   04   05   06
  ==== ==== ====

  .. csv-table:: Personnel
    :header: "Prénom", "Nom"
    :widths: 40, 40

    "Franc", "GEEK"
    "Emmanuel", "DICTATOR"

  .. list-table:: Lettres
    :widths: 10 10 20
    :header-rows: 1
    :stub-columns: 1

    * - Lettre
      - A
      - B
    * - Nombre
      - 25
      - 5


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html


.. image:: images/documentation_9.png
   :alt: Doc avec tableau
   :align: center
   :scale: 100%


Insertion de blocs, code, image, graphviz, liens, mathématiques, notes, citations
=================================================================================

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. role:: python(code)
     :language: python

  .. toctree::
     :caption: Contenu :
     :maxdepth: 2

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

  Pour afficher un texte en Python on utilise la fonction :python:`print("Mon texte")`.

  .. literalinclude:: ../../1_Mode_interprété/mon_1er_programme.py

  .. code-block:: python

      print("Bonjour les zouzous")

  .. image:: ../../../Images/tux.svg
     :alt: Sphinx c’est cool
     :align: center
     :width: 120px

  .. graphviz::

      digraph "frameworks web python" {
          python [label="Python", href="https://www.python.org/", target="_top"];
          flask [label="Flask", href="https://flask.palletsprojects.com/en/1.1.x/", target="_top"];
          django [label="Django", href="https://www.djangoproject.com/", target="_top"];
          bottle [label="Bottle", href="https://bottlepy.org/", target="_top"];
          turbogears [label="TurboGears", href="https://turbogears.org/", target="_top"];
          web2py [label="web2py", href="http://www.web2py.com/", target="_top"];
          cherrypy [label="CherryPy", href="https://cherrypy.org/", target="_top"];
          quixote [label="Quixote", href="http://quixote.ca/", target="_top"];
          python -> {flask; django; bottle; turbogears; web2py; cherrypy; quixote;};
      }

  `Python <https://www.python.org>`_

  - :ref:`python:reference-index`
  - :ref:`Référence langage Python <python:reference-index>`
  - :doc:`python:library/enum`
  - :doc:`Énumérasions <python:library/enum>`
  - :docpython3:`tutorial`
  - :manpython3:`enum`

  :download: `Téléchargement <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur/-/raw/master/README.md?inline=false>`_

  .. math::
     :nowrap:

      \begin{gather*}
      (a + b)² = a² + 2ab + b² \\
      \sqrt{\frac{n}{n-\sqrt[3]{2}} S} \\
      \int_a^b x \, \mathrm dx = [x^2]_a^b = [(b)^2 – (a)²] = b² – a² \\
      \mathrm{2~H_{2(g)}+O_{2(g)}=2~H_2O_{(1)}}
      \end{gather*}

  L'équation d'Euler :eq:`euler` est utile en mathématiques.

  Référence à la citation du formateur [citation]_.

  Première note [#n1]_, deuxième note [#n2]_

  Et encore une troisième note [#n3]_

  .. [citation] «Python que oui, ou Python que non…».

  .. math:: e^{i\pi} + 1 = 0
     :label: euler

  .. rubric:: Notes de bas de page

  .. [#n1] Note 1
  .. [#n2] Note 2
  .. [#n3] Note 3


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html


.. image:: images/documentation_10.png
   :alt: Doc Blocs, code, image, graphviz, liens, mathématique, notes et citations
   :align: center
   :scale: 100%


Boites et conditions d’affichages
=================================

Il faut d’abord modifier l’extension «**sphinx.ext.todo**» au fichier **conf.py**.

.. code-block:: python

  [extensions]
  todo_include_todos = True

Puis modifier **index.rst** comme suit :

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. toctree::
     :maxdepth: 2
     :caption: Contenu :

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

  .. ifconfig:: niveau_developpement == 'alpha'

      .. only :: format_html and not builder_epub

          .. raw:: html

              <font color="Red">Liste des TODOs à faire</font>

      .. only :: latex

          .. raw:: latex

              \textcolor{red}{Liste des TODOs à faire}

      .. todolist::

  .. ifconfig:: niveau_developpement not in ( 'pre-alpha', 'alpha', 'beta', 'rc')

      En production
  .. ifconfig:: niveau_developpement == 'pre-alpha'

      En faisabilité
  .. ifconfig:: niveau_developpement == 'alpha'

      En développement
  .. ifconfig:: niveau_developpement == 'beta'

      En test
  .. ifconfig:: niveau_developpement == 'rc'

      En qualification

  Les boîtes :

  .. ifconfig:: niveau_developpement == 'alpha'

      .. todo:: Compléter la boîte voir aussi

  .. seealso:: Ceci est une boîte

  .. ifconfig:: niveau_developpement == 'alpha'

      .. todo:: Compléter la boîte note

  .. note:: Ceci est une boîte

  .. sidebar:: Ceci est une boîte

      Contenu de la barre de côté

  .. warning:: Ceci est une boîte

  .. important:: Ceci est une boîte

  .. ifconfig:: niveau_developpement == 'alpha'

      .. todo:: Compléter la boîte Remarque

  .. topic:: **Remarque**

      Ceci est le contenu du sujet

  .. only:: format_html and not builder_epub

      Les onglets à n'utiliser que pour une documentation purement html

      .. tabs::

          .. tab:: Code

              Ma documentation sur le code

              .. tabs::

                  .. code-tab:: py

                      python AfficheArguments.py Bonjour à tous

                  .. code-tab:: java

                      java AfficheArguments Bonjour à tous

              .. tabs::

                  .. code-tab:: py

                      import sys

                      for arg in sys.argv:
                          print(arg)

                  .. code-tab:: java

                      public class AfficheArguments {
                          public static void main(String[] args) {
                              int i;
                              for (String s : args) System.out.println(s);
                          }
                      }

          .. tab:: Group

              Ma documentation sur le code

              .. tabs::

                  .. group-tab:: Python

                      Exécuter le programme :

                      .. code-block:: shell

                          python AfficheArguments.py Bonjour à tous

                  .. group-tab:: Java

                      Exécuter le programme :

                      .. code-block:: shell

                          java AfficheArguments Bonjour à tous

              .. tabs::

                  .. group-tab:: Python

                      Le code Python :

                      .. code-block:: python

                          import sys

                          for arg in sys.argv:
                              print(arg)

                  .. group-tab:: Java

                      Le code java :

                      .. code-block:: java

                          public class AfficheArguments {
                              public static void main(String[] args) {
                                  int i;
                                  for (String s : args) System.out.println(s);
                              }
                          }


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make epub
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf

.. only:: not latex

  .. image:: images/documentation_11.png
     :alt: Doc boites et conditions d'affichages
     :align: center
     :scale: 100%

.. only:: latex

  .. image:: images/documentation_11.png
     :alt: Doc boites et conditions d'affichages
     :align: center
     :scale: 39%


Documenter le code Python
=========================

Modifier le fichier «**index.rst**» :

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. toctree::
     :maxdepth: 2
     :caption: Contenu :

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

  Approche manuelle :

  .. py:module:: module
     :platform: Linux
     :synopsis: Un court résumé du périmètre d’utilisation du module

  .. py:function:: fonction(paramètres)

      .. py:class:: Classe(paramètres)

          .. py:method:: méthode(paramètres)

              .. py:attribute:: attribut

  .. py:decorator:: décorateur(paramètres)

  .. py:exception:: exception


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html


.. only:: not latex

  .. image:: images/documentation_12.png
     :alt: Doc code Python
     :align: center
     :scale: 100%

.. only:: latex

  .. image:: images/documentation_12.png
     :alt: Doc code Python
     :align: center
     :scale: 35%


Pour l’approche automatique, dont nous verrons l’utilisation un peu plus loin, il faut utiliser :

.. code-block:: rest

  .. Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes. Document maître créé par sphinx-quickstart le Mercredi 24 Avril 2021 à 17h 27min 50s.
     Vous pouvez adapter ce fichier complètement à votre goût.
     Il contient la racine `toctree` de votre documentation.

  .. toctree::
     :maxdepth: 2
     :caption: Contenu :

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

  Approche automatique :

  .. automodule:: Documentation.mon_module
     :members:

Rôle des membres de **automodule** dans nos fichiers «**.rst**» :

-  **:members:** affiche les éléments publics (qui ne débute pas par «**\_**»).
-  **:special-members:** Affiche aussi les constructeurs des éléments publics.
-  **:undoc-members:** Affiche aussi les éléments sans docstring.
-  **:private-members:** Affiche aussi les éléments privés (qui commencent par «**\_**»).
-  **:inherited-members:** Affiche les éléments non hérités.
-  **:show-inheritance:** Affiche les classes mères dont hérite les classes Python.


Ces définitions vont se trouver dans la structure de la documentation «**.rst**».

Pour l’écriture de la documentation directement dans notre code il faudra renseigner nos docstring. Sphinx ajoute de nombreuses nouvelles directives et rôles de texte interprétés au balisage reST standard.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd .. ; mkdir Documentation ; cd Documentation ; touch mon_module.py


Balise de méta-information
--------------------------

sectionauthor
^^^^^^^^^^^^^

Lorsque vous commencez l'écriture de la documentation d'un code Python, il est très utile pour les autres rédacteurs, ou les développeurs, de savoir qui l'a saisi dans le code. La directive :documentation:`.. sectionauthor:: Auteur Documentation <auteur.documentation@fai.fr>` identifie l'auteur de la documentation de la section actuelle. La partie d'adresse courriel doit être en minuscules.

Actuellement, ce balisage n'est pas interprété dans la sortie documentaire, mais elle permet de garder une trace des contributions à la documentation dans le code.

Exemple à saisir dans «**mon_module.py**» :

.. code-block:: python

  # -*- coding: utf-8 -*-

  """
  .. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
  """

Balisages spécifiques au module
-------------------------------

Lorque vous commencez l'écriture d'un module Python (fichier «**mon\_module.py**»), la première des informations à fournir aux autres développeurs est sur le module lui même. Le balisage décrit dans cette section est utilisé pour fournir ces informations sur le module pour la documentation. **Chaque module doit être documenté dans son propre fichier**. Normalement, ce balisage apparaît après le titre du module dans le fichier ; un fichier typique peut commencer comme ceci :

.. code-block:: python

  # -*- coding: utf-8 -*-

  """
  .. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
  :mod:`mon_module` -- Module d'exemple
  #####################################

  .. module:: mon_paquet.mon_module
     :synopsis:: Ce module illustre comment écrire une docstring de module avec Python
     :platform: Linux
  """

.. note::

  Il est important de donner un titre de module puisque cela sera inséré dans l'arborescence de la table des matières pour la documentation.

module
^^^^^^

La directive :documentation:`.. module:: nom_du_module` marque le début de la description d'un module, d'un package ou d'un sous-module. **Le nom doit être entièrement qualifié** (c'est-à-dire incluant le nom du package pour les sous-modules). Il est paramétrable avec :

* L'option :documentation:`:synopsis: Résumé rapide` qui doit consister en une phrase décrivant l'objectif du module. Elle n'est actuellement utilisée que dans l'index global des modules.
* L'option :documentation:`:platform: Unix, Mac, Windows`, si elle est présente, qui indique les plateformes compatibles avec le code Python. C'est une liste séparée par des virgules des plates-formes. Si le code est disponible pour toutes les plates-formes, l'option doit être omise. Les clés sont des identifiants courts ; les exemples utilisés incluent «**Linux**», «**Unix**», «**Mac**» et «**Windows**». Il est important d'utiliser une clé qui a déjà été utilisée le cas échéant.
* L'option :documentation:`:deprecated:` (sans valeur) peut être donnée pour marquer un module comme obsolète ; il sera alors désigné comme tel à divers endroits.


moduleauthor
^^^^^^^^^^^^

.. code-block:: python

  # -*- coding: utf-8 -*-

  """
  .. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
  :mod:`mon_module` -- Module d'exemple
  #####################################

  .. module:: mon_module
     :platform: Unix, Windows
     :synopsis: Ce module illustre comment écrire votre docstring dans Python.
  .. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
  .. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

  """

La directive :documentation:`.. moduleauthor::`, qui peut apparaître plusieurs fois, nomme les auteurs du code Python du module, tout comme :documentation:`.. sectionauthor::` nomme le(s) auteur(s) d'une documentation. Elle suit les même règles de syntaxe que :documentation:`.. sectionauthor::`.

Pour le code, nos fonctions et nos classes
------------------------------------------

-  «**:var/ivar/cvar nom_variable: description**» : Pour nos variables.
-  «**:param/parameter/arg/argument/key/keywords nom_paramètre: description**» : Pour décrire un paramètre d’une fonction ou d’un objet.
-  «**:type element: type**» : Pour décrite le type d’une variable ou d’un paramètre (Callable, int, float, long, str, tuple, list, dict, None, True, False, boolean).
-  «**:returns/return: description**» : Décrit ce qui est retourné par une fonction ou un objet.
-  «**:rtype: type**» : Pour décrite le type de ce qui est retourné par une fonction ou un objet (Callable, int, float, long, str, tuple, list, dict, None,
   True, False, boolean).
-  «**:raises/raise/except/exception nom_exception: description**» : Décrit une exception dans votre code.

Tout ceci nous permet d'écrire un code de documentation minimal final (avec des variables Python et Sphinx utiles) :

.. code-block:: python

  # -*- coding: utf-8 -*-

  """
  .. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
  :mod:`mon_module` -- Module d'exemple
  #####################################

  .. module:: mon_module
     :platform: Unix, Windows
     :synopsis: Ce module illustre comment écrire votre docstring dans Python.
  .. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
  .. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

  """

  __title__ = "Module illustration écriture docstring Python"
  __author__ = "Formateur PYTHON"
  __version__ = '0.7.3'
  __release_life_cycle__ = 'alpha'
  # pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
  __docformat__ = 'reStructuredText'

Fichier **mon\_module.py** avec une classe Python d'exemple :

.. code-block:: python

  # -*- coding: utf-8 -*-

  """
  .. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
  :mod:`mon_module` -- Module d'exemple
  #####################################

  .. module:: mon_module
     :platform: Unix, Windows
     :synopsis: Ce module illustre comment écrire votre docstring dans Python.
  .. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
  .. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

  """

  __title__ = "Module illustration écriture docstring Python"
  __author__ = "Formateur PYTHON"
  __version__ = '0.7.3'
  __release_life_cycle__ = 'alpha'
  # pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
  __docformat__ = 'reStructuredText'

  class ClasseExemple():
      """Cette classe docstring montre comment utiliser sphinx et la syntaxe rst.
      La première ligne est une brève explication de la classe avec ses paramètres et ce que renvoi
      l’objet.
      Cela doit être complété par une description plus précise des méthodes et des attributs dans
      le code de la classe dans le code.
      La seule méthode ici est :func:`mafonction`.

      - **paramètres**, **types**, **retour** et **type de retours**::

          :param arg1: description
          :param arg2: description
          :type arg1: type arg1
          :type arg2: type arg2
          :return: description du retour
          :rtype: type du retour

      - Documentez des sections **:Exemples:** en utilisant la syntaxe des doubles points ``:``

        .. code-block:: rest

              :Exemple:

                  suivi d'une ligne vierge!

          qui apparaît comme suit :

          :Exemple:

              suivi d'une ligne vierge!

      - Des sections spéciales telles que **Voir aussi**, **Avertissements**, **Notes** avec la
        syntaxe sphinx (*directives de paragraphe*)::

          .. seealso:: blabla
          .. warnings:: blabla
          .. note:: blabla
          .. todo:: blabla

      .. warning::
         Il existe de nombreux autres champs Info mais ils peuvent être redondants:

             * param, parameter, arg, argument, key, keyword: Description d'un paramètre.
             * type: Type de paramètre.
             * raises, raise, except, exception: Quand une exception spécifique est levée.
             * var, ivar, cvar: Description d'une variable.
             * returns, return: Description de la valeur de retour.
             * rtype: Type de retour.

      .. note::
          Il existe de nombreuses autres directives telles que :
          versionadded, versionchanged, rubric, centered, …
          Voir la documentation sphinx pour plus de détails.

      Voici ci-dessous les résultats pour :func:`mafonction` docstring.
      """

  def maméthode(self, arg1, arg2, arg3):
      """Retourne (arg1 / arg2) + arg3

      Ceci est une explication plus précise, qui peut inclure des mathématiques avec la syntaxe
      latex :math:`\\alpha`.
      Ensuite, vous devez fournir une sous-section facultative (juste pour être cohérent et avoir
      une documentation uniforme. Rien ne vous empêche de changer l'ordre):

          - paramètres utilisés ``:param <nom>: <description>``
          - type des paramètres ``:type <nom>: <description>``
          - retours de la méthode ``:returns: <description>``
          - exemples (doctest)
          - utilisation de voir aussi ``.. seealso:: texte``
          - utilisation des notes ``.. note:: texte``
          - utilisation des alertes ``.. warning:: texte``
          - liste des restes à faire ``.. todo:: texte``

      **Avantages**:
          - Utilise les balises sphinx.
          - Belle sortie HTML avec les directives Seealso, Note, Warning.

      **Désavantages**:
          - En regardant simplement la docstring, les sections de paramètres, de types et de retours
            n'apparaissent pas bien dans le code.

      :param arg1: la première valeur
      :param arg2: la première valeur
      :param arg3: la première valeur
      :type arg1: int, float,...
      :type arg2: int, float,...
      :type arg3: int, float,...
      :returns: arg1/arg2 +arg3
      :rtype: int, float

      :Example:

      .. code-block:: pycon

          >>> import template
          >>> a = template.ClasseExemple()
          >>> a.mafonction(1,1,1)
          2

      .. note:: il peut être utile de souligner une caractéristique importante

      .. seealso:: :class:`AutreClasseExemple`
      .. warning:: arg2 doit être différent de zéro.
      .. todo:: vérifier que arg2 est non nul.
      """
      return arg1 / arg2 + arg3

Générer la documentation pour voir le rendu :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/Documentation$ cd ../docs
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd ..
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Configuration de la documentation du projet"
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push

.. image:: images/documentation_13.png
   :alt: Doc autodoc avec Python
   :align: center
   :scale: 100%
