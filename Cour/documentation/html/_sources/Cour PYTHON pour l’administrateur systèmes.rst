.. only:: not latex

  #####################################################################################################################################################################
  `Initiation à la programmation PYTHON pour l’administrateur systèmes <https://sefran.frama.io/initiation-a-la-programmation-python-pour-l-administrateur-systemes/>`_
  #####################################################################################################################################################################

.. only:: latex

  ###################################################################
  Initiation à la programmation PYTHON pour l’administrateur systèmes
  ###################################################################
  
  `Voir ce cour en ligne <https://sefran.frama.io/initiation-a-la-programmation-python-pour-l-administrateur-systemes/>`_


.. include:: 01_Accueil des stagiaires.rst


.. include:: 02_Présentation et installation des outils du langage.rst


.. include:: 03_Les principaux éléments de syntaxe.rst


.. include:: 04_La génération de la documentation du programme.rst


.. include:: 05_La qualité du code.rst


.. include:: 06_Le test unitaire.rst


.. include:: 07_Les procédures et fonctions.rst


.. include:: 08_Les objets.rst


.. include:: 09_La gestion des erreurs.rst


.. include:: 10_Les expressions régulières.rst


.. include:: 11_Stocker des données.rst


.. include:: 12_Le réseau.rst


.. include:: 13_Générer des documents.rst


.. include:: 14_Approfondir ce cours.rst


.. include:: 15_Fin du cours.rst
