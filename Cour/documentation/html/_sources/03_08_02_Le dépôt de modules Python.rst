Le dépôt de modules Python
==========================

Pip
---

Une des forces de **Python** est la multitude de bibliothèques disponibles (près de 6000 bibliothèques gravitent autour du projet **Django**).

Par exemple installer une bibliothèque peut vite devenir ennuyeux:

-  trouver le bon site,
-  la bonne version de la bibliothèque,
-  l'installer,
-  trouver ses dépendances,
-  etc.

Il existe une solution qui vous permet de télécharger très simplement une bibliothèque **pip**.

PIP c'est quoi ?
^^^^^^^^^^^^^^^^

**Pip** est un **système de gestion de paquets** utilisé pour installer et gérer des librairies écrites en Python. Vous pouvez trouver une grande partie de ces librairies dans le `Python Package Index <https://pypi.python.org/pypi>`_ (ou PyPI). **Pip** empêche les installations partielles en annonçant toutes les exigences avant l'installation.

.. code-block:: console

  pip install librairie

Vous pouvez choisir la version qui vous intéresse :

.. code-block:: console

  pip install librairie==2.2

Supprimer une librairie :

.. code-block:: console

  pip uninstall librairie

Mettre à jour une librairie :

.. code-block:: console

  pip install librairie --upgrade

Revenir sur une version antérieure :

.. code-block:: console

  pip install librairie==2.1 --upgrade

Rechercher une nouvelle librairie :

.. code-block:: console

  pip search librairie

Vous indiquer quelles librairies ne sont plus à jour :

.. code-block:: console

  pip list --outdated

Afficher toutes les librairies installées et leur version :

.. code-block:: console

  pip freeze

Exporter la liste des librairies, vous pourrez la réimporter ailleurs :

.. code-block:: console

  pip freeze > lib.txt

Importer la liste de librairie comme ceci :

.. code-block:: console

  pip install -r lib.txt

Créer un gros zip qui contient toutes les dépendances :

.. code-block:: console

  pip bundle <nom_du_bundle>.pybundle -r lib.txt

Pour installer les librairies :

.. code-block:: console

  pip install <nom_du_bundle>.pybundle

Pour installer depuis un dépôt distant (`Voir la section du support VCS <https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support>`_) :

.. code-block:: console

  pip install git+https://github.com/chemin/monmodule.git#egg=monmodule

Pour le lien ver le support VCS : https://pip.pypa.io/en/stable/reference/pip_install/#vcs-support

Voir plus d’informations https://docs.python.org/fr/3.6/installing/index.html

