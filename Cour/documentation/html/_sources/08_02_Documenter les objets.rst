.. role:: python(code)
  :language: python

.. role:: documentation(code)
  :language: rest

Documenter les classes d'objets
*******************************

Maintenant qu'on sait documenter une fonction, documentons une classe d'objets.

Exemple de documentation d'une classe :python:`MaClasse` dans un module :python:`mon_module` d'un paquet :python:`mon_paquet`  :

.. code-block:: python

  # -*- coding: utf-8 -*-

  """
  .. sectionauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>
  :mod:`mon_module` -- Module d'exemple de documentation d'une classe
  ###################################################################

  .. module:: mon_paquet.mon_module
     :platform: Linux
     :synopsis: Ce module illustre comment écrire votre docstring pour une classe dans Python.
  .. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
  .. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>

  """

  __title__ = "Module illustration écriture docstring d'une classe Python"
  __author__ = "Formateur PYTHON"
  __version__ = '0.7.3'
  __release_life_cycle__ = 'alpha'
  # pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
  __docformat__ = 'reStructuredText'

  class MaClasse():
      """
      Exemple de classe de mon_paquet.mon_module

      :param arg: argument du constructeur MaClasse
      :type arg: int
      """

      def __init__(self, arg):
          """
          Constructeur de MaClasse

          Les propriétés de la classe sont :
          :param param: p1
          :type param: int
          """
          self.p1 = None

      def bonjour(self, nom):
          """
          Permet d'afficher le message « Bonjour à toi <nom> »

          :param nom: Nom de la personne
          :type nom: str
          :return: Message de bonjour
          :rtype: str:
          """
          print("Bonjour " + nom)
          return "Bonjour à toi " + nom

  if __name__ == "__main__":
      mon_objet = MaClasse()
      print mon_objet.bonjour("padawan")

Finalement, il n'y a rien de bien nouveau. On a documenté les méthodes de la classe :python:`MaClasse` comme on l'a fait avec les fonctions.

**La seule différence c'est avec la méthode constructeur** :python:`__init__` de la classe :python:`MaClasse` où la docstring des paramètres de classe est directement dans la déclaration de la classe (:documentation:`:param arg: argument du constructeur MaClasse` et :documentation:`:type arg: int`).

C'est **le fonctionnement par défaut avec autodoc**.
Cette disposition permet de séparer la déclaration des propriétés de la classe, qui sont définies dans la méthode :python:`__init__`, avec les paramètres de création d'objets de la classe :python:`MaClasse` qui sont définis dans la déclaration des paramètres de la méthode :python:`__init__`.

Ce comportement est réglable via une option :python:`autoclass_content = 'configuration'` dans le fichier «**conf.py**».
Si vous préférez documenter avec le constructeur :python:`__init__` les paramètres de création d'objet avec les propriétés de la classe, ce paramètre vous permet de définir comment seront insérés les paramètres de la classe avec «**autoclass**». Exemple pour que cela soit avec la déclaration de propriétés dans :python:`__init__` :

.. code-block:: python

  autoclass_content = 'init'

Les valeurs possibles sont :

* **"class"** : Seule la docstring de la classe est insérée. C'est la valeur par défaut. Vous pouvez toujours documenter :python:`__init__` en tant que méthode distincte en utilisant «**automethod**» ou l'option «**members**» pour générer automatique la documentation de vos classes.
* **"both"** : La docstring de la classe et de la méthode :python:`__init__` sont concaténées et insérées.
* **"init"** : Seule la docstring de la méthode :python:`__init__` est insérée.

.. warning::

  Si la classe n'a pas de méthode :python:`__init__`, ou si la docstring de la méthode :python:`__init__` est vide, et que la classe a une docstring avec la méthode :python:`__new__` celle-ci sera utilisée à la place.

Nous avons maintenant abordé l'essentiel pour commencer une rédaction complète de sa documentation du code Python. Vous pouvez encore approfondir avec plein de directives Sphinx utiles `avec ce lien <https://devguide.python.org/documenting/>`_.
