Présentation et installation des outils du langage
##################################################

Approche interactive avec les stagiaires pour créer le groupe.

.. only:: html

  .. raw:: html

    <font color="Green">Avez-vous déjà programmé ?</font></br>

    <font color="Green">Avez-vous déjà programmé en Python ?</font>

.. only:: latex

  .. raw:: latex

    \textcolor{green}{Avez-vous déjà programmé ?}
    \newline
    \textcolor{green}{Avez-vous déjà programmé en Python ?}
    \newpage

.. only:: not (html or latex)

  Avez-vous déjà programmé ?

  Avez-vous déjà programmé en Python ?


.. include:: 02_01_Conception et modélisations.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_02_Les environnements systèmes.rst



.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_03_L'édition de code Python.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_04_Interpréteurs.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_05_Révision de code.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_06_Environnement virtuel PYTHON 3.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_07_Documentation.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_08_Débogages.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_09_Tests Unitaires.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_10_L’industrialisation du code DEVOPS.rst
