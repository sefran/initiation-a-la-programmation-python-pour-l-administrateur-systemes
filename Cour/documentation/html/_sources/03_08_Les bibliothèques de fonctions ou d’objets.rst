Les bibliothèques de fonctions ou d’objets
******************************************

.. include:: 03_08_01_Les modules PYTHON.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_08_02_Le dépôt de modules Python.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_08_03_Les modules de gestion des paramètres de la ligne de commande.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_08_04_Gestion des dates et du temps.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_08_05_Module Windows.rst

.. only:: latex

  .. raw:: latex

    \newpage
