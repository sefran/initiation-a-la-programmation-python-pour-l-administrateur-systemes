.. role:: python(code)
  :language: python

Configurer Sphinx pour Python
*****************************

Le répertoire racine Sphinx d'une collection de textes bruts permettant de générer la documentation est appelé le `répertoire source <https://ugaugtjjyr2ylvegfxect62ccm--www-sphinx-doc-org.translate.goog/en/master/glossary.html#term-source-directory>`_.
C’est le répertoire que nous avons nommé lors de l’installation «**sources-documents**».

Ce répertoire contient également le fichier de configuration Sphinx **conf.py**, où vous pouvez configurer tous les aspects de la façon dont Sphinx lit vos sources et construit votre documentation.

Pour paramétrer tous ces aspects, il faut éditer le fichier **conf.py** qui se trouve dans le dossier **~/repertoire_de_developpement/docs/sources-documents**.

Configurations de base de sphinx
================================

Renseigner la racine des fichiers de CODE
-----------------------------------------

.. code-block:: python

  # -*- coding: utf-8 -*-

  # == Configuration chemins =====================================
  
  
Indiquez où se trouve vos fichiers de code python pour générer votre documentation :

.. code-block:: python

  import os
  import sys

  sys.path.insert(0, os.path.abspath('../..'))
  sys.setrecursionlimit(1500)


Informations sur le projet de documentation
-------------------------------------------

.. code-block:: python

  # == Informations du projet =====================================
  
  
Titres, logos, copyright et auteur :

.. code-block:: python

  project_title = "Documentation sur l’initiation à la programmation Python pour l’administrateur systèmes"
  project_short_title = "Initiation Python 3 pour Administrateur"
  project_description = "Formation d'initiation à la programmation Python pour l'administrateur systèmes."
  project_logo = "images/logo.png"
  project_favicon = "images/favicon.png"
  copyright = '2021, Prénom NOM'
  author = 'Prénom NOM'

Versions du document :

.. code-block:: python

  from Documentation.mon_module import __version__, __release_life_cycle__

  # version 'X.Y' ou X est la version majeure incompatible avec la précédente, et Y est un ajout de fonctionnalités à la version.
  version = __version__ # utilisation restructuredtext |version|.
  #  'Pre-alpha' = faisabilité, 'Alpha' = développement, 'Beta' = tests et révisions, 'Release candidate' = qualification, 'Stable release' = prêt à déployer, 'Feature complete' = production, 'End of life' = obsolète.
  release = __release_life_cycle__ # utilisation restructuredtext |release|.

  
Paramètres de documentation
---------------------------

.. code-block:: python

  # == Configurations générales =====================================
  
  
Langue :

.. code-block:: python

  langage = 'fr'

  # L’internationalisation de la documentation
  locale_dirs = ['locales/']
  gettext_compact = False

  
Le fichier d'entrée de la documentation et la coloration syntaxique :

.. code-block:: python

  # Le document maître toctree.
  master_doc = 'index'

  # Le thème de la coloration syntaxique
  pygments_style = 'sphinx'

  
Extensions des fichiers de la documentation :

.. code-block:: python

  source_suffix = {
      '.rst': 'restructuredtext',
      '.txt': 'restructuredtext',
      '.md': 'markdown',
  }
  # ou de la forme
  # source_suffix = ['.rst', '.md']
  # source_suffix = '.rst'

Autres paramètres :

.. code-block:: python

  # Liste des modèles, relatifs au répertoire source, qui correspondent aux
  # fichiers et répertoires à ignorer lors de la recherche de fichiers source.
  # Ce modèle affecte également html_static_path et html_extra_path.
  exclude_patterns = ['.env']

  # Une liste de chemins contenant des modèles supplémentaires
  # (ou des modèles qui remplacent les modèles intégrés/spécifiques au thème).
  # Les chemins relatifs sont considérés comme relatifs au répertoire de
  # configuration.
  templates_path = ['_templates']
  

L'apparence des documents
=========================
  
Le thème HTML par défaut, Alabaster, est très minimaliste.

.. image:: images/documentation_1.png
   :alt: Thème par défaut Alabaster
   :align: center
   :scale: 100%

Pour avoir une documentation plus sexy, il est parfois préférable de changer le thème par défaut.

Changer le thème HTML de votre documentation
--------------------------------------------

Il existe un certain nombre de thèmes HTML intégrés à Sphinx, et de nombreux autres sont disponibles. Vous pouvez consulter les thèmes disponibles sur le site https://sphinx-themes.org/ .

On va donc voir comment installer le nouveau thème **sphinx-book-theme** (mais libre à vous d'en choisir un autre, le principe reste le même).

Le thème sphinx book
^^^^^^^^^^^^^^^^^^^^

.. image:: images/documentation_2.png
   :alt: Thème Sphinx Book
   :align: center
   :scale: 100%

Pour utiliser le thème «**Sphinx book**», il faut commencer par l'installer, ce qui peut être fait à l'aide de la commande suivante :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd docs ; sudo pip install sphinx-book-theme
  Collecting sphinx-book-theme
      Downloading sphinx_book_theme-0.1.0-py3-none-any.whl (87 kB)
          |████████████████████████████████| 87 kB 813 kB/s
  Requirement already satisfied: sphinx<4,>=2 in /usr/local/lib/python3.9/dist-packages (from sphinx-book-theme) (3.5.4)
  Requirement already satisfied: click in /usr/lib/python3/dist-packages (from sphinx-book-theme) (7.1.2)
  Requirement already satisfied: docutils>=0.15 in /usr/local/lib/python3.9/dist-packages (from sphinx-book-theme) (0.16)
  Collecting pydata-sphinx-theme~=0.6.0
      Downloading pydata_sphinx_theme-0.6.3-py3-none-any.whl (1.4 MB)
          |████████████████████████████████| 1.4 MB 3.9 MB/s
  Collecting beautifulsoup4<5,>=4.6.1
      Downloading beautifulsoup4-4.9.3-py3-none-any.whl (115 kB)
          |████████████████████████████████| 115 kB 4.4 MB/s
  Requirement already satisfied: pyyaml in /usr/lib/python3/dist-packages (from sphinx-book-theme) (5.3.1)
  Collecting soupsieve>1.2
      Downloading soupsieve-2.2.1-py3-none-any.whl (33 kB)
  Requirement already satisfied: packaging in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (20.9)
  Requirement already satisfied: babel>=1.3 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.9.0)
  Requirement already satisfied: imagesize in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.2.0)
  Requirement already satisfied: sphinxcontrib-qthelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.3)
  Requirement already satisfied: Pygments>=2.0 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.8.1)
  Requirement already satisfied: requests>=2.5.0 in /usr/lib/python3/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.25.1)
  Requirement already satisfied: setuptools in /usr/lib/python3/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (52.0.0)
  Requirement already satisfied: sphinxcontrib-devhelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.2)
  Requirement already satisfied: alabaster<0.8,>=0.7 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (0.7.12)
  Requirement already satisfied: snowballstemmer>=1.1 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.1.0)
  Requirement already satisfied: sphinxcontrib-serializinghtml in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.1.4)
  Requirement already satisfied: sphinxcontrib-htmlhelp in  /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.3)
  Requirement already satisfied: sphinxcontrib-jsmath in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.1)
  Requirement already satisfied: Jinja2>=2.3 in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (2.11.3)
  Requirement already satisfied: sphinxcontrib-applehelp in /usr/local/lib/python3.9/dist-packages (from sphinx<4,>=2->sphinx-book-theme) (1.0.2)
  Requirement already satisfied: pytz>=2015.7 in /usr/local/lib/python3.9/dist-packages (from babel>=1.3->sphinx<4,>=2->sphinx-book-theme) (2021.1)
  Requirement already satisfied: MarkupSafe>=0.23 in /usr/local/lib/python3.9/dist-packages (from Jinja2>=2.3->sphinx<4,>=2->sphinx-book-theme) (1.1.1)
  Requirement already satisfied: pyparsing>=2.0.2 in /usr/local/lib/python3.9/dist-packages (from packaging->sphinx<4,>=2->sphinx-book-theme) (2.4.7)
  Installing collected packages: soupsieve, beautifulsoup4, pydata-sphinx-theme, sphinx-book-theme
  Successfully installed beautifulsoup4-4.9.3 pydata-sphinx-theme-0.6.3 soupsieve-2.2.1 sphinx-book-theme-0.1.0

Ensuite, il faut indiquer à Sphinx d'utiliser ce thème.


Configuration des documents générés en sortie
=============================================

Le HTML
-------

Il faudra remplacer la variable «**html_theme**» dans «**conf.py**» et modifier dans la section HTML les paramètres pour ce thème :

.. code-block:: python

  # -- Options pour sortie HTML -------------------------------------------------
  html_theme = 'sphinx_book_theme'

  html_title = project_title
  html_short_title = project_short_title
  html_logo = project_logo
  html_favicon = project_favicon

  # -- Options du thème
  html_theme_options = {
      # Ajout du renvoie vers Gitlab
      'repository_url': "http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur",
      'use_repository_button': True,
      # Ajout de la possibilité de laisser des retours de dysfonctionnements
      'use_issues_button': True,
      # Ajout de la possibilité de laisser des propositions de corrections à la documentation
      'use_edit_page_button': True,
      # Ajout de la possibilité de travailler sur une branche définie
      #'repository_branch': 'master',
      # Ajout du chemin relatif vers les sources de la doc
      'path_to_docs': "docs/sources-documents",
      # Ajout d'un téléchargement Rest ou PDF de la page actuelle
      'use_download_button': True,
      # Ajout d'un bouton lecture plein écran
      'use_fullscreen_button': True,
      # Ajout de la table des matières dans le panneau latéral gauche
      'home_page_in_toc': False,
      # Titre du panneau latéral droite qui sera notre table des matières
      'toc_title': "Contenu",
      # Ajout au pied de page du panneau latéral gauche
      'extra_navbar': "<p>Version " + version + " " + release + "</p>",
  }

  #html_sidebars = {
  #    "**": ["sbt-sidebar-nav.html", "sbt-sidebar-footer.html"]
  #}

  # Une liste de chemins contenant des fichiers statiques personnalisés
  # (tels que des feuilles de style ou des fichiers de script).
  # Les chemins relatifs sont considérés comme relatifs au répertoire de
  # configuration.
  # Ils sont copiés dans le répertoire \_static de la sortie après les fichiers
  # statiques du thème.
  # Par conséquent, un fichier nommé default.css écrasera le fichier default.css
  # du thème.
  html_static_path = ['_static']

  
LaTeX
-----

.. code-block:: python

  # -- Options pour LaTeX -------------------------------------------------------
  latex_engine = 'xelatex'
  latex_elements = {
      # Le format du papier('letterpaper' ou 'a4paper').
      'papersize': 'a4paper',
      #
      # La taille de la police ('10pt', '11pt' or '12pt').
      'pointsize': '12pt',
      #
      # Trucs supplémentaires pour le préambule LaTeX.
      'preamble': '',
      #
      # Alignement de la figure en LaTeX (flotteur)
      'figure_align': 'htbp',
  }
  # latex_show_urls = 'footnote'
  
  # latex_docclass = {
  #   'howto': 'votreclassededocumentshowto', # Défaut 'article'
  #   'manual': 'votreclassededocumentsmanual', # Défaut 'report'
  # }

  # Regroupement de l'arborescence de documents en fichiers LaTeX. Liste des tuples
  # (fichier source, nom du fichier cible, titre, auteur, documentclass [howto, manuel ou votre classe]).
  latex_documents = [
      (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes.tex', project_title, author, 'manual'),
  ]

  
Pages de manuel
---------------

.. code-block:: python

  # -- Options pour les pages de manuel ----------------------------------------
  # Une entrée par page de manuel. Liste des tuples
  # (fichier source, nom, description, auteurs, section du manuel).
  man_pages = [
      (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes', project_description, [author], 1),
  ]

  
Texinfo
-------

.. code-block:: python

  # -- Options pour Texinfo ---------------------------------------------------
  # Regroupement de l'arborescence des documents dans des fichiers Texinfo.
  # Liste des tuples (fichier source, nom de la cible, titre, auteur, répertoire, description, catégorie)
  texinfo_documents = [
      (master_doc, 'InitiationProgrammationPythonPourAdministrateurSystèmes', project_title, author, 'InitiationProgrammationPythonPourAdministrateurSystèmes', project_description, 'Miscellaneous'),
  ]

  
Epub
----

.. code-block:: python

  # -- Options pour les Epub ---------------------------------------------------
  # Informations bibliographiques Dublin Core.
  epub_title = project_title
  # L'identifiant unique du texte. Cela peut être un numéro ISBN
  # ou la page d'accueil du projet.
  # epub_identifier = ''

  # Une identification unique pour le texte.
  # epub_uid = ''

  # A list of files that should not be packed into the epub file.
  epub_exclude_files = ['search.html']


Inclure les extensions de documentation utiles pour Python
==========================================================

Installation des extensions non incluses de base :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install sphinx-intl texinfo xindy graphviz latexmk texlive-lang-french texlive-xetex fonts-freefont-otf ; sudo pip install sphinxcontrib-inlinesyntaxhighlight sphinx-copybutton sphinx-markdown-builder sphinx-tabs pbr
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo pip install --upgrade sphinx wget https://github.com/mans0954/odfbuilder/releases/download/0.0.1/sphinxcontrib-odfbuilder-0.0.1.tar.gz
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo pip install sphinxcontrib-odfbuilder

Il existe de nombreuses extensions intégrés à Sphinx, je vous présente ici celles qui me paraissent les plus utiles :

-  `sphinx.ext.intersphinx <https://www.sphinx-doc.org/fr/master/usage/extensions/intersphinx.html>`_ : générer des liens automatiques dans la documentation suivant des mots clés.
-  `sphinx.ext.extlinks <https://www.sphinx-doc.org/en/master/usage/extensions/extlinks.html>`_ : Fournit des alias aux URL de base de votre documentation, de sorte que vous n’avez qu’à donner le nom d’alias pour la création du lien dans votre documentation.
-  `sphinx.ext.autodoc <https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html>`_ : Insère automatiquement les docstrings des fonctions, des classes ou des modules Python dans votre documentation. Permet de construire directement de la documentation à partir de votre code Python.
-  `sphinxcontrib.inlinesyntaxhighlight <https://sphinxcontrib-inlinesyntaxhighlight.readthedocs.io/en/latest/>`_ : Insère du code d'un langage de programmation dans une ligne de texte.
-  `sphinxcontrib-bibtex <https://sphinxcontrib-bibtex.readthedocs.io/en/latest/>`_ : Insère des références bibliographiques.
-  `sphinx.ext.todo <https://www.sphinx-doc.org/en/master/usage/extensions/todo.html>`_ : Insère des taches, ou liste de taches à faire dans votre documentation.
-  `sphinx.ext.githubpages <https://www.sphinx-doc.org/en/master/usage/extensions/githubpages.html>`_ : Cette extension créée un fichier **.nojekyll** dans le répertoire HTML généré pour publier le document sur les pages GitHub.
-  `sphinx.ext.imgmath <https://bizdd.readthedocs.io/en/latest/ext/math.html>`_ : Insère des formules mathématiques dans votre documentation.
-  `sphinx.ext.graphviz <https://www.sphinx-doc.org/en/master/usage/extensions/graphviz.html>`_ : Cette extension vous permet d'intégrer des `graphiques Graphviz <https://cyberzoide.developpez.com/graphviz/>`_ dans vos documents.
-  `sphinxcontrib-svg2pdfconverter <https://pypi.org/project/sphinxcontrib-svg2pdfconverter/>`_ : Pour gérer les images SVG en LaTeX pour le PDF.
-  `sphinx.ext.inheritance_diagram <https://www.sphinx-doc.org/en/master/usage/extensions/inheritance.html>`_ : Cette extension vous permet d'inclure des diagrammes d'héritage, rendus via l'extension Graphviz.
-  `sphinx_copybutton <https://sphinx-copybutton.readthedocs.io/en/latest/>`_ : insère dans votre documentation une icône pour copier dans le presse-papiers le contenu de la directive.
-  `hieroglyph <https://pvbookmarks.readthedocs.io/en/latest/documentation/doc_generators/sphinx/contributed_extensions/hieroglyph.html>`_ : Permet de générer des slides de présentations.
-  `sphinx.ext.ifconfig <https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html>`_ : Inclure le contenu de la directive uniquement si l’expression Python donnée en argument est :python:`True`.
-  `sphinx.ext.doctest <https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html>`_ : Cette extension vous permet de tester un code Python dans la documentation. Le constructeur doctest collectera le résultat et pourra agir suivant les retours d’exécutions obtenus.
-  `sphinx_markdown_builder <https://pypi.org/project/sphinx-markdown-builder/>`_ : Pour construire de la documentation au format markdown pour gitlab et créer les formats .odt ou .docx.

Ajout des extensions utiles pour Python et Sphinx
-------------------------------------------------

.. code-block:: python

  # -- Configuration des extensions ---------------------------------------------
  
  extensions = [
      'sphinx.ext.intersphinx',
      'sphinx.ext.extlinks',
      'sphinx.ext.autodoc',
      'sphinxcontrib.inlinesyntaxhighlight',
      'sphinx.ext.githubpages',
      'sphinx.ext.graphviz',
      'sphinxcontrib.cairosvgconverter',
      'sphinx.ext.inheritance_diagram',
      'sphinx_copybutton',
      'sphinx.ext.tabs',
      'sphinx.ext.todo',
      'sphinx.ext.ifconfig',
      'sphinx.ext.doctest',
      'sphinx_markdown_builder',
      'sphinxcontrib-odfbuilder',
      
  ]

  
Configuration des extensions
----------------------------

Intersphinx
^^^^^^^^^^^

.. code-block:: python

  # -- Options pour intersphinx -------------------------------------------------
  # Alias du lien de la documentation de Python 3
  intersphinx_mapping = {'python': ('https://docs.python.org/fr/3/', None)}
  # Utilisation restructuredtext:
  # index de page WEB
  # :ref:`python:reference-index`
  # :ref:`Référence langage Python <python:reference-index>`
  # lien WEB
  # :doc:`python:library/enum`
  # :doc:`Énumérations <python:library/enum>`
  

Extlinks
^^^^^^^^

.. code-block:: python

  # -- Options pour extlinks ----------------------------------------------------
  # Liens vers la documentation de Python 3
  extlinks = {
      'docpython3': ('https://docs.python.org/fr/3/%s', 'Python'),
      'manpython3': ('https://docs.python.org/fr/3/library/%s.html', 'Manuel Python de '),
  }
  # utilisation restructuredtext :docpython3:`tutorial` ou :manpython3:`enum`
  

Autodoc
^^^^^^^

.. code-block:: python

  # -- Options pour autodoc -----------------------------------------------------
  # Simule l'existence du module classes de python pour ne pas être bloqué
  autodoc_mock_imports = ['classes']
  

Inline Syntax highlight
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

  # -- Options pour inline syntax highlight --------------------------------------
  # Langage défini par la directive de surbrillance si aucun langage n'est défini par le rôle
  inline_highlight_respect_highlight = False
  # Langage défini par la directive de surbrillance si aucun rôle n'est défini
  inline_highlight_literals = False
  

Graphviz
^^^^^^^^

.. code-block:: python

  # -- Options pour Graphviz ----------------------------------------------------
  graphviz_output_format = 'svg'
  # utilisation restructuredtext :
  # .. graphviz::
  #
  #     digraph frameworks web python {
  #         python [label="python", href="https://www.python.org/", target="_top"];
  #         flask [label="Flask", href="https://flask.palletsprojects.com/en/1.1.x/", target="_top"];
  #         django [label="Bjango", href="https://www.djangoproject.com/", target="_top"];
  #         bottle [label="Bottle", href="https://bottlepy.org/", target="_top"];
  #         turbogears [label="TurboGears", href="https://turbogears.org/", target="_top"];
  #         web2py [label="web2py", href="http://www.web2py.com/", target="_top"];
  #         cherrypy [label="CherryPy", href="https://cherrypy.org/", target="_top"];
  #         quixote [label="Quixote", href="http://quixote.ca/", target="_top"];
  #         python -> {flask; django; bottle; turbogears; web2py; cherrypy; quixote;};
  #     }
  

Copybutton
^^^^^^^^^^

.. code-block:: python

  # -- Options pour copybutton ----------------------------------------------------
  # copybutton_prompt_text = '>>> '
  

Tabs
^^^^

.. code-block:: python

  # -- Options pour tabs ----------------------------------------------------
  # utilisation restructuredtext :
  # .. tabs::
  #     .. tab:: Python
  #         Ma documentation sur Python
  #         .. tabs::
  #             .. code-tab:: py
  #                 Fichier Python main.py
  #             .. code-tab:: java
  #                 Fichier java
  #         .. tabs::
  #             .. code-tab:: py
  #                 def main():
  #                     return
  #             .. code-tab:: java
  #                 class Main {
  #                     public static void main(String[] args) {
  #                     }
  #                 }
  #     .. tab:: Frameworks Python
  #         .. tabs::
  #             .. group-tab:: Flask
  #                 Ma documentation sur Flask
  #             .. group-tab:: Django
  #                 Ma documentation sur Django
  #         .. tabs::
  #             .. group-tab:: Flask
  #                 Le code d'exemple pour Flask
  #             .. group-tab:: Django
  #                 Le code d'exemple pour Django*

  
Ifconfig
^^^^^^^^

.. code-block:: python

  # -- Options pour ifconfig  ----------------------------------------------------
  def setup(app):
      app.add_config_value('niveau_developpement', 'Alpha', True)
  # utilisation restructuredtext :
  # .. ifconfig:: niveau_developpement not in ('Pre-alpha', 'Alpha', 'Beta', 'Release candidate', 'Stable release')
  #     En production
  # .. ifconfig:: 'Alpha' == niveau_developpement
  #     En developpement
  # .. ifconfig:: 'Beta' == niveau_developpement
  #     En test
  # .. ifconfig:: 'Release candidate' == niveau_developpement
  #     En qualification
  # .. ifconfig:: ''End of life'' == niveau_developpement
  #     Obsolète

  
Générer la documentation
========================

**Attention des accents dans les noms de fichiers .rst font planter LaTeX.**

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make html
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latex
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make latexpdf
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make epub
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make text
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make markdown
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make xml
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make man
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make texinfo
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make info
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make odt

Générer les formats odt ou docx
-------------------------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sudo apt install pandoc
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ cd documentation/html ; pandoc ./index.html -o ../../InitiationProgrammationPythonPourAdministrateurSystèmes.odt
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs/documentation/html$ pandoc ./index.html -o ../../InitiationProgrammationPythonPourAdministrateurSystèmes.docx

Générer l’internationalisation
------------------------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make gettext
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ sphinx-intl update -p documentation/gettext -l fr -l en

Traduire les fichiers dans **./locales/<lang>/LC_MESSAGES/**

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/docs$ make -e SPHINXOPTS="-Dlanguage='en'" html
