.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

La qualité du code
##################

Vous avez écrit votre code de votre projet, mais avez vous :

- utilisé des modules odsolètes ?
- respecté les standards d’écriture Python définis dans les spécifications de la **PEP 8** ?

Pour cela, en plus de la commande :python:`python3 -Wd` pour vérifier l'obsolescence du code, nous avons plusieurs outils **Pylint**, **pyflakes**, **pychecker**, **pep8** ou **flake8**, qui permettent de vérifier la conformité de votre code avec la **PEP 8**.

Dans ce cours nous allons utiliser **Pylint**.

Voyons comment fonctionne cet outil ?

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install pylint
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint 1_Mode_interprété/mon_1er_programme.py
  ************ Module mon_1er_programme
  1_Mode_interprété/mon_1er_programme.py:1:0: C0114: Missing module docstring (missing-module-docstring)
  -----------------------------------
  Your code has been rated at 0.00/10


Comment éviter un message d'erreur de documentation lorsque l'on ne veut pas de documentation dans son code ?

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint --disable=missing-module-docstring
  1_Mode_interprété/mon_1er_programme.py
  ---------------------
  Your code has been rated at 10.00/10 (previous run: 0.00/10, +10.00)


Comment éviter des fichiers, ou répertoires, que l'on ne veut pas tester avec :terminal:`pylint` ?

Nous allons d'abord tester dans un terminal la bonne remontée des fichiers à tester pour :terminal:`pylint` avec un script shell.

Créer un fichier **choix-fichiers-a-tester**.

.. code-block:: bash

  #! /usr/bin/env bash

  find -type f -name "*.py" ! -path "*1_Mode_interprété*" ! -path "*2_Debug*" ! -path "*3_Interpreteur_alerts*" ! -path "*4_Passage_paramètres*" ! -path "*5_Niveau_journalisation*" ! -path "*6_Chaines_split_join*" ! -path "*7_Modules*" ! -path "*/.env/*" ! -path "*docs*"

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ chmod u+x choix-fichiers-a-tester ; ./choix-fichiers-a-tester
  ./Documentation/mon_module.py

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ pylint $(find -type f -name "*.py" ! -path "*1_Mode_interprété*" ! -path "*2_Debug*" ! -path "*3_Interpreteur_alerts*" ! -path "*4_Passage_paramètres*" ! -path "*5_Niveau_journalisation*" ! -path "*6_Chaines_split_join*" ! -path "*7_Modules*" ! -path "*/.env/*" ! -path "*docs*")
  ************ Module mon_module
  Documentation/mon_module.py:72:59: C0303: Trailing whitespace (trailing-whitespace)
  Documentation/mon_module.py:127:31: C0303: Trailing whitespace (trailing-whitespace)
  Documentation/mon_module.py:19:0: R0205: Class 'ClasseExemple' inherits from object, can be safely removed from bases in python3 (useless-object-inheritance)
  Documentation/mon_module.py:79:4: R0201: Method could be a function (no-self-use)
  Documentation/mon_module.py:19:0: R0903: Too few public methods (1/2) (too-few-public-methods)

  ------------------------------------------------------------------
  Your code has been rated at 5.00/10

Nous allons maintenant voir comment mettre ces tests d'obsolescence et de qualité du code dans **GitLab**.

Test de l’environnement Python dans Gitlab
******************************************

Ici nous allons tester le déploiement de l'environnement **Python 3** pour notre code, avec le déploiement d'une page de documentation.

Modifier **.gitlab-ci.yml** :

.. code-block:: yaml

  image: python:latest

  stages:
    - build
    - deploy

  construction-environnement:
    stage: build
    script:
      - echo "Bonjour $GITLAB_USER_LOGIN !"
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules python"
      - pip install -U -r requirements.txt
    only:
      - master

  pages:
    stage: deploy
    script:
      - echo "$GITLAB_USER_LOGIN déploiement de la documentation"
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat docs-packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules python"
      - pip install -U -r docs-requirements.txt
      - echo "** Génération des diagrammes de classes **"
      - ./makediagrammes
      - echo "** Génération de la documentation HTML **"
      - sphinx-build -b html ./docs/sources-document public
    artifacts:
      paths:
        - public
    only:
      - master

.. image:: images/QualitéCode_1.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_2.png
   :alt: Tâche pages OK
   :align: center
   :scale: 100%


Test de l'obsolescence du code Python dans Gitlab
*************************************************

Ici nous allons utiliser la commande :terminal:`python3 -Wd` pour générer une image d’obsolescence du code. Nous allons le tester avec le fichier «**3_Interpreteur_alerts/monscript.py**».

Modifier le fichier **.gitlab-ci.yml**. Voici à quoi ressemble le fichier **.gitlab-ci.yml** pour ce projet :

.. code-block:: yaml

  image: python:latest

  stages:
    - build
    - Static Analysis
    - deploy

  construction-environnement:
    stage: build
    script:
      - echo "Bonjour $GITLAB_USER_LOGIN !"
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules python"
      - pip install -U -r requirements.txt
    only:
      - master

  obsolescence-code:
    stage: Static Analysis
    allow_failure: true
    script:
      - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
      - python3 -Wd Documentation/mon_module.py &2> /tmp/output.txt
      - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
      - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

  pages:
    stage: deploy
    before_script:
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat docs-packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules python"
      - pip install -U -r docs-requirements.txt
      - echo "Création de l’infrastructure pour l'obsolescence du code"
      - mkdir -p public/obsolescence public/badges
      - echo undefined > public/obsolescence/obsolescence.score
    script:
      - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
      - python3 -Wd Documentation/mon_module.py &2> /tmp/output.txt
      - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
      - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
      - \[ -s /tmp/obsolescence.txt \] && echo oui > public/obsolescence/obsolescence.score || echo non > public/obsolescence/obsolescence.score
      - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
      - echo "Génération des diagrammes de classes"
      - ./makediagrammes
      - echo "Création du logo SVG d'obsolescence de code"
      - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
      - echo "Génération de la documentation HTML"
      - sphinx-build -b html ./docs/sources-document public
    artifacts:
      paths:
        - public
    only:
      - master


Modifier le fichier **repertoire_de_developpement/docs-requirements.txt**.

Et ajouter à la fin du fichier :

.. code-block:: rest

  anybadge


Modifier le fichier **repertoire_de_developpement/docs/sources-documents/index.rst**.

.. code-block:: rest

  .. |date| date::

  :Date: |date|
  :Revision: 1.0
  :Author: Prénom NOM <prénom.nom@fai.fr>
  :Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
  :Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

  .. toctree::
     :maxdepth: 2
     :caption: Contenu

  .. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst


  .. only:: html

    .. image:: ./badges/obsolescence.svg
       :alt: Obsolescence du code Python
       :align: left
       :width: 200px

  Modules
  *******

  .. automodule:: Documentation.mon_module
     :members:

Déployez les fichiers dans gitlab.

.. image:: images/QualitéCode_3.png
   :alt: Tâche construction environnement début
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_4.png
   :alt: Log construction environnement
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_5.png
   :alt: Tâche qualité du code début
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_6.png
   :alt: Tâche qualité du code en cours
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_7.png
   :alt: Log tâche qualité du code
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_8.png
   :alt: Tâche qualité du code en erreur et début tâche pages
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_9.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_10.png
   :alt: Log tâche pages
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_11.png
   :alt: Tâche pages OK avec qualité du code en erreur
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_12.png
   :alt: Pages HTML de la documentation générée avec la qualité du code
   :align: center
   :scale: 100%


Test de qualité du code dans Gitlab
***********************************

Ici nous allons tester avec :terminal:`pylint` la conformance du code de votre projet avec le standard **PEP 8**.

Voici à quoi ressemble le fichier **.gitlab-ci.yml** pour ce projet :

.. code-block:: yaml

  image: python:latest

  stages:
    - build
    - Static Analysis
    - deploy

  construction-environnement:
    stage: build
    script:
      - echo "Bonjour $GITLAB_USER_LOGIN !"
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules python"
      - pip install -U -r requirements.txt
    only:
      - master

  obsolescence-code:
    stage: Static Analysis
    allow_failure: true
    script:
      - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
      - python3 -Wd Documentation/mon_module.py &2> /tmp/output.txt
      - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
      - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

  qualité-du-code:
    stage: Static Analysis
    allow_failure: true
    before_script:
      - echo "Installation de Pylint"
      - pip install -U pylint-gitlab
    script:
      - echo "$GITLAB_USER_LOGIN test de la qualité du code"
      - pylint --output-format=text $(bash choix-fichiers-a-tester) | tee /tmp/pylint.txt
    after_script:
      - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > pylint.score
      - echo "Votre score de qualité de code Pylint est de $(cat pylint.score)"

  pages:
    stage: deploy
    before_script:
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat docs-packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules python"
      - pip install -U -r docs-requirements.txt
      - echo "Création de l’infrastructure pour l'obsolescence et la qualité de code"
      - mkdir -p public/obsolescence public/quality public/badges public/pylint
      - echo undefined > public/obsolescence/obsolescence.score
      - echo undefined > public/quality/pylint.score
      - pip install -U pylint-gitlab
    script:
      - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
      - python3 -Wd Documentation/mon_module.py &2> /tmp/output.txt
      - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
      - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
      - \[ -s /tmp/obsolescence.txt \] && echo oui > public/obsolescence/obsolescence.score || echo non > public/obsolescence/obsolescence.score
      - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
      - pylint --exit-zero --output-format=text $(bash choix-fichiers-a-tester) | tee /tmp/pylint.txt
      - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > public/quality/pylint.score
      - echo "Votre score de qualité de code Pylint est de $(cat public/quality/pylint.score)"
      - echo "Création du rapport HTML de qualité de code"
      - pylint --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter $(bash choix-fichiers-a-tester) > public/pylint/index.html
      - echo "Génération des diagrammes de classes"
      - ./makediagrammes
      - echo "Création du logo SVG d'obsolescence de code"
      - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
      - echo "Création du logo SVG de qualité de code"
      - anybadge --overwrite --label "Qualité du code avec Pylint" --value=$(cat public/quality/pylint.score) --file=public/badges/pylint.svg 4=red 6=orange 8=yellow 10=green
      - echo "Génération de la documentation HTML"
      - sphinx-build -b html ./docs/sources-document public
    artifacts:
      paths:
        - public
    only:
      - master

Modifier le fichier **repertoire_de_developpement/docs/sources-documents/index.rst**.

.. code-block:: rest

  .. |date| date::

  :Date: |date|
  :Revision: 1.0
  :Author: Prénom NOM <prénom.nom@fai.fr>
  :Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
  :Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

  .. toctree::
     :maxdepth: 2
     :caption: Contenu

  .. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst


  .. only:: html

    .. image:: ./badges/obsolescence.svg
       :alt: Obsolescence du code Python
       :align: left
       :width: 200px

    .. image:: ./badges/pylint.svg
       :alt: Cliquez pour voir le rapport
       :align: left
       :width: 200px
       :target: ./pylint/index.html


  ----


  Modules
  *******

  .. automodule:: Documentation.mon_module
     :members:

Déployez les fichiers dans gitlab.


.. image:: images/QualitéCode_13.png
   :alt: Tâche construction environnement début
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_14.png
   :alt: Log construction environnement
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_15.png
   :alt: Tâche qualité du code début
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_16.png
   :alt: Tâche qualité du code en cours
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_17.png
   :alt: Log tâche qualité du code
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_18.png
   :alt: Tâche qualité du code en erreur et début tâche pages
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_19.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_20.png
   :alt: Log tâche pages
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_21.png
   :alt: Tâche pages OK avec qualité du code en erreur
   :align: center
   :scale: 100%

.. image:: images/QualitéCode_22.png
   :alt: Pages HTML de la documentation générée avec la qualité du code
   :align: center
   :scale: 100%


.. only:: latex

  .. raw:: latex

    \newpage
