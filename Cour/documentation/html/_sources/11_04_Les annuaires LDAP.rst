.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Les annuaires LDAP
******************

Installation d'un annuaire LDAP
===============================

Nous allons utiliser l'annuaire :terminal:`slapd`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ sudo apt install slapd

.. image:: images/LDAP_Install_1.png
   :alt: Demande mot de passe
   :align: center
   :scale: 100%

.. image:: images/LDAP_Install_2.png
   :alt: Confirmation mot de passe
   :align: center
   :scale: 100%

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ sudo ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config dn:
  dn: cn=config
  dn: cn=module{0},cn=config
  dn: cn=schema,cn=config
  dn: cn={0}core,cn=schema,cn=config
  dn: cn={1}cosine,cn=schema,cn=config
  dn: cn={2}nis,cn=schema,cn=config
  dn: cn={3}inetorgperson,cn=schema,cn=config
  dn: olcDatabase={-1}frontend,cn=config
  dn: olcDatabase={0}config,cn=config
  dn: olcDatabase={1}mdb,cn=config
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ sudo ldapsearch -xLLL -H ldap:/// -b dc=domaine-perso,dc=fr dn
  dn: dc=domaine-perso,dc=fr


Remplir l'annuaire LDAP
-----------------------

Créer le fichier «**modèle.ldif**».

.. code-block:: ldif

  # fichier de données : ~/repertoire_de_developpement/12_Données/modèle.ldif
  dn: ou=Utilisateurs,dc=domaine-perso,dc=fr
  objectClass: organizationalUnit
  ou: Utilisateurs

  dn: ou=Groupes,dc=domaine-perso,dc=fr
  objectClass: organizationalUnit
  ou: Groupes

  dn: cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr
  objectClass: posixGroup
  cn: developpeurs
  gidNumber: 5000

  dn: uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr
  objectClass: inetOrgPerson
  objectClass: posixAccount
  objectClass: shadowAccount
  uid: Prenom
  sn: NOM
  givenName: Prénom
  cn: Prénom NOM
  displayName: Prénom NOM
  uidNumber: 10000
  gidNumber: 5000
  userPassword: prenom.nom
  gecos: Prenom NOM
  loginShell: /bin/bash
  homeDirectory: /home/prenom-nom


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ ldapadd -x -D cn=admin,dc=domaine-perso,dc=fr -W -f ./modèle.ldif
  Enter LDAP Password:
  adding new entry "ou=Utilisateurs,dc=domaine-perso,dc=fr"
  adding new entry "ou=Groupes,dc=domaine-perso,dc=fr"
  adding new entry "cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr"
  adding new entry "uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr"
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ ldapsearch -xLLL -b dc=domaine-perso,dc=fr cn
  dn: dc=domaine-perso,dc=fr
  dn: ou=Utilisateurs,dc=domaine-perso,dc=fr
  dn: ou=Groupes,dc=domaine-perso,dc=fr
  dn: cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr
  cn: developpeurs
  dn: uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr
  cn:: UHLDqW5vbSBOT00=
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ ldapsearch -xLLL -b dc=domaine-perso,dc=fr ou
  dn: dc=domaine-perso,dc=fr
  dn: ou=Utilisateurs,dc=domaine-perso,dc=fr
  ou: Utilisateurs
  dn: ou=Groupes,dc=domaine-perso,dc=fr
  ou: Groupes
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ ldapsearch -xLLL -b dc=domaine-perso,dc=fr 'uid=Prenom' cn gidNumber
  dn: uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr
  cn:: UHLDqW5vbSBOT00=
  gidNumber: 5000


Module Python LDAP
==================

Installation
------------

Installation les modules Python :python:`ldap` et :python:`ldap3`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ sudo apt install python3-ldap python3-ldap3

Ajout des outils de la documentation

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo pip install pygments-ldif

Modifier le fichier «**docs-requirements.txt**»

.. code-block:: text

  sphinx
  sphinx-intl
  sphinxcontrib-inlinesyntaxhighlight
  sphinx_copybutton
  sphinx-tabs
  sphinx_markdown_builder
  sphinx-book-theme
  anybadge
  pygments-ldif


Se connecter au serveur LDAP
----------------------------

**Avec le module ldap**

.. code-block:: pycon

  >>> import ldap
  >>> try:
  ...     connexion = ldap.initialize('ldap://localhost')
  ...     connexion.set_option(ldap.OPT_REFERRALS, 0)
  ...     connexion.simple_bind_s('cn=admin,dc=domaine-perso,dc=fr', 'motdepasse')
  ... except ldap.LDAPError:
  ...     print('Erreur LDAP')
  ...
  (97, [], 1, [])


**Avec le module ldap3**

.. code-block:: pycon

  >>> from ldap3 import Server, Connection, SAFE_SYNC
  >>> serveur = Server('localhost')
  >>> connexion = Connection(serveur, 'cn=admin,dc=domaine-perso,dc=fr', 'motdepasse', client_strategy=SAFE_SYNC, auto_bind=True)
  >>> connexion.extend.standard.who_am_i()
  'dn:cn=admin,dc=domaine-perso,dc=fr'


Lire des entrées LDAP
---------------------

Lire les données LDAP
^^^^^^^^^^^^^^^^^^^^^

**Avec le module ldap**

.. code-block:: pycon

  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(objectclass=*)')
  [('dc=domaine-perso,dc=fr', {'objectClass': [b'top', b'dcObject', b'organization'], 'o': [b'domaine-perso.fr'], 'dc': [b'domaine-perso']}), ('ou=Utilisateurs,dc=domaine-perso,dc=fr', {'objectClass': [b'organizationalUnit'], 'ou': [b'Utilisateurs']}), ('ou=Groupes,dc=domaine-perso,dc=fr', {'objectClass': [b'organizationalUnit'], 'ou': [b'Groupes']}),
  ('cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr', {'objectClass': [b'posixGroup'], 'cn': [b'developpeurs'], 'gidNumber': [b'5000']}), ('uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'objectClass': [b'inetOrgPerson', b'posixAccount', b'shadowAccount'], 'uid': [b'Prenom', b'prenom.nom'], 'sn': [b'NOM'], 'givenName': [b'Pr\xc3\xa9nom'], 'cn': [b'Pr\xc3\xa9nom NOM'], 'displayName': [b'Pr\xc3\xa9nom NOM'], 'uidNumber': [b'10000'], 'gidNumber': [b'5000'], 'userPassword': [b'prenom.nom'], 'gecos': [b'Prenom NOM'], 'loginShell': [b'/bin/bash'], 'homeDirectory': [b'/home/prenom-nom']})]
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(uid=Prenom)')
  [('uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'objectClass': [b'inetOrgPerson', b'posixAccount', b'shadowAccount'], 'uid': [b'Prenom', b'prenom.nom'], 'sn': [b'NOM'], 'givenName': [b'Pr\xc3\xa9nom'], 'cn': [b'Pr\xc3\xa9nom NOM'], 'displayName': [b'Pr\xc3\xa9nom NOM'], 'uidNumber': [b'10000'], 'gidNumber': [b'5000'], 'userPassword': [b'prenom.nom'], 'gecos': [b'Prenom NOM'], 'loginShell': [b'/bin/bash'], 'homeDirectory': [b'/home/prenom-nom']})]
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(uid=Prenom)', ['cn', 'gidNumber'])
  [('uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'cn': [b'Pr\xc3\xa9nom NOM'], 'gidNumber': [b'5000']})]

**Avec le module ldap3**

.. code-block:: pycon

  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(objectclass=*)')
  >>> print(statut)
  True
  >>> print(resultat)
  {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'searchResDone'}
  >>> print(reponse)
  [{'raw_dn': b'dc=domaine-perso,dc=fr', 'dn': 'dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'ou=Groupes,dc=domaine-perso,dc=fr', 'dn': 'ou=Groupes,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr', 'dn': 'cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}]
  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(uid=Prenom)')
  >>> print(reponse)
  [{'raw_dn': b'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}]
  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(uid=Prenom)', attributes=['cn', 'gidNumber'])
  >>> print(reponse)
  [{'raw_dn': b'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {'cn': [b'Pr\xc3\xa9nom NOM'], 'gidNumber': [b'5000']}, 'attributes': {'cn': ['Prénom NOM'], 'gidNumber': 5000}, 'type': 'searchResEntry'}]


Lire le schema LDAP
^^^^^^^^^^^^^^^^^^^

**Avec le module ldap**

.. code-block:: pycon

  >>> import ldap.schema as schema
  >>> resultat = connexion.search_s('cn=subschema', ldap.SCOPE_BASE, '(objectclass=*)', ['*','+'] )
  >>> entree_schemas = resultat[0]
  >>> sousentree_schemas = ldap.cidict.cidict(entree_schemas[1])
  >>> sousschemas = schema.SubSchema(sousentree_schemas)
  >>> objets_oids = sousschemas.listall(schema.models.ObjectClass)
  >>> classesobjets = []
  >>> for oid in objets_oids:
  ...     classesobjets.append(sousschemas.get_obj(schema.models.ObjectClass, oid).names[0])
  ...
  >>> classesobjets
  ['top', 'extensibleObject', 'alias', 'referral', 'OpenLDAProotDSE', 'subentry', 'subschema', 'dynamicObject', 'olcConfig', 'olcGlobal', 'olcSchemaConfig', 'olcBackendConfig', 'olcDatabaseConfig', 'olcOverlayConfig', 'olcIncludeFile', 'olcFrontendConfig', 'olcModuleList', 'olcLdifConfig', 'olcMdbConfig', 'country', 'locality', 'organization', 'organizationalUnit', 'person', 'organizationalPerson', 'organizationalRole', 'groupOfNames', 'residentialPerson', 'applicationProcess', 'applicationEntity', 'dSA', 'device', 'strongAuthenticationUser', 'certificationAuthority', 'groupOfUniqueNames', 'userSecurityInformation', 'certificationAuthority-V2', 'cRLDistributionPoint', 'dmd', 'pkiUser', 'pkiCA', 'deltaCRL', 'labeledURIObject', 'simpleSecurityObject', 'dcObject', 'uidObject', 'pilotPerson', 'account', 'document', 'room', 'documentSeries', 'domain', 'RFC822localPart', 'dNSDomain', 'domainRelatedObject', 'friendlyCountry', 'pilotOrganization', 'pilotDSA', 'qualityLabelledData', 'posixAccount', 'shadowAccount', 'posixGroup', 'ipService', 'ipProtocol', 'oncRpc', 'ipHost', 'ipNetwork', 'nisNetgroup', 'nisMap', 'nisObject', 'ieee802Device', 'bootableDevice', 'inetOrgPerson']
  >>> schema_ou = sousschemas.get_obj(schema.models.ObjectClass, 'organizationalUnit')
  >>> schema_ou.names
  ('organizationalUnit',)
  >>> schema_ou.oid
  '2.5.6.5'
  >>> schema_ou.desc
  'RFC2256: an organizational unit'
  >>> schema_ou.sup
  ('top',)
  >>> schema_ou.must
  ('ou',)
  >>> schema_ou.may
  ('userPassword', 'searchGuide', 'seeAlso', 'businessCategory', 'x121Address', 'registeredAddress', 'destinationIndicator', 'preferredDeliveryMethod', 'telexNumber', 'teletexTerminalIdentifier', 'telephoneNumber', 'internationaliSDNNumber', 'facsimileTelephoneNumber', 'street', 'postOfficeBox', 'postalCode', 'postalAddress', 'physicalDeliveryOfficeName', 'st', 'l', 'description')
  >>> schema_ou.token_defaults
  {'NAME': (), 'DESC': (None,), 'OBSOLETE': None, 'SUP': (), 'STRUCTURAL': None, 'AUXILIARY': None, 'ABSTRACT': None, 'MUST': (), 'MAY': (), 'X-ORIGIN': ()}


**Avec le module ldap3**

.. code-block:: pycon

  >>> serveur.schema.object_classes.keys()
  dict_keys(['top', 'extensibleObject', 'alias', 'referral', 'OpenLDAProotDSE', 'subentry', 'subschema', 'dynamicObject', 'olcConfig', 'olcGlobal', 'olcSchemaConfig', 'olcBackendConfig', 'olcDatabaseConfig', 'olcOverlayConfig', 'olcIncludeFile', 'olcFrontendConfig', 'olcModuleList', 'olcLdifConfig', 'olcMdbConfig', 'country', 'locality', 'organization', 'organizationalUnit', 'person', 'organizationalPerson', 'organizationalRole', 'groupOfNames', 'residentialPerson', 'applicationProcess', 'applicationEntity', 'dSA', 'device', 'strongAuthenticationUser', 'certificationAuthority', 'groupOfUniqueNames', 'userSecurityInformation', 'certificationAuthority-V2', 'cRLDistributionPoint', 'dmd', 'pkiUser', 'pkiCA', 'deltaCRL', 'labeledURIObject', 'simpleSecurityObject', 'dcObject', 'uidObject', 'pilotPerson', 'account', 'document', 'room', 'documentSeries', 'domain', 'RFC822localPart', 'dNSDomain', 'domainRelatedObject', 'friendlyCountry', 'pilotOrganization', 'pilotDSA', 'qualityLabelledData', 'posixAccount', 'shadowAccount', 'posixGroup', 'ipService', 'ipProtocol', 'oncRpc', 'ipHost', 'ipNetwork', 'nisNetgroup', 'nisMap', 'nisObject', 'ieee802Device', 'bootableDevice', 'inetOrgPerson'])
  >>> serveur.schema.object_classes['organizationalUnit']
  Object class: 2.5.6.5
    Short name: organizationalUnit
    Description: RFC2256: an organizational unit
    Type: Structural
    Superior: top
    Must contain attributes: ou
    May contain attributes: userPassword, searchGuide, seeAlso, businessCategory, x121Address, registeredAddress, destinationIndicator, preferredDeliveryMethod, telexNumber, teletexTerminalIdentifier, telephoneNumber, internationaliSDNNumber, facsimileTelephoneNumber, street, postOfficeBox, postalCode, postalAddress, physicalDeliveryOfficeName, st, l, description
    OidInfo: ('2.5.6.5', 'OBJECT_CLASS', 'organizationalUnit', 'RFC4519')
  >>> serveur.schema.object_classes['organizationalUnit'].raw_definition
  "( 2.5.6.5 NAME 'organizationalUnit' DESC 'RFC2256: an organizational unit' SUP top STRUCTURAL MUST ou MAY ( userPassword $ searchGuide $ seeAlso $ businessCategory $ x121Address $ registeredAddress $ destinationIndicator $ preferredDeliveryMethod $ telexNumber $ teletexTerminalIdentifier $ telephoneNumber $ internationaliSDNNumber $ facsimileTelephoneNumber $ street $ postOfficeBox $ postalCode $ postalAddress $ physicalDeliveryOfficeName $ st $ l $ description ) )"


Écrire des entrées LDAP
-----------------------

**Avec le module ldap**

.. code-block:: pycon

  >>> import ldap.modlist as modlist
  >>> dn = 'ou=cour-python,dc=domaine-perso,dc=fr'
  >>> operation = {'ou': [b'cour-python'], 'objectClass': [b'organizationalunit']}
  >>> connexion.add_s(dn, modlist.addModlist(operation))
  (105, [], 3, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(ou=cour-python)')
  [('ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'top', b'organizationalUnit']})]
  >>> dn = 'cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr'
  >>> operation = {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'PERSONNE']}
  >>> connexion.add_s(dn, modlist.addModlist(operation))
  (105, [], 10, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouvelle.personne)')
  [('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'PERSONNE'], 'cn': [b'nouvelle.personne']})]


**Avec le module ldap3**

.. code-block:: pycon

  >>> connexion.add('ou=cour-python,dc=domaine-perso,dc=fr', 'organizationalUnit')
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'addResponse'}, None, {'entry': 'ou=cour-python,dc=domaine-perso,dc=fr', 'attributes': {'objectClass': ['organizationalUnit']}, 'type': 'addRequest', 'controls': None})
  >>> connexion.add('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'inetOrgPerson', {'givenName': 'Nouvelle', 'sn': 'PERSONNE'})
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'addResponse'}, None, {'entry': 'cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'attributes': {'givenName': ['Nouvelle'], 'sn': ['PERSONNE'], 'objectClass': ['inetOrgPerson']}, 'type': 'addRequest', 'controls': None})
  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(objectclass=*)')
  >>> print(reponse)
  [{'raw_dn': b'dc=domaine-perso,dc=fr', 'dn': 'dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'ou=Groupes,dc=domaine-perso,dc=fr', 'dn': 'ou=Groupes,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr', 'dn': 'cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'ou=cour-python,dc=domaine-perso,dc=fr', 'dn': 'ou=cour-python,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'dn': 'cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}]



Renommer des entrées LDAP
^^^^^^^^^^^^^^^^^^^^^^^^^

**Avec le module ldap**

.. code-block:: pycon

  >>> dn = 'cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr'
  >>> connexion.rename_s(dn, 'cn=nouv-personne')
  (109, [], 20, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouv-personne)')
  [('cn=nouv-personne,ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'PERSONNE'], 'cn': [b'nouv-personne']})]
  >>> dn = 'cn=nouv-personne,ou=cour-python,dc=domaine-perso,dc=fr'
  >>> connexion.rename_s(dn, 'cn=nouv.personne')
  (109, [], 27, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouv.personne)')
  [('cn=nouv.personne,ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'PERSONNE'], 'cn': [b'nouv.personne']})]


**Avec le module ldap3**

.. code-block:: pycon

  >>> connexion.modify_dn('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'cn=nouv.personne')
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'modDNResponse'}, None, {'entry': 'cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'newRdn': 'cn=nouv.personne', 'deleteOldRdn': True, 'newSuperior': None, 'type': 'modDNRequest', 'controls': None})
  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(objectclass=inetOrgPerson)')
  >>> print(reponse)
  [{'raw_dn': b'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'cn=nouv.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'dn': 'cn=nouv.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}]


Déplacer une entrée LDAP
^^^^^^^^^^^^^^^^^^^^^^^^

**Avec le module ldap**

.. code-block:: pycon

  >>> dn = 'cn=nouv.personne,ou=cour-python,dc=domaine-perso,dc=fr'
  >>> connexion.rename_s(dn, 'cn=nouv.personne', 'ou=Utilisateurs,dc=domaine-perso,dc=fr')
  (109, [], 29, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouv.personne)')
  [('cn=nouv.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'PERSONNE'], 'cn': [b'nouv.personne']})]

**Avec le module ldap3**

.. code-block:: pycon

  >>> connexion.modify_dn('cn=nouv.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'cn=nouv.personne', new_superior='ou=Utilisateurs,dc=domaine-perso,dc=fr')
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'modDNResponse'}, None, {'entry': 'cn=nouv.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'newRdn': 'cn=nouv.personne', 'deleteOldRdn': True, 'newSuperior': 'ou=Utilisateurs,dc=domaine-perso,dc=fr', 'type': 'modDNRequest', 'controls': None})
  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(objectclass=inetOrgPerson)')
  >>> print(reponse)
  [{'raw_dn': b'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}, {'raw_dn': b'cn=nouv.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'cn=nouv.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}]


Supprimer des entrées LDAP
--------------------------

**Avec le module ldap**

.. code-block:: pycon

  >>> connexion.delete('cn=nouv.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr')
  31
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(objectclass=*)')
  [('dc=domaine-perso,dc=fr', {'objectClass': [b'top', b'dcObject', b'organization'], 'o': [b'domaine-perso.fr'], 'dc': [b'domaine-perso']}), ('ou=Groupes,dc=domaine-perso,dc=fr', {'objectClass': [b'organizationalUnit'], 'ou': [b'Groupes']}), ('ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'top', b'organizationalUnit']}), ('ou=Utilisateurs,dc=domaine-perso,dc=fr', {'objectClass': [b'organizationalUnit'], 'ou': [b'Utilisateurs']}), ('cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr', {'objectClass': [b'posixGroup'], 'cn': [b'developpeurs'], 'gidNumber': [b'5000']}), ('uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'objectClass': [b'inetOrgPerson', b'posixAccount', b'shadowAccount'], 'uid': [b'Prenom', b'prenom.nom'], 'sn': [b'NOM'], 'givenName': [b'Pr\xc3\xa9nom'], 'cn': [b'Pr\xc3\xa9nom NOM'], 'displayName': [b'Pr\xc3\xa9nom NOM'], 'uidNumber': [b'10000'], 'gidNumber': [b'5000'], 'userPassword': [b'renom.nom'], 'gecos': [b'Prenom NOM'], 'loginShell': [b'/bin/bash'], 'homeDirectory': [b'/home/prenom-nom']})]


**Avec le module ldap3**

.. code-block:: pycon

  >>> connexion.delete('cn=nouv.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr')
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'delResponse'}, None, {'entry': 'cn=nouv.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'type': 'delRequest', 'controls': None})
  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(objectclass=inetOrgPerson)')
  >>> print(reponse)
  [{'raw_dn': b'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {}, 'attributes': {}, 'type': 'searchResEntry'}]


Modifier des données
--------------------

**Avec le module ldap**

.. code-block:: pycon

  >>> dn = 'cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr'
  >>> operation = {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'PERSONNE']}
  >>> connexion.add_s(dn, modlist.addModlist(operation))
  (105, [], 35, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouvelle.personne)')
  [('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'PERSONNE'], 'cn': [b'nouvelle.personne']})]
  >>> connexion.modify_s(dn, [(ldap.MOD_ADD, 'sn', [b'Nouvelle PERSONNE'])])
  (103, [], 37, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouvelle.personne)')
  [('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'PERSONNE', b'Nouvelle PERSONNE'], 'cn': [b'nouvelle.personne']})]
  >>> connexion.modify_s(dn, [(ldap.MOD_DELETE, 'sn', [b'PERSONNE'])])
  (103, [], 39, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouvelle.personne)')
  [('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'sn': [b'Nouvelle PERSONNE'], 'cn': [b'nouvelle.personne']})]
  >>> connexion.modify_s(dn, [(ldap.MOD_REPLACE, 'sn', [b'Personne'])])
  (103, [], 41, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouvelle.personne)')
  [('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'inetOrgPerson'], 'givenName': [b'Nouvelle'], 'cn': [b'nouvelle.personne'], 'sn': [b'Personne']})]


**Avec le module ldap3**

.. code-block:: pycon

  >>> from ldap3 import MODIFY_ADD, MODIFY_REPLACE, MODIFY_DELETE
  >>> connexion.add('cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'inetOrgPerson', {'sn': 'Personne', 'uid': 'uid=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'displayName': 'Nouvelle PERSONNE', 'givenName': 'Nouvelle', 'mail': 'nouvelle.personne@domaine-perso.fr'})
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'addResponse'}, None, {'entry': 'cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'attributes': {'sn': ['Personne'], 'uid': ['uid=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr'], 'displayName': ['Nouvelle PERSONNE'], 'givenName': ['Nouvelle'], 'mail': ['nouvelle.personne@domaine-perso.fr'], 'objectClass': ['inetOrgPerson']}, 'type': 'addRequest', 'controls': None})
  >>> connexion.modify('cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'sn': [(MODIFY_ADD, ['Nouvelle PERSONNE'])]})
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'modifyResponse'}, None, {'entry': 'cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'changes': [{'operation': 0, 'attribute': {'type': 'sn', 'value': ['Nouvelle PERSONNE']}}], 'type': 'modifyRequest', 'controls': None})
  >>> connexion.modify('cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'sn': [(MODIFY_DELETE, ['Personne'])]})
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'modifyResponse'}, None, {'entry': 'cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'changes': [{'operation': 1, 'attribute': {'type': 'sn', 'value': ['Personne']}}], 'type': 'modifyRequest', 'controls': None})
  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(cn=nouvelle.personne)', attributes=['cn', 'sn'])
  >>> print(reponse)
  [{'raw_dn': b'cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {'sn': [b'Nouvelle PERSONNE'], 'cn': [b'nouvelle.personne']}, 'attributes': {'sn': ['Nouvelle PERSONNE'], 'cn': ['nouvelle.personne']}, 'type': 'searchResEntry'}]
  >>> connexion.modify('cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'sn': [(MODIFY_REPLACE, ['Personne'])]})
  (True, {'result': 0, 'description': 'success', 'dn': '', 'message': '', 'referrals': None, 'type': 'modifyResponse'}, None, {'entry': 'cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'changes': [{'operation': 2, 'attribute': {'type': 'sn', 'value': ['Personne']}}], 'type': 'modifyRequest', 'controls': None})
  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(cn=nouvelle.personne)', attributes=['cn', 'sn'])
  >>> print(reponse)
  [{'raw_dn': b'cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'dn': 'cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'raw_attributes': {'cn': [b'nouvelle.personne'], 'sn': [b'Personne']}, 'attributes': {'cn': ['nouvelle.personne'], 'sn': ['Personne']}, 'type': 'searchResEntry'}]


Générer un fichier LDIF
-----------------------

**Avec le module ldap**

.. code-block:: pycon

  >>> import sys, ldif
  >>> affiche_ldif = ldif.LDIFWriter(sys.stdout)
  >>> for dn, resultat in connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouvelle.personne)'):
  ...     affiche_ldif.unparse(dn, resultat)
  ...
  dn: cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr
  cn: nouvelle.personne
  givenName: Nouvelle
  objectClass: inetOrgPerson
  ou: cour-python
  sn: Personne

Vous pouvez exporter le contenu de la variable :python:`sortie_ldif` dans un fichier.

Mais le module :python:`ldif` permet de travailler directement à la création d'un fichier LDIF.

.. code-block:: pycon

  >>> genereldif = ldif.LDIFWriter(open('test.ldif', 'w'))
  >>> for dn, resultat in connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(cn=nouvelle.personne)'):
  ...     genereldif.unparse(dn, resultat)
  ...


Ce qui nous donne comme fichier **repertoire_de_developpement/12_Données/test.ldif** :

.. code-block:: ldif

  dn: cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr
  cn: nouvelle.personne
  givenName: Nouvelle
  objectClass: inetOrgPerson
  ou: cour-python
  sn: Personne


Générons un fichier LDIF pour ajouter et supprimer une entrée LDAP.

.. code-block:: pycon

  >>> with open('monfichier.ldif', 'w') as fichier:
  ...     genereldif = ldif.LDIFWriter(fichier)
  ...     dn = 'cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr'
  ...     operation = {'changetype': [b'delete']}
  ...     genereldif.unparse(dn, operation)
  ...     dn = 'cn=un.individu,ou=Utilisateurs,dc=domaine-perso,dc=fr'
  ...     operation = {'changetype': [b'add'], 'objectClass': [b'inetOrgPerson', b'posixAccount', b'shadowAccount'], 'uid': [b'un.individu'], 'sn': [b'INDIVIDU'], 'givenName': [b'Un'], 'uidNumber': [b'100001'], 'gidNumber': [b'5001'], 'userPassword': [b'un.individu'], 'gecos': [b'Un INDIVIDU'], 'loginShell': [b'/bin/bash'], 'homeDirectory': [b'/home/un-individu']}
  ...     genereldif.unparse(dn, operation)...

Ce qui nous donne le fichier LDIF

.. code-block:: ldif

  dn: cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr
  changetype: delete

  dn: cn=un.individu,ou=Utilisateurs,dc=domaine-perso,dc=fr
  changetype: add
  gecos: Un INDIVIDU
  gidNumber: 5001
  givenName: Un
  homeDirectory: /home/un-individu
  loginShell: /bin/bash
  objectClass: inetOrgPerson
  objectClass: posixAccount
  objectClass: shadowAccount
  sn: INDIVIDU
  uid: un.individu
  uidNumber: 100001
  userPassword: un.individu


**Avec le module ldap3**

.. warning:: Attention la version **2.8.1** du module :python:`ldap3` est buguée mettre à jour avec :terminal:`sudo pip install --upgrade ldap3`

.. code-block:: pycon

  >>> statut, resultat, reponse, _ = connexion.search('dc=domaine-perso,dc=fr', '(objectclass=inetOrgPerson)', attributes=['cn', 'sn'])
  >>> resultat = connexion.response_to_ldif(reponse)
  >>> print(resultat)
  version: 1
  dn: uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr
  sn: NOM
  cn:: UHLDqW5vbSBOT00=

  dn: cn=nouvelle.personne,ou=Utilisateurs,dc=domaine-perso,dc=fr
  cn: nouvelle.personne
  sn: Personne

  # total number of entries: 2
  >>> from ldap3 import LDIF
  >>> generateldif = Connection(server=None, client_strategy=LDIF)
  >>> with generateldif:
  ...     generateldif.add('ou=cour-python,dc=domaine-perso,dc=fr', 'organizationalUnit')
  ...     generateldif.add('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'inetOrgPerson', {'givenName': 'Nouvelle', 'sn': 'PERSONNE'})
  ...     generateldif.delete('cn=utilisateur.bidon,ou=Utilisateurs,dc=domaine-perso,dc=fr')
  ...     result = generateldif.stream.getvalue()
  ...
  'version: 1\ndn: ou=cour-python,dc=domaine-perso,dc=fr\nchangetype: add\nobjectClass: organizationalUnit'
  'version: 1\ndn: cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr\nchangetype: add\nobjectClass: inetOrgPerson\ngivenName: Nouvelle\nsn: PERSONNE'
  'version: 1\ndn: cn=utilisateur.bidon,ou=Utilisateurs,dc=domaine-perso,dc=fr\nchangetype: delete'


Exporter un contenu LDIF dans un fichier LDIF.

.. code-block:: pycon

  >>> generateldif = Connection(server=None, client_strategy=LDIF)
  >>> generateldif.stream = open('test.ldif', 'w')
  >>> with generateldif:
  ...     generateldif.add('ou=cour-python,dc=domaine-perso,dc=fr', 'organizationalUnit')
  ...     generateldif.add('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr', 'inetOrgPerson', {'givenName': 'Nouvelle', 'sn': 'PERSONNE'})
  ...     generateldif.delete('cn=utilisateur.bidon,ou=Utilisateurs,dc=domaine-perso,dc=fr')
  ...
  'version: 1\ndn: ou=cour-python,dc=domaine-perso,dc=fr\nchangetype: add\nobjectClass: organizationalUnit'
  'version: 1\ndn: cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr\nchangetype: add\nobjectClass: inetOrgPerson\ngivenName: Nouvelle\nsn: PERSONNE'
  'version: 1\ndn: cn=utilisateur.bidon,ou=Utilisateurs,dc=domaine-perso,dc=fr\nchangetype: delete'


Ce qui nous donne comme fichier **repertoire_de_developpement/12_Données/test.ldif** :

.. code-block:: ldif

  version: 1

  dn: ou=cour-python,dc=domaine-perso,dc=fr
  changetype: add
  objectClass: organizationalUnit

  dn: cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr
  changetype: add
  objectClass: inetOrgPerson
  givenName: Nouvelle
  sn: PERSONNE

  dn: cn=utilisateur.bidon,ou=Utilisateurs,dc=domaine-perso,dc=fr
  changetype: delete


Générons un fichier LDIF pour ajouter et supprimer une entrée LDAP.

.. code-block:: pycon

  >>> from ldap3 import Connection, LDIF
  >>> generateldif = Connection(server=None, client_strategy=LDIF)
  >>> generateldif.stream = open('monfichier.ldif', 'w')
  >>> with generateldif:
  ...     generateldif.delete('cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr')
  ...     generateldif.add('cn=un.individu,ou=Utilisateurs,dc=domaine-perso,dc=fr', 'inetOrgPerson, , posixAccount, shadowAccount', {'gecos': 'Un INDIVIDU', 'gidNumber': 5001, 'givenName': 'Un', 'homeDirectory': '/home/un-individu', 'loginShell': '/bin/bash', 'sn': 'INDIVIDU', 'uid': 'un.individu', 'uidNumber': 100001, 'userPassword': 'un.individu'})
  ...
  'version: 1\ndn: cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr\nchangetype: delete'
  'version: 1\ndn: cn=un.individu,ou=Utilisateurs,dc=domaine-perso,dc=fr\nchangetype: add\nobjectClass: inetOrgPerson, , posixAccount, shadowAccount\ngecos: Un INDIVIDU\ngidNumber: 5001\ngivenName: Un\nhomeDirectory: /home/un-individu\nloginShell: /bin/bash\nsn: INDIVIDU\nuid: un.individu\nuidNumber: 100001\nuserPassword: un.individu'

Ce qui nous donne le fichier LDIF

.. code-block:: ldif

  version: 1

  dn: cn=nouvelle.personne,ou=cour-python,dc=domaine-perso,dc=fr
  changetype: delete

  dn: cn=un.individu,ou=Utilisateurs,dc=domaine-perso,dc=fr
  changetype: add
  objectClass: inetOrgPerson, , posixAccount, shadowAccount
  gecos: Un INDIVIDU
  gidNumber: 5001
  givenName: Un
  homeDirectory: /home/un-individu
  loginShell: /bin/bash
  sn: INDIVIDU
  uid: un.individu
  uidNumber: 100001
  userPassword: un.individu


Importer un fichier LDIF dans l'annuaire
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Avec le module ldap**

.. code-block:: pycon

  >>> with open('monfichier.ldif') as fichier:
  ...     parser = ldif.LDIFRecordList(fichier)
  ...     parser.parse()
  ...
  >>> for dn, entree in parser.all_records:
  ...     if entree['changetype'] == [b'add']:
  ...         entree.pop('changetype')
  ...         connexion.add_s(dn, modlist.addModlist(entree))
  ...     if entree['changetype'] == [b'delete']:
  ...         connexion.delete(dn)
  ...
  54
  [b'add']
  (105, [], 57, [])
  >>> connexion.search_s('dc=domaine-perso,dc=fr', ldap.SCOPE_SUBTREE, '(objectclass=*)')
  [('dc=domaine-perso,dc=fr', {'objectClass': [b'top', b'dcObject', b'organization'], 'o': [b'domaine-perso.fr'], 'dc': [b'domaine-perso']}), ('ou=Utilisateurs,dc=domaine-perso,dc=fr', {'objectClass': [b'organizationalUnit'], 'ou': [b'Utilisateurs']}), ('ou=Groupes,dc=domaine-perso,dc=fr', {'objectClass': [b'organizationalUnit'], 'ou': [b'Groupes']}), ('cn=developpeurs,ou=Groupes,dc=domaine-perso,dc=fr', {'objectClass': [b'posixGroup'], 'cn': [b'developpeurs'], 'gidNumber': [b'5000']}), ('uid=prenom.nom,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'objectClass': [b'inetOrgPerson', b'posixAccount', b'shadowAccount'], 'uid': [b'Prenom', b'prenom.nom'], 'sn': [b'NOM'], 'givenName': [b'Pr\xc3\xa9nom'], 'cn': [b'Pr\xc3\xa9nom NOM'], 'displayName': [b'Pr\xc3\xa9nom NOM'], 'uidNumber': [b'10000'], 'gidNumber': [b'5000'], 'userPassword': [b'renom.nom'], 'gecos': [b'Prenom NOM'], 'loginShell': [b'/bin/bash'], 'homeDirectory': [b'/home/prenom-nom']}), ('ou=cour-python,dc=domaine-perso,dc=fr', {'ou': [b'cour-python'], 'objectClass': [b'top', b'organizationalUnit']}), ('cn=un.individu,ou=Utilisateurs,dc=domaine-perso,dc=fr', {'gecos': [b'Un INDIVIDU'], 'gidNumber': [b'5001'], 'givenName': [b'Un'], 'homeDirectory': [b'/home/un-individu'], 'loginShell': [b'/bin/bash'], 'objectClass': [b'inetOrgPerson', b'posixAccount', b'shadowAccount'], 'sn': [b'INDIVIDU'], 'uid': [b'un.individu'], 'uidNumber': [b'100001'], 'userPassword': [b'un.individu'], 'cn': [b'un.individu']})]
  >>> connexion.unbind()
