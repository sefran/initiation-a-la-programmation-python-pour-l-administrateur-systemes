Approfondir ce cour
###################

Faire `ce tutoriel pour approfondir l’initiation à Python3 et parfaire son savoir faire <https://docs.python.org/fr/3/tutorial/index.html>`_.

Vous pouvez profiter aussi, comme complément à ce cour, du cour `Apprendre à programmer avec Python <https://zestedesavoir.com/tutoriels/799/apprendre-a-programmer-avec-python-3/>`_ et de son module avancé `Notions de Python avancées <https://zestedesavoir.com/tutoriels/954/notions-de-python-avancees/>`_.


