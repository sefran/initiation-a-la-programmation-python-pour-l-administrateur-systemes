Stocker des données
###################

.. include:: 11_01_Lire et Écrire des fichiers.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 11_02_Stocker des objets.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 11_03_Stocker des données de façon persistante.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 11_04_Les annuaires LDAP.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 11_05_Les bases de données.rst
