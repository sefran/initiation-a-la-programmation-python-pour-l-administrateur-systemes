.. role:: python(code)
  :language: python

Gestion des dates et du temps
=============================

Date et heure
-------------

**Datetime** est un module qui permet de manipuler des dates et des durées sous forme d’objets. L’idée est simple: vous manipulez l’objet pour faire tous vos calculs, et quand vous avez besoin de l’afficher, vous formatez l’objet en chaîne de caractères.

On peut créer artificiellement un objet **datetime** , ses paramètres sont:

.. code-block:: python

  datetime (année, mois, jour, heure, minute, seconde, microseconde, fuseau horaire)

Mais seuls «année», «mois» et «jour» sont obligatoires.

.. code-block:: pycon

  >>> from datetime import datetime
  >>> datetime(2000, 1, 1)
  datetime.datetime(2000, 1, 1, 0, 0)

Nous sommes ici le premier janvier 2000, à la seconde et la minute zéro, de l’heure zéro.

On peut bien entendu récupérer l’heure et la date du jour:

.. code-block:: pycon

  >>> actuellement = datetime.now()
  >>> actuellement
  datetime.datetime(2021, 7, 9, 10, 13, 1, 25073)
  >>> actuellement.year
  2021
  >>> actuellement.month
  7
  >>> actuellement.day
  9
  >>> actuellement.hour
  10
  >>> actuellement.minute
  13
  >>> actuellement.second
  1
  >>> actuellement.microsecond
  25073
  >>> actuellement.isocalendar() # année, semaine, jour
  datetime.IsoCalendarDate(year=2021, week=27, weekday=5)
  >>> maintenant = datetime.now # obtenir l’heure avec une variable
  >>> print(maintenant())
  2021-07-09 10:14:51.359460
  >>> print(maintenant())
  2021-07-09 10:15:0.918195

Enfin, si vous souhaitez uniquement vous occuper de la date ou de l’heure:

.. code-block:: pycon

  >>> print(maintenant().strftime('%Hh %Mmin %Ss %d/%m/%Y')) # change une date en chaîne.
  10h 16min 22s 09/07/2021
  >>> from datetime import date, time, datetime
  >>> maDate = datetime.strptime('2021-06-05 12:30:00', '%Y-%m-%d %H:%M:%S') # change une chaîne en date.
  >>> print(maDate)
  2021-06-05 12:30:00
  >>> maDate
  datetime.datetime(2021, 6, 5, 12, 30)

Durée
-----

En plus de pouvoir récupérer la date du jour, on peut calculer la différence entre deux dates. Par exemple, combien de temps y a-t-il entre aujourd’hui et le premier jour de l’an 2000 ?

.. code-block:: pycon

  >>> duree = maintenant() - datetime(2000, 1, 1)
  >>> duree
  datetime.timedelta(days=7860, seconds=39227, microseconds=140524)

Et vous découvrez ici un autre objet, le **timedelta**. Cet objet représente une durée en jours, secondes et microsecondes.

.. code-block:: pycon

  >>> duree.days
  7860
  >>> duree.seconds
  39227
  >>> duree.microseconds
  140524
  >>> duree.total_seconds
  <built-in method total_seconds of datetime.timedelta object at 0x7efc4f7655d0>
  >>> duree.total_seconds()
  679144227.140524

On peut créer son propre **timedelta** :

.. code-block:: pycon

  >>> from datetime import timedelta
  >>> print(timedelta(days=3, seconds=100))
  3 days, 0:01:40

Cela permet de répondre à la question : «Quelle date serons-nous dans 2 jours, 4 heures, 3 minutes, et 12 secondes ?»:

.. code-block:: pycon

  >>> print(maintenant() + timedelta(days=2, hours=4, minutes=3, seconds=12))
  2021-07-11 15:12:00.371922

Les objets **datetime** et **timedelta** sont immutables. Ainsi si vous voulez utiliser une version légèrement différente d’un objet **datetime** , il faudra toujours en créer un nouveau. Par exemple:

.. code-block:: pycon

  >>> actuellement.replace(year=1995) # on créer un nouvel objet
  datetime.datetime(1995, 7, 9, 10, 13, 1, 25073)

Vous noterez que je ne parles pas de fuseau horaire. Et bien c’est parce que l’implémentation Python est particulièrement ratée : l’API est compliquée et les données ne sont pas à jour. Il faut dire que la mesure du temps, contrairement à ce qu’on pourrait penser, n’est pas vraiment le truc le plus stable du monde, et des pays changent régulièrement leur manière de faire.

Calendrier
----------

Le module **calendar**.

Il permet de manipuler un calendrier comme un objet, et de déterminer les jours d’un mois, les semaines, vérifier les caractéristiques d’un jour en particulier, etc. :

.. code-block:: pycon

  >>> import calendar
  >>> calendar.mdays # combien de jour par mois ?
  [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  >>> calendar.isleap(2000) # est-ce une année bissextile ?
  True
  >>> calendar.weekday(2000, 1, 1) # quel jour était cette date ?
  5
  >>> calendar.MONDAY, calendar.TUESDAY, calendar.WEDNESDAY, calendar.THURSDAY, calendar.FRIDAY
  (0, 1, 2, 3, 4)

On peut instancier un calendrier et itérer dessus:

.. only:: latex

  .. code-block:: pycon

    >>> cal = calendar.Calendar()
    >>> cal.getfirstweekday()
    0
    >>> list(cal.iterweekdays())
    [0, 1, 2, 3, 4, 5, 6]
    >>> list(cal.itermonthdays(2000, 1))
    [0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 0, 0, 0, 0, 0]
    >>> list(cal.itermonthdates(2000, 1))
    [datetime.date(1999, 12, 27), datetime.date(1999, 12, 28), datetime.date(1999, 12, 29), datetime.date(1999, 12, 30), datetime.date(1999, 12, 31), datetime.date(2000, 1, 1), datetime.date(2000, 1, 2), datetime.date(2000, 1, 3), datetime.date(2000, 1, 4), datetime.date(2000, 1, 5), datetime.date(2000, 1, 6), datetime.date(2000, 1, 7), datetime.date(2000, 1, 8), datetime.date(2000, 1, 9), datetime.date(2000, 1, 10), datetime.date(2000, 1, 16), datetime.date(2000, 1, 17), datetime.date(2000, 1, 18), datetime.date(2000, 1, 19), datetime.date(2000, 1, 20),
    datetime.date(2000, 1, 21), datetime.date(2000, 1, 22), datetime.date(2000, 1, 23), datetime.date(2000, 1, 24), datetime.date(2000, 1, 25), datetime.date(2000, 1, 26), datetime.date(2000, 1, 27), datetime.date(2000, 1, 28), datetime.date(2000, 1, 29), datetime.date(2000, 1, 30), datetime.date(2000, 1, 31), datetime.date(2000, 2, 1), datetime.date(2000, 2, 2), datetime.date(2000, 2, 3), datetime.date(2000, 2, 4), datetime.date(2000, 2, 5), datetime.date(2000, 2, 6)]
    >>> cal.monthdayscalendar(2000, 1)
    [[0, 0, 0, 0, 0, 1, 2], [3, 4, 5, 6, 7, 8, 9], [10, 11, 12, 13, 14, 15, 16], [17, 18, 19, 20, 21, 22, 23], [24, 25, 26, 27, 28, 29, 30], [31, 0, 0, 0, 0, 0, 0]]

.. only:: not latex

  .. code-block:: pycon

    >>> cal = calendar.Calendar()
    >>> cal.getfirstweekday()
    0
    >>> list(cal.iterweekdays())
    [0, 1, 2, 3, 4, 5, 6]
    >>> list(cal.itermonthdays(2000, 1))
    [0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 0, 0, 0, 0, 0]
    >>> list(cal.itermonthdates(2000, 1))
    [datetime.date(1999, 12, 27), datetime.date(1999, 12, 28), datetime.date(1999, 12, 29), datetime.date(1999, 12, 30), datetime.date(1999, 12, 31), datetime.date(2000, 1, 1), datetime.date(2000, 1, 2), datetime.date(2000, 1, 3), datetime.date(2000, 1, 4), datetime.date(2000, 1, 5), datetime.date(2000, 1, 6), datetime.date(2000, 1, 7), datetime.date(2000, 1, 8), datetime.date(2000, 1, 9), datetime.date(2000, 1, 10), datetime.date(2000, 1, 16), datetime.date(2000, 1, 17), datetime.date(2000, 1, 18), datetime.date(2000, 1, 19), datetime.date(2000, 1, 20),     datetime.date(2000, 1, 21), datetime.date(2000, 1, 22), datetime.date(2000, 1, 23), datetime.date(2000, 1, 24), datetime.date(2000, 1, 25), datetime.date(2000, 1, 26), datetime.date(2000, 1, 27), datetime.date(2000, 1, 28), datetime.date(2000, 1, 29), datetime.date(2000, 1, 30), datetime.date(2000, 1, 31), datetime.date(2000, 2, 1), datetime.date(2000, 2, 2), datetime.date(2000, 2, 3), datetime.date(2000, 2, 4), datetime.date(2000, 2, 5), datetime.date(2000, 2, 6)]
    >>> cal.monthdayscalendar(2000, 1)
    [[0, 0, 0, 0, 0, 1, 2], [3, 4, 5, 6, 7, 8, 9], [10, 11, 12, 13, 14, 15, 16], [17, 18, 19, 20, 21, 22, 23], [24, 25, 26, 27, 28, 29, 30], [31, 0, 0, 0, 0, 0, 0]]


Comme souvent Python vient aussi avec de très bons modules tierces pour manipuler les dates :

-  **dateutils** est un datetime boosté aux hormones qui permet notamment de donner des durées floues comme “+ 1 mois” et de gérer des événements qui se répètent.
-  **babel** n’est pas spécialisé dans les dates mais dans la localisation. Le module possède des outils pour formater des dates selon le format de chaque pays, et aussi avec des formats naturels comme “il y a une minute”.
-  **pytz** est une implémentation saine de gestion des fuseaux horaires en Python.
