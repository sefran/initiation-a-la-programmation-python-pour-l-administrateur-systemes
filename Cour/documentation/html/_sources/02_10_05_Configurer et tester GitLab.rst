Configurer et tester GitLab
===========================

Saisissez dans un navigateur l’URL **gitlab.domaine-perso.fr**

.. image:: images/gitlab_1.png
   :alt: Initialisation du mot de passe de l'administrateur root de GitLab
   :align: center
   :scale: 100%


Si vous n'avez pas la fenêtre d'initialisation du mot de passe :

.. code-block:: console

  utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-rake "gitlab:password:reset"

.. image:: images/gitlab_2.png
   :alt: Connection en root dans GitLab
   :align: center
   :scale: 100%

.. only:: latex

  .. image:: images/gitlab_3.png
     :alt: Accueil projets GitLab
     :align: center
     :scale: 40%

  .. image:: images/gitlab_4.png
     :alt: Préférences de GitLab aller vers «Localization»
     :align: center
     :scale: 40%

  .. image:: images/gitlab_5.png
     :alt: Préférences language à «French» et First day of the week à «Monday»
     :align: center
     :scale: 40%

.. only:: not latex

  .. image:: images/gitlab_3.png
     :alt: Accueil projets GitLab
     :align: center
     :scale: 80%

  .. image:: images/gitlab_4.png
     :alt: Préférences de GitLab aller vers «Localization»
     :align: center
     :scale: 80%

  .. image:: images/gitlab_5.png
     :alt: Préférences language à «French» et First day of the week à «Monday»
     :align: center
     :scale: 80%


Tapez la touche **F5** pour rafraîchir l’affichage de votre navigateur.

.. image:: images/gitlab_6.png
   :alt: Icône Paramètres de l'utilisateur
   :align: center
   :scale: 100%

.. image:: images/gitlab_7.png
   :alt: Profil de l'utilisateur
   :align: center
   :scale: 100%

.. image:: images/gitlab_8.png
   :alt: Compte de l'utilisateur
   :align: center
   :scale: 100%

.. image:: images/gitlab_9.png
   :alt: Icône  espace d'administration
   :align: center
   :scale: 100%

.. image:: images/gitlab_10.png
   :alt: Espace d'administration Paramètres et menu Général
   :align: center
   :scale: 100%

.. image:: images/gitlab_11.png
   :alt: Contrôle de visibilité d'accès
   :align: center
   :scale: 100%


Intégrer le dépot git local dans Gitlab :

.. code-block:: console

  utilisateur@MachineUbuntu:~/$ cd repertoire_de_developpement
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config credential.helper store
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git remote add origin http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push -u origin --all
  Username for 'http://gitlab.domaine-perso.fr': utilisateur
  Password for 'http://gitlab.domaine-perso.fr': motdepasse
  Énumération des objets: 51, fait.
  Décompte des objets: 100% (43/43), fait.
  Compression par delta en utilisant jusqu’à 4 fils d’exécution
  Compression des objets: 100% (43/43), fait.
  Écriture des objets: 100% (51/51), 180.78 Kio \| 4.89 Mio/s, fait.
  Total 51 (delta 3), réutilisés 0 (delta 0), réutilisés du pack 0 To http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
  * [new branch] master → master
  La branche 'master' est paramétrée pour suivre la branche distante 'master' depuis 'origin'.

.. only:: latex

  .. image:: images/gitlab_12.png
     :alt: Projet local dans les projets de GitLab
     :align: center
     :scale: 34%

  .. image:: images/gitlab_13.png
     :alt: Visualisation des détails du projet
     :align: center
     :scale: 34%

  .. image:: images/gitlab_14.png
     :alt: Paramètres généraux du projet
     :align: center
     :scale: 34%

  .. image:: images/gitlab_15.png
     :alt: Intégration des modifications des paramètres généraux dans la visualisation des détails du projet
     :align: center
     :scale: 43%

  .. image:: images/gitlab_16.png
     :alt: Ajout des fichiers README.md, LICENCE, CHANGELOG et CONTRIBUTING
     :align: center
     :scale: 43%

.. only:: not latex

  .. image:: images/gitlab_12.png
     :alt: Projet local dans les projets de GitLab
     :align: center
     :scale: 70%

  .. image:: images/gitlab_13.png
     :alt: Visualisation des détails du projet
     :align: center
     :scale: 70%

  .. image:: images/gitlab_14.png
     :alt: Paramètres généraux du projet
     :align: center
     :scale: 70%

  .. image:: images/gitlab_15.png
     :alt: Intégration des modifications des paramètres généraux dans la visualisation des détails du projet
     :align: center
     :scale: 80%

  .. image:: images/gitlab_16.png
     :alt: Ajout des fichiers README.md, LICENCE, CHANGELOG et CONTRIBUTING
     :align: center
     :scale: 80%


Vous pouvez maintenant récupérer les nouveaux fichiers d’information Gitlab (**CHANGELOG**, **CONTRIBUTING.md**, **LICENSE** et **README.md**) dans votre projet local :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git fetch
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git merge
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ssh-keygen -t rsa -b 2048 -C "Ma clé de chiffrement"
  Generating public/private rsa key pair.
  Enter file in which to save the key(/home/utilisateur/.ssh/id_rsa):
  Created directory '/home/utilisateur/.ssh'.
  Enter passphrase (empty for no passphrase): motdepasse
  Enter same passphrase again: motdepasse
  Your identification has been saved in /home/utilisateur/.ssh/id_rsa
  Your public key has been saved in /home/utilisateur/.ssh/id_rsa.pub
  The key fingerprint is: SHA256:n60tA2JwGV0tptwB48YrPT6hQQWrxGYhEVegfnO9GXM Ma clé de chiffrement
  The key's randomart image is:
  +---[RSA 2048]----+
  |   +o+ooo+o..    |
  |    = ..=..+ .   |
  |   . = o+++ o    |
  |  . +.oo+o..     |
  |   . +o+ S E     |
  |    . oo=.X o    |
  |      ...=.o .   |
  |          .oo    |
  |           .o.   |
  +----[SHA256]-----+


.. image:: images/gitlab_17.png
   :alt: Synthèse des projets GitLab
   :align: center
   :scale: 100%

.. image:: images/gitlab_18.png
   :alt: Demande d'ajout d'une clé SSH dans les détails du projet
   :align: center
   :scale: 100%

.. image:: images/gitlab_19.png
   :alt: Bouton Add SSH key
   :align: center
   :scale: 100%

.. image:: images/gitlab_20.png
   :alt: Fenêtre de définition des clés SSH
   :align: center
   :scale: 100%


Copier le contenu du fichier «**/home/utilisateur/.ssh/id-rsa.pub**»

.. image:: images/gitlab_21.png
   :alt: Ajout de la clé SSH créée
   :align: center
   :scale: 100%
