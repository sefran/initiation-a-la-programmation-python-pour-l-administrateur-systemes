Les principaux éléments de syntaxe
##################################

.. include:: 03_01_Syntaxe et grammaire Python.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_02_Utilisation de Python comme calculatrice.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_03_Les variables.rst


.. only:: latex

  .. raw:: latex

    \newpage



.. include:: 03_04_L’affichage d'informations.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_05_Les types de variables.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_06_Les fonctions intégrées.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_07_Les modules.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_08_Les bibliothèques de fonctions ou d’objets.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 03_09_L’organisation du code.rst


