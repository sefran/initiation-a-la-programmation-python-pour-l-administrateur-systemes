.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Syntaxe et grammaire Python
***************************

Plusieurs choses sont nécessaires pour écrire un code lisible : **la syntaxe**, **l'organisation du code**, **le découpage en fonctions** (et possiblement en classes que nous verrons avec les objets), mais souvent, aussi, **le bon sens**.

Pour cela, les «**PEP**\ » pour **P**\ ython **E**\ nhancement **P**\ roposal (proposition d'amélioration de Python) peuvent nous aider.

.. only:: html

  .. raw:: html

    <font color="Red">Distribuer document pep8.pdf</font>.</br>

.. only:: latex

  .. raw:: latex

    \textcolor{red}{Distribuer document pep8.pdf}.
    \newline

.. only:: not (html or latex)

  Distribuer document pep8.pdf.


On va aborder dans ce chapitre sans doute la plus célèbre des PEP, à savoir la PEP 8 https://www.python.org/dev/peps/pep-0008/, qui est incontournable lorsque l'on veut écrire du code Python correctement.

La «**Style Guide for Python Code**» est une des plus anciennes PEP (les numéros sont croissants avec le temps). Elle consiste en un nombre important de recommandations sur la syntaxe de Python.

Il est vivement recommandé de lire la PEP 8 en entier au moins une fois pour avoir une bonne vue d'ensemble en complément de ce cours. On ne présentera ici qu'un rapide résumé de cette PEP 8.


.. only:: latex

  .. raw:: latex

    \newpage


L'identation
============

Dans la plus part des langages de programmation, l'indentation du code (c'est-à-dire la manière d'écrire le code en laissant des espaces de décalage en début des lignes) est laissée au choix éclairé du développeur. Mais, force est de constater, parfois le développeur n'est pas des plus experts pour rendre lisible par les autres, et même lui même, son code…


.. image:: images/GestionExpaces_1.png
   :alt: Code identé
   :align: center
   :scale: 100%


Python oblige donc le développeur à structurer son code à l'aide des indentations : ce sont elles qui détermineront les blocs (séquences d'instructions liées) et non les accolades comme dans la majorité des langages.


.. image:: images/GestionExpaces_2.png
   :alt: Indentation
   :align: center
   :scale: 100%


Les blocs de code sont déterminés par :

-  La présence du caractère «:» en fin de ligne ;
-  Une indentation des lignes suivantes à l'aide de tabulations ou d'espaces.


.. image:: images/GestionExpaces_3.png
   :alt: Indentation incorrecte
   :align: center
   :scale: 100%


Attention à ne pas mélanger les tabulations avec les espaces pour l’indentation, Python n'aime pas ça du tout. Votre code ne fonctionnera pas et vous n'obtiendrez pas de message d'erreur explicite. Je conseille l'utilisation de quatre caractères espace pour faire une indentation.


.. only:: latex

  .. raw:: latex

    \newpage


Les commentaires
================

Commentaires sur une ligne :

.. code-block:: pycon

  >>> # Ceci est le premier commentaire
  >>> bidon = 1 # et ceci est le second commentaire
  >>>           # ... et là le troisième!
  >>> "# Ceci n’est pas un commentaire parce qu’il est entre guillemets."

Commentaires sur plusieurs lignes :

.. code-block:: pycon

  >>> """
  ... Ceci est un commentaire
  ... en plusieurs lignes
  ... qui sera ignoré lors de l'exécution
  ... """


.. only:: latex

  .. raw:: latex

    \newpage


Chaînes de caractères
=====================

**Les chaînes de caractères** (ou chaînes) sont des séquences de lettres et de nombres, ou, en d'autres termes, des morceaux de textes. Elles sont entourées par deux guillemets.

Par exemple :

.. code-block:: pycon

  >>> "Bonjour, Python!"

Comment faire si vous voulez insérer des guillemets «**\"**\ » à l'intérieur d'une chaîne ?

Si vous essayez à l’interpréteur :


.. code-block:: console

  >>> "J'ai dit "Wow!" très fort"
    File "<stdin>", line 1
    "J'ai dit "Wow!" très fort"
               ^
    SyntaxError: invalid syntax

Cela génère une erreur.

Le problème est que Python voit une chaîne, :python:`"J'ai dit "` suivie de quelque chose qui n'est pas une chaîne: **Wow!** . Ce n'est pas ce que nous voulions!

Python propose deux moyens simples d'insérer des guillemets à l'intérieur d'une chaîne.

Vous pouvez commencer et terminer une chaîne littérale avec des apostrophes «**\'**» à la place des guillemets, par exemple :

.. code-block:: pycon

  >>> 'bla bla'

Les guillemets peuvent ainsi être placés à l'intérieur :

.. code-block:: pycon

  >>> 'Tu as dit "Wow!" très fort.'

Vous pouvez placer une barre oblique inversée suivie du guillemet ou de l'apostrophe (:python:`\"` ou :python:`\'` ). Cela s'appelle une séquence d'échappement.

Python va supprimer la barre oblique inversée et n'afficher que le guillemet ou l'apostrophe à l'intérieur de la chaîne. A cause des séquences d'échappement, **la barre oblique inversée (\\)** est un symbole spécial.

Pour l'inclure dans une chaîne, il faut l'échapper avec une deuxième barre oblique inversée, en d'autres termes, **il faut écrire** :python:`\\` dans votre chaîne littérale.

Voici un exemple que vous pouvez tester avec l’interpréteur :

.. code-block:: pycon

  >>> 'L\'exemple avec un apostrophe.'
  >>> "Voici un \"échappement\" de guillemets"
  >>> "Un exemple d’échappement \
  ... pour écrire sur plusieurs lignes\
  ... un texte long"

.. only:: html

  .. raw:: html

    <font color="Green">Pourquoi le dernier exemple fonctionne ?</font></br>

.. only:: latex

  .. raw:: latex

    \textcolor{green}{Pourquoi le dernier exemple fonctionne ?}.
    \newpage

.. only:: not (html or latex)

  Pourquoi le dernier exemple fonctionne ?


Majuscules et Minuscules (Variables, instructions, fonctions, objets)
=====================================================================

Nous abordons ici les règles de nommage.

.. only:: html

  .. raw:: html

    <font color="Red">Voir document pep8.pdf déjà distribué</font>.</br>

.. only:: latex

  .. raw:: latex

    \textcolor{red}{Voir document pep8.pdf déjà distribué}.
    \newline

.. only:: not (html or latex)

  Voir document pep8.pdf déjà distribué.


Les noms de **variables**, de **fonctions** et de **modules** doivent être de la forme :

.. code-block:: python

  ma_variable
  fonction_test_27()
  mon_module

C'est-à-dire en minuscules avec un caractère «**souligné**» :python:`_` («**tiret du bas**» ou underscore en anglais) pour séparer les différents «**mots**» dans le nom.

Les **constantes** sont écrites en majuscules :

.. code-block:: python

  MA_CONSTANTE
  VITESSE_LUMIÈRE

Les noms de **classes** et les **exceptions** sont de la forme :

.. code-block:: pycon

  MaClasse
  MonException

Pensez à **donner à vos variables des noms qui ont du sens**\ .

Évitez autant que possible les a1, a2, i, truc, toto...

Les noms de variables à un caractère sont néanmoins autorisés pour les boucles et les indices :

.. code-block:: pycon

  >>> ma_liste = [1, 3, 5, 7, 9, 11]
  >>> for i in range(len(ma_liste)):
  ...     ma_liste[i]
  ...
  ...
  1
  3
  5
  7
  9
  11

Enfin, des **noms de variable à une lettre peuvent être utilisés lorsque cela a un sens mathématique** (par exemple, les noms x, y et z évoquent des coordonnées cartésiennes).


.. only:: latex

  .. raw:: latex

    \newpage


Gestion des espaces
===================

La PEP 8 recommande **d'entourer les opérateurs +, -, /, \*, ==, !=, >=, not, in, and, or... d'un espace**, avant et après.

Par exemple :

.. code-block:: python

  # code recommandé
  ma_variable = 3 + 7
  mon_texte = "souris"
  mon_texte == ma_variable
  # code non recommandé :
  ma_variable=3+7
  mon_texte="souris"
  mon_texte== ma_variable

Il n'y a, par contre, **pas d'espace** à **l'intérieur** des crochets **[]**\ , des accolades **{}** et des parenthèses **()** :

.. code-block:: python

  # code recommandé :
  ma_liste[1]
  mon_dico{"clé"}
  ma_fonction(argument)
  # code non recommandé :
  ma_liste[ 1 ]
  mon_dico{"clé" }
  ma_fonction( argument )

Ni juste **avant** la parenthèse **(** ouvrante d'une fonction ou le crochet **{** ouvrant d'une liste ou d'un dictionnaire :

.. code-block:: python

  # code recommandé :
  ma_liste[1]
  mon_dico{"clé"}
  ma_fonction(argument)
  # code non recommandé :
  ma_liste [1]
  mon_dico {"clé"}
  ma_fonction (argument)

On met **un espace après** les caractères **:** et **,** (mais pas avant) :

.. code-block:: python

  # code recommandé :
  ma_liste = [1, 2, 3]
  mon_dico = {"clé1": "valeur1", "clé2": "valeur2"}
  ma_fonction(argument1, argument2)
  # code non recommandé :
  ma_liste = [1 , 2 ,3]
  mon_dico = {"clé1":"valeur1", "clé2":"valeur2"}
  ma_fonction (argument1 ,argument2)

Par contre, pour **les tranches de listes**, on ne met **pas d'espace** autour du **:** :

.. code-block:: python

  # code recommandé :
  ma_liste = [1, 3, 5, 7, 9, 1]
  ma_liste[1:3]
  ma_liste[1:4:2]
  ma_liste[::2]
  # code non recommandé :
  ma_liste[1 : 3]
  ma_liste[1: 4:2 ]
  ma_liste[ : :2]

Enfin, on n'ajoute **pas plusieurs espaces** autour du **=** ou des autres opérateurs pour faire joli :

.. code-block:: python

  # code recommandé :
  x1 = 1
  x2 = 3
  x_old = 5
  # code non recommandé :
  x1    = 1
  x2    = 3
  x_old = 5

Les règles de base d'écriture des fonctions/procédures
======================================================

Maintenant que vous êtes prêt à écrire des programmes plus longs et plus complexes, il est temps de parler du style de codage. La plupart des langages peuvent être écrits (ou plutôt formatés) selon différents styles ; certains sont plus lisibles que d'autres. Rendre la lecture de votre code plus facile aux autres est toujours une bonne idée, et adopter un bon style de codage peut énormément vous y aider.

* **Utilisez des indentations de 4 espaces et pas de tabulations**. 4 espaces constituent un bon compromis entre une indentation courte (qui permet une profondeur d'imbrication plus importante) et une longue (qui rend le code plus facile à lire). Les tabulations introduisent de la confusion et doivent être proscrites autant que possible.
* Faites en sorte que **les lignes ne dépassent pas 79 caractères**, au besoin en insérant des retours à la ligne (**actuellement cela a évolué vers 127**). Vous facilitez ainsi la lecture pour les utilisateurs qui n'ont qu'un petit écran et, pour les autres, cela leur permet de visualiser plusieurs fichiers côte à côte.
* **Utilisez des lignes vides pour séparer les fonctions et les classes**, ou pour scinder de gros blocs de code à l'intérieur de fonctions.
* Lorsque c'est possible, **placez les commentaires sur leurs propres lignes**.
* **Utilisez les chaînes de documentation**.
* **Utilisez des espaces autour des opérateurs et après les virgules**, mais pas juste à l'intérieur des parenthèses : :python:`a = f(1, 2) + g(3, 4)`.
* **Nommez toujours vos classes et fonctions de la même manière** ; la convention est d'utiliser une notation `UpperCamelCase <https://medium.com/@anthowelc/c-est-quoi-le-camelcase-7fa02dc7fcee>`_ pour **les classes**, et **minuscules_avec_trait_bas** pour **les fonctions et méthodes**. Utilisez toujours :python:`self` comme **nom du premier argument des méthodes** (voyez Une première approche des classes pour en savoir plus sur les classes et les méthodes).
* N'utilisez pas d'encodage exotique dès lors que votre code est censé être utilisé dans des environnements internationaux. Par défaut, Python travaille en `UTF-8 <https://www.w3.org/International/questions/qa-what-is-encoding.fr>`_. Préférez les caractères du simple `ASCII <https://fr.wikibooks.org/wiki/Les_ASCII_de_0_%C3%A0_127/La_table_ASCII>`_ pour votre code. N'utilisez pas de caractères exotiques lorsque votre code est censé être utilisé dans des environnements internationaux.
