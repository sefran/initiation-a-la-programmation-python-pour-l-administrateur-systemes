Les modules
***********

Lorsque vous quittez et entrez à nouveau dans l'interpréteur Python, tout ce que vous avez déclaré dans la session précédente est perdu. Afin de rédiger des programmes plus longs, vous devez utiliser un éditeur de texte, préparer votre code dans un fichier et exécuter Python avec ce fichier en paramètre. Cela s'appelle créer un script. Lorsque votre programme grandit, vous pouvez séparer votre code dans plusieurs fichiers. Ainsi, il vous est facile de réutiliser du code écrit pour un programme dans un autre sans avoir à les copier.

Pour gérer cela, Python vous permet de placer des définitions dans un fichier et de les utiliser dans un script ou une session interactive. Un tel fichier est appelé un module et les définitions d'un module peuvent être importées dans un autre module ou dans le module main (qui est le module qui contient vos variables et définitions lors de l'exécution d'un script au niveau le plus haut ou en mode interactif).

Un module est un fichier contenant des définitions et des instructions (des fonctions, des classes et des variables.). Son nom de fichier est le nom du module suffixé de «**.py**».

À l'intérieur d'un module, son propre nom est accessible par la variable :python:`__name__`. Ce module, avec ses variables, fonctions ou classes, peut être chargé à partir d’un autre module ; c’est ce que l’on appelle l’importation.

\__name__ et \__main__
========================

Lorsque l'interpréteur exécute un module, la variable **\__name__** sera définie comme **\__main__** si le module en cours d'exécution est le programme principal.

.. code-block:: pycon

  >>> print(" __name__ est défini à {}".format(__name__))
  __name__ est défini à __main__

Mais si le code importe le module depuis un autre module, la variable **\__name__** sera définie sur le nom de ce module. Jetons un œil à un exemple.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 7_Modules ; cd 7_Modules
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ nano ./mon_module.py

Créez un module Python nommé **mon_module.py** et saisissez ce code à l'intérieur:

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  # Module de fichier Python
  print("Mon module __name__ est défini à {}".format(__name__))

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ python3


.. code-block:: pycon

  Python 3.9.4 (default, Apr 4 2021, 19:38:44)
  [GCC 10.2.1 20210401] on linux
  Type "help", "copyright", "credits" or "license" for more information.
  >>> import mon_module
  Mon module __name__ est défini à mon_module
  >>> quit()

Gestion des imports
===================

La façon habituelle d'utiliser **\__name__** et **\__main__** ressemble à ceci avec le script **mon_module_2.py**:

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  # Module de fichier Python
  print("Mon module __name__ est défini à {}".format(__name__))
  if __name__ == "__main__":
      print("Fichier exécuté directement")
  else:
      print("Fichier exécuté comme importé")

Ce qui nous donne à l’exécution directe du module python :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ chmod u+x ./mon_module_2.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ ./mon_module_2.py
  Mon module __name__ est défini à __main__
  Fichier exécuté directement

Et à l’exécution comme module :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/7_Modules$ python3

.. code-block:: pycon

  Python 3.9.4 (default, Apr 4 2021, 19:38:44)
  [GCC 10.2.1 20210401] on linux
  Type "help", "copyright", "credits" or "license" for more information.
  >>> import mon_module_2
  Mon module __name__ est défini à mon_module
  Fichier exécuté comme importé
  >>> quit()


.. only:: latex

  .. raw:: latex

    \newpage
