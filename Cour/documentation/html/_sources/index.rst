.. |date| date::

:Date: |date|
:Revision: 1.0
:Author: Prenom NOM <prenom.nom@fai.fr>
:Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
:Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur/Cour> pour la mise à jour de ce cours.

.. toctree::
   :maxdepth: 3
   :caption: Contenu

.. include:: Cour PYTHON pour l’administrateur systèmes.rst
