.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Objet et caractéristiques
*************************

Plus qu’un simple langage de script, Python est aussi un langage orienté objet.

Ce langage moderne et puissant est né au début des années 1990 sous l’impulsion de Guido van Rossum.

Apparue dans les années 60 quant à elle, la programmation orientée objet (POO) est un paradigme de programmation ; c’est-à-dire une façon de concevoir un programme informatique, reposant sur l’idée qu’un programme est composé d’objets interagissant les uns avec les autres.

En définitive, **un objet est une donnée**. Une donnée constituée de diverses propriétés, et pouvant être manipulée par différentes opérations.

La programmation orientée objet est le paradigme qui nous permet de définir nos propres types d’objets, avec leurs propriétés et opérations.
Ce paradigme vient avec de nombreux concepts qui seront explicités le long de ce cour.

À travers ce cour, nous allons nous intéresser à cette façon de penser et à le programmer avec le langage Python.

Type
====

Ainsi, **tout objet est associé à un type**. Un type définit la sémantique d’un objet. On sait par exemple que les objets de type :python:`int` sont des nombres entiers, que l’on peut les additionner, les soustraire, etc.

Pour la suite de ce cours, nous utiliserons un type :python:`User` représentant un utilisateur sur un quelconque logiciel. Nous pouvons créer ce nouveau type à l’aide du code suivant :

.. code-block:: python

  class User:
      pass

Nous reviendrons sur ce code par la suite, retenez simplement que nous avons maintenant à notre disposition un type :python:`User`.

Pour créer un objet de type :python:`User`, il nous suffit de procéder ainsi :

.. code-block:: python

  john = User()

On dit alors que :python:`john` est **une instance** de :python:`User`.

Les attributs
=============

nous avons dit qu’un objet était constitué d’attributs. **Ces derniers représentent des valeurs propres à l’objet**.

Nos objets de type :python:`User` pourraient par exemple contenir un identifiant (:python:`id`), un nom (:python:`name`) et un mot de passe (:python:`password`).

En Python, nous pouvons facilement associer des valeurs à nos objets :

.. code-block:: python

  class User:
      pass

  # Instanciation d'un objet de type User
  john = User()

  # Définition d'attributs pour cet objet
  john.id = 1
  john.name = 'john'
  john.password = '12345'

  print('Bonjour, je suis {}.'.format(john.name))
  print('Mon id est le {}.'.format(john.id))
  print('Mon mot de passe est {}.'.format(john.password))

Le code ci-dessus affiche :

.. code-block:: console

  Bonjour, je suis john.
  Mon id est le 1.
  Mon mot de passe est 12345.

Nous avons instancié un objet nommé :python:`john`, de type :python:`User`, auquel nous avons attribué trois attributs. Puis nous avons affiché les valeurs de ces attributs.

Notez que l’on peut redéfinir la valeur d’un attribut, et qu’un attribut peut aussi être supprimé à l’aide de l’opérateur :python:`del`.

.. code-block:: pycon

  >>> john.password = 'mot de passe plus sécurisé !'
  >>> john.password
  'mot de passe plus sécurisé !'
  >>> del john.password
  >>> john.password
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  AttributeError: 'User' object has no attribute 'password'

Il est généralement déconseillé de nommer une valeur de la même manière qu’une fonction python:`built-in`. On évitera par exemple d’avoir une variable :python:`id`, :python:`type` ou :python:`list`.

Dans le cas d’un attribut, cela n’est pas gênant car cela ne fait pas partie du même espace de noms. En effet, :python:`john.id` n’entre pas en conflit avec :python:`id`.

Les méthodes
============

Les **méthodes sont les opérations applicables sur les objets**. Ce sont en fait **des fonctions qui recoivent notre objet en premier paramètre**.

Nos objets :python:`User` ne contiennent pas encore de méthode, nous découvrirons comment en ajouter dans le chapitre suivant. Mais nous pouvons déjà imaginer une méthode :python:`check_pwd` (check password) pour vérifier qu’un mot de passe entré correspond bien au mot de passe de notre utilisateur.

.. code-block:: python

  def user_check_pwd(user, password):
      return user.password == password

.. code-block:: pycon

  >>> user_check_pwd(john, 'toto')
  False
  >>> user_check_pwd(john, '12345')
  True

Les méthodes recevant l’objet en paramètre, elles peuvent en lire et modifier les attributs. Souvenez-vous par exemple de la méthode :python:`append()` des listes, qui permet d’insérer un nouvel élément, elle modifie bien la liste en question.

À travers cette partie nous avons défini et exploré la notion d’objet.

Un terme a pourtant été omis, le terme «**classe**». Il s’agit en Python d’un synonyme de «**type**». Un objet étant le fruit d’une classe, il est temps de nous intéresser à cette dernière et à sa construction.

Classes
*******

On définit une classe à l’aide du mot-clef :python:`class` survolé plus tôt :

.. code-block:: python

  class User:
      pass

(l’instruction :python:`pass` sert ici à indiquer à Python que le corps de notre classe est vide)

Il est conseillé en Python de nommer sa classe suivant :python:`MonNomDeClasse`, c’est à dire qu’un nom est composé d’une suite de mots dont la première lettre est une capitale. On préférera par exemple une classe :python:`MonNomDeClasse` que :python:`mon_nom_de_classe`. Exception faite des types :python:`builtins` qui sont couramment écrits en lettres minuscules.

On **instancie** une classe de la même manière qu’on appelle une fonction, **en suffixant son nom d’une paire de parenthèses**. Cela est valable pour notre classe :python:`User`, mais aussi pour les autres classes évoquées plus haut.

.. code-block:: pycon

  >>> User()
  <__main__.User object at 0x7fc28e538198>
  >>> int()
  0
  >>> str()
  ''
  >>> list()
  []

La classe :python:`User` est identique, elle ne comporte aucune méthode. Pour définir une méthode dans une classe, il suffit de procéder comme pour une définition de fonction, mais dans le corps de la classe en question.

.. code-block:: python

  class User:
      def check_pwd(self, password):
          return self.password == password

Notre nouvelle classe :python:`User` possède maintenant une méthode :python:`check_pwd` applicable sur tous ses objets.

.. code-block:: python

  >>> john = User()
  >>> john.id = 1
  >>> john.name = 'john'
  >>> john.password = '12345'
  >>> john.check_pwd('toto')
  False
  >>> john.check_pwd('12345')
  True

Quel est ce :python:`self` reçu en premier paramètre par :python:`check_pwd` ? Il s’agit simplement de l’objet sur lequel on applique la méthode, comme expliqué dans le chapitre précédent. Les autres paramètres de la méthode arrivent après.

La méthode étant définie au niveau de la classe, elle n’a que ce moyen pour savoir quel objet est utilisé. C’est un comportement particulier de Python, mais retenez simplement qu’appeler :python:`john.check_pwd('12345')` équivaut à l’appel :python:`User.check_pwd(john, '12345')`. C’est pourquoi :python:`john` correspondra ici au paramère :python:`self` de notre méthode.

:python:`self` n’est pas un mot-clef du langage Python, le paramètre pourrait donc prendre n’importe quel autre nom. Mais il conservera toujours ce nom par convention.

Notez aussi, dans le corps de la méthode :python:`check_pwd`, que :python:`password` et :python:`self.password` sont bien deux valeurs distinctes : la première est le paramètre reçu par la méthode, tandis que la seconde est l’attribut de notre objet.

Portées et espaces de nommage en Python
=======================================

Les définitions de classes font d'habiles manipulations avec les espaces de nommage, vous devez donc savoir comment les portées et les espaces de nommage fonctionnent.

Commençons par quelques définitions.

**Un espace de nommage est une table de correspondance entre des noms et des objets**. La plupart des espaces de nommage sont actuellement implémentés sous forme de dictionnaires Python, mais ceci n'est normalement pas visible (sauf pour les performances) et peut changer dans le futur. Comme exemples d'espaces de nommage, nous pouvons citer les primitives (fonctions comme :python:`abs()` et les noms des exceptions de base) ; les noms globaux dans un module ; et les noms locaux lors d'un appel de fonction. D'une certaine manière, l'ensemble des attributs d'un objet forme lui-même un espace de nommage. L'important à retenir concernant les espaces de nommage est qu'il n'y a absolument aucun lien entre les noms de différents espaces de nommage ; par exemple, deux modules différents peuvent définir une fonction :python:`maximize` sans qu'il n'y ait de confusion. Les utilisateurs des modules doivent préfixer le nom de la fonction avec celui du module.

À ce propos, nous utilisons le mot «**attribut**» pour tout nom suivant un point. Par exemple, dans l'expression :python:`z.real`, :python:`real` est un attribut de l'objet :python:`z`. Rigoureusement parlant, les références à des noms dans des modules sont des références d'attributs : dans l'expression :python:`nommodule.nomfonction`, :python:`nommodule` est un objet module et :python:`nomfonction` est un attribut de cet objet. Dans ces conditions, il existe une correspondance directe entre les attributs du module et les noms globaux définis dans le module : ils partagent le même espace de nommage!

Les attributs peuvent être en lecture seule ou modifiables. S'ils sont modifiables, l'affectation à un attribut est possible. Les attributs de modules sont modifiables : vous pouvez écrire :python:`nommodule.la_reponse = 42`.
Les attributs modifiables peuvent aussi être effacés avec l'instruction :python:`del`. Par exemple, :python:`del nommodule.la_reponse` supprime l'attribut
:python:`la_reponse` de l'objet nommé :python:`nommodule`.

Les espaces de nommage sont créés à différents moments et ont différentes durées de vie. L'espace de nommage contenant les primitives est créé au démarrage de l'interpréteur Python et n'est jamais effacé. L'espace de nommage globaux pour un module est créé lorsque la définition du module est lue. Habituellement, les espaces de nommage des modules durent aussi jusqu'à l'arrêt de l'interpréteur. Les instructions exécutées par la première invocation de l'interpréteur, qu'elles soient lues depuis un fichier de script ou de manière interactive, sont considérées comme faisant partie d'un module appelé :python:`__main__`, de façon qu'elles possèdent leur propre espace de nommage (les primitives vivent elles-mêmes dans un module, appelé :python:`builtins`).

L'espace des noms locaux d'une fonction est créé lors de son appel, puis effacé lorsqu'elle renvoie un résultat ou lève une exception non prise en charge (en fait, «oublié» serait une meilleure façon de décrire ce qui se passe réellement). Bien sûr, des invocations récursives ont chacune leur propre espace de nommage.

**La portée** est la zone textuelle d'un programme Python où un espace de nommage est directement accessible. «Directement accessible» signifie ici qu'une référence non qualifiée à un nom est cherchée dans l'espace de nommage. Bien que les portées soient déterminées de manière statique, elles sont utilisées de manière dynamique. À n'importe quel moment de l'exécution, il y a au minimum trois ou quatre portées imbriquées dont les espaces de nommage sont directement accessibles :

* la portée la plus au centre, celle qui est consultée en premier, contient les noms locaux ;
* les portées des fonctions englobantes, qui sont consultées en commençant avec la portée englobante la plus proche, contiennent des noms non-locaux mais aussi non-globaux ;
* l'avant-dernière portée contient les noms globaux du module courant ;
* la portée englobante, consultée en dernier, est l'espace de nommage contenant les primitives.

Si un nom est déclaré comme :python:`global`, alors toutes les références et affectations vont directement dans la portée intermédiaire contenant les noms globaux du module. Pour pointer une variable qui se trouve en dehors de la portée la plus locale, vous pouvez utiliser l'instruction :python:`nonlocal`. Si une telle variable n'est pas déclarée :python:`nonlocal`, elle est en lecture seule (toute tentative de la modifier crée simplement une nouvelle variable dans la portée la plus locale, en laissant inchangée la variable du même nom dans sa portée d'origine).

Habituellement, la portée locale référence les noms locaux de la fonction courante. En dehors des fonctions, la portée locale référence le même espace de nommage que la portée globale : l'espace de nommage du module. Les définitions de classes créent un nouvel espace de nommage dans la portée locale.

Il est important de réaliser que les portées sont déterminées de manière textuelle : la portée globale d'une fonction définie dans un module est l'espace de nommage de ce module, quelle que soit la provenance de l'appel à la fonction. En revanche, la recherche réelle des noms est faite dynamiquement au moment de l'exécution. Cependant la définition du langage est en train d'évoluer vers une résolution statique des noms au moment de la «compilation», donc ne vous basez pas sur une résolution dynamique (en réalité, les variables locales sont déjà déterminées de manière statique)!

Une particularité de Python est que, si aucune instruction :python:`global` ou :python:`nonlocal` n'est active, les affectations de noms vont toujours dans la
portée la plus proche. Les affectations ne copient aucune donnée : elles se contentent de lier des noms à des objets. Ceci est également vrai pour l'effacement : l'instruction del x supprime la liaison de x dans l'espace de nommage référencé par la portée locale. En réalité, toutes les opérations qui impliquent des nouveaux noms utilisent la portée locale : en particulier, les instructions import et les définitions de fonctions effectuent une liaison du module ou du nom de fonction dans la portée locale.

L'instruction :python:`global` peut être utilisée pour indiquer que certaines variables existent dans la portée globale et doivent être reliées en local ; l'instruction :python:`nonlocal` indique que certaines variables existent dans une portée supérieure et doivent être reliées en local.

Exemple de portées et d'espaces de nommage
------------------------------------------

Ceci est un exemple montrant comment utiliser les différentes portées et espaces de nommage, et comment :python:`global` et :python:`nonlocal` modifient l'affectation de variable :

.. code-block:: python

  def scope_test():
      def do_local():
          spam = "local spam"

      def do_nonlocal():
          nonlocal spam
          spam = "nonlocal spam"

      def do_global():
          global spam
          spam = "global spam"

      spam = "test spam"
      do_local()
      print("Après affectation locale:", spam)
      do_nonlocal()
      print("Après affectation non locale:", spam)
      do_global()
      print("Après affectation générale:", spam)

  scope_test()
  print("A portée générale:", spam)

Ce code donne le résultat suivant :

.. code-block:: console

  Après affectation locale: test spam
  Après affectation non locale: nonlocal spam
  Après affectation générale: nonlocal spam
  A portée générale: global spam

Vous pouvez constater que l'affectation locale (qui est effectuée par défaut) n'a pas modifié la liaison de :python:`spam` dans :python:`scope_test`. L'affectation :python:`nonlocal` a changé la liaison de :python:`spam` dans :python:`scope_test` et l'affectation :python:`global` a changé la liaison au niveau du module.

Vous pouvez également voir qu'aucune liaison pour spam n'a été faite avant l'affectation :python:`global`.

Passages d’arguments
====================

Nous avons vu qu’instancier une classe était semblable à un appel de fonction. Dans ce cas, comment passer des arguments à une classe, comme on le ferait pour une fonction ?

Il faut pour cela comprendre les bases du mécanisme d’instanciation de Python. Quand on appelle une classe, un nouvel objet de ce type est construit en mémoire, puis initialisé. Cette initialisation permet d’assigner des valeurs à ses attributs.

L’objet est initialisé à l’aide d’une méthode spéciale de sa classe, la méthode :python:`__init__`. Cette dernière recevra les arguments passés lors de l’instanciation.

.. code-block:: python

  class User:
      def __init__(self, id, name, password):
          self.id = id
          self.name = name
          self.password = password

      def check_pwd(self, password):
          return self.password == password

Nous retrouvons dans cette méthode le paramètre :python:`self`, qui est donc utilisé pour modifier les attributs de l’objet.

.. code-block:: pycon

  >>> john = User(1, 'john', '12345')
  >>> john.check_pwd('toto')
  False
  >>> john.check_pwd('12345')
  True

Méthodes spéciales \__repr__ et \__str__
========================================

Nous avons vu précédemment la méthode :python:`__init__`, permettant d’initialiser les attributs d’un objet. On appelle cette méthode une méthode spéciale, il y en a encore beaucoup d’autres en Python. Elles sont reconnaissables par leur nom débutant et finissant par deux underscores.

Vous vous êtes peut-être déjà demandé d’où provenait le résultat affiché sur la console quand on entre simplement le nom d’un objet.

.. code-block:: pycon

  >>> import datetime
  >>> aujourdhui = datetime.datetime.now()
  >>> str(aujourdhui)
  '2020-10-06 12:19:45.099479'
  >>> repr(aujourdhui)
  'datetime.datetime(2020, 10, 6, 12, 19, 45, 99479)'
  >>> resultat = eval(repr(aujourdhui))
  >>> print(resultat)
  2020-10-06 12:19:45.099479

.. code-block:: python

  >>> john = User(1, 'john', '12345')
  >>> john
  <__main__.User object at 0x7fefd77fae10>

Il s’agit en fait de la représentation d’un objet, calculée à partir de sa méthode spéciale :python:`__repr__`.

.. code-block:: pycon

  >>> john.__repr__()
  '<__main__.User object at 0x7fefd77fae10>'

À noter qu’une méthode spéciale n’est presque jamais directement appelée en Python, on lui préférera dans le cas présent la fonction builtin :python:`repr`.

.. code-block:: pycon

  >>> repr(john)
  '<__main__.User object at 0x7fefd77fae10>'

Il nous suffit alors de redéfinir cette méthode :python:`__repr__` pour bénéficier de notre propre représentation.

.. code-block:: pycon

  >>> class User:
  ...     def __repr__(self):
  ...         return '<User: {}, {}>'.format(self.id, self.name)
  >>> User(1, 'john', '12345')
  <User: 1, john>

Une autre opération courante est la conversion de notre objet en chaîne de caractères afin d’être affiché via print par exemple. Par défaut, la conversion en chaîne correspond à la représentation de l’objet, mais elle peut être surchargée par la méthode :python:`__str__`.

.. code-block:: pycon

  >>> class User:
  ...
  ... def __repr__(self):
  ...     return '<User: {}, {}>'.format(self.id, self.name)
  ...
  ... def __str__(self):
  ...     return '{}-{}'.format(self.id, self.name)
  >>> john = User(1, 'john', 12345)
  >>> john
  <User: 1, john>
  >>> repr(john)
  '<User: 1, john>'
  >>> str(john)
  '1-john'
  >>> print(john)
  1-john

Exemple :

.. code-block:: pycon

  >>> class Fraction:
  ...     def __init__(self, num, den):
  ...         self.__num = num
  ...         self.__den = den
  ...
  ...     def resultat(self):
  ...         return 1/2
  ...
  ...     def __str__(self):
  ...         return str(self.resultat())
  ...
  ...     def __repr__(self):
  ...         return str(self.__num) + '/' + str(self.__den)
  >>> f = Fraction(1,2)
  >>> f.resultat()
  0.5
  >>> print(f)
  >>> print('Valeur numérique de ma fraction : ' + str(f))
  Valeur numérique de ma fraction : 0.5
  >>> print('Représentation de ma fraction : ', repr(f))
  Représentation de ma fraction :  1/2

Exercice :

.. code-block:: pycon

  >>> from math import *
  >>> class Racine:
  ...     def __init__(self, valeur):
  ...         self.__valeur = valeur
  ...
  ...     def resultat(self):
  ...         return sqrt(self.__valeur)
  ...
  ...     def __str__(self):
  ...         return str(self.resultat())
  ...
  ...     def __repr__(self):
  ...         return '√' + str(self.__valeur)
  ...
  >>> nombre = Racine(2)
  >>> nombre.resultat()
  1.4142135623730951
  >>> print(nombre)
  1.4142135623730951
  >>> repr(nombre)
  '√2'


Variables privées, l’encapsulation
==================================

Au commencement étaient les invariants

Les différents attributs de notre objet forment un état de cet objet, normalement stable. Ils sont en effet liés les uns aux autres, la modification d’un attribut pouvant avoir des conséquences sur un autre. Les invariants correspondent aux relations qui lient ces différents attributs.

Imaginons que nos objets :python:`User` soient dotés d’un attribut contenant une évaluation du mot de passe (savoir si ce mot de passe est assez sécurisé ou non), il doit alors être mis à jour chaque fois que nous modifions l’attribut :python:`password` d’un objet :python:`User`.

Dans le cas contraire, le mot de passe et l’évaluation ne seraient plus corrélés, et notre objet :python:`User` ne serait alors plus dans un état stable. Il est donc important de veiller à ces invariants pour assurer la stabilité de nos objets.

Protège-moi

Au sein d’un objet, les attributs peuvent avoir des sémantiques différentes. Certains attributs vont représenter des propriétés de l’objet et faire partie de son interface (tels que le prénom et le nom de nos objets :python:`User`). Ils pourront alors être lus et modifiés depuis l’extérieur de l’objet, on parle dans ce cas d’attributs publics.

D’autres vont contenir des données internes à l’objet, n’ayant pas vocation à être accessibles depuis l’extérieur. Nous allons sécuriser notre stockage du mot de passe en ajoutant une méthode pour le hasher (à l’aide du module :python:`crypt`), afin de ne pas stocker d’informations sensibles dans l’objet. Ce condensat du mot de passe ne devrait pas être accessible de l’extérieur, et encore moins modifié (ce qui en altérerait la sécurité).

De la même manière que pour les attributs, certaines méthodes vont avoir une portée publique et d’autres privée (on peut imaginer une méthode interne de la classe pour générer notre identifiant unique). On nomme **encapsulation** cette notion de protection des attributs et méthodes d’un objet, dans le respect de ses invariants.

Certains langages implémentent dans leur syntaxe des outils pour gérer la visibilité des attributs et méthodes, mais il n’y a rien de tel en Python. Il existe à la place des conventions, qui indiquent aux développeurs quels attributs/méthodes sont publics ou privés. Quand vous voyez un nom d’attribut ou méthode débuter par un «**_**» au sein d’un objet, il indique quelque chose d’interne à l’objet (privé), dont la modification peut avoir des conséquences graves sur la stabilité.

.. code-block:: pycon

  >>> import crypt
  >>> class User:
  ...     def __init__(self, id, name, password):
  ...         self.id = id
  ...         self.name = name
  ...         self._salt = crypt.mksalt() # sel utilisé pour le hash du mot de passe
  ...         self._password = self._crypt_pwd(password)
  ...
  ...     def _crypt_pwd(self, password):
  ...         return crypt.crypt(password, self._salt)
  ...
  ...     def check_pwd(self, password):
  ...         return self._password == self._crypt_pwd(password)
  ...
  >>> john = User(1, 'john', '12345')
  >>> john.check_pwd('12345')
  True

On note toutefois qu’il ne s’agit que d’une convention, l’attribut :python:`_password` étant parfaitement visible depuis l’extérieur.

.. code-block:: pycon

  >>> john._password
  '$6$DwdvE5H8sT71Huf/$9a.H/VIK4fdwIFdLJYL34yml/QC3KZ7'

Il reste possible de masquer un peu plus l’attribut à l’aide du préfixe :python:`__`. Ce préfixe a pour effet de renommer l’attribut en y insérant le nom de la classe courante.

.. code-block:: pycon

  >>> class User:
  ...     def __init__(self, id, name, password):
  ...         self.id = id
  ...         self.name = name
  ...         self.__salt = crypt.mksalt()
  ...         self.__password = self.__crypt_pwd(password)
  ...
  ...     def __crypt_pwd(self, password):
  ...         return crypt.crypt(password, self.__salt)
  ...
  ...     def check_pwd(self, password):
  ...         return self.__password == self.__crypt_pwd(password)
  >>> john = User(1, 'john', '12345')
  >>> john.__password
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  AttributeError: 'User' object has no attribute '__password'
  >>> john._User__password
  '$6$kjwoqPPHRQAamRHT$591frrNfNNb3.RdLXYiB/bgdCC4Z0p.B'

Ce comportement pourra surtout être utile pour éviter des conflits de noms entre attributs internes de plusieurs classes sur un même objet, que nous verrons lors de l’héritage.

Le hashage d’un mot de passe correspond à une opération non-réversible qui permet de calculer un condensat (hash) du mot de passe. Ce condensat peut-être utilisé pour vérifier la validité d’un mot de passe, mais ne permet pas de retrouver le mot de passe d’origine.

C++, Java, Ruby, etc.

Duck-typing
===========

Un objet en Python est défini par sa structure (les attributs qu’il contient et les méthodes qui lui sont applicables) plutôt que par son type.

Ainsi, pour faire simple, un fichier sera un objet possédant des méthodes :python:`read`, :python:`write` et :python:`close`. Tout objet respectant cette définition sera considéré par Python comme un fichier.

.. code-block:: python

  class FakeFile:
      def read(self, size=0):
          return ''

      def write(self, s):
          return 0

      def close(self):
          pass

  f = FakeFile()
  print('foo', file=f)

Python est entièrement construit autour de cette idée, appelée **duck-typing** : «**Si je vois un animal qui vole comme un canard, cancane comme un canard, et nage comme un canard, alors j’appelle cet oiseau un canard**» (James Whitcomb Riley)

Exercice :

Pour ce premier exercice, nous allons nous intéresser aux classes d’un forum. Forts de notre type :python:`User` pour représenter un utilisateur, nous souhaitons ajouter une classe :python:`Post`, correspondant à un quelconque message.

Cette classe sera inititalisée avec un auteur (un objet :python:`User`) et un contenu textuel (le corps du message). Une date sera de plus générée lors de la création.

Un Post possèdera une méthode format pour retourner le message formaté, correspondant au HTML suivant :

.. code-block:: html

  <div>
      <span>Par NOM_DE_L_AUTEUR le DATE_AU_FORMAT_JJ_MM_YYYY à HEURE_AU_FORMAT_HH_MM_SS</span>
      <p>
          CORPS_DU_MESSAGE
      </p>
  </div>

De plus, nous ajouterons une méthode post à notre classe User, recevant un corps de message en paramètre et retournant un nouvel objet Post.

.. code-block:: python

  import crypt
  import datetime

  class User:
      def __init__(self, id, name, password):
          self.id = id
          self.name = name
          self._salt = crypt.mksalt()
          self._password = self._crypt_pwd(password)

      def _crypt_pwd(self, password):
          return crypt.crypt(password, self._salt)

      def check_pwd(self, password):
          return self._password == self._crypt_pwd(password)

      def post(self, message):
          return Post(self, message)

  class Post:
      def __init__(self, author, message):
          self.author = author
          self.message = message
          self.date = datetime.datetime.now()

      def format(self):
          date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
          return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

  if __name__ == '__main__':
      user = User(1, 'john', '12345')
      p = user.post('Salut à tous')
      print(p.format())

Nous savons maintenant définir une classe et ses méthodes, initialiser nos objets, et protéger les noms d’attributs/méthodes.

Mais jusqu’ici, quand nous voulons étendre le comportement d’une classe, nous la redéfinissons entièrement en ajoutant de nouveaux attributs/méthodes. Le chapitre suivant présente l’héritage, un concept qui permet d’étendre une ou plusieurs classes sans toucher au code initial.
