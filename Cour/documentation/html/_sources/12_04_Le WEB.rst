Le WEB
******

Il peut être intéressant, dans certains cas, d'implémenter un serveur web dans votre application. Cela permet notamment une communication entre vos programmes via un navigateur.

En Python créer un serveur web , c'est quelques lignes de code.

.. include:: 12_04_1_Créer un serveur HTTP.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 12_04_2_Flask.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 12_04_3_Remi.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 12_04_4_Django.rst
