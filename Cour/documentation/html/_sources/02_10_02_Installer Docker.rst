.. role:: terminal(code)
  :language: console

Installer Docker
================

Installer les applications de base :

.. code-block:: console

  utilisateur@MachineUbuntu:~$ sudo apt install docker.io curl openssh-server ca-certificates postfix mailutils

.. image:: images/postfix_1.png
   :alt: Fenêtre d'information Postfix à valider
   :align: center
   :scale: 100%


.. image:: images/postfix_2.png
   :alt: Type de serveur de messagerie Local
   :align: center
   :scale: 100%


.. image:: images/postfix_3.png
   :alt: Nom de courrier courriel.domaine-perso.fr
   :align: center
   :scale: 100%



Autorisez le compte utilisateur à utiliser docker :

.. code-block:: console

  utilisateur@MachineUbuntu:~$ sudo usermod -aG docker $USER

Démarrez le service docker et ajoutez-le au démarrage du système :

.. code-block:: console

  utilisateur@MachineUbuntu:~$ sudo systemctl start docker

Vérifiez le bon fonctionnement du service docker à l'aide de la commande :terminal:`systemctl` ci-dessous.

.. code-block:: console

  utilisateur@MachineUbuntu:~$ systemctl status docker
  ● docker.service - Docker Application Container Engine
       Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
       Active: active (running) since Fri 2020-10-09 11:07:10 CEST; 47s ago
  TriggeredBy: ● docker.socket
         Docs: https://docs.docker.com
     Main PID: 6241 (dockerd)
        Tasks: 12
       Memory: 38.6M
       CGroup: /system.slice/docker.service
               └─6241 /usr/bin/dockerd -H fd://
  --containerd=/run/containerd/containerd.sock
  q

Activez le service au démarrage.

.. code-block:: console

  utilisateur@MachineUbuntu:~$ sudo systemctl enable docker
  utilisateur@MachineUbuntu:~$ ip a
  …
  4: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 02:42:a3:0c:9c:fb brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
      valid_lft forever preferred_lft forever
    inet6 fa80::42:a3ff:fe0c:9cfb/64 scope link
      valid_lft forever preferred_lft forever
  …

Éditer «**/etc/bind/named.conf.option**» pour ajouter l’interface de docker

.. code-block:: c

  options {
      directory "/var/cache/bind";

      // Pour des raisons de sécurité.
      // Cache la version du serveur DNS pour les clients.
      version "Pas pour les crackers";*

      listen-on { 127.0.0.1; 10.10.10.1; 172.17.0.1; };
      listen-on-v6 { ::1; fd00::; fe80::42:a3ff:fe0c:9cfb; };

      // Optionnel - Comportement par défaut de BIND en récursions.
      recursion yes;

      allow-query { 127.0.0.1; 10.10.10.1; ::1; fd00::; 172.17.0.0/16; fe80::42:a3ff:fe0c:9cfb; };

      // Récursions autorisées seulement pour les interfaces clients
      allow-recursion { 127.0.0.1; 10.10.10.0/24; ::1; fd00::/8; 172.17.0.0/16; fe80::42:a3ff:fe0c:9cfb; };

      dnssec-validation auto;

      // Activer la journalisation des requêtes DNS
      querylog yes;
  };

.. code-block:: console

  utilisateur@MachineUbuntu:~$ sudo named-checkconf

Redémarrer votre Ubuntu pour valider les modifications

.. code-block:: console

  utilisateur@MachineUbuntu:~$ reboot
