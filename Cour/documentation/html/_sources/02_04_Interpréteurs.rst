.. role:: python(code)
  :language: python

Interpréteurs
*************

Python est un langage de haut niveau, c’est à dire que l’on n’a pas à
tenir compte des contraintes du système d’exploitation, comme la gestion
du matériel ou de la mémoire avec le code par exemple.

Python est un langage interprété, c’est-à-dire que son code pour
s’exécuter n’a pas besoin d’être «compilé» (traduit dans le langage
machine) pour une architecture matérielle. Il s’exécute avec
l’interpréteur Python de l’architecture matérielle.

En tant que langage interprété, lorsque nous installons Python, nous
installons un interpréteur.

En réalité Python est un langage semi-interprété, l’interpréteur Python
va passer par une étape de compilation qui ne produira pas un code
adapté à la machine, mais un code intermédiaire. Souvent appelé byte
code, celui-ci sera le code réel interprété par l’interpréteur Python de
l’environnement matériel du système d’exploitation.

Il existe de nombreux interpréteurs Python écrits dans différents
langages qui fonctionnent sur différentes architectures matérielles et
systèmes d’exploitations.

-  **Cpython** : L’interpréteur «classique» écrit en C
-  **Pypy** : Un interpréteur écrit en… Python
-  **Jython** : Un interpréteur écrit en Java qui permet d’accéder en
   Python aux bibliothèques d’objets Java
-  **IronPython** : Un interpréteur écrit en .Net et intégré à Visual
   Studio
-  **PythonNet** (.Net) : Un interpréteur distribué avec vos
   développements d’applications .Net
-  **Rustpython** : Un interpréteur écrit en Rust, langage système bas
   niveau (comme le C, mais plus moderne et très à la mode actuellement)
-  etc.


Installer Python
================

Exercice :

Distribuer la procédure sous forme papier (Windows, MAC, Linux) https://openclassrooms.com/fr/courses/4262331-demarrez-votre-projet-avec-python/4262506-installez-python

Voir la doc https://docs.python.org/fr/3/using/index.html

Mode Interactif
===============

On peut essentiellement distinguer trois types d’interpréteurs
interactifs Python :

-  **python** : l’interpréteur interactif classique et basique intégré à
   Python.
-  **IPython** (intégré avec Jupyter Notebook, le mode ordinateur de
   présentations scientifiques ou d’Intelligence Artificielle) : Un
   interpréteur interactif adapté à l’affichage en temps réel de courbes
   et graphiques dessinés avec Matplotlib.
-  **BPython** (le mode test de codes ou d’exposés pédagogiques de
   codes) : Un interpréteur interactif amélioré grâce à l’utilisation de
   la coloration syntaxique, la mise à disposition d’un historique des
   commandes, la complétion automatique, l’auto indentation, etc.

Suivant nos besoins d’utilisation de Python en mode interactif nous
pourrons être amenés à évoluer de l’interpréteur python classique vers
un des deux autres types (IPython ou BPython).

Exercice :

.. code-block:: console

  utilisateur@MachineUbuntu:~$ python3
  Python 3.9.4 (default, Apr 4 2021, 19:38:44)
  [GCC 10.2.1 20210401] on linux
  Type "help", "copyright", "credits" or "license" for more information.


.. code-block:: pycon

  >>> help()
  Welcome to Python 3.9's help utility!

  If this is your first time using Python, you should definitely check out
  the tutorial on the Internet at https://docs.python.org/3.8/tutorial/.

  Enter the name of any module, keyword, or topic to get help on writing
  Python programs and using Python modules. To quit this help utility and
  return to the interpreter, just type "quit".

  To get a list of available modules, keywords, symbols, or topics, type
  "modules", "keywords", "symbols", or "topics". Each module also comes
  with a one-line summary of what it does; to list the modules whose name
  or summary contain a given string such as "spam", type "modules spam".

  help> quit
  You are now leaving help and returning to the Python interpreter.
  If you want to ask for help on a particular object directly from the
  interpreter, you can type "help(object)". Executing "help('string')"
  has the same effect as typing a particular string at the help> prompt.
  >>> help(quit)
  Help on Quit in module _sitebuiltins object:

  class Quit(builtins.object)
   | Quit(name, eof)
   |
   | Methods defined here:
   |
   | __call__(self, code=None)
   | Call self as a function.
   |
   | __init__(self, name, eof)
   | Initialize self. See help(type(self)) for accurate signature.
   |
   | __repr__(self)
   | Return repr(self).
   |
   |
   | Data descriptors defined here:
   |
   | __dict__
   | dictionary for instance variables (if defined)
   |
   | __weakref__
   | list of weak references to the object (if defined)
  (END)
  q
  >>> quit()


.. only:: latex

  .. raw:: latex

    \newpage


Les mots clé
============

`Distribuer Lexique Mots Clé <../InitiationALaProgrammationPythonPourLAdministrateurSystèmesLexiqueMotsClé.odt>`_


.. only:: latex

  .. raw:: latex

    \newpage


Les fonctions de base de Python
===============================

`Distribuer Lexique Les Fonctions de base <../InitiationALaProgrammationPythonPourLAdministrateurSystèmesLesFonctionsDeBase.odt>`_


.. only:: latex

  .. raw:: latex

    \newpage


Mode interprété
===============

Exercice :

Créer le répertoire répertoire_de_développement :

.. code-block:: console

  utilisateur@MachineUbuntu:~$ mkdir -p repertoire_de_developpement/1_Mode_interprété; cd repertoire_de_developpement/1_Mode_interprété

Créer dans ce répertoire le fichier **mon_1er_programme.py** avec l’éditeur de code choisi, et le modifier comme suit :

.. code-block:: python
  :linenos:

  #! /usr/bin/env python3
  # -*- coding: utf8 -*-

  print('Bonjour à toutes et tous !')


Le **shebang**, représenté par **#!**, c’est un en-tête d'un fichier texte qui indique au système d'exploitation (de type Unix) que ce fichier n'est pas un fichier binaire mais un script (ensemble de commandes) ; sur la même ligne est précisé l'interpréteur permettant d'exécuter ce script.

Exécuter le programme :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ python3 mon_1er_programme.py

Ou sur Unix le rendre exécutable (chmod u+x) et le lancer en ligne de commande comme une simple application :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ chmod u+x mon_1er_programme.py ; ./mon_1er_programme.py

Conversion Python 2 vers Python 3
---------------------------------

.. code-block:: console

  $ python2.7
  Python 2.7.18 (default, Sep  5 2020, 11:17:26)
  [GCC 10.2.0] on linux2
  Type "help", "copyright", "credits" or "license" for more information.


.. code-block:: pycon

  >>> type('chaine') # bits => encodée
  <type 'str'>
  >>> type(u'chaine') # unicode => décodée
  <type 'unicode'>

.. code-block:: console

  $ python3
  Python 3.8.5 (default, Sep  5 2020, 10:50:12)
  [GCC 10.2.0] on linux2
  Type "help", "copyright", "credits" or "license" for more information.


.. code-block:: pycon

  >>> type("chaine") # unicode => decodée
  <class 'str'>
  >>> type(b"chaine") # bits => encodée
  <class 'bytes'>

Votre but, c’est de n’avoir dans votre code que des chaînes de type ‘unicode’.

En Python 3, c’est automatique. Toutes les chaînes sont de type ‘unicode’ (appelé ‘str’ dans cette version) par défaut. En Python 2 en revanche, il faut préfixer la chaîne par un u pour avoir de l’unicode.

Python 2 vient de prendre fin le 1\ :sup:`er` janvier 2020.

Donc si vous utilisez un interpréteur Python 2, dans votre code, TOUTES vos chaînes unicode doivent être déclarées ainsi :

.. code-block:: python

  u"votre chaîne"

Si vous voulez, vous pouvez activer le comportement de Python 3 dans Python 2 en mettant ceci au début de CHACUN de vos modules pour vous aider à migrer vos scripts et programmes :

.. code-block:: python

  from __future__ import unicode_literals

Ceci n’affecte que le fichier en cours, jamais les autres modules. On peut également le mettre au démarrage d’iPython.

Résumé pour migrer Python 2 :

1. Réglez votre éditeur sur UTF8.
2. Mettez # coding: utf8 au début de vos modules.
3. Préfixez toutes vos chaînes de **u** ou faites :python:`from \__future_\_ import unicode_literals` en début de chaque module.

Si vous ne faites pas cela, votre code marchera uniquement avec Python 2. Et un jour, quand Python 2 ne pourra plus être déployer, il ne marchera plus. Plus du tout.

Donner sous forme papier http://sametmax.com/lencoding-en-python-une-bonne-fois-pour-toute/ si besoins de migrations de python2 vers python3


.. only:: latex

  .. raw:: latex

    \newpage


Mode Compilé
============

La compilation en python existe, c’est «**Cython**» ou «LPython».


.. only:: latex

  .. raw:: latex

    \newpage
