���A      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Révision de code�h]�h	�Text����Révision de code�����}��parent�hsba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhh�	_document�h�source���/home/franc/Documents/Projets/Python/initiation-a-la-programmation-python-pour-l-administrateur-systemes/Cour/sources-documents/02_05_Révision de code.rst��line�Kubh	�	paragraph���)��}�(h��Lorsque l’on développe un logiciel, ce dernier est voué à évoluer. On ne part malheureusement pas de l’idée pour aboutir immédiatement au programme fini.�h]�h��Lorsque l’on développe un logiciel, ce dernier est voué à évoluer. On ne part malheureusement pas de l’idée pour aboutir immédiatement au programme fini.�����}�hh/sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khhh)hubh.)��}�(hX]  Même si les spécifications sont précises, il y aura toujours de petits bugs à corriger et donc des lignes de codes seront modifiées, supprimées ou ajoutées. Mais que se passe-t-il lorsque plusieurs développeurs travaillent sur le même fichier ou programme, ou lorsqu’une correction n’en est pas une et qu’il faut revenir en arrière ?�h]�hX]  Même si les spécifications sont précises, il y aura toujours de petits bugs à corriger et donc des lignes de codes seront modifiées, supprimées ou ajoutées. Mais que se passe-t-il lorsque plusieurs développeurs travaillent sur le même fichier ou programme, ou lorsqu’une correction n’en est pas une et qu’il faut revenir en arrière ?�����}�hh=sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khhh)hubh.)��}�(h��C’est là qu’interviennent **les logiciels de gestion de versions concurrentes**, vision collective, **ou de révision de code**, vision individuelle.�h]�(h�C’est là qu’interviennent �����}�hhKsbh	�strong���)��}�(h�5**les logiciels de gestion de versions concurrentes**�h]�h�1les logiciels de gestion de versions concurrentes�����}�hhUsbah}�(h]�h ]�h"]�h$]�h&]�uh(hShhKubh�, vision collective, �����}�hhKsbhT)��}�(h�**ou de révision de code**�h]�h�ou de révision de code�����}�hhgsbah}�(h]�h ]�h"]�h$]�h&]�uh(hShhKubh�, vision individuelle.�����}�hhKsbeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khhh)hubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(h�5**Git** : le standard de fait en mode décentralisé.�h]�h.)��}�(hh�h]�(hT)��}�(h�**Git**�h]�h�Git�����}�hh�sbah}�(h]�h ]�h"]�h$]�h&]�uh(hShh�ubh�. : le standard de fait en mode décentralisé.�����}�hh�sbeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,K
hh�ubah}�(h]�h ]�h"]�h$]�h&]�uh(h�hh�h)hh*h+h,Nubh�)��}�(h�%**Visualsource** : celui de Microsoft�h]�h.)��}�(hh�h]�(hT)��}�(h�**Visualsource**�h]�h�Visualsource�����}�hh�sbah}�(h]�h ]�h"]�h$]�h&]�uh(hShh�ubh� : celui de Microsoft�����}�hh�sbeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khh�ubah}�(h]�h ]�h"]�h$]�h&]�uh(h�hh�h)hh*h+h,Nubh�)��}�(h�Bazaar�h]�h.)��}�(hh�h]�h�Bazaar�����}�hh�sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khh�ubah}�(h]�h ]�h"]�h$]�h&]�uh(h�hh�h)hh*h+h,Nubh�)��}�(h�	Mercurial�h]�h.)��}�(hh�h]�h�	Mercurial�����}�hh�sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khh�ubah}�(h]�h ]�h"]�h$]�h&]�uh(h�hh�h)hh*h+h,Nubh�)��}�(h�$dinosaures (**rcs**, **svn**) etc.

�h]�h.)��}�(h�"dinosaures (**rcs**, **svn**) etc.�h]�(h�dinosaures (�����}�hj  sbhT)��}�(h�**rcs**�h]�h�rcs�����}�hj
  sbah}�(h]�h ]�h"]�h$]�h&]�uh(hShj  ubh�, �����}�hj  sbhT)��}�(h�**svn**�h]�h�svn�����}�hj  sbah}�(h]�h ]�h"]�h$]�h&]�uh(hShj  ubh�) etc.�����}�hj  sbeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khh�ubah}�(h]�h ]�h"]�h$]�h&]�uh(h�hh�h)hh*h+h,Nubeh}�(h]�h ]�h"]�h$]�h&]��bullet��-�uh(hh*h+h,K
hhh)hubh �only���)��}�(hhh]�h	�raw���)��}�(h�\newpage�h]�h�\newpage�����}�hjI  sbah}�(h]�h ]�h"]�h$]�h&]��format��latex��	xml:space��preserve�uh(jG  h*h+h,KhjD  h)hubah}�(h]�h ]�h"]�h$]�h&]��expr��latex�uh(jB  h)hh*h+h,Khhubh)��}�(hhh]�(h)��}�(h�IInstaller un logiciel de révision de code sur le poste de développement�h]�h�IInstaller un logiciel de révision de code sur le poste de développement�����}�hjf  sbah}�(h]�h ]�h"]�h$]�h&]�uh(hhjc  h)hh*h+h,Kubh.)��}�(h�
Exercice :�h]�h�
Exercice :�����}�hjt  sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khjc  h)hubh.)��}�(h��Distribuer procédure installation de git voir https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/6113016-installez-git-sur-votre-ordinateur�h]�(h�/Distribuer procédure installation de git voir �����}�hj�  sbh	�	reference���)��}�(h��https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/6113016-installez-git-sur-votre-ordinateur�h]�h��https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/6113016-installez-git-sur-votre-ordinateur�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]��refuri�j�  uh(j�  hj�  ubeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khjc  h)hubh.)��}�(h�1Documentation voir https://git-scm.com/book/fr/v2�h]�(h�Documentation voir �����}�hj�  sbj�  )��}�(h�https://git-scm.com/book/fr/v2�h]�h�https://git-scm.com/book/fr/v2�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]��refuri�j�  uh(j�  hj�  ubeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,Khjc  h)hubh)��}�(hhh]�(h)��}�(h�Installer git�h]�h�Installer git�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]�uh(hhj�  h)hh*h+h,K"ubh	�literal_block���)��}�(h��utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ cd .. ; sudo apt update; sudo apt upgrade
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install git�h]�h��utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ cd .. ; sudo apt update; sudo apt upgrade
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install git�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]�jY  jZ  �force���language��console��highlight_args�}�uh(j�  h*h+h,K$hj�  h)hubeh}�(h]��installer-git�ah ]�h"]��installer git�ah$]�h&]�uh(h
hjc  h)hh*h+h,K"ubh)��}�(hhh]�(h)��}�(h�Configurer git�h]�h�Configurer git�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]�uh(hhj�  h)hh*h+h,K*ubh.)��}�(h��Récupérer le fichier https://github.com/github/gitignore/blob/master/Python.gitignore et le renommer en **.gitignore** dans le répertoire :�h]�(h�Récupérer le fichier �����}�hj�  sbj�  )��}�(h�@https://github.com/github/gitignore/blob/master/Python.gitignore�h]�h�@https://github.com/github/gitignore/blob/master/Python.gitignore�����}�hj  sbah}�(h]�h ]�h"]�h$]�h&]��refuri�j  uh(j�  hj�  ubh� et le renommer en �����}�hj�  sbhT)��}�(h�**.gitignore**�h]�h�
.gitignore�����}�hj  sbah}�(h]�h ]�h"]�h$]�h&]�uh(hShj�  ubh� dans le répertoire :�����}�hj�  sbeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,K,hj�  h)hubj�  )��}�(h��utilisateur@MachineUbuntu:~/repertoire_de_developpement$ wget https://raw.githubusercontent.com/github/gitignore/master/Python.gitignore ; mv Python.gitignore .gitignore�h]�h��utilisateur@MachineUbuntu:~/repertoire_de_developpement$ wget https://raw.githubusercontent.com/github/gitignore/master/Python.gitignore ; mv Python.gitignore .gitignore�����}�hj0  sbah}�(h]�h ]�h"]�h$]�h&]�jY  jZ  j�  �j�  �console�j�  }�uh(j�  h*h+h,K.hj�  h)hubh.)��}�(h�0Ajouter en début de fichier de **.gitignore** :�h]�(h� Ajouter en début de fichier de �����}�hj@  sbhT)��}�(h�**.gitignore**�h]�h�
.gitignore�����}�hjH  sbah}�(h]�h ]�h"]�h$]�h&]�uh(hShj@  ubh� :�����}�hj@  sbeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,K2hj�  h)hubj�  )��}�(h�# Ignore itself
.gitignore�h]�h�# Ignore itself
.gitignore�����}�hj`  sbah}�(h]�h ]�h"]�h$]�h&]�jY  jZ  j�  �j�  �bash�j�  }�uh(j�  h*h+h,K4hj�  h)hubh.)��}�(h�3Mettre en place la coloration syntaxique dans git :�h]�h�3Mettre en place la coloration syntaxique dans git :�����}�hjp  sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,K9hj�  h)hubj�  )��}�(h�Zutilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global color.ui auto�h]�h�Zutilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global color.ui auto�����}�hj~  sbah}�(h]�h ]�h"]�h$]�h&]�jY  jZ  j�  �j�  �console�j�  }�uh(j�  h*h+h,K;hj�  h)hubh.)��}�(h�;Définir l’utilisateur de git avec son adresse courriel :�h]�h�;Définir l’utilisateur de git avec son adresse courriel :�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,K?hj�  h)hubj�  )��}�(h��utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.name "Prénom NOM"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.email "utilisateur@domaine-perso.fr"�h]�h��utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.name "Prénom NOM"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.email "utilisateur@domaine-perso.fr"�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]�jY  jZ  j�  �j�  �console�j�  }�uh(j�  h*h+h,KAhj�  h)hubh.)��}�(h�cConfigurer les paramètres de la sauvegarde des identifiants de connections aux dépôts distants :�h]�h�cConfigurer les paramètres de la sauvegarde des identifiants de connections aux dépôts distants :�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,KFhj�  h)hubj�  )��}�(h��utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global http.sslVerify false
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git  config --global http.postBuffer 524288000�h]�h��utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global http.sslVerify false
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git  config --global http.postBuffer 524288000�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]�jY  jZ  j�  �j�  �console�j�  }�uh(j�  h*h+h,KHhj�  h)hubjC  )��}�(hhh]�jH  )��}�(h�N<font color="Red">Distribuer un lexique sur Git «commandes git»</font>.</br>�h]�h�N<font color="Red">Distribuer un lexique sur Git «commandes git»</font>.</br>�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]��format��html�jY  jZ  uh(jG  h*h+h,KOhj�  h)hubah}�(h]�h ]�h"]�h$]�h&]�ja  �html�uh(jB  h)hh*h+h,KMhj�  ubjC  )��}�(hhh]�jH  )��}�(h�J\textcolor{red}{Distribuer un lexique sur Git «commandes git»}.
\newline�h]�h�J\textcolor{red}{Distribuer un lexique sur Git «commandes git»}.
\newline�����}�hj�  sbah}�(h]�h ]�h"]�h$]�h&]��format��latex�jY  jZ  uh(jG  h*h+h,KUhj�  h)hubah}�(h]�h ]�h"]�h$]�h&]�ja  �latex�uh(jB  h)hh*h+h,KShj�  ubjC  )��}�(hhh]�h.)��}�(h�0Distribuer un lexique sur Git «commandes git».�h]�h�0Distribuer un lexique sur Git «commandes git».�����}�hj  sbah}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,K\hj�  h)hubah}�(h]�h ]�h"]�h$]�h&]�ja  �not (html or latex)�uh(jB  h)hh*h+h,KZhj�  ubh.)��}�(h�VInitialiser le dépôt git et ajouter le fichier Python «**mon_1er_programme.py**» :�h]�(h�:Initialiser le dépôt git et ajouter le fichier Python «�����}�hj  sbhT)��}�(h�**mon_1er_programme.py**�h]�h�mon_1er_programme.py�����}�hj  sbah}�(h]�h ]�h"]�h$]�h&]�uh(hShj  ubh�» :�����}�hj  sbeh}�(h]�h ]�h"]�h$]�h&]�uh(h-h*h+h,K_hj�  h)hubj�  )��}�(hX  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git init
…
Dépôt Git vide initialisé dans /home/utilisateur/repertoire_de_developpement/.git/
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master

Aucun commit

Fichiers non suivis:
 (utilisez "git add <fichier>…" pour inclure dans ce qui sera validé)
 "1_Mode_interpr\303\251t\303\251/"

 aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)
 utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
 utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
 Sur la branche master

 Aucun commit

Modifications qui seront validées :
 (utilisez "git rm --cached <fichier>…" pour désindexer)
 nouveau fichier : "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout du fichier mon_1er_programme.py"
[master (commit racine) dd36b76] Ajout du fichier mon_1er_programme.py
 1 file changed, 4 insertions(+)
 create mode 100755 "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
rien à valider, la copie de travail est propre�h]�hX  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git init
…
Dépôt Git vide initialisé dans /home/utilisateur/repertoire_de_developpement/.git/
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master

Aucun commit

Fichiers non suivis:
 (utilisez "git add <fichier>…" pour inclure dans ce qui sera validé)
 "1_Mode_interpr\303\251t\303\251/"

 aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)
 utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
 utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
 Sur la branche master

 Aucun commit

Modifications qui seront validées :
 (utilisez "git rm --cached <fichier>…" pour désindexer)
 nouveau fichier : "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout du fichier mon_1er_programme.py"
[master (commit racine) dd36b76] Ajout du fichier mon_1er_programme.py
 1 file changed, 4 insertions(+)
 create mode 100755 "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
Sur la branche master
rien à valider, la copie de travail est propre�����}�hj6  sbah}�(h]�h ]�h"]�h$]�h&]�jY  jZ  j�  �j�  �console�j�  }�uh(j�  h*h+h,Kahj�  h)hubjC  )��}�(hhh]�jH  )��}�(h�\newpage�h]�h�\newpage�����}�hjI  sbah}�(h]�h ]�h"]�h$]�h&]��format��latex�jY  jZ  uh(jG  h*h+h,K�hjF  h)hubah}�(h]�h ]�h"]�h$]�h&]�ja  �latex�uh(jB  h)hh*h+h,K�hj�  ubeh}�(h]��configurer-git�ah ]�h"]��configurer git�ah$]�h&]�uh(h
hjc  h)hh*h+h,K*ubeh}�(h]��Ginstaller-un-logiciel-de-revision-de-code-sur-le-poste-de-developpement�ah ]�h"]��Iinstaller un logiciel de révision de code sur le poste de développement�ah$]�h&]�uh(h
hhh)hh*h+h,Kubeh}�(h]��revision-de-code�ah ]�h"]��révision de code�ah$]�h&]�uh(h
hhh)hh*h+h,Kubah}�(h]�h ]�h"]�h$]�h&]��source�h+uh(h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��fr��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(ju  jr  jm  jj  j�  j�  je  jb  u�	nametypes�}�(ju  �jm  �j�  �je  �uh}�(jr  hjj  jc  j�  j�  jb  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nh)hub.