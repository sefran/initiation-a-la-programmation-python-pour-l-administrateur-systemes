Générer des documents
*********************


PDF
===


Installation
------------

   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo pip install fpdf2


Premiers documents
------------------

Créer le fichier «**PremierPDF.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from fpdf import FPDF

   # Création du contenu PDF
   contenupdf = FPDF()

   # Ajout d'une page
   contenupdf.add_page()

   # Police de carractères
   contenupdf.set_font('Helvetica', size=15)

   # Création d'un cadre texte
   contenupdf.cell(200, 10, txt='Cour Python 3 pour l\'administrateur systèmes', ln=1, align='C')

   # Ajout d'un autre cadre de texte
   contenupdf.cell(200, 10, txt='Création d\'un PDF.', ln=2, align='C')

   # Sauvegarde du PDF dans un fichier
   contenupdf.output('PremierPDF.pdf')

   utilisateur@MachineUbuntu:~/repertoire_de_developpement/15_Documents$ chmod u+x PremierPDF.py
   utilisateur@MachineUbuntu:~/repertoire_de_developpement/15_Documents$ ./PremierPDF.py

[image: Rendu PDF premier exemple FPDF][image]

Transformons maintenant un fichier texte en PDF.

Créer le fichier «**texte.txt**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   «Il est certains esprits dont les sombres pensées
   sont d'un nuage épais toujours embarrassées;
   Le jour de la raison ne le saurait percer.
   Avant donc que d'écrire, apprenez à penser.
   Selon que notre idée est plus ou moins obscure,
   l'expression la suit, ou moins nette ou plus pure.
   Ce que l'on conçoit bien s'énonce clairement,
   et les mots pour le dire arrivent aisément.»

   Boileau, L'Art poétique (1669-1674), Chant premier,
   v. 147-154, éd. ULB, p. 52

   «Dans un monde où,
   avec Google et les moteurs de recherche,
   même les mots ont un prix,
   l'idéal de liberté,
   de démocratie et de gratuité absolue
   cache des opérations financières.»

   Alain Rey - février 2008, p. 102,
   (ISSN 0036-8369), nº 1085

Créer le fichier «**SecondPDF.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from fpdf import FPDF

   # Création du contenu PDF
   contenupdf = FPDF()

   # Ajout d'une page
   contenupdf.add_page()

   # Police de carractères
   contenupdf.set_font('Helvetica', size=15)

   with open('texte.txt', 'r') as fichier:
       # Ajout du texte pour le convertir en PDF
       for ligne in fichier:
           contenupdf.cell(200, 10, txt=ligne, ln=1, align='C')

   # Sauvegarde du PDF dans un fichier
   contenupdf.output('SecondPDF.pdf')

   utilisateur@MachineUbuntu:~/repertoire_de_developpement/15_Documents$ ./SecondPDF.py

[image: Rendu PDF contenu texte][image]


Mise en page
------------

Créer le fichier «**pdf-3.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   import fpdf

   # Création du contenu PDF
   # Orientation : 'P' pour portrait, 'L' pour paysage.
   # format : Format général du document 'A3', 'A4', 'A5', 'letter', 'legal'
   # unit : 'mm', 'cm', 'in', 'pt'
   contenupdf = fpdf.FPDF(unit='mm')

   # Police de carractères
   #contenupdf.add_font('Mafonte', '', 'ttf/CooperHewitt-Book.ttf', uni=True)
   #contenupdf.add_font('Mafonte', 'I', 'ttf/CooperHewitt-BookItalic.ttf', uni=True)
   #contenupdf.add_font('Mafonte', 'B', 'ttf/CooperHewitt-Bold.ttf', uni=True)
   #contenupdf.add_font('Mafonte', 'BI', 'ttf/CooperHewitt-BoldItalic.ttf', uni=True)
   #contenupdf.add_font('MafonteThin', '', 'ttf/CooperHewitt-Thin.ttf', uni=True)
   #contenupdf.add_font('MafonteThin', 'I', 'ttf/CooperHewitt-ThinItalic.ttf', uni=True)
   #contenupdf.add_font('MafonteThin', 'B', 'ttf/CooperHewitt-Light.ttf', uni=True)
   #contenupdf.add_font('MafonteThin', 'BI', 'ttf/CooperHewitt-LightItalic.ttf', uni=True)
   #contenupdf.add_font('MafonteMedium', '', 'ttf/CooperHewitt-Medium.ttf', uni=True)
   #contenupdf.add_font('MafonteMedium', 'I', 'ttf/CooperHewitt-MediumItalic.ttf', uni=True)
   #contenupdf.add_font('MafonteBold', '', 'ttf/CooperHewitt-Semibold.ttf', uni=True)
   #contenupdf.add_font('MafonteBold', 'I', 'ttf/CooperHewitt-SemiboldItalic.ttf', uni=True)
   #contenupdf.add_font('MafonteBold', 'B', 'ttf/CooperHewitt-Heavy.ttf', uni=True)
   contenupdf.add_font('MafonteBold', 'BI', 'ttf/CooperHewitt-HeavyItalic.ttf', uni=True)

   # Options du document
   contenupdf.set_compression(True)
   contenupdf.set_display_mode('fullpage', 'two')
   contenupdf.set_title('Mon PDF de développeur')
   contenupdf.set_author('Développeur Python')
   contenupdf.set_creator('FPDF Ubuntu')
   contenupdf.set_subject('Document de test FPDF généré avec Python')
   contenupdf.set_keywords('Python FPDF Ubuntu')

   #Pied de page
   def footer():
       # À 10 mm du bas
       contenupdf.set_y(-10)
       contenupdf.set_font('Helvetica', 'B', 8)
       contenupdf.cell(0, 10, 'Page ' + str(contenupdf.page_no()) + '/{nb}', 0, 0, 'C')
   contenupdf.footer = footer

   # Ajout d'une page paysage A5
   contenupdf.add_page(orientation='L', format='A5')
   # Choix police de caractère
   contenupdf.set_font('MafonteBold', 'BIU', 15)
   contenupdf.set_text_color(150, 150, 150)
   # Création d'un cadre texte
   contenupdf.cell(200, 10, txt='Cour Python 3 pour l\'administrateur systèmes', ln=1, align='C')
   # Affichage des fontes de carractères
   taille_police = 8
   for fonte in contenupdf.core_fonts:
       if any([lettre for lettre in fonte if lettre.isupper()]):
           continue
       contenupdf.set_font('MafonteBold', 'BIU', 8)
       contenupdf.cell(0, 10, txt='Fonte {} - {} pts'.format(fonte, taille_police), ln=1, align='C')
       contenupdf.set_font(fonte, size=taille_police)
       contenupdf.cell(0, 10, txt='abcdefghijklmnopqrstuvwxyz', ln=1, align='C')
       contenupdf.cell(0, 10, txt='ABCDEFGHIJKLMNOPQRSTUVWXYZ', ln=1, align='C')
       taille_police += 2
   # Ajout nouvelle page portrait A5
   contenupdf.add_page(orientation='P', format='A5')
   contenupdf.set_font('MafonteBold', 'BIU', 12)
   contenupdf.cell(0, 10, txt='UTF-8 : éÉèÈàÀùÙçÇœŒ€±≠¹²³«»…®™←↑→↓', ln=1, align='C')
   # Sauvegarde du PDF dans un fichier
   contenupdf.output('pdf-3.pdf')

[image: Rendu PDF mise en page][image][image: Propriétés du PDF mis en
page][image]


Dessin
------

Créer le fichier «**pdf-4.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from fpdf import FPDF

   # Création du contenu PDF
   # Orientation : 'P' pour portrait, 'L' pour paysage.
   contenupdf = FPDF(orientation='L', unit='mm', format='A4')
   # Ajout d'une page
   contenupdf.add_page()
   # Couleur du dessin
   contenupdf.set_draw_color(139, 0, 0)
   # Épaisseur du trait
   contenupdf.set_line_width(1)
   # Tracé d'une ligne
   contenupdf.line(10, 10, 100, 100)
   # Tracé d'un rectangle
   contenupdf.set_draw_color(255, 0, 0)
   contenupdf.set_fill_color(210, 105, 30)
   contenupdf.rect(20, 20, 60, 60, 'F')
   # Tracé d'une ellipse
   contenupdf.set_draw_color(0, 255, 0)
   contenupdf.set_fill_color(255, 140, 0)
   contenupdf.ellipse(30, 30, 40, 40, 'F')
   # Ajout d'une image
   contenupdf.image('images/graph1.png', x=120, y=30, w=150)
   # Sauvegarde du PDF dans un fichier
   contenupdf.output('pdf-4.pdf')

[image: Dessin PDF][image]


Tableaux
--------

Créer le fichier «**pdf-5.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from fpdf import FPDF

   données = [['Prénom', 'NOM', 'Age'],
              ['Moi', 'EGAUCENTRE', '25'],
              ['Lui', 'VECU', '35'],
              ['Sage', 'EXPERIENCE', '45']]

   # Création du contenu PDF
   contenupdf = FPDF()
   # Ajout d'une page
   contenupdf.add_page()
   # Police de carractères
   contenupdf.set_font('Helvetica', size=15)

   largeur_col = contenupdf.w / 4.5
   hauteur_lin = contenupdf.font_size

   for ligne in données:
       for element in ligne:
           contenupdf.cell(largeur_col, hauteur_lin * 2, txt=element, border=1, align='C')
       contenupdf.ln(hauteur_lin * 2)

   # Sauvegarde du PDF dans un fichier
   contenupdf.output('Tableau.pdf')

[image: Tableau PDF][image]


HTML vers PDF
-------------

Créer le fichier «**pdf-6.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from fpdf import FPDF, HTMLMixin

   class HTMLtoPDF(FPDF, HTMLMixin):
       pass

   # Création du contenu PDF
   contenupdf = HTMLtoPDF()
   # Ajout d'une page
   contenupdf.add_page()
   # Police de carractères
   contenupdf.write_html('''
   <!DOCTYPE html>
   <html>
   <head>
   <style>.article {
     background-color: black;
     color: white;
     padding: 20px;
   }</style>
   </head>
   <body>

   <h2>Mon site</h2>
   <p>Utilisation des styles CSS avec la classe "article" dans un tag HTML :</p>

   <div class="artivle">
     <h2>Mon titre</h2>
     <p>Texte de l'article.</p>
     <p>Encore du texte.</p>
   </div>

   </body>
   </html>
   ''')

   # Sauvegarde du PDF dans un fichier
   contenupdf.output('HTML.pdf')

[image: HTML en PDF][image]

On peut voir que le CSS du HTML n’est pas converti. Mais la structure
HTML est mise en page :-).

Si nous voulons des mises en pages plus complexes, il est préférable
d’utiliser du xsl-fo ou du LaTeX sous peine de faire de la PAO/DAO
avec votre code.


XSL-FO
------

Même si beaucoup de développeur estiment que le langage XSL-FO est
dépassé par le HTML5, il peut être très confortable pour fabriquer des
interfaces sécurisées sur la modification du contenu des données, tout
en permettant de générer des documentations PDF d’archivage ou des
mises à jour avec la dernière charte graphique.

En plus nous avons un générateur (parser) de document open source
"fop" pour créer ces documents à partir d’un fichier XML. Nous pouvons
aussi utiliser une feuille de style XSLT (fo2html.xsl) et du css pour
transformer les données XML en affichage HTML.

Et dans le meilleur des mondes qu’est Python, nous avons même un
module pour générer des documents PDF à partir de la mise en page XSL-
FO.


Installation
~~~~~~~~~~~~

   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install fop
   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo pip install pypfop==1.0a1


Générer un PDF
~~~~~~~~~~~~~~

Créer le fichier «**bonjouràtous.fo.mako**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   <%inherit file="A4-portrait.fo.mako" />
   <block>Bonjour ${nom}!</block>

   >>> import os
   >>> from pypfop import generate_document
   >>> chemin_pdf = generate_document('bonjouràtous.fo.mako', {'nom': 'Programmeur PYTHON'})
   >>> chemin_pdf
   '/tmp/tmpon_o8chz.pdf'
   >>> chemin_pdf = generate_document('bonjouràtous.fo.mako', {'nom': 'Programmeur PYTHON'}, tempdir='.')
   >>> chemin_pdf
   '/home/utilisateur/repertoire_de_developpement/15_Documents/tmpsbjw0mbg.pdf'
   >>> os.rename(chemin_pdf, os.path.join(os.path.dirname(chemin_pdf), 'fop.pdf'))

[image: PDF /tmp/tmpon_o8chz.pdf généré par FOP][image]

Nous venons de voir qu’il est assez facile de créer un document PDF
avec Python et FOP. Mais ce qui est intéressent avec le xsl-fo, c’est
que l’on peut séparer le contenu, la structure rédactionnelle, la mise
en page et le rendu de sortie.

Mais ici, nous allons ne contenter du processus suivant :

modèle xml de structure rédactionnelle avec paramètres ->
transformation Python mako des paramètres -> mise en forme css ->
résultat xsl-fo -> rendu parser FOP -> Document généré


Modèle XSL-FO
~~~~~~~~~~~~~

Créer le fichier «**tableau.fo.mako**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   <%inherit file="A4-portrait.fo.mako" />
   <table id="table-principale">
       <table-header>
           <table-row>
               % for nom in entete:
               <table-cell>
                   <block>${nom}</block>
               </table-cell>
               % endfor
           </table-row>
       </table-header>
       <table-body>
           % for ligne in lignes:
           <table-row>
               % for cellule in ligne:
               <table-cell>
                   <block>${cellule}</block>
               </table-cell>
               % endfor
           </table-row>
           % endfor
       </table-body>
   </table>

Créer le fichier «**tableau.css**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   @import url("base.css");
   @import url("couleurs.css");

   #table-principale > table-header > table-row{
       text-align: center;
       font-weight: bold;
   }

   #table-principale > table-header table-cell{
       padding: 2mm 0 0mm;
   }

Créer le fichier «**base.css**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   flow[flow-name="xsl-region-body"] {
       font-size: 10pt;
       font-family: Helvetica;
   }

Créer le fichier «**couleurs.css**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #table-principale > table-body > table-row > table-cell:first-child{
       color: red;
   }
   #table-principale > table-body > table-row > table-cell:nth-child(2){
       color: green;
   }
   #table-principale > table-body > table-row > table-cell:nth-child(3){
       color: blue;
   }
   #table-principale > table-body > table-row > table-cell:last-child{
       color: purple;
   }

Créer le fichier «**tableau-xsl-fo.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   import os, pypfop

   format_de_fichier = 'pdf' # 'pdf', 'rtf', 'tiff', 'png', 'pcl', 'ps', 'txt'
   donnees = {
       'entete': ['Nom', 'Prénom', 'Age', 'Sexe'],
       'lignes': [
           ('PYTHON', 'Programmeur', 25, 'M'),
           ('ANONYME', 'Personne', 30, 'F'),
           ('INCONNU', 'Utilisateur', 43, 'H')
       ]
   }

   chemin_document = pypfop.generate_document('tableau.fo.mako', donnees, 'tableau.css', tempdir='.', out_format=format_de_fichier)
   os.rename(chemin_document, os.path.join(os.path.dirname(chemin_document), 'tableau-xsl-fo.pdf'))

[image: PDF tableau-xsl-fo généré par FOP][image]

Nous voyons que le fichier est rendu suivant la configuration des
fichiers css.


WeasyPrint
----------

Bon, vous ne voulez vraiment pas de xsl-fo parce qu’il est soit disant
dépassé par HTML5. Et vous voulez rendre les fichiers CSS3 dans vos
PDF. Il vous reste la solution **WeasyPrint**.


Installation
~~~~~~~~~~~~

   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo pip install weasyprint


Conversion directe
~~~~~~~~~~~~~~~~~~

Créer le fichier «**exemple.html**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   <!DOCTYPE html>
   <html lang="fr">
   <head>
   <title>Modèle CSS</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <style>
   * {
     box-sizing: border-box;
   }

   body {
     font-family: Arial, Helvetica, sans-serif;
   }

   .en-tête {
     background-color: #f1f1f1;
     padding: 30px;
     text-align: center;
     font-size: 35px;
   }

   .colonne {
     float: left;
     padding: 10px;
     height: 300px; /* Should be removed. Only for demonstration */
   }

   .colonne.coté {
     width: 25%;
   }

   .colonne.milieu {
     width: 50%;
   }

   .ligne:after {
     content: "";
     display: table;
     clear: both;
   }

   .pie-de-page {
     background-color: #f1f1f1;
     padding: 10px;
     text-align: center;
   }

   @media (max-width: 600px) {
     .colonne.coté, .colonne.milieu {
       width: 100%;
     }
   }
   </style>
   </head>
   <body>

   <h2>Exemple de ducument HTML avec du CSS</h2>
   <p>Ce texte est là pour tester un rendu PDF d'un document HTML avec une mise en page CSS.</p>
   <p>Ce document quand il est ouvert avec un navigateur peut modifier son apparence.</p>

   <div class="en-tête">
     <h2>En-tête</h2>
   </div>

   <div class="ligne">
     <div class="colonne coté" style="background-color:#aaa;">Colonne de gauche</div>
     <div class="colonne milieu" style="background-color:#bbb;">Colonne centrale</div>
     <div class="colonne coté" style="background-color:#ccc;">Colonne de droite</div>
   </div>

   <div class="pied-de-page">
     <p>Pied de page</p>
   </div>

   </body>
   </html>

   utilisateur@MachineUbuntu:~/repertoire_de_developpement/15_Documents$ weasyprint exemple.html weasyprint_exemple_direct.pdf
   WARNING: Expected a media type, got '(max-width: 600px)'
   WARNING: Invalid media type ' (max-width: 600px) ' the whole @media rule was ignored at 43:1.

[image: PDF généré par WeasyPrint en ligne de commande][image]


Code
~~~~

Créer le fichier «**createpdf.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from weasyprint import HTML

   document_html = HTML(filename='exemple.html')
   document_html.write_pdf('weasyprint_exemple_code.pdf')

[image: PDF généré par WeasyPrint avec du code Python][image]


LaTeX vers PDF
--------------

Oui tout cela est bien jolie, nous avons généré un rendu WEB en PDF.
Mais si on veut passer à la gamme au dessus, pour avoir un vrai
rendu/gestion PAO, on peut utiliser LaTeX.

LaTeX est un outil de WYSIMING. Donc les documents sont construit
suivant ce que vous pensez, et non pas ce que vous voyez, mais vous
êtes sûr d’avoir un rendu PAO. La structure des documents LaTeX est la
suivante :

Classe de document (fichier .class) qui définit la PAO de base du
document -> les extensions du document qui définissent la mise en
forme du texte ou la mise en page (fichiers .sty) -> le contenu du
document.

Pour l’élaboration d’une classe ou d’une extension de document LaTeX,
je vous renvoie vers Extensions et classes du site de Gutenberg
Europe.

Pour affiner le développement TeX lire Apprendre à programmer en TeX

Nous allons ici utiliser le module Python "Pylatex" pour gérer le
rendu PDF avec des documents LaTeX.


Installation
~~~~~~~~~~~~

   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo pat install fonts-linuxlibertine
   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo pip install pylatex


Utilisation
~~~~~~~~~~~

Créer le fichier «**latex2pdf.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from pylatex import Command, Package, Document, Section, Subsection, Tabular, TextColor
   from pylatex.utils import italic, bold, NoEscape
   from pylatex import Math

   options_mise_en_page = {'tmargin': '1cm', 'lmargin': '10cm'}
   document = Document(geometry_options=options_mise_en_page, fontenc=None, inputenc=None)

   document.documentclass = Command('documentclass', options=['a4paper', 'landscape', '12pt'], arguments=['article'])

   document.packages.append(Package('babel', 'french'))
   #document.packages.append(Package('french'))
   document.preamble.append(Command('selectlanguage', 'french'))

   document.preamble.append(Command('title', NoEscape(r'\color{red}Le titre de mon document\color{black}')))
   document.preamble.append(Command('author', 'Programmeur PYTHON'))
   document.preamble.append(Command('date', NoEscape(r'\today')))
   document.append(NoEscape(r'\maketitle'))

   with document.create(Section('La section de mon document')):
       document.append('Une phrase comme ça… \n')
       document.append(italic('Du texte italique.'))
       document.append(bold('Du texte en gras.\n'))
       document.append(TextColor('violet', bold('Gras et en violet.\n')))
       document.append('Des caractères spéciaux : àÀéÉèÈçÇûÛùÙœŒ±≠×÷€$£µ…¿¡§¹²³™©\n')
       document.append('Une phrase "entre guillemets". «ou ici» l\'apostrophe, fi.')
       with document.create(Subsection(NoEscape(r'\color{gray}Les mathématiques\color{black}'))):
           document.append(Math(data=['3*3', '=', 9]))
       with document.create(Subsection(NoEscape(r'\color{gray}Un tableau\color{black}'))):
           with document.create(Tabular('rc|cl')) as tableau:
               tableau.add_hline()
               tableau.add_row((TextColor('teal', 'C1'), TextColor('orange', 'C2'), 'C3', 'C4'))
               tableau.add_hline(1, 2)
               tableau.add_empty_row()
               tableau.add_row(('C5', 'C6', 'C7', 'C8'))

   document.generate_pdf('latex', clean_tex=False, compiler='xelatex')

[image: PDF textes générés avec LaTeX][image]


Graphiques
~~~~~~~~~~

Créer le fichier «**graphiques2pdf.py**» dans le répertoire
«**repertoire_de_developpement/15_Documents**».

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   import os
   from pylatex import Command, Package, Document, Section, Subsection, TextColor
   from pylatex.utils import italic, bold, NoEscape
   from pylatex import Math, TikZ, Figure, Axis, Plot, TikZNode, TikZOptions, TikZCoordinate, TikZDraw, TikZUserPath

   options_mise_en_page = {'tmargin': '1cm', 'lmargin': '3cm'}
   document = Document(geometry_options=options_mise_en_page, fontenc=None, inputenc=None)

   document.documentclass = Command('documentclass', options=['a4paper', '10pt'], arguments=['article'])
   document.packages.append(Package('xcolor'))

   document.packages.append(Package('babel', 'french'))
   #document.packages.append(Package('french'))
   document.preamble.append(Command('selectlanguage', 'french'))

   document.preamble.append(Command('title', NoEscape(r'\color{red}Des graphiques\color{black}')))
   document.preamble.append(Command('author', 'Programmeur PYTHON'))
   document.preamble.append(Command('date', NoEscape(r'\today')))
   document.append(NoEscape(r'\maketitle'))

   with document.create(Section(NoEscape(r'\color{gray}Les graphiques\color{black}'))):
       with document.create(Subsection(NoEscape(r'\color{teal}Une image\color{black}'))):
           with document.create(Figure(position='h!')) as image:
               image.add_image(os.path.abspath('./images/graph1.png'), width='360pt')
               image.add_caption('Une image')
       with document.create(Subsection(NoEscape(r'\color{teal}Une courbe\color{black}'))):
           with document.create(TikZ()):
               options_de_trace = 'height=8cm, width=12cm, grid=major, domain=0.001:10'
               with document.create(Axis(options=options_de_trace)) as graphe:
                   graphe.append(Plot(name='Courbe', func='sin(deg(x))/x', options=['samples=200', 'patch type=quadratic spline', 'blue', 'mark=None']))
                   coordonees = [(0.0, 1.0), (1.0, 0.85), (2.0, 0.45), (3.0, 0.05), (4.0, -0.2), (5.0, -0.2), (6.0, -0.05), (7.0, 0.1), (8.0, 0.10), (9.0, 0.05), (10.0, -0.05)]
                   graphe.append(Plot(name='Mesures', coordinates=coordonees, options=['only marks', 'red', 'mark=x']))
       with document.create(Subsection(NoEscape(r'\color{teal}Un diagramme\color{black}'))):
           with document.create(TikZ()) as diagramme:
               noderond_kwargs = {'draw': 'green!60', 'fill': 'green!5', 'minimum size': '7mm'}
               noeud_rond = TikZOptions('circle','very thick', **noderond_kwargs)
               nodecarre_kwargs = {'draw': 'red!60', 'fill': 'red!5', 'minimum size': '5mm'}
               noeud_carre = TikZOptions('rectangle','very thick', **nodecarre_kwargs)
               position_noeud1 = TikZCoordinate(0, 2)
               noeud1 = TikZNode(text='1', handle='node1', options=noeud_rond, at=position_noeud1)
               position_noeud2 = TikZCoordinate(0, 1)
               noeud2 = TikZNode(text='2', handle='node2', options=noeud_carre, at=position_noeud2)
               position_noeud3 = TikZCoordinate(1, 1)
               noeud3 = TikZNode(text='3', handle='node3', options=noeud_carre, at=position_noeud3)
               position_noeud4 = TikZCoordinate(0, 0)
               noeud4 = TikZNode(text='4', handle='node4', options=noeud_rond, at=position_noeud4)
               diagramme.append(noeud1)
               diagramme.append(noeud2)
               diagramme.append(noeud3)
               diagramme.append(noeud4)
               diagramme.append(TikZDraw([noeud1.south, '--', noeud2.north], options=TikZOptions('->')))
               diagramme.append(TikZDraw([noeud2.east, '--', noeud3.west], options=TikZOptions('->')))
               diagramme.append(TikZDraw([noeud3.south, TikZUserPath('.. controls +(down:7mm) and +(right:7mm) ..'), noeud4.east], options=TikZOptions('->')))

   document.generate_pdf('graphiques', clean_tex=False, compiler='xelatex')

[image: PDF graphiques générés avec LaTeX][image]


Opendocument
============


Installation
------------

   utilisateur@MachineUbuntu:~/repertoire_de_developpement/15_Documents$ sudo pip install odfpy

Pour visualiser les documents générés nous installons libre office.

   utilisateur@MachineUbuntu:~/repertoire_de_developpement/15_Documents$ sudo apt install libreoffice


Générer des documents textes
----------------------------

Éditer «**OpenDocumentText.py**»

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from odf.opendocument import OpenDocumentText
   from odf.style import PageLayout, MasterPage, Footer, Style, TextProperties, ParagraphProperties, TableProperties, TableColumnProperties
   from odf.text import H, P, Span
   from odf.table import Table, TableColumn, TableRow, TableCell

   document_texte = OpenDocumentText()

   # Mise en page
   mise_en_page = PageLayout(name='Mise en page')
   document_texte.automaticstyles.addElement(mise_en_page)
   page_principale = MasterPage(name='Standard', pagelayoutname=mise_en_page)
   document_texte.masterstyles.addElement(page_principale)

   # Styles
   style = document_texte.styles
   # Création d'un style
   style_de_titre = Style(name='Titre principal', parentstylename='Standard', family='paragraph')
   style_de_titre.addElement(TextProperties(attributes={'fontsize':'24pt', 'fontweight':'bold'}))
   style.addElement(style_de_titre)

   # Un style automatique
   style_gras = Style(name='Texte en Gras', family='text')
   en_gras = TextProperties(fontweight='bold')
   style_gras.addElement(en_gras)
   document_texte.automaticstyles.addElement(style_gras)

   # Un tableau
   contenu_tableau = Style(name='Contenu tableau', family='paragraph')
   contenu_tableau.addElement(ParagraphProperties(numberlines='false', linenumber='0'))
   style.addElement(contenu_tableau)
   # Styles automatiques du tableau
   pagination_tableau = Style(name='Pagination tableau', family='table')
   pagination_tableau.addElement(TableProperties(width='10cm', align='center'))
   document_texte.automaticstyles.addElement(pagination_tableau)
   colonne1 = Style(name='Colonne de gauche', family='table-column')
   colonne1.addElement(TableColumnProperties(columnwidth='2cm'))
   document_texte.automaticstyles.addElement(colonne1)
   colonne2 = Style(name='Colonne de droite', family='table-column')
   colonne2.addElement(TableColumnProperties(columnwidth='8cm'))
   document_texte.automaticstyles.addElement(colonne2)

   # Un paragraphe avec un saut de page
   saut_de_page = Style(name='Saut de page', parentstylename='Standard', family='paragraph')
   saut_de_page.addElement(ParagraphProperties(breakbefore='page'))
   document_texte.automaticstyles.addElement(saut_de_page)

   # Titre de document
   ligne_de_titre = H(outlinelevel=1, stylename=style_de_titre, text="Mon titre de document")
   document_texte.text.addElement(ligne_de_titre)

   # Texte
   paragraphe1 = P(text="Bonjour à tous!")
   document_texte.text.addElement(paragraphe1)
   paragraphe2 = P(text="")
   section_en_gras = Span(stylename=style_gras, text="Ceci est un passage en gras.")
   paragraphe2.addElement(section_en_gras)
   paragraphe2.addText(" Ceci est après la section en gras.")
   document_texte.text.addElement(paragraphe2)

   # Tableau
   tableau = Table(name='Tableau_Python3', stylename='Pagination tableau')
   tableau.addElement(TableColumn(numbercolumnsrepeated=1, stylename='colonne1'))
   tableau.addElement(TableColumn(numbercolumnsrepeated=1, stylename='colonne2'))
   ligne1_tableau = TableRow()
   cellule1 = TableCell(stylename='colonne1')
   ligne1_tableau.addElement(cellule1)
   cellule2 = TableCell(stylename='colonne2')
   ligne1_tableau.addElement(cellule2)
   tableau.addElement(ligne1_tableau)
   ligne2_tableau = TableRow()
   tableau.addElement(ligne2_tableau)
   cellule3 = TableCell(stylename='colonne1')
   ligne2_tableau.addElement(cellule3)
   cellule4 = TableCell(stylename='colonne2')
   ligne2_tableau.addElement(cellule4)
   cellule1.addElement(P(stylename=contenu_tableau, text="Colonne 1"))
   cellule2.addElement(P(stylename=contenu_tableau, text="Colonne 2"))
   cellule3.addElement(P(stylename=contenu_tableau, text="Contenu 1"))
   cellule4.addElement(P(stylename=contenu_tableau, text="Contenu 2"))
   document_texte.text.addElement(tableau)

   # Saut de page
   paragraphe3 = P(stylename=saut_de_page, text='Texte de deuxième page')
   document_texte.text.addElement(paragraphe3)
   document_texte.save('monpremierdocument.odt')

[image: Résultat du fichier monpremierdocument.odt][image]


Générer des documents classeur
------------------------------

Éditer «**OpenDocumentCalc.py**»

   #! /usr/bin/env python3
   # -*- coding: utf-8 -*-

   from odf.opendocument import OpenDocumentSpreadsheet
   from odf.style import Style, TextProperties, ParagraphProperties, TableColumnProperties, TableCellProperties, Map
   from odf.number import NumberStyle, CurrencyStyle, CurrencySymbol, Number, Text
   from odf.text import P
   from odf.table import Table, TableColumn, TableRow, TableCell

   classeur = OpenDocumentSpreadsheet()

   # Création des styles du tableau
   style_contenu_classeur = Style(name='argent', family='table-cell')
   style_contenu_classeur.addElement(TableCellProperties(textalignsource='fix', repeatcontent='false', verticalalign='middle', border='1.0pt solid #808080'))
   style_contenu_classeur.addElement(ParagraphProperties(textalign='center'))
   style_contenu_classeur.addElement(TextProperties(fontfamily='Noto Sans', fontsize='15pt'))
   classeur.styles.addElement(style_contenu_classeur)
   # Style automatiques
   # Colonne tableurs
   style_colonne = Style(name='colonne1', family='table-column')
   style_colonne.addElement(TableColumnProperties(columnwidth='5.0cm', breakbefore='auto'))
   classeur.automaticstyles.addElement(style_colonne)

   # Style cellules
   # Création du style de valeur monétaire financière français euro négative
   style_euro_negatif = CurrencyStyle(name='monnaie-euro-negative', volatile='true')
   # Change la couleur du texte en rouge
   style_euro_negatif.addElement(TextProperties(color='#ff0000'))
   # Préfixe le texte avec le symbole négatif
   style_euro_negatif.addElement(Text(text=u'-'))
   # Met la valeur numérique en forme avec deux décimales après la virgule, avec au minimum 1 digit et en séparant les milliers
   style_euro_negatif.addElement(Number(decimalplaces='2', minintegerdigits='1', grouping='true'))
   # afficher le synmbole €
   style_euro_negatif.addElement(CurrencySymbol(language='fr', country='FR', text=u' €'))
   # Ajout du style
   classeur.styles.addElement(style_euro_negatif)
   # Création du style de valeur monétaire financière français euro
   style_euro = CurrencyStyle(name='monnaie-euro')
   # Met la valeur numérique en forme avec deux décimales après la virgule, avec au minimum 1 digit et en séparant les milliers
   style_euro.addElement(Number(decimalplaces='2', minintegerdigits='1', grouping='true'))
   # formatage conditionnel si négatif afficher le style négatif
   style_euro.addElement(Map(condition='value()<0', applystylename='monnaie-euro-negative'))
   # Afficher le symbole € à la fin
   style_euro.addElement(CurrencySymbol(language='fr', country='FR', text=u' €'))
   # Ajout du style
   classeur.styles.addElement(style_euro)

   # Création du style monétaire de cellule
   style_monetaire = Style(name="monnaie", family="table-cell", parentstylename=style_contenu_classeur, datastylename="monnaie-euro")
   classeur.automaticstyles.addElement(style_monetaire)

   # Création d'un tableau de données monétaires
   tableau = Table(name='Tableau de valeurs monétaires')
   tableau.addElement(TableColumn(stylename=style_colonne, defaultcellstylename='monnaie'))
   ligne1 = TableRow()
   tableau.addElement(ligne1)
   cellule1 = TableCell(valuetype='currency', currency='EUR', value="-1025.25")
   ligne1.addElement(cellule1)
   ligne2 = TableRow()
   tableau.addElement(ligne2)
   cellule2 = TableCell(valuetype="currency", currency="EUR", value="5000023.8")
   ligne2.addElement(cellule2)
   ligne3 = TableRow()
   tableau.addElement(ligne3)
   cellule3 = TableCell(valuetype='currency', currency='EUR', value="10089")
   ligne3.addElement(cellule3)
   ligne4 = TableRow()
   tableau.addElement(ligne4)
   cellule4 = TableCell(valuetype='currency', currency='EUR', value="-10")
   ligne4.addElement(cellule4)

   classeur.spreadsheet.addElement(tableau)
   #print(classeur.contentxml())
   classeur.save("monpremiertableur.ods")

[image: Résultat du fichier monpremiertableur.ods][image]
