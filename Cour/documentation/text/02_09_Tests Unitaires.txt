Tests Unitaires
***************

* **Tests unitaires en python** (Unittest et doctest)

* **Frameworks de tests** (Unittest, Robot, Pytest, Doctest, Nose2,
  Testify)

Les tests unitaires permettent de vérifier (tester) des éléments
particuliers d’un programme.

Par exemple si un programme contient plusieurs parties de code
autonome, les tests unitaires permettront de vérifier leurs présences,
le fonctionnement de chacune des parties suivant un comportement
attendu.

La mise en place de tests unitaires permet de s’assurer que la
correction de bugs, ou le développement de nouvelles fonctions,
n’entraînera pas de régressions au niveau du code.

Nous verrons ultérieurement au cours de cette formation le module
Python Unittest

* Architecture des tests

* Les valeurs de retour des tests

* Les différents tests de Unittest

* Exécution de l’ensemble des tests

Mise en place de l’infrastructure, créer le répertoire tests :

   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir tests ; touch tests/README.md

Contenu du fichier «**README.md**»

   # Tests unitaires du code Python


Sauvegarder la structure de tests
=================================

   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
   Sur la branche master
   Votre branche est à jour avec 'origin/master'.
   Modifications qui seront validées :
   (utilisez "git restore --staged <fichier>..." pour désindexer)
    nouveau fichier : tests/README.md
   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout de la structure de tests"
   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd ..
