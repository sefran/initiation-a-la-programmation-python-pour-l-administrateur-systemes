Les types de variables
**********************


Logique
=======


Booléen
-------

   >>> a = True
   >>> type(a)
   <class 'bool'>
   >>> b = False
   >>> type(b)
   <class 'bool'>

Les tests

   >>> a = 10 > 9
   >>> a
   True
   >>> a = 10 == 9
   >>> a
   False

Les valeurs vraie

   >>> bool("abc")
   True
   >>> bool(123)
   True
   >>> bool(["apple", "cherry", "banana"])
   True

Les valeurs Nules

   bool(False)
   bool(None)
   bool(0)
   bool("")
   bool(())
   bool([])
   bool({})


Chaînes de caractères
=====================


Chaîne de caractère ASCII
-------------------------

Python 3

   byte()

Python 2

   str()


Chaîne de caractère Unicode
---------------------------

Python 3

   str()

Python 2

   unicode()


Manipulation des chaînes de caractères
--------------------------------------

Python sait manipuler des chaînes de caractères, qui peuvent être
exprimées de différentes manières. **Elles peuvent être écrites entre
guillemets anglo-saxon simples ('…') ou entre guillemets anglo-saxon
doubles ("…") sans distinction. \ peut aussi être utilisé pour
protéger un guillemet** :

   >>> 'inutile bidon' # simples quotes
   'inutile bidon'
   >>> 'L\'apostrophe' # utilise \\' pour échapper le simple quote…
   "L'apostrophe"
   >>> "L'apostrophe" # …ou on utilise des doubles quotes
   "L'apostrophe"
   >>> 'Son nom est "Personne"'
   'Son nom est "Personne"'
   >>> "Son nom est \"Personne\""
   'Son nom est "Personne"'
   >>> 'Python c’est "l\'avenir"'
   'Python c’est "l\'avenir"'

En mode interactif, l’interpréteur affiche les chaînes de caractères
entre guillemets. Les guillemets et autres caractères spéciaux sont
protégés avec des barres obliques inverses (backslash en anglais).
Bien que cela puisse être affiché différemment de ce qui a été entré
(les guillemets peuvent changer), les deux formats sont équivalents.
La chaîne est affichée entre guillemets si elle contient un guillemet
simple et aucun guillemet, sinon elle est affichée entre guillemets
simples. **La fonction print() affiche les chaînes de manière plus
lisible, en retirant les guillemets et en affichant les caractères
spéciaux qui étaient protégés par une barre oblique inverse** :

   >>> print('Python c’est "l\'avenir"')
   Python c’est "l'avenir"
   >>> s = 'Première ligne.\nSeconde ligne.' # \n c’est nouvelle ligne
   >>> s # sans print(), \n est incluse dans la sortie
   'Première ligne.\nSeconde ligne.'
   >>> print(s) # avec print(), \n est traduit comme une nouvelle ligne
   Première ligne.
   Seconde ligne.

**Si vous ne voulez pas que les caractères précédés d’un \ soient
interprétés** comme étant spéciaux, utilisez les chaînes brutes (raw
strings en anglais) en préfixant la chaîne d’un **r** :

   >>> print('C:\son\nom') # \n veut dire nouvelle ligne!
   C:\son
   om
   >>> print(r'C:\son\nom') # avec r avant le quote
   C:\son\nom

**Les chaînes de caractères peuvent s’étendre sur plusieurs lignes.
Utilisez alors des triples guillemets, simples ou doubles : '''…''' ou
"""…"""**. Les retours à la ligne sont automatiquement inclus, mais on
peut l’empêcher en ajoutant \ à la fin de la ligne. L’exemple suivant
:

   >>> print("""\
   ... Utilisation: programme [OPTIONS]
   ...     -h            Affiche ce message d’utilisation
   ...     -H nomMachine Nom de la machine où se connecter
   ... """)

produit l’affichage suivant (notez que le premier retour à la ligne
n’est pas inclus) :

   Utilisation: programme [OPTIONS]
       -h            Affiche ce message d’utilisation
       -H nomMachine Nom de la machine où se connecter

**Les chaînes peuvent être concaténées** (collées ensemble) avec
l’opérateur «**+**» et **répétées** avec l’opérateur «*****» :

   >>> # 2 fois 'an', suivit par 'as'
   >>> 2 * 'an' + 'as'
   'ananas'

Plusieurs **chaînes de caractères**, écrites littéralement
(c’est-à-dire entre guillemets), **côte à côte, sont automatiquement
concaténées**.

   >>> 'Py' 'thon'
   'Python'

Cette fonctionnalité est surtout intéressante pour couper des chaînes
trop longues :

   >>> texte = ('Mettez plusieurs chaînes entre les parenthèses '
   ... 'pour les avoir réunis.')
   >>> texte
   'Mettez plusieurs chaînes entre les parenthèses pour les avoir réunis.'

**Cela ne fonctionne cependant qu’avec les chaînes littérales, pas
avec les variables ni les expressions** :

   >>> prefixe = 'Py'
   >>> prefixe 'thon' # impossible de concaténer une variable et une chaîne littérale
   File "<input>", line 1
    prefixe 'thon' # impossible de concaténer une variable et une chaîne littérale
            ^
   SyntaxError: invalid syntax
   >>> ('un' * 3) 'ium'
   File "<input>", line 1
    ('un' * 3) 'ium'
               ^
   SyntaxError: invalid syntax

Pour **concaténer des variables**, ou des variables avec des chaînes
littérales, utilisez l’opérateur «**+**» :

   >>> prefixe + 'thon'
   'Python'

**Les chaînes de caractères peuvent être indexées** (ou indicées,
c’est-à-dire que l’on peut accéder aux caractères par leur position),
le premier caractère d’une chaîne étant à la position 0. Il n’existe
pas de type distinct pour les caractères, un caractère est simplement
une chaîne de longueur 1 :

Pour visualiser la façon dont les indices fonctionnent

   Position :   1   2   3   4   5   6
              +---+---+---+---+---+---+
              | P | y | t | h | o | n |
              +---+---+---+---+---+---+
   Indice :     0   1   2   3   4   5

Exemples :

   >>> mot = 'Python'
   >>> mot[0] # caractère en position 1
   'P'
   >>> mot[5] # caractère en position 6
   'n'

Les indices peuvent également être négatifs, on compte alors en
partant de la droite. Par exemple :

Pour visualiser la façon dont les indices négatifs fonctionnent

   Position :   1   2   3   4   5   6
              +---+---+---+---+---+---+
              | P | y | t | h | o | n |
              +---+---+---+---+---+---+
   Indice :    -6  -5  -4  -3  -2  -1

Exemples :

   >>> mot[-1] # dernier caractère
   'n'
   >>> mot[-2] # avant-dernier caractère
   'o'
   >>> mot[-6]
   'P'

Notez que, comme -0 égale 0, les indices négatifs commencent par -1.

En plus d’accéder à un élément par son indice, il est aussi possible
de « trancher » (slice en anglais) une chaîne. Accéder à une chaîne
par un indice permet d’obtenir un caractère, trancher permet d’obtenir
une sous-chaîne :

Pour mémoriser la façon dont les tranches fonctionnent, vous pouvez
imaginer que les indices pointent entre les caractères, le côté gauche
du premier caractère ayant la position 0. Le côté droit du dernier
caractère d’une chaîne de n caractères a alors pour indice n.

   Position :   1   2   3   4   5   6
              +---+---+---+---+---+---+
              | P | y | t | h | o | n |
              +---+---+---+---+---+---+
              0   1   2   3   4   5   6
             -6  -5  -4  -3  -2  -1

La première ligne de nombres donne la position des indices 0…6 dans la
chaîne ; la deuxième ligne donne l’indice négatif correspondant. La
tranche de i à j est constituée de tous les caractères situés entre
les bords libellés i et j, respectivement.

Pour des indices non négatifs, la longueur d’une tranche est la
différence entre ces indices, si les deux sont entre les bornes. Par
exemple, la longueur de mot[1:3] est 2.

Exemples :

   >>> mot[0:2] # caractères de la position 1 (inclus) à 3 (exclus)
   'Py'
   >>> mot[2:5] # caractères de la position 3 (inclus) à 6 (exclus)
   'tho'
   >>> mot[-6:-4] # caractères de la position 1 (inclus) à 3 (exclus)
   'Py'
   >>> mot[-4:-1] # caractères de la position 3 (inclus) à 6 (exclus)
   'tho'

**Notez que le début est toujours inclus et la fin toujours exclue**.
Cela assure que s[:i] + s[i:] est toujours égal à s :

   >>> mot[:2] + mot[2:]
   'Python'
   >>> mot[:4] + mot[4:]
   'Python'

Les valeurs par défaut des indices de tranches ont une utilité ; le
premier indice vaut zéro par défaut (c.-à-d. lorsqu’il est omis), le
deuxième correspond par défaut à la taille de la chaîne de caractères

   >>> mot[:2] # caractère du début à la position 3 (exclu)
   'Py'
   >>> mot[4:] # caractères de la position 5 (inclus) à la fin
   'on'
   >>> mot[-2:] # caractères de l'avant-dernier (inclus) à la fin
   'on'

Utiliser un indice trop grand produit une erreur :

   >>> mot[42] # le mot n'a que 6 caractères
   Traceback (most recent call last):
   File "<input>", line 1, in <module>
    mot[42] # le mot n'a que 6 caractères
   IndexError: string index out of range

Cependant, les indices hors bornes sont gérés silencieusement
lorsqu’ils sont utilisés dans des tranches :

   >>> mot[4:42]
   'on'
   >>> mot[42:]
   ''

Les chaînes de caractères, en Python, ne peuvent pas être modifiées.
On dit qu’elles sont immuables. Affecter une nouvelle valeur à un
indice dans une chaîne produit une erreur :

   >>> mot[0] = 'J'
   Traceback (most recent call last):
   File "<input>", line 1, in <module>
    mot[0] = 'J'
   TypeError: 'str' object does not support item assignment
   >>> mot[2:] = 'py'
   Traceback (most recent call last):
   File "<input>", line 1, in <module>
    mot[2:] = 'py'
   TypeError: 'str' object does not support item assignment

Si vous avez besoin d’une chaîne différente, vous devez en créer une
nouvelle :

   >>> 'J' + mot[1:]
   'Jython'
   >>> mot[:2] + 'py'
   'Pypy'

La fonction native len() renvoie la longueur d’une chaîne :

   >>> s = 'anticonstitutionnellement'
   >>> len(s)
   25


Nombres
=======

Vu avec la calculatrice python


Nombre entier optimisé (int)
----------------------------

Python 2

   int()


Nombre entier de taille arbitraire (long int)
---------------------------------------------

Python 3

   int()

Python 2

   long()


Nombre à virgule flottante
--------------------------

   float()


Nombre complexe
---------------

   complex()


Données multiples
=================

Python connaît différents types de données combinés, utilisés pour
regrouper plusieurs valeurs.


Liste de longueur fixe
----------------------

   tuple()

le tuple (ou n-uplet, dénomination que nous utiliserons dans la suite
de cette documentation).

Un n-uplet consiste en différentes valeurs séparées par des virgules,
par exemple :

   >>> t = 12345, 54321, 'hello!'
   >>> t[0]
   12345
   >>> t
   (12345, 54321, 'hello!')
   >>> # Les tuples peuvent être imbriqués
   >>> u = t, (1, 2, 3, 4, 5)
   >>> u
   ((12345, 54321, 'hello!'), (1, 2, 3, 4, 5))
   >>> # Les tuples sont immuables
   >>> t[0] = 88888
   Traceback (most recent call last):
   File "<input>", line 1, in <module>
    t[0] = 88888
   TypeError: 'tuple' object does not support item assignment
   >>> # mais ils peuvent contenir des objets mutables
   >>> v = ([1, 2, 3], [3, 2, 1])
   >>> v
   ([1, 2, 3], [3, 2, 1])

Comme vous pouvez le voir, les n-uplets sont toujours affichés entre
parenthèses, de façon à ce que des n-uplets imbriqués soient
interprétés correctement ; ils peuvent être saisis avec ou sans
parenthèses, même si celles-ci sont souvent nécessaires (notamment
lorsqu’un n-uplet fait partie d’une expression plus longue). Il n’est
pas possible d’affecter de valeur à un élément d’un n-uplet ; par
contre, il est possible de créer des n-uplets contenant des objets
muables, comme des listes.

Si les n-uplets peuvent sembler similaires aux listes, ils sont
souvent utilisés dans des cas différents et pour des raisons
différentes. Les n-uplets sont immuables et contiennent souvent des
séquences hétérogènes d’éléments qui sont accédés par « dissociation »
(unpacking en anglais, voir plus loin) ou par indice (ou même par
attributs dans le cas des namedtuples). Les listes sont souvent
muables et contiennent des éléments généralement homogènes qui sont
accédés par itération sur la liste.

Un problème spécifique est la construction de n-uplets ne contenant
aucun ou un seul élément : la syntaxe a quelques tournures spécifiques
pour s’en accommoder. Les n-uplets vides sont construits par une paire
de parenthèses vides ; un n-uplet avec un seul élément est construit
en faisant suivre la valeur par une virgule (il n’est pas suffisant de
placer cette valeur entre parenthèses). Pas très joli, mais efficace.

Par exemple :

   >>> vide = ()
   >>> singleton = 'bonjour', # <-- noter la virgule de fin
   >>> len(vide)
   0
   >>> len(singleton)
   1
   >>> singleton
   ('bonjour',)

L’instruction "t = 12345, 54321, 'hello !'" est un exemple d’une
agrégation de n-uplet (tuple packing en anglais) : les valeurs
«12345», «54321» et «hello !» sont agrégées ensemble dans un n-uplet.
L’opération inverse est aussi possible :

   >>> x, y, z = t

Ceci est appelé, de façon plus ou moins appropriée, une distribution
de séquence (sequence unpacking en anglais) et fonctionne pour toute
séquence placée à droite de l’expression. Cette distribution requiert
autant de variables dans la partie gauche qu’il y a d’éléments dans la
séquence. Notez également que cette affectation multiple est juste une
combinaison entre une agrégation de n-uplet et une distribution de
séquence.


Les ensembles
-------------

   set()

Python fournit également un type de donnée pour les ensembles. Un
ensemble est une collection non ordonnée sans élément dupliqué. Des
utilisations basiques concernent par exemple des tests d’appartenance
ou des suppressions de doublons. Les ensembles savent également
effectuer les opérations mathématiques telles que les unions,
intersections, différences et différences symétriques.

Des accolades ou la fonction set() peuvent être utilisés pour créer
des ensembles. **Notez que pour créer un ensemble vide, {} ne
fonctionne pas**, cela crée un dictionnaire vide. **Utilisez plutôt
set()**.

Voici une brève démonstration :

   >>> panier = {'pomme', 'orange', 'pomme', 'poire', 'orange', 'banane'}
   >>> print(panier) # montre que les doublons ont été supprimés
   {'poire', 'pomme', 'banane', 'orange'}
   >>> 'orange' in panier # test d'adhésion rapide
   True
   >>> 'digitaire' in panier # la digitaire est une plante
   False
   >>> # Démontrer les opérations d'ensemble sur des lettres uniques à partir de deux mots
   >>> a = set('abracadabra')
   >>> b = set('alacazam')
   >>> a # lettres uniques dans a
   {'b', 'a', 'c', 'r', 'd'}
   >>> a - b # lettres en a mais pas en b
   {'r', 'b', 'd'}
   >>> a | b # lettres en a ou b ou les deux
   {'b', 'a', 'c', 'r', 'm', 'l', 'z', 'd'}
   >>> a & b # lettres en a et b
   {'a', 'c'}
   >>> a ^ b # lettres en a ou b mais pas les deux
   {'r', 'b', 'm', 'l', 'z', 'd'}

Il est possible d’écrire des expressions dans des ensembles :

   >>> a = {x for x in 'abracadabra' if x not in 'abc'}
   >>> a
   {'r', 'd'}


Liste de longueur variable
--------------------------

   list()

Le plus souple est la liste, qui peut être écrit comme une suite,
placée entre crochets, de valeurs (éléments) séparées par des
virgules. Les listes et les chaînes de caractères ont beaucoup de
propriétés en commun, comme l’indiçage et les opérations sur des
tranches. Les éléments d’une liste ne sont pas obligatoirement tous du
même type, bien qu’à l’usage ce soit souvent le cas.

   >>> carrés = [1, 4, 9, 16, 25]
   >>> carrés
   [1, 4, 9, 16, 25]

Comme les chaînes de caractères (et toute autre type de séquence), les
listes peuvent être indicées et découpées :

   >>> carrés[0] # l'indexation renvoie l'élément
   1
   >>> carrés[-1]
   25
   >>> carrés[-3:] # slicing renvoie une nouvelle liste
   [9, 16, 25]

Toutes les opérations par tranches renvoient une nouvelle liste
contenant les éléments demandés. Cela signifie que l’opération
suivante renvoie une copie distincte de la liste :

   >>> carrés[:]
   [1, 4, 9, 16, 25]

Les listes gèrent aussi les opérations comme les concaténations :

   >>> carrés + [36, 49, 64, 81, 100]
   [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

Mais à la différence des chaînes qui sont immuables, les listes sont
muables : il est possible de modifier leur contenu :

   >>> cubes = [1, 8, 27, 63, 125] # Quelque chose ne va pas ici
   >>> 4 ** 3 # le cube de 4 est 64, pas 63!
   64
   >>> cubes[3] = 64 # remplacer la mauvaise valeur
   >>> cubes
   [1, 8, 27, 64, 125]

Il est aussi possible d’ajouter de nouveaux éléments à la fin d’une
liste avec la méthode append() (les méthodes sont abordées plus tard)
:

   >>> cubes.append(216) # ajouter le cube de 6
   >>> cubes.append(7 ** 3) # et le cube de 7
   >>> cubes
   [1, 8, 27, 64, 125, 216, 343]

Des affectations de tranches sont également possibles, ce qui peut
même modifier la taille de la liste ou la vider complètement :

   >>> lettres = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
   >>> lettres
   ['a', 'b', 'c', 'd', 'e', 'f', 'g']
   >>> # remplacer certaines valeurs
   >>> lettres[2:5] = ['C', 'D', 'E']
   >>> lettres
   ['a', 'b', 'C', 'D', 'E', 'f', 'g']
   >>> # maintenant supprimez-les
   >>> lettres[2:5] = []
   >>> lettres
   ['a', 'b', 'f', 'g']
   >>> # effacer la liste en remplaçant tous les éléments par une liste vide
   >>> lettres[:] = []
   >>> lettres
   []

La primitive len() s’applique aussi aux listes :

   >>> lettres = ['a', 'b', 'c', 'd']
   >>> len(lettres)
   4

Il est possible d’imbriquer des listes (c’est à dire de créer des
listes contenant d’autres listes).

Par exemple :

   >>> a = ['a', 'b', 'c']
   >>> n = [1, 2, 3]
   >>> x = [a, n]
   >>> x
   [['a', 'b', 'c'], [1, 2, 3]]
   >>> x[0]
   ['a', 'b', 'c']
   >>> x[0][1]
   'b'


Premiers pas vers la programmation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Bien entendu, on peut utiliser Python pour des tâches plus compliquées
que d’additionner deux et deux. Par exemple, on peut écrire le début
de la suite de Fibonacci comme ceci :

   >>> # Série de Fibonacci
   >>> # la somme de deux éléments définit le suivant
   >>> a, b = 0, 1
   >>> while a < 10:
   ... print(a)
   ... a, b = b, a+b
   ...
   ...
   0
   1
   1
   2
   3
   5
   8

Cet exemple introduit plusieurs nouvelles fonctionnalités.

**La première ligne contient une affectation multiple** : les
variables a et b se voient affecter simultanément leurs nouvelles
valeurs 0 et 1. Cette méthode est encore utilisée à la dernière ligne,
pour démontrer que les expressions sur la partie droite de
l’affectation sont toutes évaluées avant que les affectations ne
soient effectuées. Ces expressions en partie droite sont toujours
évaluées de la gauche vers la droite.

**La boucle while s’exécute tant que la condition** (ici : a < 10)
reste vraie. En Python, comme en C, tout entier différent de zéro est
vrai et zéro est faux. La condition peut aussi être une chaîne de
caractères, une liste, ou en fait toute séquence ; une séquence avec
une valeur non nulle est vraie, une séquence vide est fausse. Le test
utilisé dans l’exemple est une simple comparaison. Les opérateurs de
comparaison standards sont écrits comme en C : < (inférieur), >
(supérieur), == (égal), <= (inférieur ou égal), >= (supérieur ou égal)
et != (non égal).

**Le corps de la boucle est indenté** : l’indentation est la méthode
utilisée par Python pour regrouper des instructions. En mode
interactif, vous devez saisir une tabulation ou des espaces pour
chaque ligne indentée. En pratique, vous aurez intérêt à utiliser un
éditeur de texte pour les saisies plus compliquées ; tous les éditeurs
de texte dignes de ce nom disposent d’une fonction d’auto-indentation.
Lorsqu’une expression composée est saisie en mode interactif, elle
doit être suivie d’une ligne vide pour indiquer qu’elle est terminée
(car l’analyseur ne peut pas deviner que vous venez de saisir la
dernière ligne). Notez bien que toutes les lignes à l’intérieur d’un
bloc doivent être indentées au même niveau.

**La fonction print() écrit les valeurs des paramètres qui lui sont
fournis**. Ce n’est pas la même chose que d’écrire l’expression que
vous voulez afficher (comme nous l’avons fait dans l’exemple de la
calculatrice), en raison de la manière qu’a **print()** de gérer les
paramètres multiples, les nombres décimaux et les chaînes. Les chaînes
sont affichées sans apostrophe et une espace est insérée entre les
éléments de telle sorte que vous pouvez facilement formater les
choses, comme ceci :

   >>> i = 256*256
   >>> print('La valeur de i est', i)
   La valeur de i est 65536

Le paramètre nommé **end** peut servir pour enlever le retour à la
ligne ou pour terminer la ligne par une autre chaîne :

   >>> a, b = 0, 1
   >>> while a < 1000:
   ... print(a, end=',')
   ... a, b = b, a+b
   ...
   ...
   0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,>>>

Lexique à distribuer :

Le type liste dispose de méthodes supplémentaires. Voici toutes les
méthodes des objets de type liste :

   list.append(x)

Ajoute un élément à la fin de la liste. Équivalent à a[len(a):] = [x].

   list.extend(iterable)

Étend la liste en y ajoutant tous les éléments de l’itérable.
Équivalent à a[len(a):] = iterable.

   list.insert(i, x)

Insère un élément à la position indiquée. Le premier argument est la
position de l’élément courant avant lequel l’insertion doit
s’effectuer, donc a.insert(0, x) insère l’élément en tête de la liste
et a.insert(len(a), x) est équivalent à a.append(x).

   list.remove(x)

Supprime de la liste le premier élément dont la valeur est égale à x.
Une exception ValueError est levée s’il n’existe aucun élément avec
cette valeur.

   list.pop([i])

Enlève de la liste l’élément situé à la position indiquée et le
renvoie en valeur de retour. Si aucune position n’est spécifiée,
a.pop() enlève et renvoie le dernier élément de la liste (les crochets
autour du i dans la signature de la méthode indiquent que ce paramètre
est facultatif et non que vous devez placer des crochets dans votre
code ! Vous retrouverez cette notation fréquemment dans le Guide de
Référence de la Bibliothèque Python).

   list.clear()

Supprime tous les éléments de la liste. Équivalent à del a[:].

   list.index(x[, start[, end]])

Renvoie la position du premier élément de la liste dont la valeur
égale x (en commençant à compter les positions à partir de zéro). Une
exception ValueError est levée si aucun élément n’est trouvé.

Les arguments optionnels start et end sont interprétés de la même
manière que dans la notation des tranches et sont utilisés pour
limiter la recherche à une sous-séquence particulière. L’indice
renvoyé est calculé relativement au début de la séquence complète et
non relativement à start.

   list.count(x)

Renvoie le nombre d’éléments ayant la valeur x dans la liste.

   list.sort(key=None, reverse=False)

Ordonne les éléments dans la liste (les arguments peuvent
personnaliser l’ordonnancement, voir sorted() pour leur explication).

   list.reverse()

Inverse l’ordre des éléments dans la liste.

   list.copy()

Renvoie une copie superficielle de la liste. Équivalent à a[:].

Exemple suivant utilise la plupart des méthodes des listes :

   >>> fruits = ['orange', 'pomme', 'poire', 'banane', 'kiwi', 'pomme', 'banane']
   >>> fruits.count('pomme')
   2
   >>> fruits.count('mandarine')
   0
   >>> fruits.index('banane')
   3
   >>> fruits.index('banane', 4) # Trouver la prochaine banane à partir d'une position 4
   6
   >>> fruits.reverse()
   >>> fruits
   ['banane', 'pomme', 'kiwi', 'banane', 'poire', 'pomme', 'orange']
   >>> fruits.append('raisin')
   >>> fruits
   ['banane', 'pomme', 'kiwi', 'banane', 'poire', 'pomme', 'orange', 'raisin']
   >>> fruits.sort()
   >>> fruits
   ['banane', 'banane', 'kiwi', 'orange', 'poire', 'pomme', 'pomme', 'raisin']
   >>> fruits.pop()
   'raisin'
   >>> fruits
   ['banane', 'banane', 'kiwi', 'orange', 'poire', 'pomme', 'pomme']

Vous avez probablement remarqué que les méthodes telles que
**insert**, **remove** ou **sort**, qui ne font que modifier la liste,
n’affichent pas de valeur de retour (elles renvoient None) 1. C’est un
principe respecté par toutes les structures de données variables en
Python.

Une autre chose que vous remarquerez peut-être est que toutes les
données ne peuvent pas être ordonnées ou comparées. Par exemple,
[None, “hello”, 10] ne sera pas ordonné parce que les entiers ne
peuvent pas être comparés aux chaînes de caractères et None ne peut
pas être comparé à d’autres types. En outre, il existe certains types
qui n’ont pas de relation d’ordre définie. Par exemple, 3+4j < 5+7j
n’est pas une comparaison valide.

Approfondir chez soit ou au travail voir
https://docs.python.org/fr/3/tutorial/datastructures.html#using-lists-
as-stacks et la suite


Dictionnaire
------------

   dict()

Un autre type de donnée très utile, natif dans Python, est le
dictionnaire (voir Les types de correspondances — **dict**). Ces
dictionnaires sont parfois présents dans d’autres langages sous le nom
de « mémoires associatives » ou de « tableaux associatifs ». À la
différence des séquences, qui sont indexées par des nombres, **les
dictionnaires sont indexés par des clés**, qui peuvent être de
n’importe quel type immuable ; les chaînes de caractères et les
nombres peuvent toujours être des clés. Des n-uplets peuvent être
utilisés comme clés s’ils ne contiennent que des chaînes, des nombres
ou des n-uplets ; si un n-uplet contient un objet muable, de façon
directe ou indirecte, il ne peut pas être utilisé comme une clé. Vous
ne pouvez pas utiliser des listes comme clés, car les listes peuvent
être modifiées en place en utilisant des affectations par position,
par tranches ou via des méthodes comme **append()** ou **extend()**.

Le plus simple est de considérer les dictionnaires comme des ensembles
de paires clé: valeur, les clés devant être uniques (au sein d’un
dictionnaire). Une paire d’accolades crée un dictionnaire vide : {}.
Placer une liste de paires clé:valeur séparées par des virgules à
l’intérieur des accolades ajoute les valeurs correspondantes au
dictionnaire ; c’est également de cette façon que les dictionnaires
sont affichés.

Les opérations classiques sur un dictionnaire consistent à stocker une
valeur pour une clé et à extraire la valeur correspondant à une clé.
Il est également possible de supprimer une paire clé-valeur avec
**del**. Si vous stockez une valeur pour une clé qui est déjà
utilisée, l’ancienne valeur associée à cette clé est perdue. Si vous
tentez d’extraire une valeur associée à une clé qui n’existe pas, une
exception est levée.

Exécuter **list(d)** sur un dictionnaire d renvoie une liste de toutes
les clés utilisées dans le dictionnaire, dans l’ordre d’insertion (si
vous voulez qu’elles soient ordonnées, utilisez **sorted(d))**. Pour
tester si une clé est dans le dictionnaire, utilisez le mot-clé in.

Voici un petit exemple utilisant un dictionnaire :

   >>> téléphone = {'daniel': 4098, 'paul': 4139}
   >>> téléphone['luc'] = 4127
   >>> téléphone
   {'daniel': 4098, 'paul': 4139, 'luc': 4127}
   >>> téléphone['daniel']
   4098
   >>> del téléphone['paul']
   >>> téléphone['sami'] = 4127
   >>> téléphone
   {'daniel': 4098, 'luc': 4127, 'sami': 4127}
   >>> list(téléphone)
   ['daniel', 'luc', 'sami']
   >>> sorted(téléphone)
   ['daniel', 'luc', 'sami']
   >>> 'luc' in téléphone
   True
   >>> 'daniel' not in téléphone
   False

Le constructeur **dict()** fabrique un dictionnaire directement à
partir d’une liste de paires clé-valeur stockées sous la forme de
n-uplets :

   >>> dict([('paul', 4139), ('luc', 4127), ('daniel', 4098)])
   {'paul': 4139, 'luc': 4127, 'daniel': 4098}

De plus, il est possible de créer des dictionnaires par compréhension
depuis un jeu de clef et valeurs :

   >>> {x: x**2 for x in (2, 4, 6)}
   {2: 4, 4: 16, 6: 36}

Lorsque les clés sont de simples chaînes de caractères, il est parfois
plus facile de spécifier les paires en utilisant des paramètres nommés
:

   >>> dict(paul=4139, luc=4127, daniel=4098)
   {'paul': 4139, 'luc': 4127, 'daniel': 4098}


Autres
======


Fichier
-------

   File


Absence de type
---------------

   NoneType


Absence d’implémentation
------------------------

   NotImplementedType


fonction
--------

   Function


module
------

   module
