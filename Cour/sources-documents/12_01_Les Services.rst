.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Les services
************

Pour lancer un serveur, généralement nous lançons ce serveur sous forme de service pour nos systèmes. Nous allons voir ici comment faire sur des systèmes Linux avec systemd.

systemd
=======

Systemd supporte des services systèmes ou utilisateurs. Il démarre ces services dans leur propre instance.

Creation d'un service utilisateur
---------------------------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user edit service_python.service --full --force


Saisir la définition de service ci-dessous

.. code-block:: text

  [Unit]
  # Nom du service pour les humains ;-p
  Description=Exemple de service Python


Vérifier la présence du service

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user list-unit-files | grep service_python
  service_python.service                                            static    -


Créer un programme Python à exécuter comme service

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 13_Services ; cd 13_Services


Créer le fichier «**repertoire_de_developpement/13_Services/demo_service_python.py**».
Et saisissez :

.. code-block:: python

  if __name__ == '__main__':
      import time

      while True:
          print('Réponse du service python de démonstration')
          time.sleep(5)


Modifier le service pour intégrer le programme Python

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user edit service_python.service --full


Saisir la modification du service ci-dessous

.. code-block:: text

  [Unit]
  # Nom du service pour les humains ;-p
  Description=Exemple de service Python

  [Service]
  #Commande à exécuter quand le service est démarré
  ExecStart=/usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py


Démarrer le service

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user start service_python.service
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ systemctl --user status service_python.service
  ● service_python.service - Exemple de service Python
       Loaded: loaded (/home/utilisateur/.config/systemd/user/service_python.service; static)
       Active: active (running) since Mon 2021-11-22 10:05:30 CET; 9s ago
     Main PID: 589845 (python3)
       CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/service_python.service
               └─589845 /usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py

  nov. 22 10:05:30 MachineUbuntu.domaine-perso.fr systemd[1897]: Started Exemple de service Python.
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ journalctl --user -u service_python
  -- Journal begins at Thu 2021-10-21 10:48:14 CEST, ends at Mon 2021-11-22 10:15:03 CET. --
  nov. 22 10:05:30 MachineUbuntu.domaine-perso.fr systemd[1897]: Started Exemple de service Python.
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ cat /var/log/syslog
  …
  Nov 22 10:17:22 MachineUbuntu systemd[1897]: service_python.service: Succeeded.
  Nov 22 10:17:44 MachineUbuntu systemd[1897]: Started Exemple de service Python.


La sotie :python:`print('Réponse du service python de démonstration')` n'apparait pas dans l'exécution du service dans votre terminal. **Systemd** exécute le service dans une instance séparée ce qui redirige donc :terminal:`STDOUT` et :terminal:`STDERR`.

Pour remédier à cela avec Python, modifier le service avec :terminal:`Environment=PYTHONUNBUFFERED=1` pour intégrer le programme Python

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user edit service_python --full


Saisir la modification du service ci-dessous

.. code-block:: text

  [Unit]
  # Nom du service pour les humains ;-p
  Description=Exemple de service Python

  [Service]
  Environment=PYTHONUNBUFFERED=1
  #Commande à exécuter quand le service est démarré
  ExecStart=/usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ systemctl --user restart service_python
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ tail -1 /var/log/syslog
  Nov 22 10:31:37 MachineUbuntu python3[592413]: Réponse du service python de démonstration
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ systemctl --user status service_python
  ● service_python.service - Exemple de service Python
       Loaded: loaded (/home/utilisateur/.config/systemd/user/service_python.service; static)
       Active: active (running) since Mon 2021-11-22 10:30:22 CET; 1min 52s ago
     Main PID: 592413 (python3)
       CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/service_python.service
               └─592413 /usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py

  nov. 22 10:31:27 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  nov. 22 10:31:32 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  nov. 22 10:31:37 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  nov. 22 10:31:42 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  nov. 22 10:31:47 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ journalctl --user-unit service_python
  -- Journal begins at Thu 2021-10-21 10:23:23 CEST, ends at Mon 2021-11-22 10:35:32 CET. --
  nov. 22 10:30:22 MachineUbuntu.domaine-perso.fr systemd[1897]: service_python.service: Succeeded.
  nov. 22 10:30:22 MachineUbuntu.domaine-perso.fr systemd[1897]: Stopped Exemple de service Python.
  nov. 22 10:30:22 MachineUbuntu.domaine-perso.fr systemd[1897]: Started Exemple de service Python.
  nov. 22 10:30:22 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  nov. 22 10:30:27 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  nov. 22 10:30:32 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  nov. 22 10:30:37 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration
  nov. 22 10:30:42 MachineUbuntu.domaine-perso.fr python3[592413]: Réponse du service python de démonstration


Démarrer le service automatiquement
-----------------------------------

Pour démarrer le service automatiquement au démarrage de votre machine Ubuntu.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user edit service_python --full


Saisir la modification du service ci-dessous

.. code-block:: text

  [Unit]
  # Nom du service pour les humains ;-p
  Description=Exemple de service Python

  [Service]
  Environment=PYTHONUNBUFFERED=1
  #Commande à exécuter quand le service est démarré
  ExecStart=/usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py

  [Install]
  WantedBy=default.target


Ajouter le service au démarrage du système

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ systemctl --user enable service_python
  Created symlink /home/utilisateur/.config/systemd/user/default.target.wants/service_python.service → /home/utilisateur/.config/systemd/user/service_python.service.


Redémarrer le système.

.. code-block:: console

  utilisateur@MachineUbuntu:~$ systemctl --user list-unit-files | grep service_python
  service_python.service                                            enabled   enabled
  utilisateur@MachineUbuntu:~$ systemctl --user status service_python
  ● service_python.service - Exemple de service Python
       Loaded: loaded (/home/utilisateur/.config/systemd/user/service_python.service; enabled; vendor preset: enabled)
       Active: active (running) since Mon 2021-11-22 10:48:49 CET; 12min ago
     Main PID: 2107 (python3)
       CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/service_python.service
               └─2107 /usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py

  nov. 22 11:01:34 MachineUbuntu.domaine-perso.fr python3[2107]: Réponse du service python de démonstration
  nov. 22 11:01:39 MachineUbuntu.domaine-perso.fr python3[2107]: Réponse du service python de démonstration
  nov. 22 11:01:44 MachineUbuntu.domaine-perso.fr python3[2107]: Réponse du service python de démonstration
  nov. 22 11:01:49 MachineUbuntu.domaine-perso.fr python3[2107]: Réponse du service python de démonstration
  …


Le service ne démarre que lorsque l'utilisateur se connecte. Si nous voulons que le service démarre au démarage du système il faut passer la commande

.. code-block:: console

  utilisateur@MachineUbuntu:~$ sudo loginctl enable-linger $USER


Redémarrage automatique après échec
-----------------------------------

Avec systemd il est possible de redémarrer automatiquement le service en cas d'échec.

Si nous tuons le processus

.. code-block:: console

  utilisateur@MachineUbuntu:~$ systemctl --user --signal=SIGKILL kill service_python
  utilisateur@MachineUbuntu:~$ systemctl --user status service_python
  ● service_python.service - Exemple de service Python
       Loaded: loaded (/home/utilisateur/.config/systemd/user/service_python.service; enabled; vendor preset: enabled)
       Active: failed (Result: signal) since Mon 2021-11-22 11:15:56 CET; 8s ago
      Process: 2107 ExecStart=/usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py (code=killed, signal=K>
     Main PID: 2107 (code=killed, signal=KILL)

  …
  nov. 22 11:15:56 MachineUbuntu.domaine-perso.fr systemd[1892]: service_python.service: Sent signal SIGKILL to main process 2107 (python3) on client >
  nov. 22 11:15:56 MachineUbuntu.domaine-perso.fr systemd[1892]: service_python.service: Main process exited, code=killed, status=9/KILL
  nov. 22 11:15:56 MachineUbuntu.domaine-perso.fr systemd[1892]: service_python.service: Failed with result 'signal'.


On voit que le service s'arrête.

Pour que le service redémarre automatiquement il faut ajouter :terminal:`Restart=on-failure`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user edit service_python --full


.. code-block:: text

  [Unit]
  # Nom du service pour les humains ;-p
  Description=Exemple de service Python

  [Service]
  Environment=PYTHONUNBUFFERED=1
  #Commande à exécuter quand le service est démarré
  ExecStart=/usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py
  Restart=on-failure

  [Install]
  WantedBy=default.target


.. code-block:: console

  utilisateur@MachineUbuntu:~$ systemctl --user restart service_python
  utilisateur@MachineUbuntu:~$ systemctl --user status service_python
  ● service_python.service - Exemple de service Python
       Loaded: loaded (/home/utilisateur/.config/systemd/user/service_python.service; enabled; vendor preset: enabled)
       Active: active (running) since Mon 2021-11-22 11:25:17 CET; 8s ago
     Main PID: 7249 (python3)
       CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/service_python.service
               └─7249 /usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py

  nov. 22 11:25:17 MachineUbuntu.domaine-perso.fr systemd[1892]: Started Exemple de service Python.
  nov. 22 11:25:17 MachineUbuntu.domaine-perso.fr python3[7249]: Réponse du service python de démonstration
  nov. 22 11:25:22 MachineUbuntu.domaine-perso.fr python3[7249]: Réponse du service python de démonstration
  utilisateur@MachineUbuntu:~$ systemctl --user --signal=SIGKILL kill service_python
  utilisateur@MachineUbuntu:~$ systemctl --user status service_python
  ● service_python.service - Exemple de service Python
       Loaded: loaded (/home/utilisateur/.config/systemd/user/service_python.service; enabled; vendor preset: enabled)
       Active: active (running) since Mon 2021-11-22 11:26:10 CET; 7s ago
     Main PID: 7266 (python3)
       CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/service_python.service
               └─7266 /usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py

  nov. 22 11:26:10 MachineUbuntu.domaine-perso.fr systemd[1892]: service_python.service: Sent signal SIGKILL to main process 7249 (python3) on client >
  nov. 22 11:26:13 MachineUbuntu.domaine-perso.fr python3[7266]: Réponse du service python de démonstration
  nov. 22 11:26:10 MachineUbuntu.domaine-perso.fr systemd[1892]: service_python.service: Main process exited, code=killed, status=9/KILL
  nov. 22 11:26:10 MachineUbuntu.domaine-perso.fr systemd[1892]: service_python.service: Failed with result 'signal'.
  nov. 22 11:26:15 MachineUbuntu.domaine-perso.fr python3[7266]: Réponse du service python de démonstration
  nov. 22 11:26:10 MachineUbuntu.domaine-perso.fr systemd[1892]: service_python.service: Scheduled restart job, restart counter is at 1.
  nov. 22 11:26:10 MachineUbuntu.domaine-perso.fr systemd[1892]: Stopped Exemple de service Python.
  nov. 22 11:26:10 MachineUbuntu.domaine-perso.fr systemd[1892]: Started Exemple de service Python.
  nov. 22 11:26:25 MachineUbuntu.domaine-perso.fr python3[7266]: Réponse du service python de démonstration
  nov. 22 11:26:30 MachineUbuntu.domaine-perso.fr python3[7266]: Réponse du service python de démonstration
  nov. 22 11:26:35 MachineUbuntu.domaine-perso.fr python3[7266]: Réponse du service python de démonstration


Notification de démarrage à Systemd
-----------------------------------

Maintenant nous allons agir avec Systend par l'intermédiaire de Python. Nous allons indiquer à Systemd quand le service Python a démarré.

Modifiez le fichier «**repertoire_de_developpement/13_Services/demo_service_python.py**»

.. code-block:: python

  if __name__ == '__main__':
      import time
      import systemd.deamon as service

      print('Démarrage de service_python …')
      time.sleep(5)
      print('Démarrage OK')
      service.notify('READY=1')

      while True:
          print('Réponse du service python de démonstration')
          time.sleep(5)


Puis

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user edit service_python --full


.. code-block:: text

  [Unit]
  # Nom du service pour les humains ;-p
  Description=Exemple de service Python

  [Service]
  Environment=PYTHONUNBUFFERED=1
  #Commande à exécuter quand le service est démarré
  ExecStart=/usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py
  Restart=on-failure
  Type=notify
  #StandardOutput=journal+console

  [Install]
  WantedBy=default.target


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user restart service_python
  utilisateur@MachineUbuntu:~$ systemctl --user status service_python
  ● service_python.service - Exemple de service Python
       Loaded: loaded (/home/utilisateur/.config/systemd/user/service_python.service; enabled; vendor preset: enabled)
       Active: active (running) since Mon 2021-11-22 11:54:39 CET; 8s ago
     Main PID: 10233 (python3)
       CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/service_python.service
               └─10233 /usr/bin/python3 /home/utilisateur/repertoire_de_developpement/13_Services/demo_service_python.py

  nov. 22 11:54:34 MachineUbuntu.domaine-perso.fr systemd[1892]: Starting Exemple de service Python...
  nov. 22 11:54:34 MachineUbuntu.domaine-perso.fr python3[10233]: Démarrage de service_python …
  nov. 22 11:54:39 MachineUbuntu.domaine-perso.fr python3[10233]: Démarrage OK
  nov. 22 11:54:39 MachineUbuntu.domaine-perso.fr python3[10233]: Réponse du service python de démonstration
  nov. 22 11:54:39 MachineUbuntu.domaine-perso.fr systemd[1892]: Started Exemple de service Python.
  nov. 22 11:54:44 MachineUbuntu.domaine-perso.fr python3[10233]: Réponse du service python de démonstration


Supprimer le démarrage automatique du service
---------------------------------------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/13_Services$ systemctl --user disable service_python
  Removed /home/utilisateur/.config/systemd/user/default.target.wants/service_python.service.


Transformer en service système
------------------------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ systemctl --user stop service_python
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mv ~/.config/systemd/user/service_python.service /etc/systemd/system/
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo chown root:root /etc/systemd/system/service_python.service
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo chmod 644 /etc/systemd/system/service_python.service
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo mkdir /usr/local/lib/demo_service_python
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo cp ~/repertoire_de_developpement/13_Services/demo_service_python.py /usr/local/lib/demo_service_python/
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo chown root:root /usr/local/lib/demo_service_python/demo_service_python.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo chmod 644  /usr/local/lib/demo_service_python/demo_service_python.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo systemctl edit service_python --full


.. code-block:: text

  [Unit]
  # Nom du service pour les humains ;-p
  Description=Exemple de service Python

  [Service]
  Environment=PYTHONUNBUFFERED=1
  #Commande à exécuter quand le service est démarré
  ExecStart=/usr/bin/python3 /usr/local/lib/demo_service_python/demo_service_python.py
  Restart=on-failure
  Type=notify
  StandardOutput=journal+console

  [Install]
  WantedBy=default.target


Utiliser un service en tant que :terminal:`root` est un risque en sécurité. Sécurisons cela en ajoutant un utilisateur service.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo useradd -r -s /bin/false service_python


Définissons le service pour qu'il s'exécute avec l'utilisateur :terminal:`service_python`


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo systemctl edit service_python --full


.. code-block:: text

  [Unit]
  # Nom du service pour les humains ;-p
  Description=Exemple de service Python

  [Service]
  User=service_python
  Environment=PYTHONUNBUFFERED=1
  #Commande à exécuter quand le service est démarré
  ExecStart=/usr/bin/python3 /usr/local/lib/demo_service_python/demo_service_python.py
  Restart=on-failure
  Type=notify
  StandardOutput=journal+console

  [Install]
  WantedBy=default.target


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo systemctl start service_python
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo systemctl status service_python
  ● service_python.service - Exemple de service Python
       Loaded: loaded (/etc/systemd/system/service_python.service; disabled; vendor preset: enabled)
       Active: active (running) since Mon 2021-11-22 13:00:31 CET; 9s ago
     Main PID: 16955 (python3)
        Tasks: 1 (limit: 9150)
       Memory: 4.3M
       CGroup: /system.slice/service_python.service
               └─16955 /usr/bin/python3 /usr/local/lib/demo_service_python/demo_service_python.py

  nov. 22 13:00:26 MachineUbuntu.domaine-perso.fr systemd[1]: Starting Exemple de service Python...
  nov. 22 13:00:26 MachineUbuntu.domaine-perso.fr python3[16955]: Démarrage de service_python …
  nov. 22 13:00:31 MachineUbuntu.domaine-perso.fr python3[16955]: Démarrage OK
  nov. 22 13:00:31 MachineUbuntu.domaine-perso.fr python3[16955]: Réponse du service python de démonstration
  nov. 22 13:00:31 MachineUbuntu.domaine-perso.fr systemd[1]: Started Exemple de service Python.
  nov. 22 13:00:36 MachineUbuntu.domaine-perso.fr python3[16955]: Réponse du service python de démonstration
  nov. 22 13:00:41 MachineUbuntu.domaine-perso.fr python3[16955]: Réponse du service python de démonstration
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo systemctl --property=MainPID show service_python
  MainPID=16955
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ps -o uname= -p 16955
  service_python
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ps -o user= -p 16955
  service_python
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ps -o command= -p 16955
  /usr/bin/python3 /usr/local/lib/demo_service_python/demo_service_python.py
