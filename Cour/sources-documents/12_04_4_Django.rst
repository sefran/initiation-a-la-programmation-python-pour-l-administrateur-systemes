.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

.. role:: requête(code)
  :language: sql

Django
======

Jusqu'à maintenant nous avons utilisé le framework Flask pour fabriquer des serveurs WEBs d'infrastructures systèmes et pour générer un petit site WEB. Puis nous avons utilisé REMI pour générer des GUI WEB avec Python. GUI WEB pouvant servir d'interface utilisateur pour des administrateurs systèmes.

Maintenant nous passons à Django un framework Python Serveur WEB plus intégré site WEB applicatif, ORM et gros projets.

Installation
------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ sudo pip install Django
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ python3 -m django --version
  3.2.9


Création d'un projet
--------------------

Django permet la création d'un projet avec la commande :terminal:`django-admin startproject «nom du projet»`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ django-admin startproject monsitedjango
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ tree monsitedjango
  monsitedjango
  ├── manage.py
  └── monsitedjango
      ├── __init__.py
      ├── settings.py
      ├── urls.py
      ├── asgi.py
      └── wsgi.py

  1 directory, 6 files


Le répertoire «**monsitedjango**» contient votre projet Django.

* «**manage.py**» : Ce fichier est un utilitaire de gestion du site Django.
* Le sous-répertoire «**monsitedjango**» : Paquet Python du site monsitedjango.
* «**__init__.py**» : Les informations du paquet Python monsitedjango.
* «**setting.py**» : Configuration de votre projet Django monsitedjango.
* «**urls.py**» : Vos chemins d'accès WEB de votre projet.
* «**asgi.py**» et «**wsgi**» : fichiers pour configurer les technologies WEB aSGI (Asynchronous Server Gateway Interface, technologie asynchrone de serveurs WEB genre Daphne, Hypercorn, Uvicorn, etc.), et WSGI (Web Server Gateway Interface, est un standard Python décrit dans la PEP 3333 sur des serveurs WEB genre Gunicorn, uWSGI, mod_wsgi d'apache) pour des déploiements sur des infrastructures de productions industrielles. Tout ceci ne sera pas abordé dans ce cour.


Modules applicatifs
^^^^^^^^^^^^^^^^^^^

Avec Django il est préférable de développer son application sous forme de modules d'applications. Ceci permet de rendre son développement modulaire, d'avoir une meilleure maintenabilité du site, et une réutilisation du code applicatif pour d'autres projets.

Pour gérer le site Django nous utiliserons la commande :terminal:`manage.py`.

Sous Ubuntu il faut préciser la version de Python dans :terminal:`manage.py` pour que cela fonctionne. Corrigeons cela en modifiant le fichier «**repertoire_de_developpement/14_Serveurs/monsitedjango/manage.py**».

.. code-block:: python

  #!/usr/bin/env python3


Maintenant avec la commande :terminal:`manage.py startapp «mon application»` nous pouvons créer des modules applicatifs Django.

Nous voulons pour notre projet :terminal:`monsitedjango` une gestion de connexions utilisateurs sous forme de module. Nous devons donc créer une application de connexion pour notre projet.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ cd monsitedjango
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py startapp connexion
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ tree
  .
  ├── connexion
  │   ├── admin.py
  │   ├── apps.py
  │   ├── __init__.py
  │   ├── migrations
  │   │   └── __init__.py
  │   ├── models.py
  │   ├── tests.py
  │   └── views.py
  ├── manage.py
  └── monsitedjango
      ├── asgi.py
      ├── __init__.py
      ├── __pycache__
      │   ├── __init__.cpython-39.pyc
      │   ├── settings.cpython-39.pyc
      ├── settings.py
      ├── urls.py
      └── wsgi.py

  4 directories, 15 files


Nous allons maintenant intégrer ce module applicatif dans notre projet. Pour cela il nous faut modifier :python:`INSTALLED_APPS = ['«module applicatif Django»',]` du fichier «**settings.py**» du répertoire «**repertoire_de_developpement/14_Serveurs/monsitedjango/monsitedjango**» :

.. code-block:: python

  INSTALLED_APPS = [
      'django.contrib.admin',
      'django.contrib.auth',
      'django.contrib.contenttypes',
      'django.contrib.sessions',
      'django.contrib.messages',
      'django.contrib.staticfiles',
      'connexion',
  ]


Une fois le module **connexion** développé, on pourra l'intègrer dans d'autres projets en copiant le dossier dans le répertoire projet. On activera ce module en modifiant «**settings.py**» du projet.


Lancer le serveur
-----------------

Pour démarrer le serveur Django utiliser la commande :terminal:`manage.py runserver`, et pour l'arrêter tapez :terminal:`Ctrl+C`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py runserver
  Watching for file changes with StatReloader
  Performing system checks...

  System check identified no issues (0 silenced).

  You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
  Run 'python manage.py migrate' to apply them.
  December 02, 2021 - 08:50:33
  Django version 3.2.9, using settings 'monsitedjango.settings'
  Starting development server at http://127.0.0.1:8000/
  Quit the server with CONTROL-C.
  ^C


Nous remarquons que nous avons l'erreur :terminal:`You have 18 unapplied migration(s).` lors de l'exécution de notre serveur Django. Corrigeons cela comme suit :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py makemigrations
  No changes detected
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py migrate
  Operations to perform:
    Apply all migrations: admin, auth, contenttypes, sessions
  Running migrations:
    Applying contenttypes.0001_initial... OK
    Applying auth.0001_initial... OK
    Applying admin.0001_initial... OK
    Applying admin.0002_logentry_remove_auto_add... OK
    Applying admin.0003_logentry_add_action_flag_choices... OK
    Applying contenttypes.0002_remove_content_type_name... OK
    Applying auth.0002_alter_permission_name_max_length... OK
    Applying auth.0003_alter_user_email_max_length... OK
    Applying auth.0004_alter_user_username_opts... OK
    Applying auth.0005_alter_user_last_login_null... OK
    Applying auth.0006_require_contenttypes_0002... OK
    Applying auth.0007_alter_validators_add_error_messages... OK
    Applying auth.0008_alter_user_username_max_length... OK
    Applying auth.0009_alter_user_last_name_max_length... OK
    Applying auth.0010_alter_group_name_max_length... OK
    Applying auth.0011_update_proxy_permissions... OK
    Applying auth.0012_alter_user_first_name_max_length... OK
    Applying sessions.0001_initial... OK


Nous reviendrons plus tard sur ces commandes Django :terminal:`manage.py makemigrations` et :terminal:`manage.py migrate`.

Nous pouvons maintenant démarrer normalement Django.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py runserver
  Watching for file changes with StatReloader
  Performing system checks...

  System check identified no issues (0 silenced).
  December 02, 2021 - 08:54:22
  Django version 3.2.9, using settings 'monsitedjango.settings'
  Starting development server at http://127.0.0.1:8000/
  Quit the server with CONTROL-C.


.. image:: images/Django-1.png
   :alt: Affichage page par défaut de Django
   :align: center
   :scale: 100%

Le serveur démarre sur :terminal:`http://127.0.0.1:8000`.

Vous pouvez préciser avec la commande :terminal:`manage.py` sur quel port, ou même quelle adresse démarrer votre serveur Django.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py runserver 5000
  …
  Starting development server at http://127.0.0.1:5000/
  …
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py runserver 10.10.10.1:5000
  …
  Starting development server at http://10.10.10.1:5000/
  …
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py runserver 0:5000
  …
  Starting development server at http://0:5000/
  …


:terminal:`0` est un raccourci vers 0.0.0.0

On peut aussi préciser cela dans le fichier «**settings.py**» du répertoire «**repertoire_de_developpement/14_Serveurs/monsitedjango/monsitedjango**» en ajoutant l'import :python:`from django.core.management.commands.runserver import Command as runserver`, et les variables :python:`runserver.default_addr = '«ip»'`, :python:`runserver.default_port = '«port»'`. Pour que le serveur Django accède à l'addresse ip il faut aussi l'autoriser avec :python:`ALLOWED_HOSTS = ['«ip»']`.

.. code-block:: python

  from django.core.management.commands.runserver import Command as runserver
  ''''''
  ALLOWED_HOSTS = ['10.10.10.1']
  runserver.default_port = '5000'
  runserver.default_addr = '10.10.10.1'
  ''''''

Profitons aussi de l'édition du fichier «**settings.py**» pour passer le site en Français.

.. code-block:: python

  LANGUAGE_CODE = 'fr-fr'


Ce qui nous donne à l'exécution.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py runserver
  …
  Starting development server at http://10.10.10.1:5000/
  …


.. image:: images/Django-2.png
   :alt: Affichage page par défaut en français de Django
   :align: center
   :scale: 100%


Les vues
--------

Django est développé suivant l'architecture MVC (Modèle Vues Contrôleurs). Dans cette section, nous allons voir comment élaborer une vue et gérer son articulation dans le site avec un contrôleur.

Écrivons d’abord la première vue de l'application Django connexion en modifiant le fichier des vues «**views.py**» dans «**repertoire_de_developpement/14_Serveurs/monsitedjango/connexion**».

.. code-block:: python

  from django.shortcuts import render
  from django.http import HttpResponse

  def racine(request):
      return HttpResponse('Bonjour tout le monde!')


Les contrôleurs sont, pour les vues, les fichiers «**urls.py**» que nous allons modifier.

Les vues seront prises en compte dans ce fichier avec la commande :python:`path(route='«chemin»', view=«vue», kwarg, name='«nom django»')`.

* «**route**» : indique l'URL de la vue.
* «**view**» : l'objet vue.
* «**kwarg**» : Des paramètres à passer à la vue.
* «**name**» : le nom Django du lien pour être utilisé lors de renvois URL dans le code.

Créons le fichier «**urls.py**», contrôleur du module applicatif connexion, dans le répertoire «**repertoire_de_developpement/14_Serveurs/monsitedjango/connexion**» de l'application **connexion**. Ce fichier va prendre en compte la vue d'affichage lors d'une connexion.

Éditons ce fichier comme suit :

.. code-block:: python

  from django.urls import path
  from . import views

  urlpatterns = [
      path('connexion/', views.racine, name='connexion'),
  ]


Modifions le fichier contrôleur «**urls.py**» du répertoire «**repertoire_de_developpement/14_Serveurs/monsitedjango**» pour qu'il prenne en compte la vue du module apllicatif **connexion**.

On peut déjà remarqué qu'il n'est pas vide, et qu'il contient un lien vers «**/admin**».

.. code-block:: python

  from django.contrib import admin
  from django.urls import path

  urlpatterns = [
      path('admin/', admin.site.urls),
  ]


.. image:: images/Django-3.png
   :alt: Affichage page Admin de Django
   :align: center
   :scale: 100%

Nous pouvons alors ajouter un renvoi vers le module applicatif connexion avec l'objet :python:`include('mon_module_applicatif_django.urls')` du module Python :python:`django.urls`.

Ajoutons l'URL de l'application **connexion** à notre projet dans le fichier.

.. code-block:: python

  from django.contrib import admin
  from django.urls import path, include

  urlpatterns = [
      path('', include('connexion.urls')),
      path('admin/', admin.site.urls),
  ]


.. image:: images/Django-4.png
   :alt: Django première page de connexion
   :align: center
   :scale: 100%

On peut remarquer que la racine du site n'est plus accéssible.

.. image:: images/Django-5.png
   :alt: Django racine du site non accéssible
   :align: center
   :scale: 100%


Modèles
-------

Ici nous abordons le traitement des données de l'application Django. Généralement celles-ci se font sous forme de Base de données.

Regardons comment est configuré notre projet Django au niveau de la base de données en consultant le fichier «**settings.py**».

.. code-block:: python

  # Database
  # https://docs.djangoproject.com/en/3.2/ref/settings/#databases

  DATABASES = {
      'default': {
          'ENGINE': 'django.db.backends.sqlite3',
          'NAME': BASE_DIR / 'db.sqlite3',
      }
  }

Donc par défaut Django utilise une base de données SQLite3, et nous avons vu comment l'utiliser avec Python dans la section `SQLite3`_.

Nous pouvons remarquer qu'après l'exécution du projet Django, un fichier «**db.sqlite3**» a été créé à la racine du projet.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ tree --dirsfirst -L 1
  .
  ├── connexion
  ├── monsitedjango
  ├── db.sqlite3
  └── manage.py

  2 directories, 2 files


C'est le modèle de données de gestion du site Django.

Utiliser le modèle Django
^^^^^^^^^^^^^^^^^^^^^^^^^

Regardons le contenu de cette base de données Django SQLite3 avec la commande SQL :requête:`SELECT name FROM sqlite_master WHERE type='table';`.

.. code-block:: pycon

  >>> import sqlite3
  >>> mabasedjango = sqlite3.connect('db.sqlite3')
  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT name FROM sqlite_master WHERE type='table';")
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1b720a0>
  [('django_migrations',), ('sqlite_sequence',), ('auth_group_permissions',), ('auth_user_groups',), ('auth_user_user_permissions',), ('django_admin_log',), ('django_content_type',), ('auth_permission',), ('auth_group',), ('auth_user',), ('django_session',)]
  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT * FROM auth_user;")
  ...     list(map(lambda x: x[0], curseurbd.description))
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1a78f10>
  ['id', 'password', 'last_login', 'is_superuser', 'username', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined', 'first_name']
  []
  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT * FROM auth_group;")
  ...     list(map(lambda x: x[0], curseurbd.description))
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1b720a0>
  ['id', 'name']
  []
  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT * FROM auth_user_groups;")
  ...     list(map(lambda x: x[0], curseurbd.description))
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1a78f10>
  ['id', 'user_id', 'group_id']
  []
  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT * FROM auth_permission;")
  ...     list(map(lambda x: x[0], curseurbd.description))
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1b72180>
  ['id', 'content_type_id', 'codename', 'name']
  [(1, 1, 'add_logentry', 'Can add log entry'), (2, 1, 'change_logentry', 'Can change log entry'), (3, 1, 'delete_logentry', 'Can delete log entry'), (4, 1, 'view_logentry', 'Can view log entry'), (5, 2, 'add_permission', 'Can add permission'), (6, 2, 'change_permission', 'Can change permission'), (7, 2, 'delete_permission', 'Can delete permission'), (8, 2, 'view_permission', 'Can view permission'), (9, 3, 'add_group', 'Can add group'), (10, 3, 'change_group', 'Can change group'), (11, 3, 'delete_group', 'Can delete group'), (12, 3, 'view_group', 'Can view group'), (13, 4, 'add_user', 'Can add user'), (14, 4, 'change_user', 'Can change user'), (15, 4, 'delete_user', 'Can delete user'), (16, 4, 'view_user', 'Can view user'), (17, 5, 'add_contenttype', 'Can add content type'), (18, 5, 'change_contenttype', 'Can change content type'), (19, 5, 'delete_contenttype', 'Can delete content type'), (20, 5, 'view_contenttype', 'Can view content type'), (21, 6, 'add_session', 'Can add session'), (22, 6, 'change_session', 'Can change session'), (23, 6, 'delete_session', 'Can delete session'), (24, 6, 'view_session', 'Can view session')]
  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT * FROM auth_user_user_permissions;")
  ...     list(map(lambda x: x[0], curseurbd.description))
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1b720a0>
  ['id', 'user_id', 'permission_id']
  []

Nous n'avons pas d'utilisateurs dans la base de données, ni même de groupe de gestion des autorisations. Django a un système de gestion simple des permissions. Ce système permet l'attribution de permissions ou de groupes ouvrant à des autorisations pour un utilisateur.

Ce système est utilisé par la partie administration du site de Django, et vous pouvez l'utiliser dans votre code pour gérer l'accès de vos utilisateurs.

Il existe des sortes de templates d’autorisations que l'on peut ajouter dans Django avec le fichier «**settings.py**». Par exemple :python:`django.contrib.auth`, déjà présent dans la configuration, qui ajoute les droits d'ajout, de suppression et de visualisation ; ou aussi :python:`django.contrib.auth.models.Group` qui permet d'attribuer des permissions au travers de groupes.

.. code-block:: python

  INSTALLED_APPS = [
      'django.contrib.auth',
      'django.contrib.contenttypes', # Gère les utilisateurs
      ''''''
  ]


Pour la gestion des connexions pour le site, nous avons aussi besoins des briques Django pour gérer les authentifications. C'est ce que l'on appelle un «middleware». Il nous faut donc :python:`django.contrib.sessions.middleware.SessionMiddleware` et :python:`django.contrib.auth.middleware.AuthenticationMiddleware` pour avoir les outils Django de gestion des connexions. Ils sont normalement présent par défaut dans le fichier «**settings.py**».

.. code-block:: python

  MIDDLEWARE = [
      'django.contrib.sessions.middleware.SessionMiddleware',
      'django.contrib.auth.middleware.AuthenticationMiddleware',
      ''''''
  ]


Nous allons maintenant créer un administrateur du site et voir le résultat dans le modèle de la base de données.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py createsuperuser --username=programmeur --email=programmeur.python@fai.fr
  Password:
  Password (again):
  Ce mot de passe est trop courant.
  Bypass password validation and create user anyway? [y/N]: y
  Superuser created successfully.


Regardons ce qu'il s'est passé sur la base de données Django.

.. code-block:: pycon

  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT * FROM auth_user;")
  ...     list(map(lambda x: x[0], curseurbd.description))
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1b72180>
  ['id', 'password', 'last_login', 'is_superuser', 'username', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined', 'first_name']
  [(2, 'pbkdf2_sha256$260000$Tqv2p77phrnn0eaEcEbPAK$Tijor0UG6MTkOkKPTjQScflflncw13iizO1SorHKaU0=', None, 1, 'programmeur', '', 'programmeur.python@fai.fr', 1, 1, '2021-12-03 12:39:26.109575', '')]
  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT * FROM auth_user_user_permissions;")
  ...     list(map(lambda x: x[0], curseurbd.description))
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1a78f10>
  ['id', 'user_id', 'permission_id']
  []


Créons l'interface de connexion avec Djando. Commençons par créer le fichier de formulaire de connection «**formulaire.py**» dans «**repertoire_de_developpement/14_Serveurs/monsitedjango/connexion**».

.. code-block:: python

  from django import forms

  class FormulaireDeConnexion(forms.Form):
      utilisateur = forms.CharField(label='Utilisateur : ', max_length=30)
      motdepasse = forms.CharField(label='Mot de passe : ', widget=forms.PasswordInput)


Créer le répertoire «**repertoire_de_developpement/14_Serveurs/monsitedjango/connexion/templates**». Editer dedans le fichier «**connexion.html**»

.. code-block:: jinja

  <h1>Se connecter</h1>

  {% if mauvaisutilisateur %}<p><strong>L'utilisateur n'est pas reconnu</strong></p>{% endif %}
  {% if mauvaismotdepasse %}<p><strong>Vérifiez votre mot de passe</strong></p>{% endif %}

  {% if utilisateur.is_authenticated %}{{ utilisateur.username }} vous êtes connecté :-){% else %}
  <form method="post" action=".">
      {% csrf_token %}
      {{ formulaire.as_p }}
      <input type="submit"/>
  </form>
  {% endif %}


Puis modifions le fichier des vues «**views.py**» dans «**repertoire_de_developpement/14_Serveurs/monsitedjango/connexion**».

.. code-block:: python

  from django.shortcuts import render, redirect
  from django.contrib.auth import authenticate, login
  from django.contrib.auth.models import User
  from connexion.formulaire import FormulaireDeConnexion

  def connexion(request):

      mauvaisutilisateur = False
      mauvaismotdepasse = False
      utilisateurexiste = False

      if request.method == 'POST':
          formulaire = FormulaireDeConnexion(request.POST)
          if formulaire.is_valid():
              nomutilisateur = formulaire.cleaned_data['utilisateur']
              motdepasseutilisateur = formulaire.cleaned_data['motdepasse']
              try: # Teste si l'utilisateur est dans la base de données du modèle
                  User.objects.get(username=nomutilisateur)
                  utilisateurexiste = True
              except User.DoesNotExist:
                  mauvaisutilisateur = True
              if utilisateurexiste:
                  utilisateur = authenticate(username=nomutilisateur, password=motdepasseutilisateur)  # Teste si le nom d'utilisateur et le mot de passe correspondent
                  if utilisateur:
                      login(request, utilisateur)
                      #return redirect('/admin/')
                  else:
                      mauvaismotdepasse = True
      else:
          formulaire = FormulaireDeConnexion()

      return render(request, 'connexion.html', locals())


Enfin mettez à jours «**urls.py**» dans «**repertoire_de_developpement/14_Serveurs/monsitedjango/connexion**».

.. code-block:: python

  from django.urls import path
  from . import views

  urlpatterns = [
      path('connexion/', views.connexion, name='connexion'),
  ]


.. image:: images/Django-6.png
   :alt: Django fenêtre connexion
   :align: center
   :scale: 100%

.. image:: images/Django-7.png
   :alt: Django connexion utilisateur Bidon
   :align: center
   :scale: 100%

.. image:: images/Django-8.png
   :alt: Django échec connexion mauvais utilisateur
   :align: center
   :scale: 100%

.. image:: images/Django-9.png
   :alt: Django connexion programmeur
   :align: center
   :scale: 100%

.. image:: images/Django-10.png
   :alt: Django mauvais mot de passe connexion programmeur
   :align: center
   :scale: 100%

.. image:: images/Django-11.png
   :alt: Django réussite connexion programmeur
   :align: center
   :scale: 100%

.. image:: images/Django-12.png
   :alt: Django redirection vers l'administration suite à succès connexion
   :align: center
   :scale: 100%

Nous avons une erreur lorsque nous nous connectons directement avec l'adresse :python:`http://10.10.10.1:5000/`. Pour cela nous devrons créer une vue racine avec un module d'application, par exemple :terminal:`./manage.py startapp appli`, importer le décorateur :python:`login_required` du module :python:`django.contrib.auth.decorators`, et ajouter :python:`@login_required()` en début de fonction de vue :python:`racine` pour renvoyer vers la fenêtre de connexion.

.. code-block:: python

  from django.shortcuts import render
  from django.http import HttpResponse
  from django.contrib.auth.decorators import login_required

  @login_required()
  def racine(request):
      return HttpResponse('Bonjour tout le monde!')


Il faudra en suite éditer le fichier «**settings.py**» dans «**repertoire_de_developpement/14_Serveurs/monsitedjango/monsitedjango**», et préciser le lien http de connexion en ajoutant la variable :python:`LOGIN_URL`.

.. code-block:: python

  # URL de connexion
  LOGIN_URL = '/connexion/'

  INSTALLED_APPS = [
      ''''''
      'connexion',
      'appli',
  ]


Rajouter le lien dans le fichier «**urls.py**» du module applicatif :python:`appli` :

.. code-block:: python

  from django.urls import path
  from . import views

  urlpatterns = [
      path('', views.racine, name='racine'),
  ]


Et bien sur rajouter le lien du module applicatif :python:`appli` dans le fichier «**urls.py**» du projet.

.. code-block:: python

  from django.contrib import admin
  from django.urls import path, include

  urlpatterns = [
      path('', include('appli.urls')),
      path('', include('connexion.urls')),
      path('admin/', admin.site.urls),
  ]


.. note:: On peut aussi directement préciser le lien avec :python:`@login_required(login_url='/autre_système_connexion')`, pour éventuellement gérer plusieurs types d'authentifications.

Pour créer un lien de déconnexion, il suffira de créer une fonction de déconnexion dans le fichier «**views.py**» du module applicatif :python:`connexion`.

.. code-block:: python

  from django.shortcuts import render, redirect
  from django.contrib.auth import authenticate, login, logout
  from django.contrib.auth.models import User
  from connexion.formulaire import FormulaireDeConnexion

  ''''''

  def deconnexion(request):
      logout(request)
      return redirect('/connexion')


de modifier «**urls.py**» du module applicatif :python:`connexion`.

.. code-block:: python

  from django.urls import path
  from . import views

  urlpatterns = [
      path('connexion/', views.connexion, name='connexion'),
      path('deconnexion/', views.deconnexion, name='deconnexion'),
  ]


Avec ces exemples, nous venons de voir comment interagir avec le modèle des utilisateurs de Django. Cela nous renvoi aussi sur l'interface d'administration où l'on peut créer des utilisateurs, se déconnecter et plein de choses…

Mais comment créer son propre modèle de données ?


Créer son modèle
^^^^^^^^^^^^^^^^

Pour voir comment on utilise les modèles, nous allons étendre le modèle User de Django.

Par défaut nous avons les données :terminal:`['id', 'password', 'last_login', 'is_superuser', 'username', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined', 'first_name']` pour l'utilisateur. Nous souhaiterions donc un modèle :python:`Administratif` avec en plus la **date de naissance**, le **sexe**, la **ville**, le **code postal**, l'\ **adresse** et le **numéro de téléphone**.

Modifions le modèle en éditant «**models.py**» dans «**repertoire_de_developpement/14_Serveurs/monsitedjango/appli**».

.. code-block:: python

  from django.db import models
  from django.contrib.auth.models import User
  from django.core.validators import RegexValidator

  class Administratif(models.Model):
      utilisateur = models.OneToOneField(User, on_delete=models.PROTECT) # Lien avec le modèle User
      datenaissance = models.DateField()
      SEXES = (
          ('M', 'Masculin'),
          ('F', 'Féminin'),
          ('H', 'Hermaphrodite'),
          ('I', 'Itersexuation'),
          )
      sexe = models.CharField('Sexe', max_length=100, choices = SEXES)
      ville = models.CharField('Ville', max_length=180)
      message_codepostal = 'Le code postal doit-être de la forme 00000'
      codepostal_regex = RegexValidator(
              regex = r'^[0-9]{5}$',
              message = message_codepostal,
          )
      codepostal = models.CharField('Code postal', validators=[codepostal_regex], max_length=12)
      addresse = models.TextField(blank=True)
      message_téléphone = 'Le numéro de téléphone saisi doit être de la forme : 0000000000'
      téléphone_regex = RegexValidator(
              regex = r'^(0|\+33|0033)[1-9][0-9]{8}$'',
              message = message_téléphone,
          )
      telephone = models.CharField(validators=[téléphone_regex], max_length=60, null=True, blank=True)

      def __str__(self):
          return "Administratif de {0}".format(self.utilisateur.username)


Maintenant nous allons voir à quoi servent les commandes :terminal:`makemigrations` et :terminal:`migrate`. Ces commande servent à mettre à jours les modèles avec la base de données.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py makemigrations appli
  Migrations for 'appli':
    appli/migrations/0001_initial.py
      - Create model Administratif
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py migrate
  Operations to perform:
    Apply all migrations: admin, auth, connexion, contenttypes, sessions
  Running migrations:
    Applying appli.0001_initial... OK
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs/monsitedjango$ ./manage.py runserver


Vérifions cela avec la base de données.

.. code-block:: pycon

  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT name FROM sqlite_master WHERE type='table';")
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b09a1960>
  [('django_migrations',), ('sqlite_sequence',), ('auth_group_permissions',), ('auth_user_groups',), ('auth_user_user_permissions',), ('django_admin_log',), ('django_content_type',), ('auth_permission',), ('auth_group',), ('auth_user',), ('django_session',), ('appli_administratif',)]
  >>> with mabasedjango:
  ...     curseurbd = mabasedjango.cursor()
  ...     curseurbd.execute("SELECT * FROM appli_administratif;")
  ...     list(map(lambda x: x[0], curseurbd.description))
  ...     print(curseurbd.fetchall())
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f57b1a78f10>
  ['id', 'datenaissance', 'sexe', 'ville', 'codepostal', 'addresse', 'telephone', 'utilisateur_id']
  []


Le modèle :python:`Administratif` a bien été créé. Mais comment le renseigner ?


Administration
--------------

Pour renseigner le modèle :python:`Administratif`, nous allons utiliser l'interface d'administration de Django.

Pour cela éditer «**admin.py**» dans «**repertoire_de_developpement/14_Serveurs/monsitedjango/appli**».

.. code-block:: python

  from django.contrib import admin
  from . import models

  admin.site.register(models.Administratif)

Rien de plus simple…

Ce qui nous donne :

.. image:: images/Django-13.png
   :alt: Django modèle Administratif dans l'administration
   :align: center
   :scale: 100%

.. image:: images/Django-14.png
   :alt: Django ajout dans l'administration
   :align: center
   :scale: 100%

.. image:: images/Django-15.png
   :alt: Django ajout Administratif d'un utilisateur dans l'administration
   :align: center
   :scale: 100%

.. image:: images/Django-16.png
   :alt: Django Saisie administratif utilisateur programmeur dans l'administration
   :align: center
   :scale: 100%

.. image:: images/Django-17.png
   :alt: Django Administratif de l'utilisateur programmeur ajouté dans l'administration
   :align: center
   :scale: 100%

Le modèle Administratif prend aussi en charge la gestion des erreurs de saisies.

.. image:: images/Django-18.png
   :alt: Django Gestion des erreurs du modèle Administratif
   :align: center
   :scale: 100%

On peut modifier tout modèle de données ainsi déclaré avec l'interface d'administration. Comme pour le modèle système User.

.. image:: images/Django-19.png
   :alt: Django modiffication modèle utilisateur dans l'administration
   :align: center
   :scale: 100%

Vous pouvez pousser ce tutoriel sur l'apparence, les tests, les modules Django, les paquets applicatifs, etc avec `le tutoriel Django <https://docs.djangoproject.com/fr/3.2/intro>`_.

Je conseille aussi le site `Zeste de savoir <https://zestedesavoir.com/tutoriels/598/developpez-votre-site-web-avec-le-framework-django/262_presentation-de-django/1516_creez-vos-applications-web-avec-django>`_.
