.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Les bases de données
********************

SQLite3
=======

Installation
------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ sudo apt install sqlite3


Créer une base de données SQLite
--------------------------------

.. code-block:: pycon

  >>> import sqlite3
  >>> mabasesqlite = sqlite3.connect('BaseDonnéesSQLite') # Création ou ouverture de la base

Un fichier **BaseDonnéesSQLite** vient d'être créer dans **repertoire_de_developpement/12_Données/**

Vérifions que la base de données a bien été crée

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ sqlite3 BaseDonnéesSQLite
  SQLite version 3.34.1 2021-01-20 14:10:07
  Enter ".help" for usage hints.


.. code-block:: sqlite3

  sqlite> .databases
  main: /home/utilisateur/repertoire_de_developpement/12_Données/BaseDonnéesSQLite r/w
  sqlite> .quit


Créer des tables
----------------

.. code-block:: pycon

  >>> curseurbd = mabasesqlite.cursor() # Curseur pour exécuté des requettes SQL
  >>> curseurbd.execute('''create table personne (identifiant integer primary key, prénom text, nom text)''') # Création SQL de la table «personne» avec les attributs «prénom» et «nom»
  <sqlite3.Cursor object at 0x7f35d6c8c030>
  >>> mabasesqlite.commit() # sauvegarde des modifications dans la base SQLite
  >>> curseurbd.close() # Ferme le curseur de requêtes SQL


Vérifions que la table a bien été crée

.. code-block:: sqlite3

  sqlite> .table
  personne
  sqlite> select name from PRAGMA_TABLE_INFO('personne');
  identifiant
  prénom
  nom
  sqlite> PRAGMA table_info(personne);
  0    identifiant  integer  0                    1
  1    prénom       text     0                    0
  2    nom          text     0                    0


Insérer des données
-------------------

.. code-block:: pycon

  >>> curseurbd = mabasesqlite.cursor()
  >>> for donnée in [('Prénom', 'NOM'), ('Utilisateur', 'DÉVELOPPEUR'), ('Stagiaire', 'PYTHON')]:
  ...     curseurbd.execute('insert into personne (prénom, nom) values (?,?)', donnée)
  ...
  <sqlite3.Cursor object at 0x7f35d6c8c110>
  <sqlite3.Cursor object at 0x7f35d6c8c110>
  <sqlite3.Cursor object at 0x7f35d6c8c110>
  >>> mabasesqlite.commit()
  >>> curseurbd.close()


Vérifions que les données ont été bien ajoutées

.. code-block:: sqlite3

  sqlite> select * from personne;
  1            Prénom       NOM
  2            Utilisateur  DÉVELOPPEUR
  3            Stagiaire    PYTHON

Mais cette façon de coder n'est pas bonne.

.. code-block:: pycon

  >>> with mabasesqlite:
  ...     curseurbd = mabasesqlite.cursor()
  ...     for donnée in [('Prénom2', 'NOM2'), ('Utilisateur2', 'DÉVELOPPEUR2'), ('Stagiaire2', 'PYTHON2')]:
  ...         curseurbd.execute('insert into personne (prénom, nom) values (?,?)', donnée)
  ...     mabasesqlite.commit()
  ...     curseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f35d6b93e30>
  <sqlite3.Cursor object at 0x7f35d6b93e30>
  <sqlite3.Cursor object at 0x7f35d6b93e30>


Vérifions que les données ont été bien ajoutées

.. code-block:: sqlite3

  sqlite> select * from personne;
  1            Prénom        NOM
  2            Utilisateur   DÉVELOPPEUR
  3            Stagiaire     PYTHON
  4            Prénom2       NOM2
  5            Utilisateur2  DÉVELOPPEUR2
  6            Stagiaire2    PYTHON2


Lire des données
----------------

.. code-block:: pycon

  >>> with mabasesqlite:
  ...     cuseurbd = mabasesqlite.cursor()
  ...     cuseurbd.execute('select * from personne')
  ...     for valeur in cuseurbd:
  ...         print('{}|{}|{}'.format(*valeur))
  ...     cuseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f35d6b93e30>
  1|Prénom|NOM
  2|Utilisateur|DÉVELOPPEUR
  3|Stagiaire|PYTHON
  4|Prénom2|NOM2
  5|Utilisateur2|DÉVELOPPEUR2
  6|Stagiaire2|PYTHON2


Effacer des données
-------------------

.. code-block:: pycon

  >>> with mabasesqlite:
  ...     cuseurbd = mabasesqlite.cursor()
  ...     cuseurbd.execute('delete from personne where nom like \'%2\'')
  ...     mabasesqlite.commit()
  ...     cuseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f35d6b93e30>
  >>> with mabasesqlite:
  ...     cuseurbd = mabasesqlite.cursor()
  ...     cuseurbd.execute('select * from personne')
  ...     for valeur in cuseurbd:
  ...         print('{}|{}|{}'.format(*valeur))
  ...     cuseurbd.close()
  ...
  <sqlite3.Cursor object at 0x7f35d6c8c030>
  1|Prénom|NOM
  2|Utilisateur|DÉVELOPPEUR
  3|Stagiaire|PYTHON


SQLAlchemy
==========

Installation
------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install python3-sqlalchemy


connexion à la base de données
------------------------------

Exécuter des requêtes SQL directement.

.. code-block:: pycon

  >>> from sqlalchemy import create_engine
  >>> mabasesqlite = create_engine('sqlite:///BaseDonnéesSQLite', echo=True)
  >>> resultat = mabasesqlite.execute('select * from personne')
  2021-11-19 12:42:41,875 INFO sqlalchemy.engine.base.Engine SELECT CAST('test plain returns' AS VARCHAR(60)) AS anon_1
  2021-11-19 12:42:41,875 INFO sqlalchemy.engine.base.Engine ()
  2021-11-19 12:42:41,876 INFO sqlalchemy.engine.base.Engine SELECT CAST('test unicode returns' AS VARCHAR(60)) AS anon_1
  2021-11-19 12:42:41,876 INFO sqlalchemy.engine.base.Engine ()
  2021-11-19 12:42:41,877 INFO sqlalchemy.engine.base.Engine select * from personne
  2021-11-19 12:42:41,877 INFO sqlalchemy.engine.base.Engine ()
  >>> resultat.fetchall()
  [(1, 'Prénom', 'NOM'), (2, 'Utilisateur', 'DÉVELOPPEUR'), (3, 'Stagiaire', 'PYTHON')]


Exécuter des requêtes SQL en mode transactionnel.

.. code-block:: pycon

  >>> from sqlalchemy import create_engine
  >>> mabasesqlite = create_engine('sqlite:///BaseDonnéesSQLite', echo=True)
  >>> transactionnel = mabasesqlite.connect()
  >>> transaction = transactionnel.begin()
  2021-11-19 12:50:00,985 INFO sqlalchemy.engine.base.Engine BEGIN (implicit)
  >>> resultat = transactionnel.execute('select * from personne')
  2021-11-19 12:50:15,513 INFO sqlalchemy.engine.base.Engine select * from personne
  2021-11-19 12:50:15,514 INFO sqlalchemy.engine.base.Engine ()
  >>> transaction.commit()
  2021-11-19 12:50:23,568 INFO sqlalchemy.engine.base.Engine COMMIT
  >>> resultat.fetchall()
  [(1, 'Prénom', 'NOM'), (2, 'Utilisateur', 'DÉVELOPPEUR'), (3, 'Stagiaire', 'PYTHON')]
  >>> transaction = transactionnel.begin()
  2021-11-19 12:52:26,793 INFO sqlalchemy.engine.base.Engine BEGIN (implicit)
  >>> resultat = transactionnel.execute('select * from personne').fetchall()
  2021-11-19 12:52:38,307 INFO sqlalchemy.engine.base.Engine select * from personne
  2021-11-19 12:52:38,307 INFO sqlalchemy.engine.base.Engine ()
  >>> resultat
  [(1, 'Prénom', 'NOM'), (2, 'Utilisateur', 'DÉVELOPPEUR'), (3, 'Stagiaire', 'PYTHON')]
  >>> transaction.commit()
  2021-11-19 12:52:47,537 INFO sqlalchemy.engine.base.Engine COMMIT


Mais SQLAlchemy est ce que l'on appelle un `ORM <https://fr.wikipedia.org/wiki/Mapping_objet_relationnel>`_, c'est-à-dire une interface logicielle standardisée pour transformer une base relationnelle en une base de données orientée objet.

L'**ORM** doit avoir une session pour s'interfacer entre le moteur, qui communique réellement avec la base de données, et les objets que nous traiterons en Python.

.. code-block:: pycon

  >>> from sqlalchemy import create_engine
  >>> mabasesqlite = create_engine('sqlite:///BaseDonnéesSQLite', echo=True)
  >>> from sqlalchemy.orm import sessionmaker
  >>> Sessionbd = sessionmaker(bind=mabasesqlite)
  >>> sessionsql = Sessionbd()
  >>> resultat = sessionsql.execute('select * from personne').fetchall()
  2021-11-19 13:10:11,087 INFO sqlalchemy.engine.base.Engine select * from personne
  2021-11-19 13:10:11,088 INFO sqlalchemy.engine.base.Engine ()
  >>> resultat
  [(1, 'Prénom', 'NOM'), (2, 'Utilisateur', 'DÉVELOPPEUR'), (3, 'Stagiaire', 'PYTHON')]


Création d'une base de données
------------------------------

.. code-block:: pycon

  >>> from sqlalchemy import create_engine
  >>> from sqlalchemy.ext.declarative import declarative_base
  >>> mabasesqlite = create_engine('sqlite:///BaseDonnées.db', echo=True)
  >>> outilbd = declarative_base()
  >>> outilbd.metadata.create_all(mabasesqlite)
  2021-11-19 13:23:12,544 INFO sqlalchemy.engine.base.Engine SELECT CAST('test plain returns' AS VARCHAR(60)) AS anon_1
  2021-11-19 13:23:12,544 INFO sqlalchemy.engine.base.Engine ()
  2021-11-19 13:23:12,545 INFO sqlalchemy.engine.base.Engine SELECT CAST('test unicode returns' AS VARCHAR(60)) AS anon_1
  2021-11-19 13:23:12,545 INFO sqlalchemy.engine.base.Engine ()


Création avec une table
-----------------------

Création d'une base de données avec une table «**personnes**» et les colonnes «**identifiant**», «**prénom**» et «**nom**».

.. code-block:: pycon

  >>> from sqlalchemy import create_engine
  >>> from sqlalchemy import Column, Integer, String
  >>> from sqlalchemy.ext.declarative import declarative_base
  >>> mabasesqlite = create_engine('sqlite:///BaseDonnées.db', echo=True)
  >>> basesqlite = declarative_base()
  >>> class Personne(basesqlite):
  ...     __tablename__ = 'personnes'
  ...     identifiant = Column(Integer, primary_key=True)
  ...     prénom = Column(String)
  ...     nom = Column(String)
  ...     def __init__(self, prénom, nom):
  ...         self.prénom = prénom
  ...         self.nom = nom
  ...
  >>> Personne.metadata.create_all(mabasesqlite)
  2021-11-19 13:59:46,534 INFO sqlalchemy.engine.base.Engine SELECT CAST('test plain returns' AS VARCHAR(60)) AS anon_1
  2021-11-19 13:59:46,534 INFO sqlalchemy.engine.base.Engine ()
  2021-11-19 13:59:46,534 INFO sqlalchemy.engine.base.Engine SELECT CAST('test unicode returns' AS VARCHAR(60)) AS anon_1
  2021-11-19 13:59:46,535 INFO sqlalchemy.engine.base.Engine ()
  2021-11-19 13:59:46,535 INFO sqlalchemy.engine.base.Engine PRAGMA main.table_info("personnes")
  2021-11-19 13:59:46,535 INFO sqlalchemy.engine.base.Engine ()
  2021-11-19 13:59:46,536 INFO sqlalchemy.engine.base.Engine PRAGMA temp.table_info("personnes")
  2021-11-19 13:59:46,536 INFO sqlalchemy.engine.base.Engine ()
  2021-11-19 13:59:46,538 INFO sqlalchemy.engine.base.Engine
  CREATE TABLE personnes (
          identifiant INTEGER NOT NULL,
          "prénom" VARCHAR,
          nom VARCHAR,
          PRIMARY KEY (identifiant)
  )
  2021-11-19 13:59:46,538 INFO sqlalchemy.engine.base.Engine ()
  2021-11-19 13:59:46,572 INFO sqlalchemy.engine.base.Engine COMMIT


Vérifions que la base de données a été bien crée.

.. code-block:: console

utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sqlite3 12_Données/BaseDonnées.db
SQLite version 3.34.1 2021-01-20 14:10:07
Enter ".help" for usage hints.


.. code-block:: sqlite3

  sqlite> select name from PRAGMA_TABLE_INFO('personnes');
  identifiant
  prénom
  nom
  sqlite> PRAGMA table_info(personnes);
  0|identifiant|INTEGER|1||1
  1|prénom|VARCHAR|0||0
  2|nom|VARCHAR|0||0


Ajout d'un enregistrement
-------------------------

.. code-block:: pycon

  >>> from sqlalchemy import create_engine
  >>> mabasesqlite = create_engine('sqlite:///BaseDonnées.db')
  >>> from sqlalchemy.orm import sessionmaker
  >>> Sessionbd = sessionmaker(bind=mabasesqlite)
  >>> sessionsql = Sessionbd()
  >>> resultat = sessionsql.execute('select * from personnes').fetchall()
  >>> resultat
  []
  >>> from sqlalchemy.ext.declarative import declarative_base
  >>> basesqlite = declarative_base()
  >>> from sqlalchemy import Column, Integer, String
  >>> class Personne(basesqlite):
  ...     __tablename__ = 'personnes'
  ...     identifiant = Column(Integer, primary_key=True)
  ...     prénom = Column(String)
  ...     nom = Column(String)
  ...     def __init__(self, prénom, nom):
  ...         self.prénom = prénom
  ...         self.nom = nom
  ...
  >>> utilisateur1 = Personne('Prénom', 'NOM')
  >>> sessionsql.add(utilisateur1)
  >>> utilisateur1.identifiant
  >>> sessionsql.flush()
  >>> sessionsql.commit()
  >>> utilisateur1.identifiant
  1
  >>> utilisateur2 = Personne('Utilisateur', 'DÉVELOPPEUR')
  >>> sessionsql.add(utilisateur2)
  >>> sessionsql.flush()
  >>> utilisateur2.identifiant
  2
  >>> sessionsql.commit()
  >>> utilisateur2.identifiant
  2
  >>> resultat = sessionsql.execute('select * from personnes').fetchall()
  >>> resultat
  [(1, 'Prénom', 'NOM'), (2, 'Utilisateur', 'DÉVELOPPEUR')]


Faire des recherches
--------------------

.. code-block:: pycon

  >>> recherche = sessionsql.query(Personne).filter_by(nom='NOM')
  >>> for index in range(recherche.count()):
  ...     print('{}|{}|{}'.format(recherche[index - 1].identifiant, recherche[index].prénom, recherche[index].nom))
  ...
  1|Prénom|NOM


Ajouter une table
-----------------

.. code-block:: pycon

  >>> class Programmeur(basesqlite):
  ...     __tablename__ = 'programmeur'
  ...     identifiant = Column(Integer, primary_key=True)
  ...     langage = Column(String)
  ...
  >>> Programmeur.__table__.create(mabasesqlite)


.. code-block:: sqlite3

  sqlite> .tables
  personnes    programmeur
  sqlite> PRAGMA table_info(programmeur);
  0|identifiant|INTEGER|1||1
  1|langage|VARCHAR|0||0

Créer une relation
------------------

Supprimez la base de données «**repertoire_de_developpement/12_Données/BaseDonnées.db**»

.. code-block:: pycon

  >>> from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
  >>> from sqlalchemy.orm import sessionmaker, relationship
  >>> from sqlalchemy.ext.declarative import declarative_base
  >>> basesqlite = declarative_base()
  >>> mabasesqlite = create_engine('sqlite:///BaseDonnées.db')
  >>> Sessionbd = sessionmaker(bind=mabasesqlite)
  >>> sessionsql = Sessionbd()
  >>> class Programmeur(basesqlite):
  ...     __tablename__ = 'programmeur'
  ...     identifiant = Column(Integer, primary_key=True)
  ...     langage = Column(String)
  ...     id_utilisateur = Column(Integer, ForeignKey('personnes.identifiant'))
  ...     utilisateur = relationship('Personne')
  ...     def __init__(self, langage, utilisateur):
  ...         self.langage = langage
  ...         self.utilisateur = utilisateur
  ...
  >>> class Personne(basesqlite):
  ...     __tablename__ = 'personnes'
  ...     identifiant = Column(Integer, primary_key=True)
  ...     prénom = Column(String)
  ...     nom = Column(String)
  ...     langage = relationship(Programmeur, backref='users')
  ...     def __init__(self, prénom, nom, langage):
  ...         self.prénom = prénom
  ...         self.nom = nom
  ...
  >>> utilisateur = Personne('Stagiaire', 'DÉVELOPPEUR', 'Python')
  >>> programmeur = Programmeur('Python', utilisateur)


.. code-block:: sqlite3

  sqlite> .tables
  personnes    programmeur
  sqlite> PRAGMA table_info(pragrammeur);
  sqlite> .tables
  personnes    programmeur
  sqlite> PRAGMA table_info(programmeur);
  0|identifiant|INTEGER|1||1
  1|langage|VARCHAR|0||0
  2|id_utilisateur|INTEGER|0||0
  sqlite> PRAGMA foreign_key_list(programmeur);
  0|0|personnes|id_utilisateur|identifiant|NO ACTION|NO ACTION|NONE
  sqlite> PRAGMA table_info(personnes);
  0|identifiant|INTEGER|1||1
  1|prénom|VARCHAR|0||0
  2|nom|VARCHAR|0||0
  sqlite> PRAGMA foreign_key_list(personnes);
  sqlite> select * from personnes;
  1|Stagiaire|DÉVELOPPEUR
  sqlite> select * from programmeur;
  1|Python|1
