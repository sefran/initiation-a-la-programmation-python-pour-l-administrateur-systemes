.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

La visualisation de l’architecture objets
*****************************************

Le programme :terminal:`pylint`, que nous avons installé pour tester le code Python, est livré avec un outil de ligne de commande fort pratique :terminal:`pyreverse`. Celui-ci permet d'imager les classes Python et de créer des diagrammes `UML <https://www.ibm.com/docs/fr/rational-soft-arch/9.5?topic=diagrams-uml-models>`_ des classes Python.

Pyreverse
=========

**Pyreverse** permet de générer des diagrammes avec :

* des attributs de classes et si possible avec leurs types,
* des méthodes de classes avec leurs paramètres,
* la représentation des exceptions,
* les liens d'héritages de classes,
* et les liens d'association de classes.

Les options de la ligne de commande
-----------------------------------

.. list-table:: Options de la ligne de commande de Pyreverse
   :widths: 20 30 50
   :header-rows: 1
   :stub-columns: 2

   * - Option courte
     - Option verbeuse
     - Description
   * - :terminal:`-p <nom du projet>`
     - :terminal:`--project=<nom du projet>`
     - Nom du fichier en sortie
   * - :terminal:`-o <format>`
     - :terminal:`--output=<format>`
     - Format de sortie de fichier (**svg**, **svgz**, **png**, **jpeg**/**jpg**/**jpe**, **gif**, **ps**/**ps2**/**eps**, **pdf**, pic, pcl, hpgl, gd/gd2, fig, dia, **dot**/**xdot**, **plain**/**plain-ext**, vrml/vml/vmlz, tk, wbmp, xlib, etc.)
   * - :terminal:`-c <classe>`
     - :terminal:`--class <classe>`
     - Afficher la classe passée en paramètre
   * - :terminal:`-k`
     - :terminal:`--only-classnames`
     - Afficher seulement les noms des classes
   * - :terminal:`-m[y/n]`
     - :terminal:`--module-names=[y/n]`
     - Inclure le nom des modules pour la représentation des classes.
   * - :terminal:`-b`
     - :terminal:`--show-builtin`
     - Afficher les classes des objets natifs Python
   * - :terminal:`-a <niveau>`
     - :terminal:`--show-ancestors=<niveau>`
     - Aficher le niveaux d'héritage passé en paramètre
   * - :terminal:`-A`
     - :terminal:`--all-ancestors`
     - Afficher tout l'arbre d'héritage
   * - :terminal:`-s <niveau>`
     - :terminal:`--show-associated=<niveau>`
     - Liens des imports suivant le niveau d'imports
   * - :terminal:`-S`
     - :terminal:`--all-associated`
     - Toutes les liaisons d'imports
   * - :terminal:`-f <mode>`
     - :terminal:`--filter-mode=<mode>`
     - Filtre ce qu'il faut faire apparaître (par défaut PUB_ONLY) :

       * **PUB_ONLY** : Affiche les méthodes et les propriétés publiques.
       * **SPECIAL** : Affiche les attributs privés ou protégés en plus.
       * **OTHER** : Affiche le méthodes privées ou protégés en plus.
       * **ALL** : Affiche tout.


Utilisation en ligne de commande
--------------------------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ cd 9_objets
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -p test -o png scene.py

L'option :terminal:`-p test` ajoute le suffixe **test** au nom du fichier (qui par défaut est «**classes.ext**»).
L'option :terminal:`-o png` choisit le format de sortie du diagramme, ici une image `PNG <https://fr.wikipedia.org/wiki/Portable_Network_Graphics>`_.

Cela génère donc le fichier «**classes_test.png**».

Qui nous donne comme image :

.. image:: images/pyreverse_test1.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

Le diagramme UML contient le nom de la classe :python:`Decor` (cellule du haut) avec sa propriété :python:`fabriqueBatiments` (cellule du milieu) affectée à l'objet :python:`FabriqueBatiments`. Ceci nous permet de comprendre que cela est en fait un objet puisque c'est le type de la propriété. La case vide est celle des méthodes.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -c Decor -o png scene.py

L'option :terminal:`-c Decor` choisit la classe à générer comme diagramme UML, et va produire une sortie de nom de fichier avec le nom de la classe «**Decor.png**».

Ce qui va nous donner un diagramme plus intéressant :

.. image:: images/pyreverse_test2.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

Là nous voyons avec les **flêches** l'héritage des classes :python:`Vegetation`, :python:`Urbanisation`, :python:`Hydrotopologie`, :python:`Geomorphologie` et :python:`Atmosphere` qui elle même hérite de :python:`Nuages`.

Nous voyon aussi les propriétés et les méthodes (dernière cellule) de :python:`FabriqueBatiments`. Classe qui est bien affectée (en vert) à la propriété :python:`fabriqueBatiments` de la classe :python:`Decor`

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -k -c Decor -o png scene.py

L'option :terminal:`-k` permet de n'afficher que le nom des classes :

.. image:: images/pyreverse_test3.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%


.. code-block:: console

   utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -f ALL -S -c Decor -o png scene.py

L'option :terminal:`-f ALL` permet d'afficher les propriétés et les méthodes cachées :

.. image:: images/pyreverse_test4.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -a 1 -c Decor -o png scene.py

L'option :terminal:`-a 1` permet de limiter le niveau de liens en héritage par rapport à la classe :

.. image:: images/pyreverse_test5.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

En limitant au premier niveau :terminal:`-a 1` la classe :python:`Nuages` n'est plus affichée.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -b -c Decor -o png scene.py

L'option :terminal:`-b` permet d'afficher l'héritage des classes natives Python :

.. image:: images/pyreverse_test6.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

On voit alors bien apparaître la classe :python:`object` comme on l'attendait, mais aussi les classes :python:`builtin.list` et :python:`builtin.str`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -mn -c Decor -o png scene.py

L'option :terminal:`-mn` permet de supprimer le chemin d'importation des classes dans les intitulés de classes :

.. image:: images/pyreverse_test7.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ pyreverse -s 0 -c Decor -o png scene.py

L'option :terminal:`-s 0` permet de ne visualiser que les classes importées directement :

.. image:: images/pyreverse_test8.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

On voit bien que la classe :python:`FabriqueBatiments` ne s'affiche plus avec un niveau supplémentaire d'importation.

Vous pouvez maintenant générer tous les diagrammes de classes Python que vous voulez pour agrémenter les documentations de vos programmes.

Mise en œuvre dans GitLab
=========================

Avec un script
--------------

Nous allons maintenant générer les diagrammes UML pour «**Unittest.Calculatrice.py**».

Modifier le fichier «**makediagrammes**».

.. code-block:: bash

  #!/bin/bash

  echo -e "___________ Génère les Classes ___________"

  echo -e "=========== Supprime les anciens diagrammes de Classes ==========="
  rm -f docs/sources-documents/classes/*

  echo -e "+++++++++++ Génère les diagrammes UML des Classes +++++++++++"
  echo -e "Classes"
  pyreverse -mn -A -S -k -f PUB_ONLY -o png Unittest/Calculatrice.py
  echo -e "Génère le diagramme de Calculatrice"
  pyreverse -mn -A -S -f PUB_ONLY -o png -c Calculatrice Unittest/Calculatrice.py

  echo -e "+++++++++++ End Generate UML Classes +++++++++++"

  echo -e "=========== Move Classes to correct folder ==========="
  mv *.png docs/sources-documents/classes

  echo -e "___________ Classes Generated ___________"


Tester le script :

.. code-block:: console

  ___________ Génère les Classes ___________
  =========== Supprime les anciens diagrammes de Classes ===========
  +++++++++++ Génère les diagrammes UML des Classes +++++++++++
  Classes
  parsing Calculatrice.py...
  Génère le diagramme de Calculatrice
  parsing Calculatrice.py...
  +++++++++++ End Generate UML Classes +++++++++++
  =========== Move Classes to correct folder ===========
  ___________ Classes Generated ___________


Et nous retrouvons nos digrammes UML dans «**repertoire_de_developpement/docs/sources-documents/classes**» avec les noms de fichiers
«**classes.png**»

.. image:: images/pyreverse_script1.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

et «**Calculatrice.png**»

.. image:: images/pyreverse_script2.png
   :alt: pyreverse -p test -o png scene.py
   :align: center
   :scale: 100%

Modifier alors dans «**repertoire_de_developpement/docs/sources-documents**» le fichier «**index.rst**»

.. code-block:: rest

  .. |date| date::

  :Date: |date|
  :Revision: 1.0
  :Author: Prénom NOM <prénom.nom@fai.fr>
  :Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
  :Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

  .. toctree::
     :maxdepth: 2
     :caption: Contenu

  .. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst


  .. only:: html

    .. image:: ./badges/obsolescence.svg
       :alt: Obsolescence du code Python
       :align: left
       :width: 200px

    .. image:: ./badges/pylint.svg
       :alt: Cliquez pour voir le rapport
       :align: left
       :width: 200px
       :target: ./pylint/index.html

  .. image:: classes/classes.png
     :alt: UML des classes de calculatrice.py
     :align: left
     :width: 200px

  .. image:: classes/Calculatrice.png
     :alt: UML de Calculatrice
     :align: left
     :width: 200px


  ----

  Modules
  *******

  .. automodule:: Unittest.Calculatrice
    :members:

Générer la documentation.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ./makedocs

Mettre le résultat dans le dépot GitLab.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout diagrammes UML à la documentation"
  [master 2aac2d0] Ajout diagrammes UML à la documentation
   45 files changed, 1043 insertions(+), 2083 deletions(-)
   rewrite README.md (92%)
   rewrite docs/documentation/doctrees/environment.pickle (74%)
   rewrite docs/documentation/doctrees/index.doctree (90%)
   create mode 100644 docs/documentation/epub/_images/Calculatrice.png
   create mode 100644 docs/documentation/epub/_images/classes.png
   rewrite docs/documentation/epub/index.xhtml (85%)
   create mode 100644 docs/documentation/html/_images/Calculatrice.png
   create mode 100644 docs/documentation/html/_images/classes.png
   rewrite docs/documentation/html/searchindex.js (97%)
   create mode 100644 docs/documentation/latex/Calculatrice.png
   rewrite "docs/documentation/latex/InitiationProgrammationPythonPourAdministrateurSyst\303\250mes.idx" (100%)
   rewrite "docs/documentation/latex/InitiationProgrammationPythonPourAdministrateurSyst\303\250mes.pdf" (94%)
   rewrite "docs/documentation/latex/InitiationProgrammationPythonPourAdministrateurSyst\303\250mes.tex" (75%)
   create mode 100644 docs/documentation/latex/classes.png
   rewrite "docs/documentation/man/InitiationProgrammationPythonPourAdministrateurSyst\303\250mes.1" (77%)
   rewrite docs/documentation/markdown/index.md (92%)
   create mode 100644 "docs/documentation/texinfo/InitiationProgrammationPythonPourAdministrateurSyst\303\250mes-figures/Calculatrice.png"
   create mode 100644 "docs/documentation/texinfo/InitiationProgrammationPythonPourAdministrateurSyst\303\250mes-figures/classes.png"
   rewrite "docs/documentation/texinfo/InitiationProgrammationPythonPourAdministrateurSyst\303\250mes.info" (73%)
   rewrite "docs/documentation/texinfo/InitiationProgrammationPythonPourAdministrateurSyst\303\250mes.texi" (72%)
   rewrite docs/documentation/text/index.txt (93%)
   rewrite docs/documentation/xml/index.xml (89%)
   create mode 100644 docs/sources-documents/classes/Calculatrice.png
   create mode 100644 docs/sources-documents/classes/classes.png
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push
  Énumération des objets: 106, fait.
  Décompte des objets: 100% (105/105), fait.
  Compression par delta en utilisant jusqu'à 4 fils d'exécution
  Compression des objets: 100% (54/54), fait.
  Écriture des objets: 100% (55/55), 310.98 Kio | 2.96 Mio/s, fait.
  Total 55 (delta 40), réutilisés 0 (delta 0), réutilisés du pack 0
  To http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur.git
     42c3a24..2aac2d0  master -> master

Ce qui nous donne après traitement par GitLab pour le fichier «**README.md**»

.. image:: images/pyreverse_script3.png
   :alt: Rendu de la documentation avec les diagrammmes UML
   :align: center
   :scale: 100%

Et pour la page générée en HTML par GitLab «**http://utilisateur.documentation.domaine-perso.fr/initiation_developpement_python_pour_administrateur/**»

.. image:: images/pyreverse_script4.png
   :alt: Rendu de la documentation avec les diagrammmes UML
   :align: center
   :scale: 100%


Générer des diagrammes UML avec GitLab
--------------------------------------

Éditons le fichier «**.gitlab-ci.yml**», et modifions la section «**pages**».

.. code-block:: yaml

  image: python:latest

  stages:
    - build
    - Static Analysis
    - test
    - deploy

  construction-environnement:
    stage: build
    script:
      - echo "Bonjour $GITLAB_USER_LOGIN !"
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules Python"
      - pip install -U -r requirements.txt
    only:
      - master

  obsolescence-code:
    stage: Static Analysis
    allow_failure: true
    script:
      - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
      - python3 -Wd Unittest.Calculatrice.py 2> /tmp/output.txt
      - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
      - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

  qualité-du-code:
    stage: Static Analysis
    allow_failure: true
    before_script:
      - echo "Installation de Pylint"
      - pip install -U pylint-gitlab
    script:
      - echo "$GITLAB_USER_LOGIN test de la qualité du code"
      - pylint --output-format=text Unittest.Calculatrice.py | tee /tmp/pylint.txt
    after_script:
      - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > pylint.score
      - echo "Votre score de qualité de code Pylint est de $(cat pylint.score)"

  tests-unitaires:
    stage: test
    script:
      - echo "Lancement des tests Unittest"
      - python3 -m unittest

  pages:
    stage: deploy
    before_script:
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat docs-packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules Python"
      - pip install -U -r docs-requirements.txt
      - echo "Création de l’infrastructure pour l'obsolescence et la qualité de code"
      - mkdir -p public/obsolescence public/quality public/badges public/pylint public/classes
      - echo undefined > public/obsolescence/obsolescence.score
      - echo undefined > public/quality/pylint.score
      - pip install -U pylint-gitlab
    script:
      - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
      - python3 -Wd Unittest.Calculatrice.py 2> /tmp/output.txt
      - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
      - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
      - \[ -s /tmp/obsolescence.txt \] && echo oui > public/obsolescence/obsolescence.score || echo non > public/obsolescence/obsolescence.score
      - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
      - pylint --exit-zero --output-format=text Unittest.Calculatrice.py | tee /tmp/pylint.txt
      - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > public/quality/pylint.score
      - echo "Votre score de qualité de code Pylint est de $(cat public/quality/pylint.score)"
      - echo "Création du rapport HTML de qualité de code"
      - pylint --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter Unittest.Calculatrice.py > public/pylint/index.html
      - echo "Génération des diagrammes de classes"
      - cd Unittest public/classes
      - pyreverse -mn -A -S -k -f PUB_ONLY -o png ../../Calculatrice.py
      - pyreverse -mn -A -S -f PUB_ONLY -o png -c Calculatrice ../../Unittest/Calculatrice.py
      - cd ../..
      - echo "Création du logo SVG d'obsolescence de code"
      - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
      - echo "Création du logo SVG de qualité de code"
      - anybadge --overwrite --label "Qualité du code avec Pylint" --value=$(cat public/quality/pylint.score) --file=public/badges/pylint.svg 4=red 6=orange 8=yellow 10=green
      - echo "Génération de la documentation html"
      - sphinx-build -b html ./docs/sources-documents public
    artifacts:
      paths:
        - public
    only:
      - master

Et après déploiement du fichier dans GitLab et génération de la page de documentation.

Une fois fini, aller dans la tâche «**pages**».

.. image:: images/pyreverse_gitlab1.png
   :alt: Rendu de la documentation avec les diagrammmes UML
   :align: center
   :scale: 100%

«**Artefacts de la tâche**»

.. image:: images/pyreverse_gitlab2.png
   :alt: Rendu de la documentation avec les diagrammmes UML
   :align: center
   :scale: 100%

Cliquer sur le bouton «**Parcourir**»

naviguer dans l'arborescence «**public/classes**»

.. image:: images/pyreverse_gitlab3.png
   :alt: Rendu de la documentation avec les diagrammmes UML
   :align: center
   :scale: 100%
