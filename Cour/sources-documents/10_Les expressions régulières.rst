.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Les expressions régulières
##########################

C'est un outil très puissant qui permet de vérifier un contenu suivant une forme attendue. Par exemple si on récupère un numéro de téléphone, une adresse postale, une adresse courriel, une adresse ip, une adresse mac, etc. on s'attend à ce que le contenu soit formaté d'une certaine façon. Les expressions régulières permettent non seulement de vous avertir d'une mauvaise syntaxe, mais également de supprimer/modifier cette syntaxe.

On utilise des symboles qui ont une action:

.. code-block:: text

  . ^ $ * + ? { } [ ] \ | ( )

.. list-table:: Symboles de commandes d'expressions régulières
  :widths: 20 80
  :header-rows: 1

  * - Symbole
    - Description de l'action
  * - **^**
    - Indique un commencement de segment, signifie aussi "contraire de".
  * - **$**
    - Fin de segment.
  * - **.**
    - Le point correspond à n'importe quel caractère.
  * - **\\**
    - Est un caractère d'échappement
  * - **\\s**
    - Un espace, ce qui équivaut à [ \\t\\n\\r\\f\\v].
  * - **\\S**
    - Pas d'espace, ce qui équivaut à [^ \\t\\n\\r\\f\\v].
  * - **\\d**
    - le segment est composé uniquement de chiffre, ce qui équivaut à [0-9].
  * - **\\D**
    - le segment n'est pas composé de chiffre, ce qui équivaut à [^0-9].
  * - **\\w**
    - Présence alphanumérique, ce qui équivaut à [a-zA-Z0-9\_].
  * - **\\W**
    - Pas de présence alphanumérique [^a-zA-Z0-9\_].
  * - **[xy]**
    - Une liste de segment possibble. Exemple [abc] équivaut à : a, b ou c.
  * - **(x|y)**
    - Indique un choix multiple type (ps|top) équivaut à "ps" OU "top".
  * - **a{2}**
    - s'attend à ce que la lettre «a» se répète 2 fois consécutives.
  * - **ba{1,9}**
    - s'attend à ce que le segment «ba» se répète de 1 à 9 fois consécutives.
  * - **abc{,10}**
    - s'attend à ce que le segment «abc» ne soit pas présent du tout ou présent jusqu'à 10 fois consécutives.
  * - **ab{1,}**
    - s'attend à ce que le segment «ba» soit présent au mois une fois.


.. list-table:: Actions de catactères multiples
  :widths: 20 30 20 30
  :header-rows: 1

  * - Symbole
    - Nb Caractères attendus
    - Exemple
    - Cas possibles
  * - ?
    - 0 ou 1
    - (.)?NIX
    - NIX, U NIX etc.
  * - \+
    - 1 ou plus
    - (.)+NIX
    - U NIX, PHORO NIX, etc.
  * - \*
    - 0, 1 ou plus
    - (.)*NIX
    - NIX, U NIX , PHORO NIX, etc.


La bibliothèque re
******************

Créer un objet pour la recherche
================================

Si vous êtes amenés à utiliser plusieurs fois la même expression, vous pouvez la compiler pour gagner en performence. :python:`re.compile()` permet de créer un objet expression régulière.

Lancez votre interpréteur python et importez la bibliothèque :python:`re`.

.. code-block:: pycon

  >>> import re
  >>> recherche = re.compile(r"(.)?NIX")

On affecte l'objet avec une expression régulière :python:`re.compile()`, initialisé avec l'expression régulière :python:`r"(.)?NIX"`, à la variable Python :python:`recherche`.


Recherches
==========

Recherches sur des mots
-----------------------

:python:`match()` est une méthode de l'objet :python:`re.compile()` qui permet d'appliquer cette expression régulière sur une chaîne de caractères.

Testons avec une chaîne de caractères :

.. code-block:: pycon

  >>> recherche.match("UNIX")
  <re.Match object; span=(0, 4), match='UNIX'>

La méthode :python:`match()` recherche suivant l'expression régulière :python:`r"(.)?NIX"` s'il y a une occurrence dans la chaîne :python:`"UNIX"`.

Si la réponse est :python:`…, match='UNIX'` c'est que la chaîne a été trouvée.

.. code-block:: pycon

  >>> for mot in ["NIX", "UNIX", "LINUX", "PHORONIX", "WINDOWS", "MAC"]:
  ...     recherche.match(mot)
  ...
  <re.Match object; span=(0, 3), match='NIX'>
  <re.Match object; span=(0, 4), match='UNIX'>
  >>> recherche = re.compile(r"(.)+NIX")
  >>> for mot in ["NIX", "UNIX", "LINUX", "PHORONIX", "WINDOWS", "MAC"]:
  ...     recherche.match(mot)
  ...
  <re.Match object; span=(0, 4), match='UNIX'>
  <re.Match object; span=(0, 8), match='PHORONIX'>
  >>> recherche = re.compile(r"(.)*NIX")
  >>> for mot in ["NIX", "UNIX", "LINUX", "PHORONIX", "WINDOWS", "MAC"]:
  ...     recherche.match(mot)
  ...
  <re.Match object; span=(0, 3), match='NIX'>
  <re.Match object; span=(0, 4), match='UNIX'>
  <re.Match object; span=(0, 8), match='PHORONIX'>


Le but c'est d'anticiper si une expression est Vraie ou Fausse pour repérer le fonctionnement des expressions régulières.

.. code-block:: pycon

  >>> recherche = re.compile(r"L(.)?NUX")
  >>> bool(recherche.match("LINUX"))
  True

.. list-table:: Actions de caractères multiples
  :widths: 50 30 20
  :header-rows: 1

  * - EXPRESSION
    - CHAÎNE
    - SOLUTION
  * - L(.)?NUX
    - LINUX
    - Vrai
  * - L(.)?NUX
    - LNUX
    - Vrai
  * - LI(.)?NUX
    - LINUX
    - Vrai
  * - LIN(.)?
    - LINUX
    - Vrai
  * - L(.)?UX
    - LINUX
    - Faux
  * - L(I)?N(U)?X
    - LNX
    - Vrai
  * - L(.)+X
    - LINUX
    - Vrai
  * - L(.)+(U)+X
    - LINUX
    - Vrai
  * - L(.)+([a-z])+X
    - LINUX
    - Faux
  * - L(.)+([A-Z])+X
    - LINUX
    - Vrai
  * - ^!
    - LINUX!
    - Faux
  * - !$
    - LINUX!
    - Faux
  * - ^([A-Z])+$
    - LINUX!
    - Faux
  * - ^([A-Z!])+$
    - !LINUX!
    - Vrai
  * - ^!L(.)+!$
    - !LINUX!
    - Vrai
  * - ([0-9 ])
    - 01 23 45 67 89
    - Vrai
  * - ^0[0-9]([ .-/]?[0-9]{2}){4}
    - 01 23 45 67 89
    - Vrai
  * - ^0[0-9]([ .-/]?[0-9]{2}){4}
    - 01-23-45-67-89
    - Faux
  * - ^0[0-9]([ .\-/]?[0-9]{2}){4}
    - 01-23-45-67-89
    - Vrai
  * - ^0[0-9]([ .\-/]?[0-9]{2}){4}
    - 01 23.45-67/89
    - Vrai


Recherches dans une phrase
--------------------------

Le :python:`recherche.match()` est très intéressant pour valider l'intégrité d'un mot ou de numéros. Il est également possible de chercher des expressions spécifiques dans une chaîne de caractères.

.. code-block:: pycon

  >>> recherche.match("UNIX et mon LINUX sont dans les articles de PHORONIX.")
  <re.Match object; span=(0, 52), match='UNIX et mon LINUX sont dans les articles de PHORO>
  >>> recherche.match("UNIX et mon LINUX sont dans les articles de PHORONIX.").string
  'UNIX et mon LINUX sont dans les articles de PHORONIX.'
  >>> recherche = re.compile(r"(.)?NIX")
  >>> recherche.match("UNIX et mon LINUX sont dans les articles de PHORONIX.")
  <re.Match object; span=(0, 52), match='UNIX'>
  >>> recherche.match("Mon UNIX et mon LINUX sont dans les articles de PHORONIX.")

La recherche commence dès le début de la chaîne de caractères et ne trouve donc aucune occurrence dans notre dernier exemple.

Pour rechercher l'occurrence dans une chaîne de caractères il faut utiliser :python:`recherche.search()`.

.. code-block:: pycon

  >>> recherche = re.compile(r"(.)?NIX")
  >>> recherche.search("Mon UNIX et mon LINUX sont dans les articles de PHORONIX.")
  <re.Match object; span=(4, 8), match='UNIX'>

Pour rechercher toutes les occurrences c'est :python:`recherche.findall()`.

.. code-block:: pycon

  >>> recherche = re.compile(r"\b[A-Z]+NIX\b")
  >>> recherche.findall("Mon UNIX et mon LINUX sont dans les articles de PHORONIX.")
  ['UNIX', 'PHORONIX']

Il est également possible de chercher par groupe:

.. code-block:: pycon

  >>> recherche = re.search("Mon (?P<système1>\w+) et mon (?P<système2>\w+) sont dans les articles de (?P<revue>\w+).", "Mon UNIX et mon LINUX sont dans les articles de PHORONIX.")
  >>> if recherche:
  ...     print(recherche.group('système1'))
  ...     print(recherche.group('système2'))
  ...     print(recherche.group('revue'))
  ...
  UNIX
  LINUX
  PHORONIX


Remplacer une expression
========================

Pour remplacer une expression on utilise la méthode :python:`re.sub()`.

.. code-block:: pycon

  >>> re.sub(r"Mon (?P<système1>\w+) et mon (?P<système2>\w+) sont dans les articles de (?P<revue>\w+).", r"\g<système1> et \g<système2> ont des articles dans \g<revue>","Mon UNIX et mon LINUX sont dans les articles de PHORONIX.")
  'UNIX et LINUX ont des articles dans PHORONIX'

Le remplacement d'expressions se fait sur toutes les occurrences possibles:

.. code-block:: pycon

  >>> données = """
  ... Prénom1 Nom1 Age1;
  ... Prénom2 Nom2 Age2;
  ... Prénom2 Nom2 Age3;
  ... """
  >>> print(re.sub(r"(?P<prenom>\w+) (?P<nom>\w+) (?P<age>\w+);", r"\g<nom> \g<prenom> a \g<age>", données))

  Nom1 Prénom1 a Age1
  Nom2 Prénom2 a Age2
  Nom2 Prénom2 a Age3


Exercice de mise en pratique des expressions régulières
-------------------------------------------------------

**Créer une expression qui reconnaît une adresse courriel**

Lorsque vous commencez à rédiger une expression régulière, il ne faut pas être très ambitieux.

Toujours commencer petit, et coder étape par étape.

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf8 -*-

  import re

  mot = "TEST"
  expressionreg = r"(TEST)"

  if re.match(expressionreg, mot):
      print("Vrai")
      print(re.match(expressionreg, mot))
  else:
      print("Faux")

Si vous exécutez ce script, :terminal:`Vrai` et :terminal:`<re.Match object; span=(0, 4), match='TEST'>` seront affichés.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/11_ExpReg$ ./exemple1.py
  Vrai
  <re.Match object; span=(0, 4), match='TEST'>


Une adresse mail en gros ressemble à ça **nom.prénom@fai.fr**.

Commençons par le début, recherchons **nom.prénom@**, cela peut se traduire par :python:`^[a-z0-9._\-]+@`.

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf8 -*-

  import re

  courriel = "nom.prénom@fai.fr"
  expressionreg = r"^[a-z0-9._\-]+@"

  if re.match(expressionreg, courriel):
      print("Vrai")
      print(re.match(expressionreg, courriel))
  else:
      print("Faux")


.. code-block:: console

 utilisateur@MachineUbuntu:~/repertoire_de_developpement/11_ExpReg$ ./exemple2.py
 Faux

Le test est bon car la `RFC 822 <https://datatracker.ietf.org/doc/html/rfc822>`_ précise que les lettres accentuées ne sont pas autorisées pour les adresses courriel.

Modifier :python:`courriel = "nom.prenom@fai.fr"`

Si vous exécutez à nouveau ce script, :terminal:`Vrai` et :terminal:`<re.Match object; span=(0, 11), match='nom.prenom@'>` seront affichés. Nous avons donc en retour d'occurrence :terminal:`nom.prenom@`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/11_ExpReg$ ./exemple2.py
  Vrai
  <re.Match object; span=(0, 11), match='nom.prenom@'>

Continuons en ajoutant :python:`[a-z0-9._\-]+`.

.. code-block:: python

  courriel = "nom.prenom@fai.fr"
  expressionreg = r"^[a-z0-9._\-]+@[a-z0-9._\-]+"


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/11_ExpReg$ ./exemple3.py
  Vrai
  <re.Match object; span=(0, 17), match='nom.prenom@fai.fr'>

Continuons en ajoutant :python:`\.` pour ne pas avoir l'extension :python:`fr`.

.. code-block:: python

  expressionreg = r"^[a-z0-9._\-]+@[a-z0-9._\-]+\."


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/11_ExpReg$ ./exemple4.py
  Vrai
  <re.Match object; span=(0, 15), match='nom.prenom@fai.'>


Testons ce code avec :python:`courriel = "nom.prenom@fai.service.fr"`.

.. code-block:: python

  courriel = "nom.prenom@fai.service.fr"

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/11_ExpReg$ ./exemple4.py
  Vrai
  <re.Match object; span=(0, 23), match='nom.prenom@fai.service.'>

Et enfin ajoutons un choix d'extensions avec :python:`(fr|org|net|com)`.

.. code-block:: python

  expressionreg = r"^[a-z0-9._\-]+@[a-z0-9._\-]+\.(fr|org|net|com)"

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/11_ExpReg$ ./exemple5.py
  Vrai
  <re.Match object; span=(0, 25), match='nom.prenom@fai.service.fr'>

Et voila, le résultat devrait être bon.
