Les objets
##########

.. include:: 08_01_Objet et caractéristiques.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 08_02_Documenter les objets.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 08_03_Notions avancées en objet.rst

.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 08_04_La visualisation de l’architecture objets.rst
