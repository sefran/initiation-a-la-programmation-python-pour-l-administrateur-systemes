Mise en œuvre de la documentation avec Gitlab
*********************************************

Définition de la structure du document
======================================

Supposons que vous ayez exécuté **sphinx-quickstart**. Il a créé un répertoire source avec **conf.py** et un document maître, **index.rst**.

La fonction principale du `document maître <https://ugaugtjjyr2ylvegfxect62ccm--www-sphinx-doc-org.translate.goog/en/master/glossary.html#term-master-document>`_ (ou *toctree*) est de servir de page d'accueil et de contenir la racine de «**l'arborescence de la documentation du projet**». C'est l'une des principales choses que Sphinx ajoute à reStructuredText, un moyen de connecter plusieurs fichiers à une seule hiérarchie de documents.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ makedir docs/sources-documents/classes docs/sources-documents/cours

Modifiez **index.rst** :

.. code-block:: rest

  .. |date| date::

  :Date: |date|
  :Revision: 1.0
  :Author: Prénom NOM <prénom.nom@fai.fr>
  :Description: Documentation sur l'initiation à la programmation Python pour l'administrateur systèmes
  :Info: Voir <http://gitlab.domaine-perso.fr/utilisateur/initiation_developpement_python_pour_administrateur> pour la mise à jour de ce cours.

  .. toctree::
     :maxdepth: 2
     :caption: Contenu

  .. include:: cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst

  Modules
  *******

  .. automodule:: Documentation.mon_module
     :members:

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch docs/sources-documents/cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst

Modifiez **InitiationProgrammationPythonPourAdministrateurSystemes.rst** :

.. code-block:: rest

  .. Cours Initiation à la programmation Python pour l'administrateur systèmes.

  Initiation à la programmation Python pour l'administrateur systèmes
  ###################################################################

Générer la documentation avec un script
=======================================

Créer un fichier **makedocs** pour générer la documentation, et **makediagrammes** pour générer ultérieurement les diagrammes de Classes Python.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch makedocs makediagrammes

Modifier **makedocs** avec le contenu ci-dessous :

.. code-block:: bash

  #!/bin/bash

  titre=$(tput bold ; tput setaf 1 ; tput setab 53)
  section=$(tput bold ; tput setaf 3 ; tput setab 240)
  soussection=$(tput bold ; tput setaf 4 ; tput setab 0)
  ordinaire=$(tput sgr0)

  echo -e "$titre""Création de la documentation du projet""$ordinaire"

  echo -e "$section""Création des diagrammes de classes""$ordinaire"
  ( exec "./makediagrammes" )

  cd docs
  echo -e "$section""Création des fichiers de documentations""$ordinaire"
  echo -e "$soussection""Format html""$ordinaire"
  make html
  echo -e "$soussection""Format LaTeX""$ordinaire"
  make latex >/dev/null
  echo -e "$soussection""Format epub""$ordinaire"
  make epub
  echo -e "$soussection""Format pdf""$ordinaire"
  make latexpdf >/dev/null
  echo -e "$soussection""Format texte""$ordinaire"
  make text
  echo -e "$soussection""Format xml""$ordinaire"
  make xml
  echo -e "$soussection""Format markdown""$ordinaire"
  make markdown
  echo -e "$soussection""Création des pages de manuel""$ordinaire"
  make man
  echo -e "$soussection""Création des pages texinfo""$ordinaire"
  make texinfo
  echo -e "$soussection""Création des pages info""$ordinaire"
  make info
  echo -e "$soussection""Format ODT""$ordinaire"
  make odt
  pandoc ./documentation/html/index.html -o InitiationProgrammationPythonPourAdministrateurSystèmes.odt
  echo -e "$soussection""Format DOCX""$ordinaire"
  pandoc ./documentation/html/index.html -o InitiationProgrammationPythonPourAdministrateurSystèmes.docx

  echo -e "$section""Création du README.md du projet""$ordinaire"
  cp ./documentation/markdown/index.md ../README.md

  echo -e "$section""Changement du chemin des images dans README.md""$ordinaire"
  cd..
  sed -i 's/classes\//docs\/sources-documents\/classes\//g'\ README.md
  sed -i 's/images\//docs\/sources-documents\/images\//g'\ README.md

Modifier **makediagrammes** avec le contenu ci-dessous :

.. code-block:: bash

  #!/bin/bash

Rendre exécutable.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ chmod u+x makedocs makediagrammes

Créez les documents de la documentation :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ ./makedocs

Sauvegarder les documents dans le dépôt git et dans Gitlab :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Génération par un script de la documentation du projet"
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push

.. image:: images/documentation_14.png
   :alt: Doc README.md GitLab
   :align: center
   :scale: 100%


Générer la documentation avec Gitlab
====================================

Créer un fichier vide **requirements.txt** pour préparer à l’installation des modules Python et **packages.txt** pour installer les applications utiles dans l’environnement de test Python. Créer aussi des fichiers vides **docs-requirements.txt** et **docs-packages.txt** pour la construction de la documentation.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ touch requirements.txt packages.txt docs-requirements.txt docs-packages.txt

Génération de la documentation dans Gitlab
------------------------------------------

Modifier **.gitlab-ci.yml** :

Remarque : il faut impérativement saisir «**pages**» comme section

.. code-block:: yaml

  image: python:latest

  stages:
      - deploy

  pages:
      stage: deploy
      script:
          - echo "$GITLAB_USER_LOGIN déploiement de la documentation"
          - echo "** Mises à jour et installation des applications supplémentaires **"
          - echo "Mises à jour système"
          - apt -y update
          - apt -y upgrade
          - echo "Installation des applications supplémentaires"
          - cat docs-packages.txt | xargs apt -y install
          - echo "Mise à jour de PIP"
          - pip install --upgrade pip
          - echo "Installation des dépendances de modules python"
          - pip install -U -r docs-requirements.txt
          - echo "** Génération des diagrammes de classes **"
          - ./makediagrammes
          - echo "** Génération de la documentation HTML **"
          - sphinx-build -b html ./docs/sources-document public
      artifacts:
          paths:
              - public
      only:
          - master

Modifier **docs-packages.txt**:

.. code-block:: text

  dnsutils
  sphinx-intl
  graphviz
  cairosvg

Modifier **docs-requirements.txt**:

.. code-block:: text

  sphinx
  sphinx-intl
  sphinxcontrib-inlinesyntaxhighlight
  sphinx-copybutton
  sphinx-tabs
  sphinx-markdown-builder
  sphinx-book-theme
  sphinxcontrib-svg2pdfconverter
  sphinxcontrib-svg2pdfconverter[CairoSVG]
  pygments-ldif

Modifier **conf.py** pour supprimer les extensions de sortie inutiles:

.. code-block:: python

  extensions = [
      'sphinx.ext.intersphinx',
      'sphinx.ext.extlinks',
      'sphinx.ext.autodoc',
      'sphinx.ext.githubpages',
      'sphinx.ext.graphviz',
      'sphinxcontrib.cairosvgconverter',
      'sphinx.ext.inheritance_diagram',
      'sphinx_copybutton',
      'sphinx.ext.tabs',
      'sphinx.ext.todo',
      'sphinx.ext.ifconfig',
      'sphinx.ext.doctest',
      'sphinx_markdown_builder',
      'sphinxcontrib-odfbuilder',
  ]


Modifiez **cours/InitiationProgrammationPythonPourAdministrateurSystemes.rst** :

.. code-block:: rest

  .. Cours Initiation à la programmation Python pour l'administrateur systèmes.

  `Initiation à la programmation Python pour l'administrateur systèmes <http://utilisateur.documentation.domaine-perso.fr/initiation_developpement_python_pour_administrateur/>`_
  ###############################################################################################################################################################################

.. image:: images/documentation_15.png
   :alt: Pipeline
   :align: center
   :scale: 100%

.. image:: images/documentation_16.png
   :alt: Tâches pages OK
   :align: center
   :scale: 100%

.. image:: images/documentation_17.png
   :alt: Logs tâche Pages
   :align: center
   :scale: 100%

.. image:: images/documentation_18.png
   :alt: Menu Pages
   :align: center
   :scale: 100%

.. image:: images/documentation_19.png
   :alt: Lien vers la documentation dans pages
   :align: center
   :scale: 100%

.. image:: images/documentation_20.png
   :alt: La documentation en ligne
   :align: center
   :scale: 100%
