.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Tests unitaire Python
#####################

Après avoir testé la qualité de rédaction du code Python suivant les standards, nous allons maintenant tester le comportement du programme tel qu’attendu par le programmeur.

Le module Unittest
******************

Les tests unitaires permettent de vérifier le comportement logiciel des éléments spécifiques d’un programme.

Par exemple ils permettent de vérifier le fonctionnement de méthodes, d’objets, de fonctions. La mise en place des tests unitaires est aussi utile pour s’assurer que la correction de dysfonctionnements logiciels n’entraînera pas de régressions dans la code ailleurs.

**Unittest** est disponible nativement dans Python, et il est basé sur le modèle de framework Xunit imaginé par Kent Beck et Erich Gamma.

Créer le répertoire «**repertoire_de_developpement/Unittest**».

Puis dans ce répertoire créer le fichier «**Calculatrice.py**» :

.. code-block:: python

  class Calculatrice:
      """ Fait des opérations entre deux valeurs """
      def __init__(self):
          """ Initialisation de la classe """
          self.efface()

      def valeur1(self, première_valeur):
          """ Affecte la première valeur """
          self.__a = première_valeur

      def valeur2(self, deuxième_valeur):
          """ Affecte la deuxième valeur """
          self.__b = deuxième_valeur

      def efface(self):
          """\ Efface les valeurs\ """
          self.__a = None
          self.__b = None

      def ajoute(self):
          """\ Ajoute les valeurs\ """
          return self.__a + self.__b

      def divise(self):
          """\ Divise les valeurs\ """
          return self.__a / self.__b

  if __name__ == '__main__':
      calcule = Calculatrice()
      calcule.valeur1(4)
      calcule.valeur2(2)
      print("Valeur des opérandes : 4 et 2")
      print("Addition : ", calcule.ajoute())
      print('Division : ', calcule.divise())

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/Unittest$ python3 Calculatrice.py
  Valeur des opérandes : 4 et 2
  Addition : 6
  Division : 2.0

Exemple de test unitaires :

Créer le fichier **Calculatrice_test.py** :

.. code-block:: python

  # -*- coding: utf-8 -*-
  import unittest
  from Calculatrice import Calculatrice

  class Calculatricetest(unittest.TestCase):
      """ Tests de la classe Calculatrice """
      def test_simple_ajoute(self):
          """ Tests sur la somme """
          print("\n Début du test de la somme")
          self.objet = Calculatrice()
          self.objet.valeur1(2)
          self.objet.valeur2(3)
          self.assertAlmostEqual(self.objet.ajoute(), 5)

      def test_simple_divise(self):
          """ Tests sur la division """
          print("\n Début du test de la division")
          self.objet = Calculatrice()
          self.objet.valeur1(10)
          self.objet.valeur2(2)
          self.assertAlmostEqual(self.objet.divise(), 5)

  if __name__ == '__main__':
      unittest.main()

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement//Unittest$ python3 Calculatrice_test.py
  Début du test de la somme
  .
  Début du test de la division
  .
  ----------------------------------------------------------------------
  Ran 2 tests in 0.000s

  OK

Les valeurs de retour des tests
===============================

Il y a trois valeurs de retour pour un test :

-  **OK** : le test s’est déroulé correctement.
-  **F** : (Fail) le test a échoué.
-  **E** : (Error) une erreur est présente dans le code.

Tests réussits
--------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/Unittest$ python3 Calculatrice_test.py
  Début du test de la somme
  .
  Début du test de la division
  .
  ----------------------------------------------------------------------
  Ran 2 tests in 0.000s

  OK

Modification engendrant un échec
--------------------------------

Modifiez dans Calculatrice.py

.. code-block:: python

   def ajoute(self):
       """ Ajoute les valeurs """
       return self.__a + self.__b + 1

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/Unittest$ python3 Calculatrice_test.py
  Début du test de la somme
  F
  Début du test de la division
  .
  ======================================================================
  FAIL: test_simple_ajoute (__main__.Calculatricetest)
  ----------------------------------------------------------------------
  Traceback (most recent call last):
  File "/home/utilisateur/Calculatrice_test.py", line 7, in test_simple_ajoute
   self.assertAlmostEqual(self.objet.ajoute(), 5)
  AssertionError: 6 != 5 within 7 places (1 difference)

  ----------------------------------------------------------------------
  Ran 2 tests in 0.000s

  FAILED (failures=1)

Modification engendrant une erreur
----------------------------------

Modifiez dans Calculatrice_test.py

.. code-block:: python

  class Calculatricetest(unittest.TestCase):
      """ Tests de la classe Calculatrice """
      def test_simple_ajoute(self):
          """ Tests sur la somme """
          self.objet = Calculatrice(2, 3)
          self.objet.valeur1(2)
          self.objet.valeur2(3)
          self.assertAlmostEqual(self.objet.ajoute(), 5)

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/Unittest$ python3 Calculatrice_test.py
  Début du test de la somme
  E
  Début du test de la division
  .
  ======================================================================
  ERROR: test_simple_ajoute (__main__.Calculatricetest)
  ----------------------------------------------------------------------
  Traceback (most recent call last):
  File "/home/utilisateur/Calculatrice_test.py", line 8, in test_simple_ajoute
   self.objet = Calculatrice(2, 3)
  TypeError: \__init__() takes 1 positional argument but 3 were given
  ----------------------------------------------------------------------
  Ran 2 tests in 0.000s

  FAILED (errors=1)

Exécution de l’ensemble de tests
================================

Pour les programmes de nos projets nous pouvons avoir un nombre conséquent de fichiers de tests. On va donc tenter d’appeler ces tests à la façon d’un batch. C’est à dire de les exécuter automatiquement lorsqu’ils sont découverts (Test Discovery) dans le répertoire courant du projet ou dans ses sous-répertoires.

Testons avec le code actuel ce mode :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m unittest

  ----------------------------------------------------------------------
  Ran 0 tests in 0.000s

  OK

Cela ne fonctionne pas !

Pour que ce mode fonctionne il faut impérativement nommer les fichiers de test en commençant par le mot «**test**».

Il faut donc renommer notre fichier **Calculatrice_test.py** en **test_Calculatrice.py**.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m unittest
  Début du test de la somme
  .
  Début du test de la division
  .
  ----------------------------------------------------------------------
  Ran 2 tests in 0.000s

  OK

Présentation et architecture des tests 
======================================

Pour un gros projet, ou pour une instance possédant les mêmes valeurs, il peut-être intéressant de factoriser le code de tests des méthodes. Les tests peuvent être nombreux, et la mise en place du code des méthodes peut être répétitif dans la classe de tests.

Heureusement, nous pouvons le prendre en compte dans le code de configuration en implémentant une méthode appelée `setUp() <https://uyse4yahxpznxcqghpyn5sgqw4--docs-python-org.translate.goog/3/library/unittest.html#unittest.TestCase.setUp>`_. Le framework de test l’appellera automatiquement pour chaque méthode de tests que nous exécutons. Si la méthode :python:`setUp()` lève une exception pendant l'exécution d’une méthode de tests de la classe, le framework considérera l’exécution de la méthode comme ayant subi une erreur. La méthode de tests ne sera alors pas exécutée.

De même, nous pouvons fournir une méthode `tearDown() <https://uyse4yahxpznxcqghpyn5sgqw4--docs-python-org.translate.goog/3/library/unittest.html#unittest.TestCase.tearDown>`_ qui s’exécutera à la fin de la méthode de test appelée :

Résumé des méthodes :

-  La méthode :python:`setUp()`. Elle est appelée pour réaliser la mise en place du test des méthodes. Elle est exécutée immédiatement avant l'appel d’une méthode de la classe de tests.
-  La méthode :python:`tearDown()`. Elle est appelée immédiatement après l'appel d’une méthode de test et de l'enregistrement de son résultat. Elle est appelée même si la méthode de test a levé une exception. De fait, l'implémentation d'un sous-classes doit être fait avec précaution si vous vérifiez l'état interne de la classe. Cette méthode est appelée uniquement si l'exécution de :python:`setUp()` est réussie quel que soit le résultat de la méthode de test. L'implémentation par défaut ne fait rien.

Factorisons l’initialisation de l’objet Calculatrice dans nos méthodes de tests.

Modifier le fichier «**test_Calculatrice.py**» :

.. code-block:: python

  # -*- coding: utf-8 -*-*

  import unittest
  from Calculatrice import Calculatrice

  class Calculatricetest(unittest.TestCase):
      """ Tests de la classe Calculatrice """
      def setUp (self):
          """ Traitements de début d’exécution """
          print('\nClasse Calculatrice')
          self.objet = Calculatrice()

      def tearDown(self):
          """ Traitements de fin d’exécution """
          self.objet.efface()
          print('\nFin du test')

      def test_simple_ajoute(self):
          """ Tests sur la somme """
          print('\nTest de la somme')
          self.objet.valeur1(2)
          self.objet.valeur2(3)
          self.assertAlmostEqual(self.objet.ajoute(), 5)

      def test_simple_divise(self):
          """ Tests sur la division """
          print('\nTest de la division')
          self.objet.valeur1(10)
          self.objet.valeur2(2)
          self.assertAlmostEqual(self.objet.divise(), 5)

  if __name__ == '__main__':
      unittest.main()

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ python3 -m unittest
  Classe Calculatrice
  Test de la somme
  Fin du test
  .
  Classe Calculatrice
  Test de la division
  Fin du test
  .
  ----------------------------------------------------------------------
  Ran 2 tests in 0.000s

  OK

Il est plus maintenable de séparer les tests du code source, pour cela nous pouvons placer les fichiers de tests dans un répertoire tests.

Pour que celui-ci soit accessible pour la découverte des tests, n’oubliez pas d’y ajouter un fichier vide **\__init__.py**

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ tree
  .
  ├── Unittest
  │   └── Calculatrice.py
  └── tests
      ├── Calculatrice
      │   ├── \__init__.py
      │   └── test_Calculatrice.py
      └── \__init__.py
  2 directories, 4 files
  
Et modifier l'import de la classe calculatrice :

.. code-block:: python

  # -*- coding: utf-8 -*-*

  import unittest
  from Unittest.Calculatrice import Calculatrice

  
Les différents tests de Unittest
================================

.. list-table:: tests Unittest
   :widths: 50 20 30

   * - `assertEqual(a, b) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertEqual>`_
     - a == b
     - Teste l’égalité entre la valeur a et b
   * - `assertNotEqual(a, b) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotEqual>`_
     - a != b
     - Vérifie que a et b sont différents
   * - `assertTrue(x) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertTrue>`_
     - bool(x) is True
     - Vérifie que x est vrai
   * - `assertFalse(x) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertFalse>`_
     - bool(x) is False
     - Vérifie que x est faux
   * - `assertIs(a, b) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIs>`_
     - a is b
     - Vérifie que a et b sont équivalents
   * - `assertIsNot(a, b) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIsNot>`_
     - a is not b
     - Vérifie que a et b ne sont pas équivalents
   * - `assertIsNone(x) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIsNone>`_
     - x is None
     - Vérifie que la valeur de x est None
   * - `assertIsNotNone(x) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIsNotNone>`_
     - x is not None
     - Vérifie que la valeur de x n’est pas None
   * - `assertIn(a, b) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIn>`_
     - a in b
     - Vérifie que a est dans b
   * - `assertNotIn(a, b) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotIn>`_
     - a not in b
     - Vérifie que a n’est pas dans b
   * - `assertIsInstance(a, b) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertIsInstance>`_
     - isinstance(a, b)
     - Vérifie que a est une instance de b
   * - `assertNotIsInstance(a, b) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotIsInstance>`_
     - not isinstance(a, b)
     - Vérifie que a n’est pas une instance de
   * - `assertRaises(exeption) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotIsInstance>`_
     - exception
     - Vérifie que l’exception est levée
   * - `assertWarns(warning) <https://docs.python.org/fr/3.8/library/unittest.html#unittest.TestCase.assertNotIsInstance>`_
     - warning
     - Vérifie que l’avertissement est actif

Plus d’informations dans https://docs.python.org/fr/3.8/library/unittest.html

Mise en œuvre avec Gitlab
*************************

Modifier le fichier **repertoire_de_developpement/docs/sources-documents/index.rst**.

.. code-block:: rest

  Modules
  *******
  
  .. automodule:: Unittest.Calculatrice
     :members:
  
  
Modifier le fichier **.gitlab-ci.yml**.

.. code-block:: yaml

  image: python:latest

  stages :
      - build
      - Static Analysis
      - test
      - deploy

  construction-environnement:
      stage : build
      script :
          - echo "Bonjour $GITLAB_USER_LOGIN !"
          - echo "** Mises à jour et installation des applications supplémentaires **"
          - echo "Mises à jour système"
          - apt -y update
          - apt -y upgrade
          - echo "Installation des applications supplémentaires"
          - cat packages.txt | xargs apt -y install
          - echo "Mise à jour de PIP"
          - pip install --upgrade pip
          - echo "Installation des dépendances de modules python"
          - pip install -U -r requirements.txt
      only:
          - master

  obsolescence-code:
    stage: Static Analysis
    allow_failure: true
    script:
      - echo "$GITLAB_USER_LOGIN test de l'obsolescence du code"
      - python3 -Wd Unittest/Calculatrice.py 2> /tmp/output.txt
      - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
      - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"

  qualité-du-code:
    stage: Static Analysis
    allow_failure: true
    before_script:
      - echo "Installation de Pylint"
      - pip install -U pylint-gitlab
    script:
      - echo "$GITLAB_USER_LOGIN test de la qualité du code"
      - pylint --output-format=text Unittest/Calculatrice.py | tee /tmp/pylint.txt
    after_script:
      - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > pylint.score
      - echo "Votre score de qualité de code Pylint est de $(cat pylint.score)"

  tests-unitaires:
    stage: test
    script:
      - echo "Lancement des tests Unittest"
      - python3 -m unittest

  pages:
    stage: deploy
    before_script:
      - echo "** Mises à jour et installation des applications supplémentaires **"
      - echo "Mises à jour système"
      - apt -y update
      - apt -y upgrade
      - echo "Installation des applications supplémentaires"
      - cat docs-packages.txt | xargs apt -y install
      - echo "Mise à jour de PIP"
      - pip install --upgrade pip
      - echo "Installation des dépendances de modules python"
      - pip install -U -r docs-requirements.txt
      - echo "Création de l’infrastructure pour l'obsolescence et la qualité de code"
      - mkdir -p public/obsolescence public/quality public/badges public/pylint
      - echo undefined > public/obsolescence/obsolescence.score
      - echo undefined > public/quality/pylint.score
      - pip install -U pylint-gitlab
    script:
      - echo "** $GITLAB_USER_LOGIN déploiement de la documentation **"
      - python3 -Wd Unittest/Calculatrice.py 2> /tmp/output.txt
      - sed -n '/DeprecationWarning:/p' /tmp/output.txt > /tmp/obsolescence.txt
      - \[ -s /tmp/obsolescence.txt \] && cat /tmp/obsolescence.txt || echo "Pas d'obsolescences"
      - \[ -s /tmp/obsolescence.txt \] && echo oui > public/obsolescence/obsolescence.score || echo non > public/obsolescence/obsolescence.score
      - echo "Obsolescence $(cat public/obsolescence/obsolescence.score)"
      - pylint --exit-zero --output-format=text Unittest/Calculatrice.py | tee /tmp/pylint.txt
      - sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' /tmp/pylint.txt > public/quality/pylint.score
      - echo "Votre score de qualité de code Pylint est de $(cat public/quality/pylint.score)"
      - echo "Création du rapport HTML de qualité de code"
      - pylint --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter Unittest/Calculatrice.py > public/pylint/index.html
      - echo "Génération des diagrammes de classes"
      - ./makediagrammes
      - echo "Création du logo SVG d'obsolescence de code"
      - anybadge --overwrite --label "Obsolescence du code" --value=$(cat public/obsolescence/obsolescence.score) --file=public/badges/obsolescence.svg oui=red non=green
      - echo "Création du logo SVG de qualité de code"
      - anybadge --overwrite --label "Qualité du code avec Pylint" --value=$(cat public/quality/pylint.score) --file=public/badges/pylint.svg 4=red 6=orange 8=yellow 10=green
      - echo "Génération de la documentation HTML"
      - sphinx-build -b html ./docs/sources-document public
    artifacts:
      paths:
        - public
    only:
      - master


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Configuration Python des tests avec Gitlab"
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git push

.. image:: images/TestsUnitaires_1.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/TestsUnitaires_2.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/TestsUnitaires_3.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/TestsUnitaires_4.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/TestsUnitaires_5.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/TestsUnitaires_6.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/TestsUnitaires_7.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%

.. image:: images/TestsUnitaires_8.png
   :alt: Tâche pages en cours
   :align: center
   :scale: 100%
