.. role:: python(code)
  :language: python

Les fonctions intégrées
***********************

Lexique pédagogique à fournir «Les fonctions de base» :

Détermination du type d'une variable
====================================

.. code-block:: python

  type()

Conversion de types
===================

.. code-block:: python

  bool()

Convertit en booléen : :python:`"0"`, :python:`""` et :python:`"None"` donnent :python:`"False"` et le reste :python:`"True"`.

.. code-block:: python

  int()

Permet de modifier une variable en entier. Provoque une erreur si cela n'est pas possible.

.. code-block:: python

  str()

Permet de transformer la plupart des variables d'un autre type en chaînes de caractère.

.. code-block:: python

  float()

Permet la transformation en flottant.

.. code-block:: python

  repr()

Similaire à "str". Voir la partie sur les objets.

.. code-block:: python

  eval()

Évalue le contenu de son argument comme si c'était du code Python.

.. code-block:: python

  long() # Python 2

Transforme une valeur en long.


.. only:: latex

  .. raw:: latex

    \newpage


Voir les propriétés des fonctions
=================================

Fonction d'aide sur les fonctions Python
----------------------------------------

.. code-block:: python

  help()

Fonction de visualisation des propriétés et méthodes des fonctions Python
-------------------------------------------------------------------------

.. code-block:: python

  dir()

La fonction interne dir() est utilisée pour trouver quels noms sont définis par un module. Elle donne une liste de chaînes classées par ordre lexicographique :

.. code-block:: pycon

  >>> import math, sys
  >>> dir(math)
  ['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'comb', 'copysign', 'cos', 'cosh', 'degrees', 'dist', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'isqrt', 'lcm', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'nextafter', 'perm', 'pi', 'pow', 'prod', 'radians', 'remainder', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc', 'ulp']

Sans paramètre, **dir()** liste les noms actuellement définis :

.. code-block:: pycon

  >>> a = [1, 2, 3, 4, 5]
  >>> import math
  >>> cos = math.cos
  >>> dir()
  ['__builtins__', '__doc__', '__loader__', '__name__', '__package__', '__spec__', 'a', 'b', 'cos', 'cubes', 'fruits', 'help', 'i', 'lettres', 'math', 'n', 'sys', 'téléphone', 'x']

Notez qu'elle liste tous les types de noms : les variables, fonctions, modules, etc.

**dir()** ne liste ni les fonctions primitives, ni les variables internes. Si vous voulez les lister, elles sont définies dans le module **builtins** :

.. only:: latex

  .. code-block:: pycon

    >>> import builtins
    >>> dir(builtins)
    ['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BlockingIOError', 'BrokenPipeError', 'BufferError', 'BytesWarning', 'ChildProcessError', 'ConnectionAbortedError', 'ConnectionError', 'ConnectionRefusedError', 'ConnectionResetError', 'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False', 'FileExistsError', 'FileNotFoundError',
    'FloatingPointError', 'FutureWarning', 'GeneratorExit',   'IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'InterruptedError', 'IsADirectoryError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'ModuleNotFoundError', 'NameError', 'None', 'NotADirectoryError', 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'PermissionError', 'ProcessLookupError', 'RecursionError', 'ReferenceError', 'ResourceWarning', 'RuntimeError', 'RuntimeWarning', 'StopAsyncIteration', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TimeoutError', 'True', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'ZeroDivisionError', '_', '__build_class__', '__debug__', '__doc__', '__import\__', '__loader__', '__name__', '__package__', '__spec__', 'abs', 'all', 'any', 'ascii', 'bin', 'bool', 'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'divmod', 'enumerate', 'eval', 'exec', 'exit', 'filter', 'float', 'format', 'frozenset', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'quit', 'range', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'vars', 'zip']

.. only:: not latex

  .. code-block:: pycon

    >>> import builtins
    >>> dir(builtins)
    ['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BlockingIOError', 'BrokenPipeError', 'BufferError', 'BytesWarning', 'ChildProcessError', 'ConnectionAbortedError', 'ConnectionError', 'ConnectionRefusedError', 'ConnectionResetError', 'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False', 'FileExistsError', 'FileNotFoundError', 'FloatingPointError', 'FutureWarning', 'GeneratorExit',   'IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'InterruptedError', 'IsADirectoryError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'ModuleNotFoundError', 'NameError', 'None', 'NotADirectoryError', 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'PermissionError', 'ProcessLookupError', 'RecursionError', 'ReferenceError', 'ResourceWarning', 'RuntimeError', 'RuntimeWarning', 'StopAsyncIteration', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TimeoutError', 'True', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'ZeroDivisionError', '_', '__build_class__', '__debug__', '__doc__', '__import\__', '__loader__', '__name__', '__package__', '__spec__', 'abs', 'all', 'any', 'ascii', 'bin', 'bool', 'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'divmod', 'enumerate', 'eval', 'exec', 'exit', 'filter', 'float', 'format', 'frozenset', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'quit', 'range', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'vars', 'zip']

L'identification des objets
---------------------------

.. code-block:: python

  id()

.. code-block:: pycon

  >>> id(cos)
  140293507063376


.. only:: latex

  .. raw:: latex

    \newpage


Zoom sur fonctions et propriétés
================================

Range
-----

Si vous devez itérer sur une suite de nombres, la fonction native **range()** est faite pour cela. Elle génère des suites arithmétiques :

.. code-block:: pycon

  >>> for i in range(5):
  ... print(i)
  ...
  ...
  0
  1
  2
  3
  4

Le dernier élément fourni en paramètre ne fait jamais partie de la liste générée ; **range(10)** génère une liste de 10 valeurs, dont les valeurs
vont de **0 à 9**. Il est possible de spécifier une valeur de début et une valeur d'incrément différentes (y compris négative pour cette dernière, que l'on appelle également parfois le 'pas') :

.. code-block:: pycon

  >>> tuple(range(5, 10))
  (5, 6, 7, 8, 9)
  >>> tuple(range(0, 10, 3))
  (0, 3, 6, 9)
  >>> tuple(range(-10, -100, -30))
  (-10, -40, -70)

Une chose étrange se produit lorsqu'on affiche un range :

.. code-block:: pycon

  >>> print(range(10))
  range(0, 10)

L'objet renvoyé par **range()** se comporte presque comme une liste, mais ce n'en est pas une. Cet objet génère les éléments de la séquence au fur et à mesure de l'itération, sans réellement produire la liste en tant que telle, économisant ainsi de l'espace.

On appelle de tels objets des **iterable**, c'est-à-dire des objets qui conviennent à des fonctions ou constructions qui s'attendent à quelque chose duquel ils peuvent tirer des éléments, successivement, jusqu'à épuisement. Nous avons vu que l'instruction **for** est une de ces constructions, et un exemple de fonction qui prend un itérable en paramètre est sum() :

.. code-block:: pycon

  >>> sum(range(4))  # 0 + 1 + 2 + 3
  6

Plus loin nous voyons d'autres fonctions qui donnent des itérables ou en prennent en paramètre. Si vous vous demandez comment obtenir une liste à partir d'un range, voilà la solution :

.. code-block:: pycon

  >>> list(range(4))
  [0, 1, 2, 3]


.. only:: latex

  .. raw:: latex

    \newpage


Chaînes de caractères
---------------------

split() : sépare une chaîne en liste
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Fractionne une chaîne de caractères Python suivant un délimiteur. Si le paramètre du nombre de divisions est spécifié split() retourne seulement dans la liste les premiers élément fractionnés suivant la quantité demandée.

**Syntaxe :**

.. code-block:: python

  str.split(str="", num=string.count(str))

**Paramètres :**

-   str : séparateur, espaces par défaut.
-   num : le nombre de divisions.

**Valeur de retour :**

  Renvoie une liste de chaînes après division.

Exemples

L'exemple suivant montre la distribution de **split()** :

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  chaine = "Ligne1-abcdef \\nLigne2-abc \\nLigne3-abcd"
  print(chaine.split())
  print(chaine.split(' ', 1))

Exemple du résultat de sortie ci-dessus :

.. code-block:: text

  ['Ligne1-abcdef', 'Ligne2-abc', 'Ligne3-abcd']
  ['Ligne1-abcdef', '\nLigne2-abc \\nLigne3-abcd']

join() : Concatène une liste de caractères
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Transforme une liste en chaîne avec le séparateur en préfixe (:python:`"".join(MaListe)`).

**Syntaxe :**

.. code-block:: python

  string.join(iterable)

**Paramètres :**

La méthode join() prend un seul paramètre.

-  iterable(Obligatoire) : Tout objet itérable où toutes les valeurs renvoyées sont des chaînes

**Valeur de retour :**

  La méthode join() renvoie une chaîne créée en joignant les éléments d’un itérable par un séparateur.

Exemple

Joindre tous les éléments d’un tuple dans une chaîne, en utilisant le caractère «\|» comme séparateur :

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  tupl = ("Python", "Rust", "Julia")
  résultat = "|".join(tupl)
  print(résultat)

Sortie :

.. code-block:: text

  Python|Rust|Julia


Joignez tous les éléments d’un dictionnaire dans une chaîne, en utilisant le caractère «#» comme séparateur :

.. code-block:: python

  #!/usr/bin/env python3
  # -*- coding: utf-8 -*-

  dict = {"nom": "Alexandre", "age": "25"}
  résultat = "#".join(dict)
  print(résultat)
  résultat = "#".join(dict.values())
  print(résultat)

Sortie :

.. code-block:: text

  nom#age
  Alexandre#25


.. only:: latex

  .. raw:: latex

    \newpage
