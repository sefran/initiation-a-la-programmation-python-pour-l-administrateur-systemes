Installer GitLab
================

GitLab est un gestionnaire de référentiels open source basé sur Rails (langage Rubis) développé par la société GitLab. Il s'agit d'un gestionnaire de révisions de code WEB basé sur git qui permet à votre équipe de collaborer sur le codage, le test et le déploiement d'applications. GitLab fournit plusieurs fonctionnalités, notamment les wikis, le suivi des problèmes, les révisions de code et les flux d'activité.

Téléchargez le paquet d’installation GitLab pour Ubuntu et l’installer
----------------------------------------------------------------------

Installation longue (prévoir une image VM ou USB ?)

https://packages.gitlab.com/gitlab/gitlab-ce et choisissez la dernière version gitlab-ce pour ubuntu xenial

.. code-block:: console

  utilisateur@MachineUbuntu:~/gitlab$ wget https://packages.gitlab.com/gitlab/gitlab-ce/packages/ubuntu/focal/gitlab-ce_14.1.3-ce.0_amd64.deb/download.deb
  utilisateur@MachineUbuntu:~/gitlab$ sudo apt update ; sudo EXTERNAL_URL="http://gitlab.domaine-perso.fr" dpkg -i download.deb

Paramétrer GitLab
-----------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-ctl show-config
  utilisateur@MachineUbuntu:~/gitlab$ sudo chmod o+r /etc/gitlab/gitlab.rb
  utilisateur@MachineUbuntu:~/gitlab$ sudo nano /etc/gitlab/gitlab.rb

.. code-block:: ruby

  external_url "http://gitlab.domaine-perso.fr"
  # Pour activer les fonctions artifacts (tester la qualité du code, déployer sur un serveur distant en SSH, etc.)
  gitlab_rails['artifacts_enabled'] = true
  # pour générer la doc et l’afficher avec Gitlab
  pages_external_url "http://documentation.domaine-perso.fr"

.. code-block:: console

  utilisateur@MachineUbuntu:~/gitlab$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/gitlab/trusted-certs/MachineUbuntu.key -out /etc/gitlab/trusted-certs/MachineUbuntu.crt
  utilisateur@MachineUbuntu:~/gitlab$ sudo gitlab-ctl reconfigure
