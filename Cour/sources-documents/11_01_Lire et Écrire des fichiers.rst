.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Lire et Écrire des fichiers
***************************

Lecture et écriture de fichiers
===============================

La fonction :python:`open()` renvoie un objet fichier et est le plus souvent utilisée avec deux arguments : :python:`open(nomfichier, mode)`.

.. code-block:: python

  fichier = open(fichier, [code], [encoding=None])

**Le premier argument** est une chaîne contenant **le nom du fichier** (recherche le fichier dans le répertoire d'exécution de Python) ou le chemin avec le nom de fichier.

**Le deuxième argument** est une autre chaîne contenant quelques caractères décrivant la façon dont le fichier est utilisé :

.. list-table:: Modes d'ouverture d'un fichiers avec :python:`open()`
  :widths: 20 80
  :header-rows: 1

  * - Option
    - Signification
  * - :python:`'r'`
    - Le fichier n'est accédé qu'en lecture (par défaut).
  * - :python:`'x'`
    - Ouvre le fichier en création exclusive (échoue si le fichier existe déjà).
  * - :python:`'w'`
    - Ouvre le fichier en création (un fichier existant portant le même nom sera alors écrasé).
  * - :python:`'a'`
    - Ouvre le fichier en mode ajout (toute donnée écrite dans le fichier est automatiquement ajoutée à la fin).

.. list-table:: Options aux modes d'ouverture de :python:`open()`
  :widths: 20 80
  :header-rows: 1

  * - Complément d'option
    - Signification
  * - :python:`'+'`
    - Ouvre le fichier en mode lecture/écriture.
  * - :python:`'t'`
    - Mode texte (par défaut), les données sont lues et écrites sous formes de caractères.
  * - :python:`'b'`
    - Mode binaire, les données sont lues et écrites sous formes d'octets (type bytes).

L'argument mode est optionnel, sa valeur par défaut est :python:`'r'`.

:python:`'b'` collé à la fin du mode est à utiliser pour les fichiers contenant autre chose que du texte.

.. code-block:: python

  fichier = open('mon_fichier.bin', 'rb')


**L'argument** :python:`encoding` définit en mode texte l'encodage des données. Si aucun encodage n'est spécifié, l'encodage par défaut dépend de la plateforme (voir `open() <https://docs.python.org/fr/3/library/functions.html#open>`_). Pour connaître cet encodage utilisez la méthode Python :python:`locale.getpreferredencoding(False)`.

En mode texte à la lecture, le comportement par défaut est de convertir les fins de lignes (:python:`\\n` sur Unix) avec celles spécifiques à la plateforme. Donc tous les :python:`\\n` sont convertis dans leur équivalent de la plateforme courante.

Ces modifications effectuées automatiquement sont normales pour du texte, mais détérioreraient les données binaires contenues dans un fichier de type **JPEG** ou **EXE**. Soyez particulièrement attentifs à ouvrir ces fichiers binaires en mode binaire.

Console Python ouvrir un fichier existant avec python.

.. code-block:: pycon

  >>> fichier = open('mon_fichier.ext')

Bien ouvrir ses fichiers
------------------------

C'est une bonne pratique d'utiliser le mot-clé :Python:`with` lorsque vous traitez des fichiers. Vous fermez ainsi toujours correctement le fichier, même si une exception est levée. Utiliser :Python:`with` est aussi beaucoup plus court que d'utiliser l'équivalent avec des blocs :python:`try … finally`.

.. code-block:: pycon

  >>> with open('fichier.ext') as fichier:
  ...     lecture_données = fichier.read()
  ...
  >>> # Nous pouvons vérifier que le dossier a été automatiquement fermé.
  >>> fichier.closed
  True

Si vous n'utilisez pas le mot clef :Python:`with`, vous devez appeler :python:`fichier.close()` pour fermer le fichier et immédiatement libérer les ressources système qu'il utilise. Si vous ne fermez pas explicitement le fichier, le ramasse-miette de Python finira par détruire l'objet et fermer le
fichier pour vous. Mais le fichier peut rester ouvert pendant un moment. Un autre risque est que les différentes implémentations de Python fassent ce nettoyage à des moments différents.

Après la fermeture du fichier, que ce soit via une instruction :Python:`with` ou en appelant :python:`fichier.close()`, toute tentative d'utilisation de l'objet fichier échoue systématiquement.

.. code-block:: pycon

  >>> fichier.close()
  >>> fichier.read()
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  ValueError: I/O operation on closed file.

Méthodes des objets fichiers
----------------------------

Pour lire le contenu d'un fichier partiellement, vous pouvez appeler la méthode :python:`read` comme cela :python:`fichier.read(taille)`. Cette dernière lit une certaine quantité de données et la renvoie sous forme de chaîne (en mode texte) ou d'objet bytes (en mode binaire). :python:`taille` est un argument numérique facultatif.

Lorsque :python:`taille` est **omis ou négatif**, la totalité du contenu du fichier sera lue et retournée. C'est votre problème de développeur si le fichier est deux fois plus grand que la mémoire de votre machine.

La méthode ne peut lire ou renvoyer **au maximum** que la taille de caractères (en mode texte) ou la taille d'octets (en mode binaire).

Lorsque la fin du fichier est atteinte, :python:`fichier.read()` renvoie une chaîne vide :python:`''`.

.. code-block:: pycon

  >>> fichier.read()
  'Ceci est le fichier entier.\n'
  >>> fichier.read()
  ''

:python:`fichier.readline()` lit une seule ligne du fichier.

Un caractère de fin de ligne :python:`\\n` est laissé à la fin de la chaîne. Si le fichier ne se termine pas par un caractère de fin de ligne, ce caractère n'est omis par :python:`fichier.readline()` que pour la dernière ligne du fichier. Ceci permet de rendre la valeur de retour non ambigüe.

Si :python:`fichier.readline()` renvoie une chaîne vide, c'est que la fin du fichier a été atteinte.

.. code-block:: pycon

  >>> fichier.readline()
  'Ceci est la première ligne du fichier.\n'
  >>> fichier.readline()
  'Deuxième ligne du fichier\n'
  >>> fichier.readline()
  ''

Pour lire ligne à ligne, vous pouvez aussi boucler sur l'objet fichier. C'est plus efficace en terme de gestion mémoire, plus rapide et donne un code plus simple :

.. code-block:: pycon

  >>> for ligne in fichier:
  ...     print(ligne, end='')
  ...
  Ceci est la première ligne du fichier.
  Deuxième ligne du fichier

Pour construire une liste avec toutes les lignes d'un fichier, il est aussi possible d'utiliser :python:`list(fichier)` ou :python:`fichier.readlines()`.

:python:`fichier.write(chaine)` écrit le contenu d'une chaîne dans le fichier et renvoie le nombre de caractères écrits.

.. code-block:: pycon

  >>> fichier.write('Ceci est un test\n')
  17

Les autres types de données Python doivent être convertis. Soit en une chaîne (en mode texte) :Python:`str()`, soit en objet bytes (en mode binaire) :python:`bytes()` avant de les écrire :

.. code-block:: pycon

  >>> valeur = ('la réponse', 42)
  >>> untuple = str(valeur) # convertir le tuple en chaîne
  >>> fichier.write(untuple)
  18

:python:`ficher.tell()` renvoie un entier indiquant la position actuelle dans le fichier, mesurée en octets à partir du début du fichier lorsque le fichier est ouvert en mode binaire, ou en nombre de caractères pour le mode texte.

Pour modifier la position dans le fichier, utilisez :python:`fichier.seek(décalage, origine)`. La position est calculée en ajoutant :python:`décalage` à un point de référence. Ce point de référence est déterminé par l'argument :python:`origine`.

.. list-table:: Les valeurs de :python:`origine` de :python:`seek(décalage, origine)`
  :widths: 20 80
  :header-rows: 1

  * - Valeur
    - Signification
  * - :python:`0`
    - Pour le début du fichier.
  * - 1
    - Pour la position actuelle.
  * - 2
    - Pour la fin du fichier.

:python:`origine` peut être omis et sa valeur par défaut est :python:`0` (Python utilise le début du fichier comme point de référence).

.. code-block:: pycon

  >>> fichier = open('fichier.bin', 'rb+')
  >>> fichier.write(b'0123456789abcdef')
  16
  >>> fichier.seek(5) # Va au 6ème octet du fichier
  5
  >>> fichier.read(1)
  b'5'
  >>> fichier.seek(-3, 2) # Va au 3ème octet avant la fin
  13
  >>> fichier.read(1)
  b'd'

Sur un fichier en mode texte (ceux ouverts sans :python:`'b'` dans le mode), seuls les changements de positions relatifs au début du fichier sont autorisés (sauf après une exception où Python se rend alors à la fin du fichier avec :python:`seek(0, 2)`), et les seules valeurs possibles pour le paramètre décalage sont les valeurs renvoyées par :python:`fichier.tell()`, ou zéro. **Toute autre valeur pour le paramètre décalage produit un comportement indéfini**.

L'objet fichier dispose de méthodes supplémentaires, telles que :python:`isatty()` et :python:`truncate()` qui sont moins souvent utilisées. Consultez la `Référence de la Bibliothèque Standard <https://docs.python.org/fr/3/tutorial/inputoutput.html#methods-of-file-objects>`_ pour avoir un guide complet des objets fichiers.
