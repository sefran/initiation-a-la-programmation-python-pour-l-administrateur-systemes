Accueil des stagiaires
######################

-  Présentation du centre de formation

-  Présentation du formateur

-  Règles de vie

   -  Individuelles
   -  Collectives

-  Présentation interactive des stagiaires

-  Horaires et logistique (pauses, repas, locaux, service administratif,
   etc.)

-  Présentation de la formation

   -  Prérequis (Utilisation poste travail Windows ou Unix, savoir
      installer une application, bases de l’algorithmique et de l'administration systèmes)
   -  Attentes sur la formation
   -  Plan du cours
