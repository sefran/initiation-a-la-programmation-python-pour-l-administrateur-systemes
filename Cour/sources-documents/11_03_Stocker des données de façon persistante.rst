.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

.. role:: codexml(code)
  :language: xml

Stocker des données de façon persistante
****************************************

Archivage (zip)
===============

Avec python on peut compresser des fichiers au format zip avec le module :python:`zipfile`.  On peut aussi utiliser :python:`shutil` qui est de plus haut niveau mais moins souple.

Il nous faut utiliser l'objet :python:`ZipFile` du module :python:`zipfile` pour créer, modifier ou ouvrir un fichier zip à l'image du traitement des fichiers sous Python.

.. code-block:: python

  import zipfile

  with zipfile.ZipFile(fichier_destination, 'w') as donnees_zip:
      # …

Compression de fichiers individuels
-----------------------------------

La méthode :python:`write()` de l'objet :python:`ZipFile` permet d'envoyer le fichier à compresser.

.. code-block:: python

  import zipfile

  fichier_destination = 'mon_fichier.zip'
  fichier_a_compresser = 'mon_fichier.pdf'

  with zipfile.ZipFile(fichier_destination, 'w') as donnees_zip:
      donnees_zip.write(fichier_a_compresser, compress_type = zipfile.ZIP_DEFLATED)

Il faut indiquer l'option :python:`zipfile.ZIP_DEFLATED`, sinon l'archive n'est pas compressée par défaut.

Compression de plusieurs fichiers
---------------------------------

.. code-block:: python

  import os
  import zipfile

  fichier_destination = 'mon_fichier.zip'
  dossier_fichier_a_compresser = 'mon_dossier'

  with zipfile.ZipFile(fichier_destination, 'w') as donnees_zip:
      for dossier_parent, sous-dossier, fichier in os.walk(dossier_fichiers_a_compresse):
          if '.pdf' in fichier[0]: # pour les fichiers pdf
              donnees_zip.write (os.path.join(sous-dossier, fichier), os.path.relpath (os.path.join (sous-dossier, fichier) , dossier_fichiers_a_compresser), compress_type = zipfile.ZIP_DEFLATED)

Pour compresser les fichiers sans le répertoire il faut modifier :

.. code-block:: python

  donnees_zip.write (os.path.join (dossier, fichier), fichier, compress_type = zipfile.ZIP_DEFLATED)

Extraire les fichiers
---------------------

.. code-block:: python

  import zipfile

  fichier_zip = 'mon_fichier.zip'
  dossier_destination = 'mon_dossier'

  with zipfile.ZipFile(fichier_zip) as donnees_zip:
      donnees_zip.extractall(dossier_destination)

Extraire des fichiers individuels
---------------------------------

.. code-block:: python

  import zipfile

  fichier_zip = 'mon_fichier.zip'
  dossier_destination = 'mon_dossier'
  fichiers_a_extraires = ['mon_fichier1.pdf', 'mon_fichier2.pdf']

  with zipfile.ZipFile(fichier_zip) as donnees_zip:
      for fichier in fichiers_a_extraires:
          donnees_zip.extract (fichier, dossier_destination)

Mais là nous n'avons aucune garantie que le fichier soit présent dans le zip.

Lecture de fichiers Zip
-----------------------

.. code-block:: python

  import zipfile

  fichier_zip = 'mon_fichier.zip'
  dossier_destination = 'mon_dossier'
  fichiers_a_extraires = ['mon_fichier1.pdf', 'mon_fichier2.pdf']

  with zipfile.ZipFile(fichier_zip) as donnees_zip:
      for fichier in fichiers_a_extraires:
          # pour un fichier dans le zip
          for fichier in donnees_zip.namelist():
              donnees_zip.extract(fichier, dossier_destination)


.. list-table:: Les commandes utiles pour la lecture
  :widths: 60 40
  :header-rows: 1

  * - Commande
    - Signification
  * - :python:`with zipfile.ZipFile('mon_fichier.zip') as mon_objet_fichier_zip:`
    - Pour ouvrir le fichier zip au début.
  * - :python:`mon_objet_fichier_zip.setpassword(mon_mot_de_passe.encode())`
    - Pour indiquer le mot de passe si le zip est crypté (qui doit être converti en bytes, d'où :python:`encode()`).
  * - :python:`mon_objet_fichier_zip.namelist()`
    - Donne la liste des fichiers contenus dans l'archive (avec leur chemin).
  * - :python:`info_zip = mon_objet_fichier_zip.getinfo('chemin/mon_fichier.ext')`
    - Récupère un objet d'information :python:`info_zip`
  * - :python:`mon_objet_fichier_zip.infolist()`
    - On peut aussi directement avoir tous les objets info_zip de l'archive.
  * - :python:`mon_objet_fichier_zip.extractall()`
    - Extrait tous les fichiers dans le répertoire courant (en respectant l'arborescence du zip).
  * - :python:`mon_objet_fichier_zip.extractall(répertoire_cible)`
    - Extrait tous les fichiers dans le répertoire cible donné (en respectant l'arborescence du zip).
  * - :python:`mon_objet_fichier_zip.extractall('chemin/mon_fichier.ext, repertoire_cible)`
    - extrait seulement le fichier indiqué (présent dans le zip) dans le répertoire indiqué.

.. list-table:: Propriétés de :python:`info_zip`
  :widths: 40 60
  :header-rows: 1

  * - Propriété
    - Signification
  * - :python:`info_zip.file_size`
    - Taille originale du fichier.
  * - :python:`info_zip.compress_size`
    - Taille compressée.
  * - :python:`info_zip.filename`
    - Nom du fichier.
  * - :python:`info_zip.date_time`
    - Date du fichier

.. list-table:: Les commandes utiles pour l'écriture
  :widths: 50 50
  :header-rows: 1

  * - Commande
    - Signification
  * - :python:`with zipfile.ZipFile('chemin/mon_fichier.zip', 'w', zipfile.ZIP_DEFLATED) as mon_objet_fichier_zip:`
    - Création d'une nouvelle archive chemin/mon_fichier.zip (il faut indiquer ZIP_DEFLATED, sinon, elle n'est pas compressée par défaut).
  * - :python:`mon_objet_fichier_zip.write('chemin/mon_fichier.ext')`
    - Ajoute un fichier avec son chemin réel et dans l'archive.
  * - :python:`mon_objet_fichier_zip.close()`
    - Fermeture le fichier de l'archive.
  * - :python:`mon_objet_fichier_zip.write('mon_chemin1/mon_fichier.ext', 'chemin2/mon_fichier.ext')`
    - On peut décider du chemin exact du fichier dans l'archive quelque soit l'endroit où se trouve le fichier. Le fichier est physiquement dans mon_chemin1, mais dans l'archive, il sera dans chemin2.

Pour approfondir voir `Travailler avec des archives ZIP <https://docs.python.org/fr/3/library/zipfile.html>`_.

CSV
===

Nous utilisons souvent des tableurs dans notre bureautique numérique. Le format couramment utilisé pour échanger des données est le CSV (Comma Separated Values). Comme son nom anglo-saxon l'indique, c'est un format texte de données séparées avec des virgules :python:`,`. Mais ces données peuvent-être représentées avec des virgules. Il est donc préférable d'utiliser des tabulations ou encore des points virgules comme séparateur de vos données.

Créer le fichier «**exemple.csv**»

.. code-block:: text

  identifiant;nom;prénom
  PreNM;NOM,Prénom
  UtBD;BIDON;Utilisateur
  MaPER;PERSONNE;Ma;Pas;Bon;

Pour lire ce genre de fichier avec Python, on utilise le module :python:`csv` avec la méthode :python:`reader()`.

.. code-block:: pycon

  >>> import csv
  >>> with open('exemple.csv') as fichiercsv:
  ...     lecture = csv.reader(fichiercsv, delimiter=';', quotechar='\'')
  ...     for données in lecture:
  ...         print(données)
  ...
  ['identifiant', 'nom', 'prénom']
  ['PreNM', 'NOM,Prénom']
  ['UtBD', 'BIDON', 'Utilisateur']
  ['MaPER', 'PERSONNE', 'Ma', 'Pas', 'Bon', '']

Pour écrire dans ce fichier avec Python, on utilise l'objet :python:`writer` et sa méthode :python:`writerow()`.

.. code-block:: pycon

  >>> import csv
  >>> with open('exemple.csv', 'a') as fichiercsv:
  ...     écriture = csv.writer(fichiercsv, delimiter=';', quotechar='\'', quoting=csv.QUOTE_MINIMAL)
  ...     écriture.writerow(['identifiant', 'MonNOM', 'MonPrénom'])
  ...
  30

Le fichier «**exemple.csv**» est devenu

.. code-block:: text

  identifiant;nom;prénom
  PreNM;NOM,Prénom
  UtBD;BIDON;Utilisateur
  MaPER;PERSONNE;Ma;Pas;Bon;
  identifiant;MonNOM;MonPrénom

Pour plus d'informations voir la documentation `Lecture et écriture de fichiers CSV <https://docs.python.org/fr/3/library/csv.html>`_.


JSON
====

Les chaînes de caractères peuvent facilement être écrites dans un fichier et relues. Les nombres nécessitent un peu plus d'effort, car la méthode :python:`read()` ne renvoie que des chaînes. Elles doivent donc être passées à une fonction comme :python:`int()`, qui prend une chaîne comme :python:`'123'` en entrée et renvoie sa valeur numérique :python:`123`. Mais dès que vous voulez enregistrer des types de données plus complexes comme des listes, des dictionnaires ou des instances de classes, le traitement lecture/écriture avec le code devient vite compliqué.

Plutôt que de passer son temps à écrire et déboguer du code permettant de sauvegarder des types de données compliqués, Python permet d'utiliser **JSON** (JavaScript Object Notation), un format répandu de représentation et d'échange de données.

Le module standard :python:`json` peut transformer des données de Python en une représentation sous forme de chaîne de caractères.

Ce processus est nommé **sérialiser**. Reconstruire les données à partir de leur représentation sous forme de chaîne est appelé **déserialiser**. Entre sa sérialisation et sa désérialisation, la chaîne représentant les données peut avoir été stockée ou transmise à une autre machine.

.. Note::

  Le format **JSON** est couramment utilisé dans les applications modernes pour échanger des données. Beaucoup de développeurs le maîtrise, ce qui en fait un format de prédilection pour l'interopérabilité.

Si vous avez un objet quelconque, vous pouvez voir sa représentation **JSON** en tapant simplement :

.. code-block:: pycon

  >>> import json
  >>> json.dumps([1, 'simple', 'list'])
  '[1, "simple", "list"]'

Une variante de la fonction :python:`dumps()`, nommée :python:`dump()`, sérialise simplement l'objet donné vers un fichier texte. Donc si :python:`fichier` est un fichier texte ouvert en écriture, il est possible de faire :

.. code-block:: python

  json.dump(x, fichier)

Pour reconstruire l'objet, si :python:`fichier` est cette fois un fichier texte ouvert en lecture :

.. code-block:: python

  x = json.load(fichier)

Cette méthode de sérialisation peut sérialiser des listes et des dictionnaires, mais aussi sérialiser d'autres types de données. Cela requiert un peu plus de travail et la documentation du module `json <https://docs.python.org/fr/3/library/json.html>`_ explique comment faire.


XML
===

Maintenant passons à la dimension supérieure du traitement de ces données. On veut maintenant échanger des données, comme avec JSON, mais avec la possibilité de définir ses propres standards de données (au delà des types de Python). Pour cela il existe un façon standard de le faire c'est le **XML** (eXtensible Markup Language).

Le **XML** permet de définir sa propre représentation des données dans un fichier «**.xml**», tout en spécifiant sa syntaxe **DTD** (Document Type Definition) dans un fichier «**.dtd**» qui va préciser la grammaire, et un **schéma** qui va préciser les propriétés de son vocabulaire.

La Validation
-------------

Nous allons devoir définir suivant la norme DSDL (Document Schema Definition Language) la syntaxe et ses propriétés du code XML.

Schéma DTD
^^^^^^^^^^

Pour tester sous Python la validité de la grammaire XML de notre code avec une DTD, il nous faut installer le module :python:`lxml`.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ sudo pip install lxml

Testons maintenant la conformité XML.

Cette syntaxe est :

* **utilisateurs** : Une liste d'utilisateurs.
* **utilisateur** : un utilisateur avec une option d’**identifiant** obligatoire.
* **nom** : le nom de l'utilisateur (chaîne de caractères alpha).
* **prenom** : le prénom de l'utilisateur (chaîne de caractères alpha).
* **sexe** : le sexe de l'utilisateur défini avec une option **biologique** obligatoire, dont les valeurs possibles sont **H** = Homme, **F** = Femme, **S** = Sans, **2** = Hermaphrodite. Et une option **social** avec les valeurs possibles **H** = Homme, **F** = Femme, **B** = 2 sexes, **S** = pas de sexualité, Faux = pas de particularité sexuelle sociale.
* **age** : l'age de l'utilisateur (nombre entier limité à 150 ans).
* **adresse** : l'adresse de l'utilisateur (chaîne alphanumérique).
* **codepostal** : Un nombre de 5 chiffres numériques.
* **ville** : la ville de l'utilisateur (chaîne alphanumérique de maximum 163 caractères).

Testons la validation de balises (ELEMENT) pour cela nous allons utiliser l'objet :python:`etree.DTD()` pour saisir nos règles de syntaxe, l'objet :python:`etree.XML()` pour saisir notre code XML, et enfin la méthode :python:`validate()` de l'objet DTD pour tester la bonne conformité de la grammaire.

.. code-block:: pycon

  >>> from io import StringIO
  >>> from lxml import etree
  >>> texte_dtd = StringIO("<!ELEMENT utilisateur EMPTY>")
  >>> dtd = etree.DTD(texte_dtd)
  >>> xml = etree.XML("<utilisateur/>")
  >>> dtd.validate(xml)
  True
  >>> xml = etree.XML("<utilisateur>Du blabla</utilisateur>")
  >>> dtd.validate(xml)
  False
  >>> dtd.error_log
  <string>:1:0:ERROR:VALID:DTD_NOT_EMPTY: Element utilisateur was declared EMPTY this one has content

Nous avons introduit à la fin :python:`dtd.error_log` qui permet de visualiser les problèmes de syntaxe dans le code XML.

Testons et saisissons maintenant la grammaire de tous les tags XML.

.. code-block:: pycon

  >>> texte_dtd = StringIO("<!ELEMENT utilisateur (#PCDATA)>")
  >>> dtd = etree.DTD(texte_dtd)
  >>> dtd.validate(xml)
  >>> texte_dtd = StringIO("<!ELEMENT utilisateurs (utilisateur+)><!ELEMENT utilisateur (#PCDATA)>")
  >>> dtd = etree.DTD(texte_dtd)
  >>> xml = etree.XML("<utilisateurs><utilisateur>Premier</utilisateur></utilisateurs>")
  >>> dtd.validate(xml)
  True
  >>> xml = etree.XML("<utilisateurs><utilisateur>Premier</utilisateur><utilisateur>Deuxième</utilisateur></utilisateurs>")
  >>> dtd.validate(xml)
  True
  >>> texte_dtd = StringIO("<!ELEMENT utilisateurs (utilisateur+)><!ELEMENT utilisateur (nom, prenom, sexe, age, adresse, codepostal, ville)><!ELEMENT nom (#PCDATA)><!ELEMENT prenom (#PCDATA)><!ELEMENT sexe EMPTY><!ELEMENT age (#PCDATA)><!ELEMENT adresse (#PCDATA)><!ELEMENT codepostal (#PCDATA)><!ELEMENT ville (#PCDATA)>")
  >>> dtd = etree.DTD(texte_dtd)
  >>> xml = etree.XML("<utilisateurs><utilisateur><nom>NOM</nom><prenom>Prénom</prenom><sexe/><age>70</age><adresse>Lieu de vie</adresse><codepostal>34110</codepostal><ville>FRONTIGNAN</ville></utilisateur></utilisateurs>")
  >>> dtd.validate(xml)
  True

Abordons maintenant les options des tags XML (ATTLIST).

.. code-block:: pycon

  >>> texte_dtd = StringIO("<!ELEMENT utilisateurs (utilisateur+)><!ELEMENT utilisateur (nom, prenom, sexe, age, adresse, codepostal, ville)><!ATTLIST utilisateur identifiant CDATA #REQUIRED><!ELEMENT nom (#PCDATA)><!ELEMENT prenom (#PCDATA)><!ELEMENT sexe (#PCDATA)><!ATTLIST sexe biologique (H|F|2|S) #REQUIRED><!ATTLIST sexe social (H|F|B|S|False) 'False'><!ELEMENT age (#PCDATA)><!ELEMENT adresse (#PCDATA)><!ELEMENT codepostal (#PCDATA)><!ELEMENT ville (#PCDATA)>")
  >>> dtd = etree.DTD(texte_dtd)
  >>> dtd.validate(xml)
  False
  >>> dtd.error_log
  <string>:1:0:ERROR:VALID:DTD_MISSING_ATTRIBUTE: Element utilisateur does not carry attribute identifiant
  <string>:1:0:ERROR:VALID:DTD_MISSING_ATTRIBUTE: Element sexe does not carry attribute biologique
  >>> xml = etree.XML("<utilisateurs><utilisateur identifiant='1'><nom>NOM</nom><prenom>Prénom</prenom><sexe biologique='H'/><age>70</age><adresse>Lieu de vie</adresse><codepostal>34110</codepostal><ville>FRONTIGNAN</ville></utilisateur></utilisateurs>")
  >>> dtd.validate(xml)
  True
  >>> xml = etree.XML("<utilisateurs><utilisateur identifiant='1'><nom>NOM</nom><prenom>Prénom</prenom><sexe biologique='H' social='F'/><age>70</age><adresse>Lieu de vie</adresse><codepostal>34110</codepostal><ville>FRONTIGNAN</ville></utilisateur></utilisateurs>")
  >>> dtd.validate(xml)
  True
  >>> xml = etree.XML("<utilisateurs><utilisateur identifiant='1'><nom>NOM</nom><prenom>Prénom</prenom><sexe biologique='H' social=''/><age>70</age><adresse>Lieu de vie</adresse><codepostal>34110</codepostal><ville>FRONTIGNAN</ville></utilisateur></utilisateurs>")
  >>> dtd.validate(xml)
  False
  >>> dtd.error_log
  <string>:1:0:ERROR:VALID:DTD_ATTRIBUTE_VALUE: Syntax of value for attribute social of sexe is not valid
  <string>:1:0:ERROR:VALID:DTD_ATTRIBUTE_VALUE: Value "" for attribute social of sexe is not among the enumerated set


Exemple xml avec le fichier «**personnes.xml**».

.. code-block:: xml

  <?xml version="1.0" encoding="utf-8"?>
  <utilisateurs identifiant="1">
      <utilisateur>
          <nom>NOM</nom>
          <prenom>Prénom</prenom>
          <sexe biologique='H'/>
          <age>70</age>
          <adresse>Lieu de vie</adresse>
          <codepostal>34110</codepostal>
          <ville>FRONTIGNAN</ville>
      </utilisateur>
      <utilisateur identifiant="2">
          <nom>MONNOM</nom>
          <prenom>MonPrénom</prenom>
          <sexe biologique='F'/>
          <age>40</age>
          <adresse>Mon lieu de vie</adresse>
          <codepostal>34000</codepostal>
          <ville>MONTPELLIER</ville>
      </utilisateur>
      <utilisateur identifiant="3">
          <nom>PERSONNE</nom>
          <prenom>Jesuis</prenom>
          <sexe biologique='H' indetermine='F'/>
          <age>25</age>
          <adresse>Sans lieu de vie</adresse>
          <codepostal>34200</codepostal>
          <ville>SÈTE</ville>
      </utilisateur>
      <utilisateur identifiant="4">
          <nom>UNIVERSEL</nom>
          <prenom>Divain</prenom>
          <sexe biologique='2'/>
          <age>15</age>
          <adresse>Lieu sain</adresse>
          <codepostal>34130</codepostal>
          <ville>SAINT-AUNÈS</ville>
      </utilisateur>
      <utilisateur identifiant="5">
          <nom>DIFFÉRENT</nom>
          <prenom>Être</prenom>
          <sexe biologique='S'/>
          <age>33</age>
          <adresse>Lieu de vie normal</adresse>
          <codepostal>34260</codepostal>
          <ville>LE BOUSQUET-D'ORB</ville>
      </utilisateur>
  </utilisateurs>


Créons le fichiers «**personnes.dtd**».

.. code-block:: dtd

  <!ELEMENT utilisateurs (utilisateur+)>
  <!ELEMENT utilisateur (nom, prenom, sexe, age, adresse, codepostal, ville)>
  <!ATTLIST utilisateur identifiant CDATA #REQUIRED>
  <!ELEMENT nom (#PCDATA)>
  <!ELEMENT prenom (#PCDATA)>
  <!ELEMENT sexe EMPTY>
  <!ATTLIST sexe biologique (H|F|2|S) #REQUIRED>
  <!ATTLIST sexe social (H|F|B|S|False) 'False'>
  <!ELEMENT age (#PCDATA)>
  <!ELEMENT adresse (#PCDATA)>
  <!ELEMENT codepostal (#PCDATA)>
  <!ELEMENT ville (#PCDATA)>

Testons avec ces deux fichiers la DTD et le code Python. Abordons l'objet :python:`etree.parse()` qui nous permet de charger directement un fichier XML.

.. code-block:: pycon

  >>> from lxml import etree
  >>> dtd = etree.DTD('personnes.dtd')
  >>> xml = etree.XML("<utilisateurs>\n<utilisateur identifiant='1'>\n<nom>NOM</nom>\n<prenom>Prénom</prenom>\n<sexe biologique='H' social='F'/>\n<age>70</age>\n<adresse>Lieu de vie</adresse>\n<codepostal>34110</codepostal>\n<ville>FRONTIGNAN</ville>\n</utilisateur>\n</utilisateurs>")
  >>> dtd.validate(xml)
  True
  >>> with open('personnes.xml', 'r') as filexml:
  ...     xml = filexml.read()
  ...     xml = etree.XML(xml)
  ...     dtd.validate(xml)
  ...
  True
  >>> xml = etree.parse('personnes.xml')
  >>> dtd.validate(xml)
  True

Schéma XML XSD
^^^^^^^^^^^^^^

Maintenant validons notre code XML avec le format XSD.

.. code-block:: pycon

  >>> xsd = StringIO('''\
  ... <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  ...     <xs:complexType name="TypeUtilisateur">
  ...         <xs:sequence>
  ...             <xs:element name="nom" type="xs:string"/>
  ...             <xs:element name="prenom" type="xs:string"/>
  ...             <xs:element name="sexe" type="xs:string"/>
  ...             <xs:element name="age" type="xs:integer"/>
  ...             <xs:element name="adresse" type="xs:string"/>
  ...             <xs:element name="codepostal" type="xs:integer"/>
  ...             <xs:element name="ville" type="xs:string"/>
  ...         </xs:sequence>
  ...     </xs:complexType>
  ...     <xs:element name="utilisateur" type="TypeUtilisateur"/>
  ... </xs:schema>
  ... ''')
  >>> parsexmlxsd = etree.parse(xsd)
  >>> xmlxsd = etree.XMLSchema(parsexmlxsd)
  >>> xml = StringIO("<utilisateur><nom>NOM</nom><prenom>Prénom</prenom><sexe/><age>70</age><adresse>Lieu de vie</adresse><codepostal>34110</codepostal><ville>FRONTIGNAN</ville></utilisateur>")
  >>> codexml = etree.parse(xml)
  >>> xmlxsd.validate(codexml)
  True

Ajoutons l'imbrication dans le tag :codexml:`utilisateurs`.

.. code-block:: pycon

  xsd = StringIO('''<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
      <xs:element name="utilisateurs">
          <xs:complexType>
              <xs:sequence>
                  <xs:element name="utilisateur" type="UtilisateurType" minOccurs="1" maxOccurs="unbounded"/>
              </xs:sequence>
          </xs:complexType>
      </xs:element>

      <xs:complexType name="UtilisateurType">
          <xs:sequence>
              <xs:element name="nom" type="xs:string"/>
              <xs:element name="prenom" type="xs:string"/>
              <xs:element name="sexe" type="xs:string"/>
              <xs:element name="age" type="xs:integer"/>
              <xs:element name="adresse" type="xs:string"/>
              <xs:element name="codepostal" type="xs:integer"/>
              <xs:element name="ville" type="xs:string"/>
          </xs:sequence>
      </xs:complexType>
  </xs:schema>
  ''')
  parsexmlxsd = etree.parse(xsd)
  xmlxsd = etree.XMLSchema(parsexmlxsd)
  >>> xml = StringIO("<utilisateurs><utilisateur><nom>NOM</nom><prenom>Prénom</prenom><sexe/><age>70</age><adresse>Lieu de vie</adresse><codepostal>34110</codepostal><ville>FRONTIGNAN</ville></utilisateur></utilisateurs>")
  >>> codexml = etree.parse(xml)
  >>> xmlxsd.validate(codexml)
  True


Ajoutons un attribut.

.. code-block:: pycon

  >>> xsd = StringIO('''<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  ...     <xs:element name="utilisateurs">
  ...         <xs:complexType>
  ...             <xs:sequence>
  ...                 <xs:element name="utilisateur" type="UtilisateurType" minOccurs="1" maxOccurs="unbounded"/>
  ...             </xs:sequence>
  ...         </xs:complexType>
  ...     </xs:element>
  ...
  ...     <xs:complexType name="UtilisateurType">
  ...         <xs:sequence>
  ...             <xs:element name="nom" type="xs:string"/>
  ...             <xs:element name="prenom" type="xs:string"/>
  ...             <xs:element name="sexe" type="xs:string"/>
  ...             <xs:element name="age" type="xs:integer"/>
  ...             <xs:element name="adresse" type="xs:string"/>
  ...             <xs:element name="codepostal" type="xs:integer"/>
  ...             <xs:element name="ville" type="xs:string"/>
  ...         </xs:sequence>
  ...         <xs:attribute name="identifiant" use="required" type="xs:positiveInteger"/>
  ...     </xs:complexType>
  ... </xs:schema>
  ... ''')
  >>> parsexmlxsd = etree.parse(xsd)
  >>> xmlxsd = etree.XMLSchema(parsexmlxsd)
  >>> codexml = etree.parse(xml)
  >>> xmlxsd.validate(codexml)
  False
  >>> xml = StringIO("<utilisateurs><utilisateur identifiant='1'><nom>NOM</nom><prenom>Prénom</prenom><sexe/><age>70</age><adresse>Lieu de vie</adresse><codepostal>34110</codepostal><ville>FRONTIGNAN</ville></utilisateur></utilisateurs>")
  >>> codexml = etree.parse(xml)
  >>> xmlxsd.validate(codexml)
  True

Pour la suite je vous laisse à l'apprentissage du XSD dans une formation XML.

La lecture
----------

Maintenant que nous avons validé la syntaxe du XML, nous allons parcourir avec Python les éléments dans un fichier XML. Pour cela nous allons utiliser la bibliothèque native :python:`xml`.

.. code-block:: pycon

  >>> import xml.etree.ElementTree as ArbreXML
  >>> structurexml = ArbreXML.parse('personnes.xml')
  >>> racinexml = structurexml.getroot()
  >>> racinexml.tag
  'utilisateurs'
  >>> racinexml.attrib
  {}
  >>> for enfant in racinexml:
  ...     print(enfant.tag, enfant.attrib)
  ...
  utilisateur {'identifiant': '1'}
  utilisateur {'identifiant': '2'}
  utilisateur {'identifiant': '3'}
  utilisateur {'identifiant': '4'}
  utilisateur {'identifiant': '5'}
  >>> racinexml[0][1].text
  'Prénom'
  >>> racinexml[0][0].text
  'NOM'
  >>> for nom in structurexml.iter('nom'):
  ...     print(nom.text)
  ...
  NOM
  MONNOM
  PERSONNE
  UNIVERSEL
  DIFFÉRENT
  >>> for utilisateur in structurexml.findall('utilisateur'):
  ...     identifiant = utilisateur.get('identifiant')
  ...     prénom = utilisateur.find('prenom').text
  ...     nom = utilisateur.find('nom').text
  ...     print(identifiant, prénom, nom)
  ...
  1 Prénom NOM
  2 MonPrénom MONNOM
  3 Jesuis PERSONNE
  4 Divain UNIVERSEL
  5 Être DIFFÉRENT

Bon on lit directement le XML, mais comment transformer un fichier XML en un dictionnaire exploitable ?

Pour cela on utilise la bibliothèque :python:`xmlschema`

Installons la bibliothèque.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/12_Données$ sudo pip install xmlschema

Utilisons là pour convertir un fichier XML avec son schéma XSD.

Testons d'abord :python:`xmlschema` avec un schema, et validons le.

.. code-block:: pycon

  >>> import xmlschema
  >>> schemaxml = xmlschema.XMLSchema('''<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  ... <xs:element name="utilisateur" type="xs:string"/>
  ... </xs:schema>
  ... ''')
  >>> schemaxml.is_valid('''<?xml version="1.0" encoding="UTF-8"?><utilisateur></utilisateur>''')
  True

Testons avec les balises imbriquées.

.. code-block:: pycon

  >>> xsd = '''
  ... <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  ...     <xs:element name="utilisateurs">
  ...         <xs:complexType>
  ...         <xs:sequence>
  ...             <xs:element name="utilisateur">
  ...                 <xs:complexType>
  ...                 <xs:sequence>
  ...                     <xs:element name="nom" type="xs:string"/>
  ...                     <xs:element name="prenom" type="xs:string"/>
  ...                     <xs:element name="age" type="xs:integer"/>
  ...                 </xs:sequence>
  ...                 </xs:complexType>
  ...             </xs:element>
  ...         </xs:sequence>
  ...         </xs:complexType>
  ...     </xs:element>
  ... </xs:schema>
  ... '''
  >>> schemaxml = xmlschema.XMLSchema(xsd)
  >>> schemaxml.is_valid('''<?xml version="1.0" encoding="UTF-8"?><utilisateurs><utilisateur><nom>MOI</nom><prenom>C'est</prenom><age>18</age></utilisateur></utilisateurs>''')
  True

Puis testons enfin avec un attribut.



.. code-block:: pycon

  >>> xsd = '''
  ... <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  ...     <xs:element name="utilisateurs">
  ...         <xs:complexType>
  ...         <xs:sequence>
  ...             <xs:element name="utilisateur">
  ...                 <xs:complexType>
  ...                 <xs:sequence>
  ...                     <xs:element name="nom" type="xs:string"/>
  ...                     <xs:element name="prenom" type="xs:string"/>
  ...                     <xs:element name="age" type="xs:integer"/>
  ...                 </xs:sequence>
  ...                 <xs:attribute name="identifiant" use="required" type="xs:positiveInteger"/>
  ...                 </xs:complexType>
  ...             </xs:element>
  ...         </xs:sequence>
  ...         </xs:complexType>
  ...     </xs:element>
  ... </xs:schema>
  ... '''
  >>> schemaxml = xmlschema.XMLSchema(xsd)
  >>> schemaxml.is_valid('''<?xml version="1.0" encoding="UTF-8"?><utilisateurs><utilisateur><nom>MOI</nom><prenom>C'est</prenom><age>18</age></utilisateur></utilisateurs>''')
  False
  >>> schemaxml.is_valid('''<?xml version="1.0" encoding="UTF-8"?><utilisateurs><utilisateur identifiant="1"><nom>MOI</nom><prenom>C'est</prenom><age>18</age></utilisateur></utilisateurs>''')
  True

Convertissons tout cela en un dictionnaire Python.

.. code-block:: pycon

  >>> schemaxml.to_dict('''<?xml version="1.0" encoding="UTF-8"?><utilisateurs><utilisateur identifiant="1"><nom>MOI</nom><prenom>C'est</prenom><age>18</age></utilisateur></utilisateurs>''', schema=schemaxml, preserve_root=True)
  {'utilisateurs': {'utilisateur': {'@identifiant': 1, 'nom': 'MOI', 'prenom': "C'est", 'age': 18}}}

On voit bien que les valeurs des variables XML ont été converties dans le type correspondant Python.

L'écriture
----------

Revenons sur nos exemples avec :python:`xml`.

Changeons la valeur de l'attribut identifiant.

.. code-block:: pycon

  >>> import xml.etree.ElementTree as ArbreXML
  >>> structurexml = ArbreXML.parse('personnes.xml')
  >>> racinexml = structurexml.getroot()
  >>> for enfant in racinexml:
  ...     nouvel_identifiant = int(enfant.get('identifiant')) + 10
  ...     enfant.set('identifiant', str(nouvel_identifiant))
  ...
  >>> structurexml.write('personnes2.xml')


Ce qui nous donne avec le fichier «**personnes2.xml**».

.. code-block:: xml

  <utilisateurs>
      <utilisateur identifiant="11">
          <nom>NOM</nom>
          <prenom>Pr&#233;nom</prenom>
          <sexe biologique="H" />
          <age>70</age>
          <adresse>Lieu de vie</adresse>
          <codepostal>34110</codepostal>
          <ville>FRONTIGNAN</ville>
      </utilisateur>
      <utilisateur identifiant="12">
          <nom>MONNOM</nom>
          <prenom>MonPr&#233;nom</prenom>
          <sexe biologique="F" />
          <age>40</age>
          <adresse>Mon lieu de vie</adresse>
          <codepostal>34000</codepostal>
          <ville>MONTPELLIER</ville>
      </utilisateur>
      <utilisateur identifiant="13">
          <nom>PERSONNE</nom>
          <prenom>Jesuis</prenom>
          <sexe biologique="H" social="F" />
          <age>25</age>
          <adresse>Sans lieu de vie</adresse>
          <codepostal>34200</codepostal>
          <ville>S&#200;TE</ville>
      </utilisateur>
      <utilisateur identifiant="14">
          <nom>UNIVERSEL</nom>
          <prenom>Divain</prenom>
          <sexe biologique="2" />
          <age>15</age>
          <adresse>Lieu sain</adresse>
          <codepostal>34130</codepostal>
          <ville>SAINT-AUN&#200;S</ville>
      </utilisateur>
      <utilisateur identifiant="15">
          <nom>DIFF&#201;RENT</nom>
          <prenom>&#202;tre</prenom>
          <sexe biologique="S" />
          <age>33</age>
          <adresse>Lieu de vie normal</adresse>
          <codepostal>34260</codepostal>
          <ville>LE BOUSQUET-D'ORB</ville>
      </utilisateur>
  </utilisateurs>


Changeons la valeur d'un tag XML.

.. code-block:: pycon

  >>> import xml.etree.ElementTree as ArbreXML
  >>> structurexml = ArbreXML.parse('personnes.xml')
  >>> racinexml = structurexml.getroot()
  >>> for nom in structurexml.iter('nom'):
  ...     nouveau_nom = '«' + nom.text + '»'
  ...     nom.text = nouveau_nom
  ...
  >>> structurexml.write('personnes3.xml', encoding='utf-8', xml_declaration=True)

Ce qui nous donne avec le fichier «**personnes3.xml**».

.. code-block:: xml

  <?xml version='1.0' encoding='utf-8'?>
  <utilisateurs>
      <utilisateur identifiant="11">
          <nom>«NOM»</nom>
          <prenom>Prénom</prenom>
          <sexe biologique="H" />
          <age>70</age>
          <adresse>Lieu de vie</adresse>
          <codepostal>34110</codepostal>
          <ville>FRONTIGNAN</ville>
      </utilisateur>
      <utilisateur identifiant="12">
          <nom>«MONNOM»</nom>
          <prenom>MonPrénom</prenom>
          <sexe biologique="F" />
          <age>40</age>
          <adresse>Mon lieu de vie</adresse>
          <codepostal>34000</codepostal>
          <ville>MONTPELLIER</ville>
      </utilisateur>
      <utilisateur identifiant="13">
          <nom>«PERSONNE»</nom>
          <prenom>Jesuis</prenom>
          <sexe biologique="H" social="F" />
          <age>25</age>
          <adresse>Sans lieu de vie</adresse>
          <codepostal>34200</codepostal>
          <ville>SÈTE</ville>
      </utilisateur>
      <utilisateur identifiant="14">
          <nom>«UNIVERSEL»</nom>
          <prenom>Divain</prenom>
          <sexe biologique="2" />
          <age>15</age>
          <adresse>Lieu sain</adresse>
          <codepostal>34130</codepostal>
          <ville>SAINT-AUNÈS</ville>
      </utilisateur>
      <utilisateur identifiant="15">
          <nom>«DIFFÉRENT»</nom>
          <prenom>Être</prenom>
          <sexe biologique="S" />
          <age>33</age>
          <adresse>Lieu de vie normal</adresse>
          <codepostal>34260</codepostal>
          <ville>LE BOUSQUET-D'ORB</ville>
      </utilisateur>
  </utilisateurs>

Supprimer un tag :codexml:`<utilisateur>`.

.. code-block:: pycon

  >>> import xml.etree.ElementTree as ArbreXML
  >>> structurexml = ArbreXML.parse('personnes.xml')
  >>> racinexml = structurexml.getroot()
  >>> for enfant in racinexml:
  ...     if enfant.get('identifiant') == '3':
  ...         racinexml.remove(enfant)
  ...
  >>> structurexml.write('personnes4.xml', encoding='utf-8', xml_declaration=True)


Ce qui nous donne avec le fichier «**personnes4.xml**».

.. code-block:: xml

  <?xml version='1.0' encoding='utf-8'?>
  <utilisateurs>
      <utilisateur identifiant="1">
          <nom>NOM</nom>
          <prenom>Prénom</prenom>
          <sexe biologique="H" />
          <age>70</age>
          <adresse>Lieu de vie</adresse>
          <codepostal>34110</codepostal>
          <ville>FRONTIGNAN</ville>
      </utilisateur>
      <utilisateur identifiant="2">
          <nom>MONNOM</nom>
          <prenom>MonPrénom</prenom>
          <sexe biologique="F" />
          <age>40</age>
          <adresse>Mon lieu de vie</adresse>
          <codepostal>34000</codepostal>
          <ville>MONTPELLIER</ville>
      </utilisateur>
      <utilisateur identifiant="4">
          <nom>UNIVERSEL</nom>
          <prenom>Divain</prenom>
          <sexe biologique="2" />
          <age>15</age>
          <adresse>Lieu sain</adresse>
          <codepostal>34130</codepostal>
          <ville>SAINT-AUNÈS</ville>
      </utilisateur>
      <utilisateur identifiant="5">
          <nom>DIFFÉRENT</nom>
          <prenom>Être</prenom>
          <sexe biologique="S" />
          <age>33</age>
          <adresse>Lieu de vie normal</adresse>
          <codepostal>34260</codepostal>
          <ville>LE BOUSQUET-D'ORB</ville>
      </utilisateur>
  </utilisateurs>

Ajouter un tag :codexml:`<utilisateur>`.

.. code-block:: pycon

  >>> import xml.etree.ElementTree as ArbreXML
  >>> structurexml = ArbreXML.parse('personnes.xml')
  >>> racinexml = structurexml.getroot()
  >>> racinexml.tag
  'utilisateurs'
  >>> utilisateur = ArbreXML.SubElement(racinexml, 'utilisateur')
  >>> utilisateur.set('identifiant', '6')
  >>> nom = ArbreXML.SubElement(utilisateur, 'nom')
  >>> nom.text = "NOUVEAU"
  >>> prenom = ArbreXML.SubElement(utilisateur, 'prenom')
  >>> prenom.text = "Super"
  >>> sexe = ArbreXML.SubElement(utilisateur, 'sexe')
  >>> sexe.set('biologique', 'H')
  >>> age = ArbreXML.SubElement(utilisateur, 'age')
  >>> age.text = '20'
  >>> adresse = ArbreXML.SubElement(utilisateur, 'adresse')
  >>> adresse.text = "Rue Paradi"
  >>> codepostal = ArbreXML.SubElement(utilisateur, 'codepostal')
  >>> codepostal.text = '00000'
  >>> ville = ArbreXML.SubElement(utilisateur, 'ville')
  >>> ville.text = "CIEUX"
  >>> structurexml.write('personnes5.xml', encoding='utf-8', xml_declaration=True)

Ce qui nous donne avec le fichier «**personnes5.xml**».

.. code-block:: xml

  <?xml version='1.0' encoding='utf-8'?>
  <utilisateurs>
      <utilisateur identifiant="1">
          <nom>NOM</nom>
          <prenom>Prénom</prenom>
          <sexe biologique="H" />
          <age>70</age>
          <adresse>Lieu de vie</adresse>
          <codepostal>34110</codepostal>
          <ville>FRONTIGNAN</ville>
      </utilisateur>
      <utilisateur identifiant="2">
          <nom>MONNOM</nom>
          <prenom>MonPrénom</prenom>
          <sexe biologique="F" />
          <age>40</age>
          <adresse>Mon lieu de vie</adresse>
          <codepostal>34000</codepostal>
          <ville>MONTPELLIER</ville>
      </utilisateur>
      <utilisateur identifiant="3">
          <nom>PERSONNE</nom>
          <prenom>Jesuis</prenom>
          <sexe biologique="H" social="F" />
          <age>25</age>
          <adresse>Sans lieu de vie</adresse>
          <codepostal>34200</codepostal>
          <ville>SÈTE</ville>
      </utilisateur>
      <utilisateur identifiant="4">
          <nom>UNIVERSEL</nom>
          <prenom>Divain</prenom>
          <sexe biologique="2" />
          <age>15</age>
          <adresse>Lieu sain</adresse>
          <codepostal>34130</codepostal>
          <ville>SAINT-AUNÈS</ville>
      </utilisateur>
      <utilisateur identifiant="5">
          <nom>DIFFÉRENT</nom>
          <prenom>Être</prenom>
          <sexe biologique="S" />
          <age>33</age>
          <adresse>Lieu de vie normal</adresse>
          <codepostal>34260</codepostal>
          <ville>LE BOUSQUET-D'ORB</ville>
      </utilisateur>
      <utilisateur identifiant="6">
          <nom>NOUVEAU</nom>
          <prenom>Super</prenom>
          <sexe biologique="H" />
          <age>20</age>
          <adresse>Rue Paradi</adresse>
          <codepostal>00000</codepostal>
          <ville>CIEUX</ville>
      </utilisateur>
  </utilisateurs>

Avec une copie et modification d'un élément existant.

.. code-block:: pycon

  >>> import xml.etree.ElementTree as ArbreXML
  >>> structurexml = ArbreXML.parse('personnes.xml')
  >>> racinexml = structurexml.getroot()
  >>> nouveau_utilisateur = ArbreXML.fromstring(ArbreXML.tostring(racinexml[0]))
  >>> nouveau_utilisateur.set('identifiant', '6')
  >>> nouveau_utilisateur.find('nom').text = "NOUVEAU"
  >>> nouveau_utilisateur.find('prenom').text = "Super"
  >>> nouveau_utilisateur.find('sexe').set('biologique', 'H')
  >>> nouveau_utilisateur.find('age').text = '20'
  >>> nouveau_utilisateur.find('adresse').text = "Rue Paradi"
  >>> nouveau_utilisateur.find('codepostal').text = '00000'
  >>> nouveau_utilisateur.find('ville').text = "CIEUX"
  >>> racinexml.append(nouveau_utilisateur)
  >>> structurexml.write('personnes6.xml', encoding='utf-8', xml_declaration=True)

Ce qui nous donne avec le fichier «**personnes5.xml**».

.. code-block:: xml

  <?xml version='1.0' encoding='utf-8'?>
  <utilisateurs>
      <utilisateur identifiant="1">
          <nom>NOM</nom>
          <prenom>Prénom</prenom>
          <sexe biologique="H" />
          <age>70</age>
          <adresse>Lieu de vie</adresse>
          <codepostal>34110</codepostal>
          <ville>FRONTIGNAN</ville>
      </utilisateur>
      <utilisateur identifiant="2">
          <nom>MONNOM</nom>
          <prenom>MonPrénom</prenom>
          <sexe biologique="F" />
          <age>40</age>
          <adresse>Mon lieu de vie</adresse>
          <codepostal>34000</codepostal>
          <ville>MONTPELLIER</ville>
      </utilisateur>
      <utilisateur identifiant="3">
          <nom>PERSONNE</nom>
          <prenom>Jesuis</prenom>
          <sexe biologique="H" social="F" />
          <age>25</age>
          <adresse>Sans lieu de vie</adresse>
          <codepostal>34200</codepostal>
          <ville>SÈTE</ville>
      </utilisateur>
      <utilisateur identifiant="4">
          <nom>UNIVERSEL</nom>
          <prenom>Divain</prenom>
          <sexe biologique="2" />
          <age>15</age>
          <adresse>Lieu sain</adresse>
          <codepostal>34130</codepostal>
          <ville>SAINT-AUNÈS</ville>
      </utilisateur>
      <utilisateur identifiant="5">
          <nom>DIFFÉRENT</nom>
          <prenom>Être</prenom>
          <sexe biologique="S" />
          <age>33</age>
          <adresse>Lieu de vie normal</adresse>
          <codepostal>34260</codepostal>
          <ville>LE BOUSQUET-D'ORB</ville>
      </utilisateur>
      <utilisateur identifiant="6">
          <nom>NOUVEAU</nom>
          <prenom>Super</prenom>
          <sexe biologique="H" />
          <age>20</age>
          <adresse>Rue Paradi</adresse>
          <codepostal>00000</codepostal>
          <ville>CIEUX</ville>
      </utilisateur>
  </utilisateurs>
