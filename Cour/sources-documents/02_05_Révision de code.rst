Révision de code
****************

Lorsque l’on développe un logiciel, ce dernier est voué à évoluer. On ne part malheureusement pas de l’idée pour aboutir immédiatement au programme fini.

Même si les spécifications sont précises, il y aura toujours de petits bugs à corriger et donc des lignes de codes seront modifiées, supprimées ou ajoutées. Mais que se passe-t-il lorsque plusieurs développeurs travaillent sur le même fichier ou programme, ou lorsqu’une correction n’en est pas une et qu’il faut revenir en arrière ?

C’est là qu’interviennent **les logiciels de gestion de versions concurrentes**, vision collective, **ou de révision de code**, vision individuelle.

-  **Git** : le standard de fait en mode décentralisé.
-  **Visualsource** : celui de Microsoft
-  Bazaar
-  Mercurial
-  dinosaures (**rcs**, **svn**) etc.


.. only:: latex

  .. raw:: latex

    \newpage


Installer un logiciel de révision de code sur le poste de développement
=======================================================================

Exercice :

Distribuer procédure installation de git voir https://openclassrooms.com/fr/courses/5641721-utilisez-git-et-github-pour-vos-projets-de-developpement/6113016-installez-git-sur-votre-ordinateur

Documentation voir https://git-scm.com/book/fr/v2

Installer git
-------------

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/1_Mode_interprété$ cd .. ; sudo apt update; sudo apt upgrade
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ sudo apt install git

Configurer git
--------------

Récupérer le fichier https://github.com/github/gitignore/blob/master/Python.gitignore et le renommer en **.gitignore** dans le répertoire :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ wget https://raw.githubusercontent.com/github/gitignore/master/Python.gitignore ; mv Python.gitignore .gitignore

Ajouter en début de fichier de **.gitignore** :

.. code-block:: bash

  # Ignore itself
  .gitignore

Mettre en place la coloration syntaxique dans git :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global color.ui auto

Définir l’utilisateur de git avec son adresse courriel :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.name "Prénom NOM"
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global user.email "utilisateur@domaine-perso.fr"

Configurer les paramètres de la sauvegarde des identifiants de connections aux dépôts distants :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git config --global http.sslVerify false
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git  config --global http.postBuffer 524288000

.. only:: html

  .. raw:: html

    <font color="Red">Distribuer un lexique sur Git «commandes git»</font>.</br>

.. only:: latex

  .. raw:: latex

    \textcolor{red}{Distribuer un lexique sur Git «commandes git»}.
    \newline

.. only:: not (html or latex)

  Distribuer un lexique sur Git «commandes git».


Initialiser le dépôt git et ajouter le fichier Python «**mon_1er_programme.py**» :

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git init
  …
  Dépôt Git vide initialisé dans /home/utilisateur/repertoire_de_developpement/.git/
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
  Sur la branche master

  Aucun commit

  Fichiers non suivis:
   (utilisez "git add <fichier>…" pour inclure dans ce qui sera validé)
   "1_Mode_interpr\303\251t\303\251/"

   aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)
   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git add .
   utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
   Sur la branche master

   Aucun commit

  Modifications qui seront validées :
   (utilisez "git rm --cached <fichier>…" pour désindexer)
   nouveau fichier : "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git commit -m "Ajout du fichier mon_1er_programme.py"
  [master (commit racine) dd36b76] Ajout du fichier mon_1er_programme.py
   1 file changed, 4 insertions(+)
   create mode 100755 "1_Mode_interpr\303\251t\303\251/mon_1er_programme.py"
  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ git status
  Sur la branche master
  rien à valider, la copie de travail est propre


.. only:: latex

  .. raw:: latex

    \newpage
