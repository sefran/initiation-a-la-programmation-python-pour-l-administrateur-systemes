.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Notions avancées en objet
*************************

Extension de classes
====================

Nous allons nous intéresser à l’extension de classes.

Imaginons que nous voulions définir une classe :python:`Admin`, pour gérer des administrateurs, qui réutiliserait le même code que la classe :python:`User`. Tout ce que nous savons faire actuellement c’est copier/coller le code de la classe :python:`User` en changeant son nom pour :python:`Admin`.

Nous allons maintenant voir comment faire ça de manière plus élégante, grâce à l’héritage. Nous étudierons de plus les relations entre classes ansi créées.

Nous utiliserons donc la classe :python:`User` suivante pour la suite de ce chapitre.

.. code-block:: python

  class User:
      """ Défini des propriétés d'un utilisateur """
      def __init__(self, id, name, password):
          """ Initialisation de l'utilisateur """
          self.id = id
          self.name = name
          self._salt = crypt.mksalt()
          self._password = self._crypt_pwd(password)

      def _crypt_pwd(self, password):
          """ Calcule un mot de passe crypté """
          return crypt.crypt(password, self._salt)

      def check_pwd(self, password):
          """ Vérifie le mot de passe """
          return self._password == self._crypt_pwd(password)

Hériter
-------

L’héritage simple est le mécanisme permettant d’étendre une unique classe. Il consiste à créer une nouvelle classe (fille) qui bénéficiera des mêmes méthodes et attributs que sa classe mère. Il sera aisé d’en définir de nouveaux dans la classe fille, et cela n’altèrera pas le fonctionnement de la mère.

Par exemple, nous voudrions étendre notre classe :python:`User` pour ajouter la possibilité d’avoir des administrateurs. Les administrateurs (:python:`Admin`) possèderaient une nouvelle méthode, :python:`manage`, pour administrer le système.

.. code-block:: python

  class Admin(User):
      """ Défini des propriétés d'un administrateur """
      def manage(self):
          """ Ajoute des fonctions d'administrateur """
          print('Je suis un Jedi!')

En plus des méthodes de la classe :python:`User` (:python:`__init__`, :python:`_crypt_pwd` et :python:`check_pwd`), Admin possède aussi une méthode :python:`manage`.

.. code-block:: pycon

  >>> root = Admin(1, 'root', 'toor')
  >>> root.check_password('toor')
  True
  >>> root.manage()
  Je suis un Jedi!
  >>> john = User(2, 'john', '12345')
  >>> john.manage()
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  AttributeError: 'User' object has no attribute 'manage'

Nous pouvons avoir deux classes différentes héritant d’une même mère

.. code-block:: python

  class Guest(User):
      pass

:python:`Admin` et :python:`Guest` sont alors deux classes filles de :python:`User`.

L’héritage simple permet aussi d’hériter d’une classe qui hérite elle-même d’une autre classe.

.. code-block:: python

  class SuperAdmin(Admin):
      pass

:python:`SuperAdmin` est alors la fille de :python:`Admin`, elle-même la fille de :python:`User`. On dit alors que :python:`User` **est une ancêtre** de :python:`SuperAdmin`.

On peut constater quels sont les parents d’une classe à l’aide de l’attribut spécial :python:`__bases__` des classes :

.. code-block:: pycon

  >>> Admin.__bases__
  (<class '__main__.User'>,)
  >>> Guest.__bases__
  (<class '__main__.User'>,)
  >>> SuperAdmin.__bases__
  (<class '__main__.Admin'>,)

Que vaudrait alors :python:`User.__bases__`, sachant que la classe :python:`User` est définie sans héritage ?

.. code-block:: pycon

  >>> User.__bases__
  (<class 'object'>,)

On remarque que, sans que nous n’ayons rien demandé, :python:`User` hérite de :python:`object`. En fait, :python:`object` est l’ancêtre de toute classe Python. Ainsi, quand aucune classe parente n’est définie, c’est :python:`object` qui est choisi.

Sous-typage
^^^^^^^^^^^

Nous avons vu que l’héritage permettait d’étendre le comportement d’une classe, mais ce n’est pas tout. L’héritage a aussi du sens au niveau des types, en créant un nouveau type compatible avec le parent.

En Python, la fonction :python:`isinstance` permet de tester si un objet est l’instance d’une certaine classe.

.. code-block:: pycon

  >>> isinstance(root, Admin)
  True
  >>> isinstance(root, User)
  True
  >>> isinstance(root, Guest)
  False
  >>> isinstance(root, object)
  True

Mais gardez toujours à l’esprit qu’en Python, on préfère se référer à la structure d’un objet qu’à son type (duck-typing), les tests à base de :python:`isinstance` sont donc à utiliser pour des cas particuliers uniquement, où il serait difficile de procéder autrement.

Redéfinition de méthodes, la surcharge
--------------------------------------

Nous savons hériter d’une classe pour y insérer de nouvelles méthodes, mais nous ne savons pas étendre les méthodes déjà présentes dans la classe mère. La redéfinition est un concept qui permet de remplacer une méthode du parent.

Nous voudrions que la classe :python:`Guest` ne possède plus aucun mot de passe. Celle-ci devra modifier la méthode :python:`check_pwd` pour accepter tout mot de passe, et simplifier la méthode :python:`__init__`.

On ne peut pas à proprement parler étendre le contenu d’une méthode, mais on peut la redéfinir :

.. code-block:: python

  class Guest(User):
  def __init__(self, id, name):
      self.id = id
      self.name = name
      self._salt = ''
      self._password = ''

  def check_pwd(self, password):
      return True

Cela fonctionne comme souhaité, mais vient avec un petit problème, le code de la méthode :python:`__init__` est répété. En l’occurrence il ne s’agit que de 2 lignes de code, mais lorsque nous voudrons apporter des modifications à la méthode de la classe :python:`User`, il faudra les répercuter sur :python:`Guest`, ce qui donne vite quelque chose de difficile à maintenir.

Heureusement, Python nous offre un moyen de remédier à ce mécanisme, :python:`super`! Oui, :python:`super`, littéralement, une fonction un peu spéciale en Python, qui nous permet d’utiliser la classe parente (superclass).

:python:`super` est une fonction qui prend initialement en paramètre une classe et une instance de cette classe. Elle retourne un objet proxy (Un proxy est un intermédiaire transparent entre deux entités) qui s’utilise comme une instance de la classe parente.

.. code-block:: pycon

  >>> guest = Guest(3, 'Guest')
  >>> guest.check_pwd('password')
  True
  >>> super(Guest, guest).check_pwd('password')
  False

Au sein de la classe en question, les arguments de :python:`super` peuvent être omis (ils correspondront à la classe et à l’instance courantes), ce qui nous permet de simplifier notre méthode :python:`__init__` et d’éviter les répétitions.

.. code-block:: python

  class Guest(User):
  def __init__(self, id, name):
      super().__init__(id, name, '')

  def check_pwd(self, password):
      return True

On notera tout de même que contrairement aux versions précédentes, l’initialisateur de :python:`User` est appelé en plus de celui de :python:`Guest`, et donc qu’un :python:`self` et un hash du mot de passe sont générés alors qu’ils ne serviront pas.

Ça n’est pas très grave dans le cas présent, mais pensez-y dans vos développements futurs, afin de ne pas exécuter d’opérations coûteuses inutilement.

Héritage Conditionnel
---------------------

Il nous arrive souvent en Python d'être bloqué, lors de la création d'une classe, parce que l'héritage est conditionné à une variable passée en paramètre de la classe.

Se présente alors deux cas :

* Le premier est un comportement de classe complètement différent avec ses méthodes et propriétés, c'est un proxy (filtre) de classes qu'il nous faudra utiliser.
* Le deuxième est un ajout de propriétés et de méthodes à la classe avec un vrai héritage conditionnel.

Nous allons voir comment résoudre cela avec la déclaration de classe :python:`def __new__()`

Proxy de classes
^^^^^^^^^^^^^^^^

Dans cet exercice, nous allons créer deux classes distinctes (:python:`A` et :python:`B`) qui suivant un paramètre passé à une métaclasse :python:`Proxy` va retourner la bonne classe.

.. code-block:: python

  class A:
      def __init__(self, mon_paramètreA):
          self.ma_propriétéA = mon_paramètreA

      def maMéthodeA(self):
          pass

  class B:
      def __init__(self, mon_paramètreB):
          self.ma_propriétéB = mon_paramètreB

      def maMéthodeB(self):
          pass

  class Proxy:
      def __new__(cls, mon_paramètre):
          if mon_paramètre == 'valeur1':
              return A(mon_paramètre)
          if mon_paramètre == 'valeur2':
              return B(mon_paramètre)

Nous voyons ici que la déclaration :python:`def __new__()` nous permet de récupérer les paramètres passés à la classe :python:`Proxy`, ce qui nous permet avec un test de renvoyer un objet créé avec la bonne classe. C'est pour cela que l'on parle de Proxy de classe. Le paramètre :python:`cls` représente la classe qui a besoin d'être instanciée.

On peut bien sur changer les conditions de filtrage suivant le type de test que l'on veut, ou augmenter le nombre de classes en option. Mais ici dans cette section nous parlons d'héritage de classe, nous allons voir avec ce procédé comment créer un héritage conditionnel.

Héritage conditionnel
^^^^^^^^^^^^^^^^^^^^^

Nous introduisons ici un nouveau concept pour les héritages, l'héritage conditionnel de classes :python:`ClasseAHeriter if mon_paramètre == 'Valeur' else object`. La difficulté de la solution vient du fait que la variable passée à la classe ne peut être récupérée avant l'héritage de classe, :python:`mon_paramètre` doit donc être :python:`global` pour que la condition d'héritage fonctionne. Avec le proxy nous outrepassons cette limite de façon élégante…

Ici dans cet exercice, nous allons créer un héritage conditionnel de la classe :python:`A` dans la classe :python:`B` suivant un paramètre passé au travers de la classe proxy :python:`ProxyHeritage`.

.. code-block:: python

  class A:
      def __init__(self, mon_paramètreA):
          self.ma_propriétéA = mon_paramètreA

      def maMéthodeA(self):
          pass

  class ProxyHeritage:
      def __new__(cls, mon_paramètre):
          class B(A if mon_paramètre == 'Valeur' else object):
              def __init__(self, mon_paramètreB):
                  self.ma_propriétéB = mon_paramètreB

              def maMéthodeB(self):
                  pass
          return B(mon_paramètre)


Cette solution n'est pas satisfaisante. En effet si on veut hériter la classe :python:`ProxyHeritage` le comportement de gestion des héritages de classes normal de Python n'est plus possible. Nous verrons plus loin dans ce cour comment faire.

Tout ceci nous permet maintenant d'introduire la section suivante.

Héritages Multiples
-------------------

Avec l’héritage simple, nous pouvions étendre le comportement d’une classe. L’héritage multiple va nous permettre de le faire pour plusieurs classes à la fois. Il nous suffit de préciser plusieurs classes séparées par des virgules lors de la création de notre classe fille.

.. code-block:: python

  class A:
      def foo(self):
          return '!'

  class B:
      def bar(self):
          return '?'

  class C(A, B):
      pass

Notre classe :python:`C` a donc deux mères : :python:`A` et :python:`B`. Cela veut aussi dire que les objets de type :python:`C` possèdent à la fois les méthodes :python:`foo` et :python:`bar`.

.. code-block:: pycon

  >>> c = C()
  >>> c.foo()
  '!'
  >>> c.bar()
  '?'

Ordre d’héritage
^^^^^^^^^^^^^^^^

L’ordre dans lequel on hérite des parents est important, il détermine dans quel ordre les méthodes seront recherchées dans les classes mères.
Ainsi, dans le cas où la méthode existe dans plusieurs parents, celle de la première classe sera conservée.

.. code-block:: python

  class A:
      def foo(self):
          return '!'

  class B:
      def foo(self):
          return '?'

  class C(A, B):
      pass

  class D(B, A):
      pass

.. code-block:: pycon

  >>> C().foo()
  '!'
  >>> D().foo()
  '?'

Cet ordre dans lequel les classes parentes sont explorées pour la recherche des méthodes est appelé **Method Resolution Order (MRO)**. On peut le connaître à l’aide de la méthode :python:`mro` des classes.

.. code-block:: pycon

  >>> A.mro()
  [<class '__main__.A'>, <class 'object'>]
  >>> B.mro()
  [<class '__main__.B'>, <class 'object'>]
  >>> C.mro()
  [<class '__main__.C'>, <class '__main__.A'>, <class '__main__.B'>, <class 'object'>]
  >>> D.mro()
  [<class '__main__.D'>, <class '__main__.B'>, <class '__main__.A'>, <class 'object'>]

C’est aussi ce MRO qui est utilisé par super pour trouver à quelle classe faire appel. super se charge d’explorer le MRO de la classe de l’instance qui lui est donnée en second paramètre, et de retourner un proxy sur la classe juste à droite de celle donnée en premier paramètre.

Ainsi, avec :python:`c` une instance de :python:`C`, :python`super(C, c)` retournera un objet se comportant comme une instance de :python:`A`, :python:`super(A, c)` comme une instance de :python:`B`, et :python:`super(B, c)` comme une instance de :python:`object`.

.. code-block:: pycon

  >>> c = C()
  >>> c.foo() # C.foo == A.foo
  '!'
  >>> super(C, c).foo() # A.foo
  '!'
  >>> super(A, c).foo() # B.foo
  '?'
  >>> super(B, c).foo() # object.foo -> méthode introuvable
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  AttributeError: 'super' object has no attribute 'foo'

Les classes parentes n’ont alors pas besoin de se connaître les unes les autres pour se référencer.

.. code-block:: python

  class A:
      def __init__(self):
          print("Début initialisation d'un objet de type A")
          super().__init__()
          print("Fin initialisation d'un objet de type A")

  class B:
      def __init__(self):
          print("Début initialisation d'un objet de type B")
          super().__init__()
          print("Fin initialisation d'un objet de type B")

  class C(A, B):
      def __init__(self):
          print("Début initialisation d'un objet de type C")
          super().__init__()
          print("Fin initialisation d'un objet de type C")

  class D(B, A):
      def __init__(self):
          print("Début initialisation d'un objet de type D")
          super().__init__()
          print("Fin initialisation d'un objet de type D")

.. code-block:: pycon

  >>> C()
  Début initialisation d'un objet de type C
  Début initialisation d'un objet de type A
  Début initialisation d'un objet de type B
  Fin initialisation d'un objet de type B
  Fin initialisation d'un objet de type A
  Fin initialisation d'un objet de type C
  <__main__.C object at 0x7f0ccaa970b8>
  >>> D()
  Début initialisation d'un objet de type D
  Début initialisation d'un objet de type B
  Début initialisation d'un objet de type A
  Fin initialisation d'un objet de type A
  Fin initialisation d'un objet de type B
  Fin initialisation d'un objet de type D
  <__main__.D object at 0x7f0ccaa971d0>

La méthode :python:`__init__` des classes parentes n’est pas appelée automatiquement, et l’appel doit donc être réalisé explicitement.

C’est ainsi le :python:`super().__init__()` présent dans la classe :python:`C` qui appelle l’initialiseur de la classe :python:`A`, qui appelle lui-même celui de la classe :python:`B`. Inversement, pour la classe :python:`D`, :python:`super().__init__()` appelle l’initialiseur de :python:`B` qui appelle celui de :python:`A`.

On notera que les exemple donnés n’utilisent jamais plus de deux classes mères, mais il est possible d’en avoir autant que vous le souhaitez.

.. code-block:: python

  class A:
      pass

  class B:
      pass

  class C:
      pass

  class D:
      pass

  class E(A, B, C, D):
      pass

Mixins
^^^^^^

Les **mixins** sont des classes dédiées à une fonctionnalité particulière, utilisable en héritant d’une classe de base et de ce mixin.

Par exemple, plusieurs types que l’on connaît sont appelés séquences (:python:`str`, :python:`list`, :python:`tuple`). Ils ont en commun le fait d’implémenter l’opérateur :python:`[]` et de gérer le slicing. On peut ainsi obtenir l’objet en ordre inverse à l’aide de :python:`obj[::-1]`.

Un mixin qui pourrait nous être utile serait une classe avec une méthode :python:`reverse` pour nous retourner l’objet inversé.

.. code-block:: python

  class Reversable:
      def reverse(self):
          return self[::-1]

  class ReversableStr(Reversable, str):
      pass

  class ReversableTuple(Reversable, tuple):
      pass

.. code-block:: pycon

  >>> s = ReversableStr('abc')
  >>> s
  'abc'
  >>> s.reverse()
  'cba'
  >>> ReversableTuple((1, 2, 3)).reverse()
  (3, 2, 1)

Ou encore nous pourrions vouloir ajouter la gestion d’une photo de profil à nos classes :python:`User` et dérivées.

.. code-block:: python

  class ProfilePicture:
      def __init__(self, *args, **kwargs):
          super().__init__(*args, **kwargs)
          self.picture = '{}-{}.png'.format(self.id, self.name)

  class UserPicture(ProfilePicture, User):
      pass

  class AdminPicture(ProfilePicture, Admin):
      pass

  class GuestPicture(ProfilePicture, Guest):
      pass

.. code-block:: pycon

  >>> john = UserPicture(1, 'john', '12345')
  >>> john.picture
  '1-john.png'

Exercice : Fils de discussion

Vous vous souvenez de la classe :python:`Post` pour représenter un message ? Nous aimerions maintenant pouvoir instancier des fils de discussion (:python:`Thread`) sur notre forum.

Qu’est-ce qu’un fil de discussion ?

Un message associé à un auteur et à une date ;

Mais qui comporte aussi un titre ;

Et une liste de posts (les réponses).

Le premier point indique clairement que nous allons réutiliser le code de la classe :python:`Post`, donc en hériter.

Notre nouvelle classe sera initialisée avec un titre, un auteur et un message. :python:`Thread` sera dotée d’une méthode :python:`answer` recevant un auteur et un texte, et s’occupant de créer le post correspondant et de l’ajouter au fil. Nous changerons aussi la méthode :python:`format` du :python:`Thread` afin qu’elle concatène au fil l’ensemble de ses réponses.

La classe :python:`Post` restera inchangée. Enfin, nous supprimerons la méthode :python:`post` de la classe :python:`User`, pour lui en ajouter deux nouvelles :

* :python:`new_thread(title, message)` pour créer un nouveau fil de discussion associé à cet utilisateur ;
* :python:`answer_thread(thread, message)` pour répondre à un fil existant.

.. code-block:: python

  import crypt
  import datetime

  class User:
      def __init__(self, id, name, password):
          self.id = id
          self.name = name
          self._salt = crypt.mksalt()
          self._password = self._crypt_pwd(password)

      def _crypt_pwd(self, password):
          return crypt.crypt(password, self._salt)

      def check_pwd(self, password):
          return self._password == self._crypt_pwd(password)

      def new_thread(self, title, message):
          return Thread(title, self, message)

      def answer_thread(self, thread, message):
          thread.answer(self, message)

  class Post:
      def __init__(self, author, message):
          self.author = author
          self.message = message
          self.date = datetime.datetime.now()

      def format(self):
          date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
          return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

  class Thread(Post):
      def __init__(self, title, author, message):
          super().__init__(author, message)
          self.title = title
          self.posts = []

      def answer(self, author, message):
          self.posts.append(Post(author, message))

      def format(self):
          posts = [super().format()]
          posts += [p.format() for p in self.posts]
          return '\n'.join(posts)

  if __name__ == '__main__':
      john = User(1, 'john', '12345')
      peter = User(2, 'peter', 'toto')
      thread = john.new_thread('Bienvenue', 'Bienvenue à tous')
      peter.answer_thread(thread, 'Merci')
      print(thread.format())


L'héritage conditionnel
=======================

Nous revenons ici sur le traitement des héritages conditionnels déjà abordés. 

Avec init()
-----------

Pour optimiser notre code, et la réutilisation de classes, nous voulons pouvoir avoir un héritage conditionnel lors de l'initialisation de nos objets. Nous voulons donc utiliser la méthode :python:`__init__` pour le faire. Nous allons aussi avoir la possibilité de supprimer une méthode héritée avec des paramètres passés à la classe.

Voici un exemple de comment le faire avec le fichier «**heritageconditionnel.py**» :

.. code-block:: python

  class Pere():
      def __init__(self, **kwargs):
          self.mapropriété_père = 'mapropriété_père'
          if 'paramètrePère' in kwargs and kwargs['paramètrePère']:
              self.mapropriété_conditionnellepère = 'mapropriété_conditionnellepère'
              def ma_Methode_Conditionnelle_Père():
                  return self.mapropriété_conditionnellepère
              self.ma_Methode_Conditionnelle_Père = ma_Methode_Conditionnelle_Père

      def ma_Methode_Père(self):
          return self.mapropriété_père


  class Enfant(Pere):
      def __init__(self, **kwargs):
          Pere.__init__(Pere, **kwargs)
          if 'paramètreEnfant' in kwargs and kwargs['paramètreEnfant']:
              self.mapropriété_conditionnelleenfant = 'mapropriété_conditionnelleenfant'
              def ma_Methode_Conditionnelle_Enfant():
                  return self.mapropriété_conditionnelleenfant
              self.ma_Methode_Conditionnelle_Enfant = ma_Methode_Conditionnelle_Enfant
          class new_parent(Pere.__base__):
              pass
          parent_list = dir(Pere)
          new_parent_list = dir(new_parent)
          if 'paramètrePère' in kwargs and kwargs['paramètrePère']:
              methodes = set(parent_list) - set(new_parent_list)
          else:
              methodes = set(parent_list) - set(new_parent_list) - {'mapropriété_conditionnellepère', 'ma_Methode_Conditionnelle_Père'}
          if 'banieméthodes' in kwargs and kwargs['banieméthodes'] != []:
              for methode in methodes:
                  if methode not in kwargs['banieméthodes']:
                      setattr(new_parent, methode, Pere.__getattribute__(Pere, methode))
          else:
              for methode in methodes:
                  setattr(new_parent, methode, Pere.__getattribute__(Pere, methode))
          Enfant.__bases__ = (new_parent, )
          self.mapropriété_enfant = 'mapropriété_enfant'

      def ma_Methode_Enfant(self):
          return self.mapropriété_enfant


  class PetitEnfant(Enfant):
      def __init__(self, paramètrePetitEnfant=False, paramètreEnfant=False, options=[], paramètrePère=False):
          super().__init__(paramètreEnfant=paramètreEnfant, banieméthodes=options, paramètrePère=paramètrePère)
          self.mapropriétépetitenfant = 'mapropriétépetitenfant'
          if paramètrePetitEnfant:
              self.mapropriété_conditionnellepetitenfant = 'mapropriété_conditionnellepetitenfant'
              def ma_Methode_Conditionnelle_PetitEnfant():
                  return self.mapropriété_conditionnellepetitenfant
              self.ma_Methode_Conditionnelle_PetitEnfant = ma_Methode_Conditionnelle_PetitEnfant

      def ma_Methode_PetitEnfant(self):
          return self.mapropriétépetitenfant

  if __name__ == '__main__':
      class Vide():
          pass
      vide = Vide()
      vide_list = set(dir(vide))
      print('Pere()')
      a = Pere()
      a_list = set(dir(a))
      print(str(a_list - vide_list))
      print('Pere(paramètrePère=True)')
      a = Pere(paramètrePère=True)
      a_list = set(dir(a))
      print(str(a_list - vide_list))
      print('Enfant()')
      b = Enfant()
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(paramètrePère=True)')
      b = Enfant(paramètrePère=True)
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(banieméthodes=[\'ma_Methode_Père\'])')
      b = Enfant(banieméthodes=['ma_Methode_Père'])
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(paramètreEnfant=True)')
      b = Enfant(paramètreEnfant=True)
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(paramètreEnfant=True, paramètrePère=True)')
      b = Enfant(paramètreEnfant=True, paramètrePère=True)
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=[\'ma_Methode_Père\'])')
      b = Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=['ma_Methode_Père'])
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('PetitEnfant()')
      c = PetitEnfant()
      c_list = set(dir(c))
      print(str(c_list - vide_list))
      print('PetitEnfant(paramètrePère=True)')
      c = PetitEnfant(paramètrePère=True)
      c_list = set(dir(c))
      print(str(c_list - vide_list))
      print('PetitEnfant(options=[\'ma_Methode_Père\']')
      c = PetitEnfant(options=['ma_Methode_Père'])
      c_list = set(dir(c))
      print(str(c_list - vide_list))
      print('PetitEnfant(paramètreEnfant=True)')
      c = Enfant(paramètreEnfant=True)
      c_list = set(dir(c))
      print(str(c_list - vide_list))
      print('PetitEnfant(paramètrePetitEnfant=True)')
      c = PetitEnfant(paramètrePetitEnfant=True)
      c_list = set(dir(c))
      print(str(c_list - vide_list))


Ce qui nous donne en sortie d'exécution

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ python3 ./heritageconditionnel.py
  Pere()
  {'ma_Methode_Père', 'mapropriété_père'}
  Pere(paramètrePère=True)
  {'mapropriété_conditionnellepère', 'ma_Methode_Père', 'mapropriété_père', 'ma_Methode_Conditionnelle_Père'}
  Enfant()
  {'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_enfant', 'mapropriété_père'}
  Enfant(paramètrePère=True)
  {'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_père', 'ma_Methode_Conditionnelle_Père', 'mapropriété_conditionnellepère', 'mapropriété_enfant'}
  Enfant(banieméthodes=['ma_Methode_Père'])
  {'ma_Methode_Enfant', 'mapropriété_enfant', 'mapropriété_père'}
  Enfant(paramètreEnfant=True)
  {'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_père', 'mapropriété_conditionnelleenfant', 'mapropriété_enfant', 'ma_Methode_Conditionnelle_Enfant'}
  Enfant(paramètreEnfant=True, paramètrePère=True)
  {'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_père', 'ma_Methode_Conditionnelle_Père', 'mapropriété_conditionnelleenfant', 'mapropriété_conditionnellepère', 'mapropriété_enfant', 'ma_Methode_Conditionnelle_Enfant'}
  Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=['ma_Methode_Père'])
  {'ma_Methode_Enfant', 'mapropriété_père', 'ma_Methode_Conditionnelle_Père', 'mapropriété_conditionnelleenfant', 'mapropriété_conditionnellepère', 'mapropriété_enfant', 'ma_Methode_Conditionnelle_Enfant'}
  PetitEnfant()
  {'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_père', 'mapropriétépetitenfant', 'mapropriété_enfant', 'ma_Methode_PetitEnfant'}
  PetitEnfant(paramètrePère=True)
  {'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_père', 'ma_Methode_Conditionnelle_Père', 'mapropriété_conditionnellepère', 'mapropriétépetitenfant', 'mapropriété_enfant', 'ma_Methode_PetitEnfant'}
  PetitEnfant(options=['ma_Methode_Père']
  {'ma_Methode_Enfant', 'mapropriété_père', 'mapropriétépetitenfant', 'mapropriété_enfant', 'ma_Methode_PetitEnfant'}
  PetitEnfant(paramètreEnfant=True)
  {'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_père', 'mapropriété_conditionnelleenfant', 'mapropriété_enfant', 'ma_Methode_Conditionnelle_Enfant'}
  PetitEnfant(paramètrePetitEnfant=True)
  {'ma_Methode_Enfant', 'ma_Methode_Père', 'mapropriété_conditionnellepetitenfant', 'mapropriété_père', 'mapropriétépetitenfant', 'mapropriété_enfant', 'ma_Methode_Conditionnelle_PetitEnfant', 'ma_Methode_PetitEnfant'}


Avec new()
----------

Chaque fois qu'une classe est instanciée, les méthodes :python:`__new__` et :python:`__init__` sont appelées. 

On sait déjà que la méthode :python:`__init__` sera appelée pour initialiser l'objet. La méthode :python:`__new__` sera elle appelée lors de la création d'un objet, son instanciation. 

Dans l'objet de classe de base, la méthode :python:`__new__` est définie comme une méthode statique qui nécessite de passer un paramètre :python:`cls`. :python:`cls` représente la classe qui doit être instanciée et le compilateur fournit automatiquement ce paramètre au moment de l'instanciation.

Nous pouvons alors modifier directement la classe :python:`cls`, mais les modifications seront prises en compte par tous les objets actifs l'utilisant. Pour éviter cela nous travaillerons donc directement avec l'instance de la classe que l'on obtiendra avec :python:`super(MaClasse, cls).__new__(cls, *args, **kwargs)`.

Nous allons donc filtrer l'héritage de nos classes sur l'instance de l'objet avec la méthode :python:`__new__`. Comme précédemment nous allons aussi supprimer une méthode héritée.

Voici un exemple de comment le faire avec le fichier «**heritageconditionnelnew.py**» :

.. code-block:: python

  class Pere():
      def __new__(cls, *args, **kwargs):
          newclass = super(Pere, cls).__new__(cls)
          if 'paramètrePère' in kwargs.keys():
              if kwargs['paramètrePère']:
                  setattr(newclass, 'mapropriété_conditionnellepère', None)
                  def ma_Methode_Conditionnelle_Père():
                      return newclass.mapropriété_conditionnellepère
                  setattr(newclass, ma_Methode_Conditionnelle_Père.__name__, ma_Methode_Conditionnelle_Père)
              del kwargs['paramètrePère']
          if 'banieméthodes' in kwargs and 'ma_Methode_Père' in kwargs['banieméthodes']:
              setattr(newclass, 'mapropriété_père', 'mapropriété_père')
          else:
              setattr(newclass, 'mapropriété_père', 'mapropriété_père')
              def ma_Methode_Père(self):
                  return newclass.mapropriété_père
              setattr(newclass, ma_Methode_Père.__name__, ma_Methode_Père)
          return newclass
      def __init__(self, *args, **kwargs):
          pass


  class Enfant(Pere):
      def __new__(cls, *args, **kwargs):
          newclass = super(Enfant, cls).__new__(cls, *args, **kwargs)
          if 'paramètreEnfant' in kwargs.keys():
              if kwargs['paramètreEnfant']:
                  setattr(newclass, 'mapropriété_conditionnelleenfant', None)
                  def ma_Methode_Conditionnelle_Enfant():
                      return newclass.mapropriété_conditionnelleenfant
                  setattr(newclass, ma_Methode_Conditionnelle_Enfant.__name__, ma_Methode_Conditionnelle_Enfant)
              del kwargs['paramètreEnfant']
          return newclass
      def __init__(self, *args, **kwargs):
          super().__init__(**kwargs)
          self.mapropriété_enfant = None
      def ma_Methode_Enfant(self):
          pass


  class PetitEnfant(Enfant):
      def __new__(cls, *args, **kwargs):
          newclass = super(PetitEnfant, cls).__new__(cls, *args, **kwargs)
          if 'paramètrePetitEnfant' in kwargs.keys():
              if kwargs['paramètrePetitEnfant']:
                  setattr(newclass, 'mapropriété_conditionnellepetitenfant', None)
                  def ma_Methode_Conditionnelle_PetitEnfant():
                      return newclass.mapropriété_conditionnellepetitenfant
                  setattr(newclass, ma_Methode_Conditionnelle_PetitEnfant.__name__, ma_Methode_Conditionnelle_PetitEnfant)
              del kwargs['paramètrePetitEnfant']
          return newclass
      def __init__(self, **kwargs):
          self.mapropriétépetitenfant = None
          super().__init__()
      def ma_Methode_PetitEnfant(self):
          pass

  if __name__ == '__main__':
      class Vide():
          pass
      vide = Vide()
      vide_list = set(dir(vide))
      print('Pere()')
      a = Pere()
      a_list = set(dir(a))
      print(str(a_list - vide_list))
      print('Pere(paramètrePère=True)')
      a = Pere(paramètrePère=True)
      a_list = set(dir(a))
      print(str(a_list - vide_list))
      print('Enfant()')
      b = Enfant()
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(paramètrePère=True)')
      b = Enfant(paramètrePère=True)
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(banieméthodes=[\'ma_Methode_Père\'])')
      b = Enfant(banieméthodes=['ma_Methode_Père'])
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(paramètreEnfant=True)')
      b = Enfant(paramètreEnfant=True)
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(paramètreEnfant=True, paramètrePère=True)')
      b = Enfant(paramètreEnfant=True, paramètrePère=True)
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=[\'ma_Methode_Père\'])')
      b = Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=['ma_Methode_Père'])
      b_list = set(dir(b))
      print(str(b_list - vide_list))
      print('PetitEnfant()')
      c = PetitEnfant()
      c_list = set(dir(c))
      print(str(c_list - vide_list))
      print('PetitEnfant(paramètrePère=True)')
      c = PetitEnfant(paramètrePère=True)
      c_list = set(dir(c))
      print(str(c_list - vide_list))
      print('PetitEnfant(banieméthodes=[\'ma_Methode_Père\']')
      c = PetitEnfant(banieméthodes=['ma_Methode_Père'])
      c_list = set(dir(c))
      print(str(c_list - vide_list))
      print('PetitEnfant(paramètreEnfant=True)')
      c = Enfant(paramètreEnfant=True)
      c_list = set(dir(c))
      print(str(c_list - vide_list))
      print('PetitEnfant(paramètrePetitEnfant=True)')
      c = PetitEnfant(paramètrePetitEnfant=True)
      c_list = set(dir(c))
      print(str(c_list - vide_list))


Ce qui nous donne en sortie d'exécution

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/9_objets$ python3 ./heritageconditionnelnew.py
  Pere()
  {'mapropriété_père', 'ma_Methode_Père'}
  Pere(paramètrePère=True)
  {'mapropriété_père', 'mapropriété_conditionnellepère', 'ma_Methode_Conditionnelle_Père', 'ma_Methode_Père'}
  Enfant()
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'mapropriété_père', 'ma_Methode_Père'}
  Enfant(paramètrePère=True)
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'mapropriété_conditionnellepère', 'ma_Methode_Conditionnelle_Père', 'mapropriété_père', 'ma_Methode_Père'}
  Enfant(banieméthodes=['ma_Methode_Père'])
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'mapropriété_père'}
  Enfant(paramètreEnfant=True)
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'ma_Methode_Conditionnelle_Enfant', 'mapropriété_père', 'ma_Methode_Père', 'mapropriété_conditionnelleenfant'}
  Enfant(paramètreEnfant=True, paramètrePère=True)
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'ma_Methode_Conditionnelle_Enfant', 'mapropriété_conditionnellepère', 'ma_Methode_Conditionnelle_Père', 'mapropriété_père', 'ma_Methode_Père', 'mapropriété_conditionnelleenfant'}
  Enfant(paramètreEnfant=True, paramètrePère=True, banieméthodes=['ma_Methode_Père'])
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'ma_Methode_Conditionnelle_Enfant', 'mapropriété_conditionnellepère', 'ma_Methode_Conditionnelle_Père', 'mapropriété_père', 'mapropriété_conditionnelleenfant'}
  PetitEnfant()
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'ma_Methode_PetitEnfant', 'mapropriété_père', 'ma_Methode_Père', 'mapropriétépetitenfant'}
  PetitEnfant(paramètrePère=True)
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'mapropriété_conditionnellepère', 'ma_Methode_Conditionnelle_Père', 'ma_Methode_PetitEnfant', 'mapropriété_père', 'ma_Methode_Père', 'mapropriétépetitenfant'}
  PetitEnfant(banieméthodes=['ma_Methode_Père']
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'ma_Methode_PetitEnfant', 'mapropriété_père', 'mapropriétépetitenfant'}
  PetitEnfant(paramètreEnfant=True)
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'ma_Methode_Conditionnelle_Enfant', 'mapropriété_père', 'ma_Methode_Père', 'mapropriété_conditionnelleenfant'}
  PetitEnfant(paramètrePetitEnfant=True)
  {'mapropriété_enfant', 'ma_Methode_Enfant', 'ma_Methode_Conditionnelle_PetitEnfant', 'ma_Methode_PetitEnfant', 'mapropriété_père', 'ma_Methode_Père', 'mapropriétépetitenfant', 'mapropriété_conditionnellepetitenfant'}


Avec une metaclass
------------------

En Python 3 tout est objet et tous les objets ont un type (comme int, str, float, list, dict, etc.). Pour obtenir ce type on utilise la commande :python:`type()`. Les classes sont aussi des objets, par conséquent une classe doit avoir un type. Quel est le type d'une classe ?

.. code-block:: pycon

  >>> class MaClasse():
  ...     pass
  ... 
  >>> type(MaClasse)
  <class 'type'>

Il est alors exact de faire référence au lien entre le type d'un objet et sa classe. Une métaclasse est la classe d'une classe. Une classe définit le comportement d'une instance de la classe, c'est-à-dire un objet, tandis qu'une métaclasse définit le comportement d'une classe, c'est donc le type de l'objet. Une classe est une instance d'une métaclasse, son type.

Une métaclasse est donc un objet :python:`type`. Et pour construire sa métaclasse il faut hériter de cet objet, comme nos classes d'objet héritent de l'objet :python:`object`.

Dans notre exemple l'utilisation d'une métaclasse c'est lorsque l'on veut que le filtrage et l'héritage des classes s'effectuent au niveau de l'écriture de la classe. C'est au niveau de la classe elle même que s'appliquera le filtrage (plus au niveau des paramètres d'instanciation de l'objet avec la classe').

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf8 -*-

  class MetaTest(type):
      """ Classe Meta de test """
      def __new__(cls, name, bases, namespace, **kwargs):
          """ Set class MetaTest """
          print('new cls: %s' % cls)
          print('new name: %s' % name)
          if bases:
              print('new bases: %s' % bases)
          print('new namespace: %s' % namespace)
          print('new kwargs: %s' % kwargs)

          setattribute = False
          setmethod = False
          if 'settest' in kwargs.keys():
              if kwargs['settest']:
                  setattribute = True
                  setmethod = True
              del kwargs['settest']
          if 'setattribute' in kwargs.keys():
              if kwargs['setattribute']:
                  setattribute = True
              del kwargs['setattribute']
          if 'setmethod' in kwargs.keys():
              if kwargs['setmethod']:
                  setmethod = True
              del kwargs['setmethod']

          newclass = super(MetaTest, cls).__new__(cls, name, bases, namespace, **kwargs)

          if setattribute:
              setattr(newclass, 'my_property', None)

          if setmethod:
              def info(self):
                  return 'My method info'
              setattr(newclass, info.__name__, info)

          if setmethod and setattribute:
              setattr(newclass, 'my_method', None)
              def set_method(self, value):
                  self.my_method = value
              def get_method(self):
                  return self.my_method
              setattr(newclass, set_method.__name__, set_method)
              setattr(newclass, get_method.__name__, get_method)

          return newclass

      def __init__(cls, name, bases, namespace, **kwargs):
          """ Initialize metaclass MetaTest """
          super().__init__(name, bases, namespace)
          print('init cls: %s' % cls)
          print('init name: %s' % name)
          if bases:
              print('init bases: %s' % bases)
          print('init namespace: %s' % namespace)
          print('init kwargs: %s' % kwargs)


  class A(metaclass=MetaTest):
      pass

  class B(metaclass=MetaTest, settest=False):
      pass

  class C(metaclass=MetaTest, settest=True):
      pass

  class D(metaclass=MetaTest, setattribute=False):
      pass

  class E(metaclass=MetaTest, setattribute=True):
      pass

  class F(metaclass=MetaTest, setmethod=False):
      pass

  class G(metaclass=MetaTest, setmethod=True):
      pass

  print('A: %s' % dir(A))
  print('B: %s' % dir(B))
  print('C: %s' % dir(C))
  print('D: %s' % dir(D))
  print('E: %s' % dir(E))
  print('F: %s' % dir(F))
  print('G: %s' % dir(G))

  a = A()
  b = B()
  c = C()
  d = D()
  e = E()
  f = F()
  g = G()

  print('a: %s' % dir(a))
  print('b: %s' % dir(b))
  print('c: %s' % dir(c))
  print('c.my_property: %s' % c.my_property)
  c.my_property = 'Property value'
  print('c.my_property: %s' % c.my_property)
  print('c.get_method(): %s' % c.get_method())
  c.set_method('Method value')
  print('c.get_method(): %s' % c.get_method())
  print('d: %s' % dir(d))
  print('e: %s' % dir(e))
  print('e.my_property: %s' % e.my_property)
  e.my_property = 'Property value'
  print('e.my_property: %s' % e.my_property)
  print('f: %s' % dir(f))
  print('g: %s' % dir(g))
  print('g.info(): %s' % g.info())


Opérateurs
==========

Il est maintenant temps de nous intéresser aux opérateurs du langage Python (:python:`+`, :python:`-`, :python:`*`, etc.). En effet, un code respectant la philosophie du langage se doit de les utiliser à bon escient.

Ils sont une manière claire de représenter des opérations élémentaires (addition, concaténation, …) entre deux objets. :python:`a + b` est en effet plus lisible qu’un :python:`add(a, b)` ou encore :python:`a.add(b)`.

Ce chapitre a pour but de vous présenter les mécanismes mis en jeu par ces différents opérateurs, et la manière de les implémenter. Les opérateurs sont un autre type de méthodes spéciales que nous découvrirons dans cette section.

En effet, les opérateurs ne sont rien d’autres en Python que des fonctions, qui s’appliquent sur leurs opérandes. On peut s’en rendre compte à l’aide du module operator, qui répertorie les fonctions associées à chaque opérateur.

.. code-block:: pycon

  >>> import operator
  >>> operator.add(5, 6)
  11
  >>> operator.mul(2, 3)
  6

Ainsi, chacun des opérateurs correspondra à une méthode de l’opérande de gauche, qui recevra en paramètre l’opérande de droite.

Opérateurs arithmétiques
------------------------

L’addition, par exemple, est définie par la méthode :python:`__add__`.

.. code-block:: pycon

  >>> class A:
  ... def \__add__(self, other):
  ... return other # on considère self comme 0
  ...
  >>> A() + 5
  5

Assez simple, n’est-il pas ? Mais nous n’avons pas tout à fait terminé. Si la méthode est appelée sur l’opérande de gauche, que se passe-t-il quand notre objet se trouve à droite ?

.. code-block:: pycon

  >>> 5 + A()
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  TypeError: unsupported operand type(s) for +: 'int' and 'A'

Nous ne supportons pas cette opération. En effet, l’expression fait appel à la méthode :python:`int.__add__` qui ne connaît pas les objets de type :python:`A`.
Heureusement, ce cas a été prévu et il existe une fonction inverse, :python:`__radd__`, appelée si la première opération n’était pas supportée.

.. code-block:: pycon

  >>> class A:
  ... def __add__(self, other):
  ... return other
  ... def __radd__(self, other):
  ... return other
  ...
  >>> A() + 5
  5
  >>> 5 + A()
  5

Il faut bien noter que :python:`A.__radd__` ne sera appelée que si :python:`int.__add__` a échoué.

Les autres opérateurs arithmétques binaires auront un comportement similaire, voici une liste des méthodes à implémenter pour chacun d’eux :

* Addition/Concaténation **(a + b)** — :python:`__add__`, :python:`__radd__`
* Soustraction/Différence **(a - b)** — :python:`__sub__`, :python:`__rsub__`
* Multiplication **(a \* b)** — :python:`__mul__`, :python:`__rmul__`
* Division **(a / b)** — :python:`__truediv__`, :python:`__rtruediv__`
* Division entière **(a // b)** — :python:`__floordiv__`, :python:`__rfloordiv__`
* Modulo/Formattage **(a % b)** — :python:`__mod__`, :python:`__rmod__`
* Exponentiation **(a \*\* b)** — :python:`__pow__`, :python:`__rpow__`

On remarque aussi que chacun de ces opérateurs arithmétiques possède une version simplifiée pour l’assignation **(a += b)** qui correspond à la méthode :python:`__iadd__`. Par défaut, les méthodes :python:`__add__` :python:`__radd__` sont appelées, mais définir :python:`__iadd__` permet d’avoir un comportement différent dans le cas d’un opérateur d’assignation, par exemple sur les listes :

.. code-block:: pycon

  >>> l = [1, 2, 3]
  >>> l2 = l
  >>> l2 = l2 + [4]
  >>> l2
  [1, 2, 3, 4]
  >>> l
  [1, 2, 3]
  >>> l2 = l
  >>> l2 += [4]
  >>> l2
  [1, 2, 3, 4]
  >>> l
  [1, 2, 3, 4]

Opérateurs arithmétiques unaires
--------------------------------

Voyons maintenant les **opérateurs unaires**, qui ne prennent donc pas d’autre paramètre que :python:`self`.

* Opposé **(-a)** — :python:`__neg__`
* Positif **(+a)** - :python:`__pos__`
* Valeur abosule **(abs(a))** — :python:`__abs__`

Opérateurs de comparaison
-------------------------

De la même manière que pour les opérateurs arithmétiques et unaires, nous avons une méthode spéciale par opérateur de comparaison. Ces opérateurs s’appliqueront sur l’opérande gauche en recevant le droite en paramètre. Ils devront retourner un booléen.

Contrairement aux opérateurs arithmétiques, il n’est pas nécessaire d’avoir deux versions pour chaque opérateur puisque Python saura directement quelle opération inverse tester si la première a échoué (:python:`a == b` est équivalent à :python:`b == a`, :python:`a < b` à :python:`b > a`, etc.).

* Égalité **(a == b)** — :python:`__eq__`
* Différence **(a != b)** — :python:`__neq__`
* Stricte infériorité **(a < b)** — :python:`__lt__`
* Infériorité **(a <= b)** — :python:`__le__`
* Stricte supériorité **(a > b)** — :python:`__gt__`
* Supériorité **(a >= b)** — :python:`__ge__`

On notera aussi que beaucoup de ces opérateurs peuvent s’inférer les uns les autres. Par exemple, il suffit de savoir calculer :python:`a == b` et :python:`a < b` pour définir toutes les autres opérations. Ainsi, Python dispose d’un décorateur, :python:`total_ordering` du module :python:`functools`, pour automatiquement générer les opérations manquantes.

.. code-block:: pycon

  >>> from functools import total_ordering
  >>> @total_ordering
  ... class Inferior:
  ...     def __eq__(self, other):
  ...         return False
  ...     def __lt__(self, other):
  ...         return True
  ...
  >>> i = Inferior()
  >>> i == 5
  False
  >>> i > 5
  False
  >>> i < 5
  True
  >>> i <= 5
  True
  >>> i != 5
  True

Autres opérateurs
-----------------

Nous avons ici étudié les principaux opérateurs du langage. Ces listes ne sont pas exhaustives et présentent juste la méthodologie à suivre.

Pour une liste complète, je vous invite à consulter la documentation du module operator : https://docs.python.org/3/library/operator.html.

Exercice Arithmétique simple
----------------------------

Oublions temporairement nos utilisateurs et notre forum, et intéressons-nous à l’évaluation mathématique.

Imaginons que nous voulions représenter une expression mathématique, qui pourrait contenir des termes variables (par exemple, :python:`2 * (-x + 1)`).

Il va nous falloir utiliser un type pour représenter cette variable :python:`x`, appelé :python:`Var`, et un second pour l’expression non évaluée, :python:`Expr`. Les :python:`Var` étant un type particulier d’expressions.

Nous aurons deux autres types d’expressions : les opérations arithmétiques unaires **(+, -)** et binaires **(+, -, \*, /, //, %, \*\*)**. Vous pouvez vous appuyer un même type pour ces deux types d’opérations.

L’expression précédente s’évaluerait par exemple à :

.. code-block:: python

  BinOp(operator.mul, 2, BinOp(operator.add, UnOp(operator.neg, Var('x')), 1))

Nous ajouterons à notre type :python:`Expr` une méthode :python:`compute(**values)`, qui permettra de calculer l’expression suivant une valeur donnée, de façon à ce que :python:`Var('x').compute(x=5)` retourne **5**.

Enfin, nous pourrons ajouter une méthode :python:`__repr__` pour obtenir une représentation lisible de notre expression.

.. code-block:: python

  import operator

  def compute(expr, **values):
      if not isinstance(expr, Expr):
          return expr
      return expr.compute(**values)

  class Expr:
      def compute(self, **values):
          raise NotImplementedError

      def __pos__(self):
          return UnOp(operator.pos, self, '+')

      def __neg__(self):
          return UnOp(operator.neg, self, '-')

      def __add__(self, rhs):
          return BinOp(operator.add, self, rhs, '+')

      def __radd__(self, lhs):
          return BinOp(operator.add, lhs, self, '+')

      def __sub__(self, rhs):
          return BinOp(operator.sub, self, rhs, '-')

      def __rsub__(self, lhs):
          return BinOp(operator.sub, lhs, self, '-')

      def __mul__(self, rhs):
          return BinOp(operator.mul, self, rhs, '*')

      def __rmul__(self, lhs):
          return BinOp(operator.mul, lhs, self, '*')

      def __truediv__(self, rhs):
          return BinOp(operator.truediv, self, rhs, '/')

      def __rtruediv__(self, lhs):
          return BinOp(operator.truediv, lhs, self, '/')

      def __floordiv__(self, rhs):
          return BinOp(operator.floordiv, self, rhs, '//')

      def __rfloordiv__(self, lhs):
          return BinOp(operator.floordiv, lhs, self, '//')

      def __mod__(self, rhs):
          return BinOp(operator.mod, self, rhs, '*')

      def __rmod__(self, lhs):
          return BinOp(operator.mod, lhs, self, '*')

  class Var(Expr):
      def __init__(self, name):
          self.name = name

      def compute(self, **values):
          if self.name in values:
              return values[self.name]
          return self

      def __repr__(self):
          return self.name

  class Op(Expr):
      def __init__(self, op, *args):
          self.op = op
          self.args = args

      def compute(self, **values):
          args = [compute(arg, **values) for arg in self.args]
          return self.op(*args)

  class UnOp(Op):
      def __init__(self, op, expr, symbol=None):
          super().__init__(op, expr)
          self.symbol = symbol

      def __repr__(self):
          if self.symbol is None:
              return super().__repr__()
          return '{}{!r}'.format(self.symbol, self.args[0])

  class BinOp(Op):
      def __init__(self, op, expr1, expr2, symbol=None):
          super().__init__(op, expr1, expr2)
          self.symbol = symbol

      def __repr__(self):
          if self.symbol is None:
              return super().__repr__()
          return '({!r} {} {!r})'.format(self.args[0], self.symbol, self.args[1])

  if __name__ == '__main__':
      x = Var('x')
      expr = 2 * (-x + 1)
      print(expr)
      print(compute(expr, x=1))

      y = Var('y')
      expr += y
      print(compute(expr, x=0, y=10))

Les opérateurs sont une notion importante en Python, mais ils sont loin d’être la seule. Le chapitre suivant vous présentera d’autres concepts avancés du Python, qu’il est important de connaître, pour être en mesure de les utiliser quand cela s’avère nécessaire.


Les attributs de classe
=======================

Nous avons déjà rencontré un attribut de classe, quand nous nous intéressions aux parents d’une classe. Souvenez-vous de :python:`__bases__`, nous ne l’utilisions pas sur des instances mais sur notre classe directement.

En Python, les classes sont des objets comme les autres, et peuvent donc posséder leurs propres attributs.

.. code-block:: pycon

  >>> class User:
  ...     pass
  ...
  >>> User.type = 'simple_user'
  >>> User.type
  'simple_user'

Les attributs de classe peuvent aussi se définir dans le corps de la classe, de la même manière que les méthodes.

.. code-block:: python

  class User:
      type = 'simple_user'

On notera à l’inverse qu’il est aussi possible de définir une méthode de la classe depuis l’extérieur :

.. code-block:: pycon

  >>> def User_repr(self):
  ...     return '<User>'
  ...
  >>> User.__repr_\_ = User_repr
  >>> User()
  <User>

L’avantage des attributs de classe, c’est qu’ils sont aussi disponibles pour les instances de cette classe. Ils sont partagés par toutes les instances.

.. code-block:: pycon

  >>> john = User()
  >>> john.type
  'simple_user'
  >>> User.type = 'admin'
  >>> john.type
  'admin'

C’est le fonctionnement du MRO de Python, il cherche d’abord si l’attribut existe dans l’objet, puis si ce n’est pas le cas, le cherche dans les classes parentes.

Attention donc, quand l’attribut est redéfini dans l’objet, il sera trouvé en premier, et n’affectera pas la classe.

.. code-block:: pycon

  >>> john = User()
  >>> john.type
  'admin'
  >>> john.type = 'superadmin'
  >>> john.type
  'superadmin'
  >>> User.type
  'admin'
  >>> joe = User()
  >>> joe.type
  'admin'

Attention aussi, quand l’attribut de classe est un objet mutable { Un objet mutable est un objet que l’on peut modifier (liste, dictionnaire) par opposition à un objet immutable (nombre, chaîne de caractères, tuple) }, il peut être modifié par n’importe quelle instance de la classe.

.. code-block:: pycon

  >>> class User:
  ...     users = []
  ...
  >>> john, joe = User(), User()
  >>> john.users.append(john)
  >>> joe.users.append(joe)
  >>> john.users
  [<__main__.User object at 0x7f3b7acf8b70>, <__main__.User object at 0x7f3b7acf8ba8>]

L’attribut de classe est aussi conservé lors de l’héritage, et partagé avec les classes filles (sauf lorsque les classes filles redéfinissent l’attribut, de la même manière que pour les instances).

.. code-block:: pycon

  >>> class Guest(User):
  ...     pass
  ...
  >>> Guest.users
  [<__main__.User object at 0x7f3b7acf8b70>, <__main__.User object at 0x7f3b7acf8ba8>]
  >>> class Admin(User):
  ...     users = []
  ...
  >>> Admin.users
  []

Méthodes de Classes
===================

Comme pour les attributs, des méthodes peuvent être définies au niveau de la classe. C’est par exemple le cas de la méthode :python:`mro`.

.. code-block:: python

  int.mro()

**Les méthodes de classe constituent des opérations relatives à la classe mais à aucune instance**. Elles recevront la classe courante en premier paramètre (nommé :python:`cls`, correspondant au :python:`self` des méthodes d’instance), et auront donc accès aux autres attributs et méthodes de classe.

Reprenons notre classe :python:`User`, à laquelle nous voudrions ajouter le stockage de tous les utilisateurs, et la génération automatique de l’id.
Il nous suffirait d’une même méthode de classe pour stocker l’utilisateur dans un attribut de classe :python:`users`, et qui lui attribuerait un :python:`id` en fonction du nombre d’utilisateurs déjà enregistrés.

.. code-block:: pycon

  >>> root = Admin('root', 'toor')
  >>> root
  <User: 1, root>
  >>> User('john', '12345')
  <User: 2, john>
  >>> guest = Guest('guest')
  <User: 3, guest>

Les méthodes de classe se définissent comme les méthodes habituelles, à la différence près qu’elles sont précédées du décorateur :python:`classmethod`.

.. code-block:: python

  import crypt

  class User:
      users = []

      def __init__(self, name, password):
          self.name = name
          self._salt = crypt.mksalt()
          self._password = self._crypt_pwd(password)
          self.register(self)

      @classmethod
      def register(cls, user):
          cls.users.append(user)
          user.id = len(cls.users)

      def _crypt_pwd(self, password):
          return crypt.crypt(password, self._salt)

      def check_pwd(self, password):
          return self._password == self._crypt_pwd(password)

      def __repr__(self):
          return '<User: {}, {}>'.format(self.id, self.name)

  class Guest(User):
      def __init__(self, name):
          super().__init__(name, '')

      def check_pwd(self, password):
          return True

  class Admin(User):
      def manage(self):
          print('Je suis un Jedi!')

Vous pouvez constater le résultat en réessayant le code donné plus haut.

Méthodes statiques
==================

Les méthodes statiques sont très proches des méthodes de classe, mais sont plus **à considérer comme des fonctions au sein d’une classe**.

Contrairement aux méthodes de classe, elles ne recevront pas le paramètre :python:`cls`, et n’auront donc pas accès aux attributs de classe,
méthodes de classe ou méthodes statiques.

Les méthodes statiques sont plutôt dédiées à des comportements annexes en rapport avec la classe, par exemple on pourrait remplacer notre attribut :python:`id` par un :python:`uuid` aléatoire, dont la génération ne dépendrait de rien d’autre dans la classe.

Elles se définissent avec le décorateur :python:`staticmethod`.

.. code-block:: python

  import uuid

  class User:
      def __init__(self, name, password):
          self.id = self._gen_uuid()
          self.name = name
          self._salt = crypt.mksalt()
          self._password = self._crypt_pwd(password)

      @staticmethod
      def _gen_uuid():
          return str(uuid.uuid4())

      def _crypt_pwd(self, password):
          return crypt.crypt(password, self._salt)

      def check_pwd(self, password):
          return self._password == self._crypt_pwd(password)

.. code-block:: pycon

  >>> john = User('john', '12345')
  >>> john.id
  '69ef1327-3d96-42a9-94e6-622619fbf666'


Recherche d’attributs
=====================

Nous savons récupérer et assigner un attribut dont le nom est fixé, cela se fait facilement à l’aide des instructions :python:`obj.foo` et :python:`obj.foo = value`.

Mais nous est-il possible d’accéder à des attributs dont le nom est variable ?

Prenons une instance :python:`john` de notre classe :python:`User`, et le nom d’un attribut que nous voudrions extraire :

.. code-block:: pycon

  >>> john = User('john', '12345')
  >>> attr = 'name'

La fonction :python:`getattr` nous permet alors de récupérer cet attribut.

.. code-block:: pycon

  >>> getattr(john, attr)
  'john'

Ainsi, :python:`getattr(obj, 'foo')` est équivalent à :python:`obj.foo`.

On trouve aussi une fonction :python:`hasattr` pour tester la présence d’un attribut dans un objet. Elle est construite comme :python:`getattr` mais retourne un booléen pour savoir si l’attribut est présent ou non.

.. code-block:: pycon

  >>> hasattr(john, 'name')
  True
  >>> hasattr(john, 'last_name')
  False
  >>> hasattr(john, 'id')
  True

De la même manière, les fonctions :python:`setattr` et :python:`delattr` servent respectivement à modifier et supprimer un attribut.

.. code-block:: pycon

  >>> setattr(john, 'name', 'peter') # équivalent à `john.name = 'peter'`
  >>> john.name
  'peter'
  >>> delattr(john, 'name') # équivalent à \`del john.name\`
  >>> john.name
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  AttributeError: 'User' object has no attribute 'name'


Les propriétés
==============

Les propriétés sont une manière en Python de «dynamiser» les attributs d’un objet. Ils permettent de générer des attributs à la volée à partir de méthodes de l’objet.

Un exemple vaut mieux qu’un long discours :

.. code-block:: pycon

  class ProfilePicture:
      @property
      def picture(self):
          return '{}-{}.png'.format(self.id, self.name)

  class UserPicture(ProfilePicture, User):
      pass

On définit donc une propriété :python:`picture` avec :python:`@property`, qui s’utilise comme un attribut. Chaque fois qu’on appelle :python:`picture`, la méthode correspondante est appelée et le résultat est calculé.

.. code-block:: pycon

  >>> john = UserPicture('john', '12345')
  >>> john.picture
  '1-john.png'
  >>> john.name = 'John'
  >>> john.picture
  '1-John.png'

Il s’agit là d’une propriété en lecture seule, il nous est en effet impossible de modifier la valeur de l’attribut :python:`picture`.

.. code-block:: pycon

  >>> john.picture = 'toto.png'
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  AttributeError: can't set attribute

Pour le rendre modifiable, il faut ajouter à notre classe la méthode permettant de gérer la modification, à l’aide du décorateur :python:`@picture.setter` (le décorateur setter de notre propriété picture, donc).

On utilisera ici un attribut :python:`_picture`, qui pourra contenir l’adresse de l’image si elle a été définie, et :python:`None` le cas échéant.

.. code-block:: python

  class ProfilePicture:
      def __init__(self, *args, **kwargs):
          super().__init__(*args, **kwargs)
          self._picture = None

      @property
      def picture(self):
          if self._picture is not None:
              return self._picture
          return '{}-{}.png'.format(self.id, self.name)

      @picture.setter
      def picture(self, value):
          self._picture = value

  class UserPicture(ProfilePicture, User):
      pass

.. code-block:: pycon

  >>> john = UserPicture('john', '12345')
  >>> john.picture
  '1-john.png'
  >>> john.picture = 'toto.png'
  >>> john.picture
  'toto.png'

Enfin, on peut aussi coder la suppression de l’attribut à l’aide de :python:`@picture.deleter`, ce qui revient à réaffecter :python:`None` à l’attribut :python:`_picture`.

.. code-block:: pycon

  class ProfilePicture:
      def __init__(self, *args, **kwargs):
          super().__init__(*args, **kwargs)
          self._picture = None

      @property
      def picture(self):
          if self._picture is not None:
              return self._picture
          return '{}-{}.png'.format(self.id, self.name)

      @picture.setter
      def picture(self, value):
          self._picture = value

      @picture.deleter
      def picture(self):
          self._picture = None

  class UserPicture(ProfilePicture, User):
      pass

.. code-block:: pycon

  >>> john = UserPicture('john', '12345')
  >>> john.picture
  '1-john.png'
  >>> john.picture = 'toto.png'
  >>> john.picture
  'toto.png'
  >>> del john.picture
  >>> john.picture
  '1-john.png'


Classes abstraites
==================

La notion de classes abstraites est utilisée lors de l’héritage pour forcer les classes filles à implémenter certaines méthodes (dites méthodes abstraites) et donc respecter une interface.

Les classes abstraites ne font pas partie du cœur même de Python, mais sont disponibles via un module de la bibliothèque standard, :python:`abc` (Abstract Pere Classes). Ce module contient notamment la classe :python:`ABC` et le décorateur :python:`@abstractmethod`, pour définir respectivement une classe abstraite et une méthode abstraite de cette classe.

Une classe abstraite doit donc hériter d’:python:`ABC`, et utiliser le décorateur cité pour définir ses méthodes abstraites.

.. code-block:: python

  import abc

  class MyABC(abc.ABC):
      @abc.abstractmethod
      def foo(self):
          pass

Il nous est impossible d’instancier des objets de type :python:`MyABC`, puisqu’une méthode abstraite n’est pas implémentée :

.. code-block:: pycon

  >>> MyABC()
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  TypeError: Can't instantiate abstract class MyABC with abstract methods foo

Il en est de même pour une classe héritant de :python:`MyABC` sans redéfinir la méthode.

.. code-block:: pycon

  >>> class A(MyABC):
  ... pass
  ...
  >>> A()
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  TypeError: Can't instantiate abstract class A with abstract methods foo

Aucun problème par contre avec une autre classe qui redéfinit bien la méthode.

.. code-block:: pycon

  >>> class B(MyABC):
  ... def foo(self):
  ...     return 7
  ...
  >>> B()
  <__main__.B object at 0x7f33065316a0>
  >>> B().foo()
  7

Exercice : Base de données
--------------------------

nous aborderons les méthodes de classe et les propriétés.

Reprenons notre forum, auquel nous souhaiterions ajouter la gestion d’une base de données.

Notre base de données sera une classe avec deux méthodes, :python:`insert` et :python:`select`. Son implémentation est libre, elle doit juste respecter l’interface suivante :

.. code-block:: pycon

  >>> class A: pass
  ...
  >>> class B: pass
  ...
  >>>
  >>> db = Database()
  >>> obj = A()
  >>> obj.value = 42
  >>> db.insert(obj)
  >>> obj = A()
  >>> obj.value = 5
  >>> db.insert(obj)
  >>> obj = B()
  >>> obj.value = 42
  >>> obj.name = 'foo'
  >>> db.insert(obj)
  >>>
  >>> db.select(A)
  <__main__.A object at 0x7f033697f358>
  >>> db.select(A, value=5)
  <__main__.A object at 0x7f033697f3c8>
  >>> db.select(B, value=42)
  <__main__.B object at 0x7f033697f438>
  >>> db.select(B, value=42, name='foo')
  <__main__.B object at 0x7f033697f438>
  >>> db.select(B, value=5)
  ValueError: item not found

Nous ajouterons ensuite une classe :python:`Model`, qui se chargera de stocker dans la base toutes les instances créées. :python:`Model` comprendra une méthode de classe :python:`get(**kwargs)` chargée de réaliser une requête select sur la base de données et de retourner l’objet correspondant. Les objets de type :python:`Model` disposeront aussi d’une propriété :python:`id`, retournant un identifiant unique de l’objet.

On pourra alors faire hériter nos classes :python:`User` et :python:`Post` de :python:`Model`, afin que les utilisateurs et messages soient stockés en base de données. Dans un second temps, on pourra faire de :python:`Model` une classe abstraite, par exemple en rendant abstraite la méthode :python:`__init__`.

.. code-block:: python

  import abc
  import datetime

  class Database:
      data = []

      def insert(self, obj):
          self.data.append(obj)

      def select(self, cls, **kwargs):
          items = (item for item in self.data
              if isinstance(item, cls)
              and all(hasattr(item, k) and getattr(item, k) == v
              for (k, v) in kwargs.items()))
          try:
              return next(items)
          except StopIteration:
              raise ValueError('item not found')

  class Model(abc.ABC):
      db = Database()
      @abc.abstractmethod
      def __init__(self):
          self.db.insert(self)
      @classmethod
      def get(cls, \**kwargs):
          return cls.db.select(cls, \**kwargs)
      @property
      def id(self):
          return id(self)

  class User(Model):
      def __init__(self, name):
          super().__init__()
          self.name = name

  class Post(Model):
      def __init__(self, author, message):
          super().__init__()
          self.author = author
          self.message = message
          self.date = datetime.datetime.now()

      def format(self):
          date = self.date.strftime('le %d/%m/%Y à %H:%M:%S')
          return '<div><span>Par {} {}</span><p>{}</p></div>'.format(self.author.name, date, self.message)

  if __name__ == '__main__':
      john = User('john')
      peter = User('peter')
      Post(john, 'salut')
      Post(peter, 'coucou')

      print(Post.get(author=User.get(name='peter')).format())
      print(Post.get(author=User.get(id=john.id)).format())

Ces dernières notions ont dû compléter vos connaissances du modèle objet de Python, et vous devriez maintenant être prêts à vous lancer dans un projet exploitant ces concepts.
