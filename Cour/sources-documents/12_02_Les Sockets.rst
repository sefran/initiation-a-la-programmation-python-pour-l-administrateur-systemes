.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Les Sockets
***********

Le sujet de ce chapitre est de pouvoir utiliser des primitives réseau de bas niveau pour se connecter sur un serveur distant. Et de la même façon, monter un serveur distant pour répondre au client.

Principes du réseau
===================

Communication des machines distantes sur le réseau ?
----------------------------------------------------

Une machine serveur (qui fournie un service) pour communiquer avec les autres machines informatiques sur un réseau doit être identifiable. Pour cela elle doit être munie de ce que l'on appelle une adresse pour le réseau. C'est une adresse IP. Elle est de la forme **xxx.xxx.xxx.xxx** pour le format d'adresse IPv4, ou de la forme **XXXX:XXXX:…:XXXX:XXXX** pour le format d'adresse IPv6.

Une machine cliente (c'est à dire qui demande un service) contacte cette machine serveur, suivant son adresse IP, et celle-ci une foi contacté répondra à sa demande.

On a donc un fonctionnement de client-serveur. L'un fait une demande, l'autre lui apporte une réponse.

Atteindre le bon service ?
--------------------------

Un serveur peut cependant héberger plusieurs services à fournir aux clients. Par exemple un serveur peut héberger un serveur web mais également un serveur de messagerie.

Alors comment se connecter au bon service ?

En utilisant **les ports**. Les ports les plus connus sont **21** pour le **FTP**, **80** pour le **HTTP**, **443** pour le **HTTPS**, le **22** pour le **SSH**, **25** pour le **SMTP**, **110** pour le service **POP**, etc.

Si vous voulez voir sur quels ports tournent vos services vous pouvez exécuter la commande suivante:

.. code-block:: console

  sudo cat /etc/services

L'idée c'est que le client fasse une demande de ce type: **192.168.0.1:9696** (adresse IP **192.168.0.1** et port **9696** ) puis de créer un lien entre ce port **9696** et notre programme.

Pour réaliser ce besoin on utilise des **stockets** .

Un socket c'est quoi ?
----------------------

En anglais un socket est un "trou" qui laisse passer des choses, comme une prise électrique, un filtre à café, une passoire etc.

Le socket est donc dans notre cas, une passerelle au niveau de l'OS entre un programme qui tourne en boucle et le port de la machine qui lui a été dédié. On dit d'ailleurs que «le programme écoute le port qui lui a été réservé». Il récupère les informations de communications sur le port et répond à cette communication par ce port.

Sockets en python
=================

Pour comprendre le fonctionnement des sockets avec python, nous allons mettre en œuvre un client et un serveur avec deux fichiers «**server.py**» et «**client.py**».
Le premier script Python sera le serveur qui écoutera les demandes des clients.
Le deuxième script script Python «**client.py**» sera donc lancé comme machine cliente, c'est lui qui fera la demande du service au serveur distant.

Fichier «**server.py**» :

.. code-block:: python

  # -*- coding: utf-8 -*-

  import socket

  socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  socket.bind(('', 9696))

  while True:
      socket.listen(5)
      client, addresse = socket.accept()
      print("{} connecté".format(addresse))

      response = client.recv(255)
      if response != "":
          print response

  print("Close")
  client.close()
  stock.close()

client.py

.. code-block:: python

  # -*- coding: utf-8 -*-

  import socket

  hote = "localhost"
  port = 9696

  socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  socket.connect((hote, port))
  print "Connection sur {}".format(port)

  socket.send("Bonjour je suis un client!")

  print "Close"
  socket.close()


Si vous exécutez ces deux programmes vous verrez donc la demande du client se réaliser côté serveur.
