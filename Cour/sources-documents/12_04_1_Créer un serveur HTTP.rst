.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Créer un serveur HTTP
=====================

Servir des pages statiques
--------------------------

Fichier «**repertoire_de_developpement/14_Serveurs/public_html/index.html**» :

.. code-block:: html

  <!DOCTYPE html>
  <html lang="fr">
    <head>
      <meta charset="utf-8">
      <title>Serveur WEB Statique Python</title>
    </head>
    <body>
      <header>Une page WEB</header>
      <main>Le corps de la page</main>
      <footer>Par un développeur Python</footer>
    </body>
  </html>


Fichier «**repertoire_de_developpement/14_Serveurs/serveur_statique_http.py**» :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import os
  import http.server
  import socketserver

  PORT = 10000

  os.chdir(os.path.expanduser('./public_html'))

  with socketserver.TCPServer(('', PORT), http.server.SimpleHTTPRequestHandler) as httpd:
      print('Serveur http sur port ', PORT)
      httpd.serve_forever()


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./serveur_statique_http.py
  Serveur http sur port  10000


.. image:: images/serveur_statique_http.png
   :alt: Affichage page statique html
   :align: center
   :scale: 100%


Servir des pages dynamiques
---------------------------

Fichier «**repertoire_de_developpement/14_Serveurs/serveur_dynamique_http.py**» :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import os
  import http.server

  PORT = 10000
  ref_serveur = ('', PORT)

  os.chdir(os.path.expanduser('./public_html'))

  entree = http.server.CGIHTTPRequestHandler
  entree.cgi_directories = ['/']
  print("Serveur actif sur le port :", PORT)

  httpd = http.server.HTTPServer(ref_serveur, entree)
  httpd.serve_forever()


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./serveur_dynamique_http.py
  Serveur actif sur le port : 10000


Créer aussi une page cgi pour le serveur WEB afin d'afficher un contenu.

Créer un fichier «**repertoire_de_developpement/14_Serveurs/public_html/index.py**» à la racine de votre projet HTML :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import cgi

  form = cgi.FieldStorage()
  print("Content-type: text/html; charset=utf-8\n")
  print(form.getvalue("nom"))

  html = """<!DOCTYPE html>
  <head>
      <title>Mon site</title>
  </head>
  <body>
      <form action="/index.py" method="post">
          <input type="text" name="nom" value="Votre nom SVP" />
          <input type="submit" name="send" value="Envoyer l'information au serveur">
      </form>
  </body>
  </html>
  """
  print(html)

Ouvrez votre navigateur et indiquez-lui l'url de votre serveur web, dans notre cas ce sera **localhost:10000/index.py**.

.. image:: images/serveur_dynamique_http-1.png
   :alt: Affichage page dynamique html
   :align: center
   :scale: 100%

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./serveur_dynamique_http.py
  Serveur actif sur le port : 10000
  127.0.0.1 - - [24/Nov/2021 15:49:37] code 403, message CGI script is not executable ('//index.py')
  127.0.0.1 - - [24/Nov/2021 15:49:37] "GET /index.py HTTP/1.1" 403 -

Arrêtez le serveur WEB avec **Ctrl+c**.

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ chmod u+x public_html/index.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./serveur_dynamique_http.py
  Serveur actif sur le port : 10000


.. image:: images/serveur_dynamique_http-2.png
   :alt: Affichage page dynamique html
   :align: center
   :scale: 100%

.. image:: images/serveur_dynamique_http-3.png
   :alt: Affichage page dynamique html
   :align: center
   :scale: 100%

.. image:: images/serveur_dynamique_http-4.png
   :alt: Affichage page dynamique html
   :align: center
   :scale: 100%


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./serveur_dynamique_http.py
  Serveur actif sur le port : 10000
  127.0.0.1 - - [24/Nov/2021 17:08:04] "GET /index.py HTTP/1.1" 200 -
  127.0.0.1 - - [24/Nov/2021 17:10:33] "POST /index.py HTTP/1.1" 200 -


Python ne fait pas de différences entre **POST** et **GET**, vous pouvez passer une variable dans l'url le résultat sera le même **http://localhost:10000/index.py?name=Prenom%20NOM**

Pour afficher les erreurs sur votre page web, vous pouvez ajouter la fonction :

.. code-block:: python

  import cgitb

  cgitb.enable()

Pour afficher les variables d'environnement, vous pouvez appeler la méthode :python:`test()`.

.. code-block:: python

  cgi.test()

Pour approfondir ces fonctionnalités allez jeter un œuil sur `Common Gateway Interface support <https://docs.python.org/fr/3/library/cgi.html>`_
