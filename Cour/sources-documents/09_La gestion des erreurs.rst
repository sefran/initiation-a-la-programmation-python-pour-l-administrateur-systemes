.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

La gestion des erreurs
######################

Les erreurs de syntaxe
**********************

Jusqu'ici, les messages d'erreurs ont seulement été mentionnés. Mais si vous avez essayé les exemples vous avez certainement vu plus que cela.
En fait, il y a au moins deux types d'erreurs à distinguer : les erreurs de syntaxe et les exceptions.

Les erreurs de syntaxe, qui sont des erreurs d'analyse du code, sont peut-être celles que vous rencontrez le plus souvent lorsque vous êtes encore en phase d'apprentissage de Python :

.. code-block:: pycon

  >>> while True print('Hello world')
    File "<stdin>", line 1
      while True print('Hello world')
                 ^
  SyntaxError: invalid syntax

L'analyseur indique la ligne incriminée et affiche une petite «flèche» pointant vers le premier endroit de la ligne où l'erreur a été détectée.
L'erreur est causée (ou, au moins, a été détectée comme telle) par le symbole placé avant la flèche. Dans cet exemple la flèche est sur la fonction :python:`print()` car il manque deux points ('**:**') juste avant. Le nom du fichier et le numéro de ligne sont affichés pour vous permettre de localiser facilement l'erreur lorsque le code provient d'un script.

Exceptions
**********

Même si une instruction ou une expression est syntaxiquement correcte, elle peut générer une erreur lors de son exécution. Les erreurs détectées durant l'exécution sont appelées des exceptions et ne sont pas toujours fatales : nous apprendrons bientôt comment les traiter dans vos programmes. La plupart des exceptions toutefois ne sont pas prises en charge par les programmes, ce qui génère des messages d'erreurs comme celui-ci :

.. code-block:: pycon

  >>> 10 * (1 / 0)
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  ZeroDivisionError: division by zero
  >>> 4 + spam * 3
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  NameError: name 'spam' is not defined
  >>> '2' + 2
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  TypeError: Can't convert 'int' object to str implicitly

La dernière ligne du message d'erreur indique ce qui s'est passé. Les exceptions peuvent être de différents types et ce type est indiqué dans le message : les types indiqués dans l'exemple sont :python:`ZeroDivisionError`, :python:`NameError` et :python:`TypeError`. Le texte affiché comme type de l'exception est le nom de l'exception native qui a été déclenchée. Ceci est vrai pour toutes les exceptions natives mais n'est pas une obligation pour les exceptions définies par l'utilisateur (même si c'est une convention bien pratique). Les noms des exceptions standards sont des identifiants natifs (pas des mots-clef réservés).

Le reste de la ligne fournit plus de détails en fonction du type de l'exception et de ce qui l'a causée.

La partie précédente du message d'erreur indique le contexte dans lequel s'est produite l'exception, sous la forme d'une trace de pile d'exécution. En général, celle-ci contient les lignes du code source ; toutefois, les lignes lues à partir de l'entrée standard ne sont pas affichées.

Vous trouvez la liste des exceptions natives et leur signification dans Exceptions natives.

Gestion des exceptions
**********************

Il est possible d'écrire des programmes qui prennent en charge certaines exceptions. Regardez l'exemple suivant, qui demande une saisie à l'utilisateur jusqu'à ce qu'un entier valide ait été entré, mais permet à l'utilisateur d'interrompre le programme (en utilisant **Control-C** ou un autre raccourci que le système accepte) ; notez qu'une interruption générée par l'utilisateur est signalée en levant l'exception :python:`KeyboardInterrupt`.

.. code-block:: pycon

  >>> while True:
  ... try:
  ...     x = int(input("Please enter a number: "))
  ...     break
  ... except ValueError:
  ...     print("Oops! That was no valid number. Try again...")

L'instruction :python:`try` fonctionne comme ceci :

premièrement, la clause :python:`try` (instruction(s) placée(s) entre les mots-clés :python:`try` et :python:`except`) est exécutée.

si aucune exception n'intervient, la clause :python:`except` est sautée et l'exécution de l'instruction :python:`try` est terminée.

si une exception intervient pendant l'exécution de la clause :python:`try`, le reste de cette clause est sauté. Si le type d'exception levée correspond
à un nom indiqué après le mot-clé :python:`except`, la clause :python:`except` correspondante est exécutée, puis l'exécution continue après l'instruction :python:`try`.

si une exception intervient et ne correspond à aucune exception mentionnée dans la clause :python:`except`, elle est transmise à l'instruction :python:`try` de niveau supérieur ; si aucun gestionnaire d'exception n'est trouvé, il s'agit d'une exception non gérée et l'exécution s'arrête avec un message comme indiqué ci-dessus.

Une instruction :python:`try` peut comporter plusieurs clauses except pour permettre la prise en charge de différentes exceptions. Mais un seul gestionnaire, au plus, sera exécuté. Les gestionnaires ne prennent en charge que les exceptions qui interviennent dans la clause ! :python:`try` correspondante, pas dans d'autres gestionnaires de la même instruction :python:`try`. Mais une même clause except peut citer plusieurs exceptions sous la forme d'un tuple entre parenthèses, comme dans cet exemple :

.. code-block:: pycon

  ... except (RuntimeError, TypeError, NameError):
  ... pass

Une classe dans une clause :python:`except` est compatible avec une exception si elle est de la même classe ou d'une de ses classes dérivées. Mais l'inverse n'est pas vrai, une clause :python:`except` spécifiant une classe dérivée n'est pas compatible avec une classe de base. Par exemple, le code suivant affiche B, C et D dans cet ordre :

.. code-block:: python

  class B(Exception):
      pass

  class C(B):
      pass

  class D(C):
      pass

  for cls in [B, C, D]:
      try:
          raise cls()
      except D:
          print("D")
      except C:
          print("C")
      except B:
          print("B")

Notez que si les clauses :python:`except` avaient été inversées (avec :python:`except B:` en premier), il aurait affiché :terminal:`B, B, B` — la première clause :python:`except` qui correspond est déclenchée.

La dernière clause :python:`except` peut omettre le(s) nom(s) d'exception(s) et joue alors le rôle de joker. C'est toutefois à utiliser avec beaucoup de précautions car il est facile de masquer une vraie erreur de programmation par ce biais. Elle peut aussi être utilisée pour afficher un message d'erreur avant de propager l'exception (en permettant à un appelant de gérer également l'exception) :

.. code-block:: python

  import sys

  try:
      f = open('myfile.txt')
      s = f.readline()
      i = int(s.strip())
  except OSError as err:
      print("OS error: {0}".format(err))
  except ValueError:
      print("Could not convert data to an integer.")
  except:
      print("Unexpected error:", sys.exc_info()[0])
      raise

L'instruction :python:`try … except` accepte également une clause :python:`else` optionnelle qui, lorsqu'elle est présente, doit se placer après toutes les clauses :python:`except`. Elle est utile pour du code qui doit être exécuté lorsqu'aucune exception n'a été levée par la clause :python:`try`. Par exemple :

.. code-block:: python

  for arg in sys.argv[1:]:
      try:
          f = open(arg, 'r')
      except OSError:
          print('cannot open', arg)
      else:
          print(arg, 'has', len(f.readlines()), 'lines')
          f.close()

Il vaut mieux utiliser la clause :python:`else` plutôt que d'ajouter du code à la clause :python:`try` car cela évite de capturer accidentellement une exception qui n'a pas été levée par le code initialement protégé par l'instruction :python:`try … except`.

Quand une exception intervient, une valeur peut lui être associée, que l'on appelle l'argument de l'exception. La présence de cet argument et son type dépendent du type de l'exception.

La clause :python:`except` peut spécifier un nom de variable après le nom de l'exception. Cette variable est liée à une instance d'exception avec les arguments stockés dans :python:`instance.args`. Pour plus de commodité, l'instance de l'exception définit la méthode :python:`__str__()` afin que les arguments puissent être affichés directement sans avoir à référencer :python:`.args`. Il est possible de construire une exception, y ajouter ses attributs, puis la lever plus tard.

.. code-block:: pycon

  >>> try:
  ...     raise Exception('spam', 'eggs')
  ... except Exception as inst:
  ...     print(type(inst)) # the exception instance
  ...     print(inst.args)  # arguments stored in .args
  ...     print(inst)       # __str__ allows args to be printed directly,
  ...                       # but may be overridden in exception subclasses
  ...     x, y = inst.args  # unpack args
  ...     print('x =', x)
  ...     print('y =', y)
  ...
  <class 'Exception'>
  ('spam', 'eggs')
  ('spam', 'eggs')
  x = spam
  y = eggs

Si une exception a un argument, il est affiché dans la dernière partie du message des exceptions non gérées.

Les gestionnaires d'exceptions n'interceptent pas que les exceptions qui sont levées immédiatement dans leur clause :python:`try`, mais aussi celles qui sont levées au sein de fonctions appelées (parfois indirectement) dans la clause :python:`try`. Par exemple :

.. code-block:: pycon

  >>> def this_fails():
  ...     x = 1 / 0
  ...
  >>> try:
  ...     this_fails()
  ... except ZeroDivisionError as err:
  ...     print('Handling run-time error:', err)
  ... Handling run-time error: division by zero

Déclencher des exceptions
*************************

L'instruction :python:`raise` permet au programmeur de déclencher une exception spécifique. Par exemple :

.. code-block:: pycon

  >>> raise NameError('HiThere')
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  NameError: HiThere

Le seul argument à :python:`raise` indique l'exception à déclencher. Cela peut être soit une instance d'exception, soit une classe d'exception (une classe dérivée de Exception). Si une classe est donnée, elle est implicitement instanciée via l'appel de son constructeur, sans argument :

.. code-block:: python

  raise ValueError # shorthand for 'raise ValueError()'

Si vous avez besoin de savoir si une exception a été levée mais que vous n'avez pas intention de la gérer, une forme plus simple de l'instruction :python:`raise` permet de propager l'exception :

.. code-block:: pycon

  >>> try:
  ...     raise NameError('HiThere')
  ... except NameError:
  ...     print('An exception flew by!')
  ...     raise
  ...
  An exception flew by!
  Traceback (most recent call last):
    File "<stdin>", line 2, in <module>
  NameError: HiThere

Chaînage d'exceptions
*********************

L'instruction :python:`raise` autorise un :python:`from` optionnel qui permets de chaîner les exceptions en définissant l'attribut :python:`__cause__` de l'exception levée. Par exemple :

.. code-block:: python

  raise RuntimeError from OSError

Cela peut être utile lorsque vous transformez des exceptions. Par exemple :

.. code-block:: pycon

  >>> def func():
  ...     raise IOError
  ...
  >>> try:
  ...     func()
  ... except IOError as exc:
  ...     raise RuntimeError('Failed to open database') from exc
  ...
  Traceback (most recent call last):
    File "<stdin>", line 2, in <module>
    File "<stdin>", line 2, in func
  OSError

L'exception ci-dessus était la cause directe de l'exception suivante :

.. code-block:: text

  Traceback (most recent call last):
    File "<stdin>", line 4, in <module>
  RuntimeError

L'expression suivant le :python:`from` doit être soit une exception soit :python:`None`. Le chaînage d'exceptions se produit automatiquement lorsqu'une exception est levée dans un gestionnaire d'exception ou dans une section :python:`finally`.
Le chaînage d'exceptions peut être désactivé en utilisant l'idiome :python:`from` None :

.. code-block:: pycon

  >>> try:
  ...     open('database.sqlite')
  ... except IOError:
  ...     raise RuntimeError from None
  Traceback (most recent call last):
    File "<stdin>", line 4, in <module>
  RuntimeError

Exceptions définies par l'utilisateur
*************************************

Les programmes peuvent nommer leurs propres exceptions en créant une nouvelle classe d'exception. Les exceptions sont typiquement dérivées de la classe :python:`Exception`, directement ou non.

Les classes d'exceptions peuvent être définies pour faire tout ce qu'une autre classe peut faire. Elles sont le plus souvent gardées assez simples, n'offrant que les attributs permettant aux gestionnaires de ces exceptions d'extraire les informations relatives à l'erreur qui s'est produite. Lorsque l'on crée un module qui peut déclencher plusieurs types d'erreurs distincts, **une pratique courante est de créer une classe de base pour l'ensemble des exceptions définies dans ce module** et de créer des **sous-classes spécifiques d'exceptions pour les différentes conditions d'erreurs** :

.. code-block:: python

  class Error(Exception):
      """Base class for exceptions in this module."""
      pass

  class InputError(Error):
      """Exception raised for errors in the input.

      Attributes:
          expression -- input expression in which the error occurred
          message -- explanation of the error
      """

      def __init__(self, expression, message):
          self.expression = expression
          self.message = message

  class TransitionError(Error):
      """Raised when an operation attempts a state transition that's not allowed.

      Attributes:
          previous -- state at beginning of transition
          next -- attempted new state
          message -- explanation of why the specific transition is not allowed
  """

  def __init__(self, previous, next, message):
      self.previous = previous
      self.next = next
      self.message = message

La plupart des exceptions sont définies avec des noms qui se terminent par «**Error**», comme les exceptions standards.

Beaucoup de modules standards définissent leurs propres exceptions pour signaler les erreurs possibles dans les fonctions qu'ils définissent.

Définition d'actions de nettoyage
*********************************

L'instruction :python:`try` a une autre clause optionnelle qui est destinée à définir des actions de nettoyage devant être exécutées dans certaines circonstances. Par exemple :

.. code-block:: pycon

  >>> try:
  ...     raise KeyboardInterrupt
  ... finally:
  ...     print('Goodbye, world!')
  ...
  Goodbye, world!
  KeyboardInterrupt
  Traceback (most recent call last):
    File "<stdin>", line 2, in <module>

Si la clause :python:`finally` est présente, la clause :python:`finally` est la dernière tâche exécutée avant la fin du bloc :python:`try`. La clause :python:`finally` se lance même si le bloc :python:`try` ne produit pas une exception.

Si une exception se produit durant l'exécution de la clause :python:`try`, elle peut être récupérée par une clause :python:`except. Si l'exception n'est pas récupérée par une clause :python:`except`, l'exception est levée à nouveau après que la clause :python:`finally` a été exécutée.

Une exception peut se produire durant l'exécution d'une clause :python:`except` ou :python:`else`. Encore une fois, l'exception est reprise après que la clause que :python:`finally` ait été exécuté.

Si dans l'exécution d'un bloc :python:`try`, on atteint une instruction :python:`break`, :python:`continue` ou :python:`return`, alors la clause :python:`finally` s'exécute juste avant l'exécution de :python:`break`, :python:`continue` ou :python:`return`.

Si la clause :python:`finally` contient une instruction :python:`return`, la valeur retournée sera celle du :python:`return` de la clause :python:`finally`, et non la valeur du :python:`return` de la clause :python:`try`.

Par exemple :

.. code-block:: pycon

  >>> def bool_return():
  ...     try:
  ...         return True
  ...     finally:
  ...         return False
  ...
  >>> bool_return()
  False

Un exemple plus compliqué :

.. code-block:: pycon

  >>> def divide(x, y):
  ...     try:
  ...         result = x / y
  ...     except ZeroDivisionError:
  ...         print("division by zero!")
  ...     else:
  ...         print("result is", result)
  ...     finally:
  ...         print("executing finally clause")
  ...
  >>> divide(2, 1)
  result is 2.0
  executing finally clause
  >>> divide(2, 0)
  division by zero!
  executing finally clause
  >>> divide("2", "1")
  executing finally clause
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    File "<stdin>", line 3, in divide
  TypeError: unsupported operand type(s) for /: 'str' and 'str'

Comme vous pouvez le voir, la clause :python:`finally` est exécutée dans tous les cas. L'exception de type :python:`TypeError`, déclenchée en divisant deux chaînes de caractères, n'est pas prise en charge par la clause :python:`except` et est donc propagée après que la clause :python:`finally` a été exécutée.

Dans les vraies applications, la clause :python:`finally` est notamment utile pour libérer des ressources externes (telles que des fichiers ou des connexions réseau), quelle qu'ait été l'utilisation de ces ressources.

Actions de nettoyage prédéfinies
********************************

Certains objets définissent des actions de nettoyage standards qui doivent être exécutées lorsque l'objet n'est plus nécessaire, indépendamment du fait que l'opération ayant utilisé l'objet ait réussi ou non. Regardez l'exemple suivant, qui tente d'ouvrir un fichier et d'afficher son contenu à l'écran :

.. code-block:: python

  for line in open("myfile.txt"):
      print(line, end="")

Le problème avec ce code est qu'il laisse le fichier ouvert pendant une durée indéterminée après que le code a fini de s'exécuter. Ce n'est pas un problème avec des scripts simples, mais peut l'être au sein d'applications plus conséquentes. L'instruction :python:`with` permet d'utiliser certains objets comme des fichiers d'une façon qui assure qu'ils seront toujours nettoyés rapidement et correctement.

.. code-block:: python

  with open("myfile.txt") as f:
      for line in f:
          print(line, end="")

Après l'exécution du bloc, le fichier :python:`f` est toujours fermé, même si un problème est survenu pendant l'exécution de ces lignes. D'autres objets qui, comme pour les fichiers, fournissent des actions de nettoyage prédéfinies l'indiquent dans leur documentation.
