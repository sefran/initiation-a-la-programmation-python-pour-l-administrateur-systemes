.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Stocker des objets
******************

pickle
======

le module :python:`pickle` permet de sauvegarder dans un fichier, **au format binaire**, n'importe quel objet Python.

En clair, si pour une raison quelconque, dans un script Python, vous avez besoin de sauvegarder, temporairement ou même de façon plus pérenne, le contenu d'un objet Python (une liste, un dictionnaire, un tuple etc.) au lieu d'utiliser une base de données ou un simple fichier texte, le module :python:`pickle` est fait pour ça.

Il permet de stocker et de restaurer un objet Python, tel quel, sans aucune manipulation supplémentaire.

.. code-block:: pycon

  >>> import pickle
  >>> import string
  >>> ma_liste = list(string.ascii_letters)
  >>> ma_liste
  ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  >>> with open('monfichierpickle', 'wb') as fichier:
  ...     pickle.dump(ma_liste, fichier)
  ...
  >>> with open('monfichierpickle', 'rb') as fichier:
  ...     fichier.read()
  ...
  b'\x80\x04\x95\xd5\x00\x00\x00\x00\x00\x00\x00]\x94(\x8c\x01a\x94\x8c\x01b\x94\x8c\x01c\x94\x8c\x01d\x94\x8c\x01e\x94\x8c\x01f\x94\x8c\x01g\x94\x8c\x01h\x94\x8c\x01i\x94\x8c\x01j\x94\x8c\x01k\x94\x8c\x01l\x94\x8c\x01m\x94\x8c\x01n\x94\x8c\x01o\x94\x8c\x01p\x94\x8c\x01q\x94\x8c\x01r\x94\x8c\x01s\x94\x8c\x01t\x94\x8c\x01u\x94\x8c\x01v\x94\x8c\x01w\x94\x8c\x01x\x94\x8c\x01y\x94\x8c\x01z\x94\x8c\x01A\x94\x8c\x01B\x94\x8c\x01C\x94\x8c\x01D\x94\x8c\x01E\x94\x8c\x01F\x94\x8c\x01G\x94\x8c\x01H\x94\x8c\x01I\x94\x8c\x01J\x94\x8c\x01K\x94\x8c\x01L\x94\x8c\x01M\x94\x8c\x01N\x94\x8c\x01O\x94\x8c\x01P\x94\x8c\x01Q\x94\x8c\x01R\x94\x8c\x01S\x94\x8c\x01T\x94\x8c\x01U\x94\x8c\x01V\x94\x8c\x01W\x94\x8c\x01X\x94\x8c\x01Y\x94\x8c\x01Z\x94e.'
  >>> autre_liste = None
  >>> print(autre_liste)
  None
  >>> with open('monfichierpickle', 'rb') as fichier:
  ...     autre_liste = pickle.load(fichier)
  >>> type(autre_liste)
  <class 'list'>
  >>> autre_liste
  ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  >>> ma_liste == autre_liste
  True

On peut faire la même chose avec un tuple :

.. code-block:: python

  mon_tuple = tuple(string.ascii_letters)

Avec un dictionnaire ou même ses propres objets… **Dans ce cas la classe de l'objet stocké doit être disponible pour pouvoir être utilisée** via :python:`load` du module :python:`pickle`, sinon une erreur :python:`AttributeError` est alors levée.
