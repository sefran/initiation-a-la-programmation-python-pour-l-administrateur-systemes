L’industrialisation du code DEVOPS
**********************************

-  Gitlabs
-  Github/Azure
-  BitbucketInstaller l’infrastructure DEV/OPS

Nous allons installer GitLab avec Docker. De plus, nous utiliserons Ubuntu 21.04 comme système d'exploitation principal.

Prérequis:

- Serveur Ubuntu 21.04
- Min 4 Go de RAM
- Privilèges root

Qu'allons nous faire?

1. Configurer le DNS local
2. Installer Docker
3. Tester Docker
4. Installer Gitlab
5. Configurer et tester Gitlab
6. Autorisations pour Docker et le Runner
7. Configurer et tester le Runner
8. Tester les Pages de Gitlab


.. only:: latex

  .. raw:: latex

    \newpage

.. include:: 02_10_01_Configurer le DNS local.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_10_02_Installer Docker.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_10_03_Tester Docker.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_10_04_Installer GitLab.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_10_05_Configurer et tester GitLab.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_10_06_Autorisations pour Docker et le Runner.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_10_07_Configurer et tester le Runner.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 02_10_08_Tester les Pages GitLab.rst
