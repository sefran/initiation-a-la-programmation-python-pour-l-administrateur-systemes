.. role:: python(code)
  :language: python

.. role:: terminal(code)
  :language: console

Clients de serveurs
*******************

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement$ mkdir 14_Serveurs ; cd 14_Serveurs


HTTP
====

Connexion à un serveur http
---------------------------

Nous allons utiliser l'objet :python:`HTTPConnection`, :python:`http.client.HTTPConnection('www.serveur.web.fr', port=80, timeout=10)`, pour se connecter à un serveur http. Sa propriété :python:`request('GET', '/chemin/vers/page/web/')` nous permettra de parcourir l'arborescence du serveur web. Enfin l'objet :python:`HTTPConnection.getresponse()` nous renverra la réponse du serveur web à cette tentative de connexion avec les propriétés :python:`status` et :python:`reason`.

Fichier «**repertoire_de_developpement/14_Serveurs/connexion_http.py**» :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import http.client

  connexion = http.client.HTTPConnection('utilisateur.documentation.domaine-perso.fr')
  connexion.request('GET', '/initiation_developpement_python_pour_administrateur/')
  resultat = connexion.getresponse()
  print(resultat.status, resultat.reason)
  connexion.close()


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./client_http.py
  200 OK


Pour se connecter en https il suffit d'utiliser l'objet :python:`HTTPSConnection` suivant le même procédé que ci-dessus.


Client à un serveur http
------------------------

Pour lire le contenu HTML renvoyé par le serveur web, nous allons maintenant utiliser la méthode :python:`readline()` de l'objet :python:`getresponse()`. Si on veut lire tout le contenu HTML il faut utiliser la propriété :python:`read()`.

Fichier «**repertoire_de_developpement/14_Serveurs/client_http.py**» :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import http.client

  connexion = http.client.HTTPConnection('utilisateur.documentation.domaine-perso.fr')
  connexion.request('GET', '/initiation_developpement_python_pour_administrateur/')
  resultat = connexion.getresponse()

  if resultat.status == 200:
      for ligne in range(8): # Imprime les 8 premières ligne du retour HTML
          print(resultat.readline())

  connexion.close()


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./client_http.py
  b'\n'
  b'<!DOCTYPE html>\n'
  b'\n'
  b'<html lang="fr">\n'
  b'  <head>\n'
  b'    <meta charset="utf-8" />\n'
  b'    <meta name="viewport" content="width=device-width, initial-scale=1.0" />\n'
  b'    <title>Initiation \xc3\xa0 la programmation Python pour l\xe2\x80\x99administrateur syst\xc3\xa8mes &#8212; Initiation \xc3\xa0 la programmation Python pour l&#39;administrateur syst\xc3\xa8mes</title>\n'


SMTP
====

Connexion à un serveur de messagerie
------------------------------------

Rédiger et envoyer un email
---------------------------

Préparons d’abord le serveur local Postfix.

Éditons le fichier «**/etc/aliases**»

.. code-block:: text

  postmaster: root
  root: utilisateur


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ sudo dpkg-reconfigure postfix


.. image:: images/Serveur_Postfix1.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix2.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix3.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix4.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix5.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix6.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix7.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix8.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix9.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. image:: images/Serveur_Postfix10.png
   :alt: Ecran d'avertissement
   :align: center
   :scale: 100%

.. code-block:: console

  setting synchronous mail queue updates: true
  setting myorigin
  setting destinations: courriel.domaine-perso.fr, MachineUbuntu.domaine-perso.fr, localhost.domaine-perso.fr, localhost
  setting relayhost:
  setting mynetworks: 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
  setting mailbox_size_limit: 0
  setting recipient_delimiter: +
  setting inet_interfaces: loopback-only
  setting inet_protocols: all
  WARNING: /etc/aliases exists, but does not have a root alias.

  Postfix (main.cf) is now set up with the changes above.  If you need to make
  changes, edit /etc/postfix/main.cf (and others) as needed.  To view Postfix
  configuration values, see postconf(1).

  After modifying main.cf, be sure to run 'systemctl reload postfix'.

  Running newaliases
  Traitement des actions différées (« triggers ») pour libc-bin (2.33-0ubuntu5) ...
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ telnet localhost 25
  Trying 127.0.0.1...
  Connected to localhost.
  Escape character is '^]'.
  220 MachineUbuntu.domaine-perso.fr ESMTP Postfix (Ubuntu)
  HELO MachineUbuntu.domaine-perso.fr
  250 MachineUbuntu.domaine-perso.fr
  MAIL FROM: prenom.nom
  250 2.1.0 Ok
  RCPT TO: utilisateur@localhost
  250 2.1.5 Ok
  DATA
  354 End data with <CR><LF>.<CR><LF>
  From: prenom.nom
  To: utilisateur
  Subject: Test de message sur port 25
  Ceci est un test d’envoi sur port 25
  Merci de votre coopération
  .
  250 2.0.0 Ok: queued as 4BD27C0566
  QUIT
  221 2.0.0 Bye
  Connection closed by foreign host.
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ mail
  "/var/mail/utilisateur": 1 message 1 nouveau
  >N   1 prenom.nom@courrie lun. nov. 22 15:  15/637   Test de message sur port 25
  ? 1
  Return-Path: <prenom.nom@courriel.domaine-perso.fr>
  X-Original-To: utilisateur@localhost
  Delivered-To: utilisateur@localhost
  Received: from MachineUbuntu.domaine-perso.fr (localhost [127.0.0.1])
          by MachineUbuntu.domaine-perso.fr (Postfix) with SMTP id 4BD27C0566
          for <utilisateur@localhost>; Mon, 22 Nov 2021 15:33:33 +0100 (CET)
  From: prenom.nom@courriel.domaine-perso.fr
  To: utilisateur@courriel.domaine-perso.fr
  Subject: Test de message sur port 25
  Message-Id: <20211122143406.4BD27C0566@MachineUbuntu.domaine-perso.fr>
  Date: Mon, 22 Nov 2021 15:33:33 +0100 (CET)

  Ceci est un test d’envoi sur port 25
  Merci de votre coopération
  ? q
  1 message sauvegardé dans /home/utilisateur/mbox
  0 message conservé dans /var/mail/utilisateur


Pour nous connecter à un serveur SMTP en Python, nous allons utiliser l'objet :python:`SMTP('serveur')`, et nous enverrons alors un message avec la propriété :python:`send_message()`. Pour rédiger le message nous le ferons avec l'objet :python:`EmailMessage()`.

Fichier «**repertoire_de_developpement/14_Serveurs/client_messagerie.py**» :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import smtplib
  from email.message import EmailMessage

  monemail = EmailMessage()
  monemail.set_content('Message du client Python')
  monemail['Subject'] = 'Objet du message du client Python'
  monemail['From'] = 'prenom.nom@domaine-perso.fr'
  monemail['To'] = 'utilisateur@localhost'

  serveursmtp = smtplib.SMTP('localhost')
  serveursmtp.send_message(monemail)
  serveursmtp.quit()

.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ chmod u+x client_messagerie.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ mail
  Pas de courrier pour utilisateur
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./client_messagerie.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ mail
  "/var/mail/utilisateur": 1 message 1 nouveau
  >N   1 prenom.nom@domaine lun. nov. 22 15:  17/654   Objet du message du client Python
  ? 1
  Return-Path: <prenom.nom@domaine-perso.fr>
  X-Original-To: utilisateur@localhost
  Delivered-To: utilisateur@localhost
  Received: from gitlab.domaine-perso.fr (localhost [127.0.0.1])
          by MachineUbuntu.domaine-perso.fr (Postfix) with ESMTP id 19FCEC0566
          for <utilisateur@localhost>; Mon, 22 Nov 2021 15:42:40 +0100 (CET)
  Content-Type: text/plain; charset="utf-8"
  Content-Transfer-Encoding: 7bit
  MIME-Version: 1.0
  Subject: Objet du message du client Python
  From: prenom.nom@domaine-perso.fr
  To: utilisateur@localhost
  Message-Id: <20211122144240.19FCEC0566@MachineUbuntu.domaine-perso.fr>
  Date: Mon, 22 Nov 2021 15:42:40 +0100 (CET)

  Message du client Python
  ? q
  1 message sauvegardé dans /home/utilisateur/mbox
  0 message conservé dans /var/mail/utilisateur

Envoi avec un fichiers.

Fichier «**repertoire_de_developpement/14_Serveurs/client_messagerie_fichiers.py**»

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import smtplib
  import mimetypes
  from email.message import EmailMessage

  monemail = EmailMessage()
  monemail.set_content('Message du client Python')
  monemail['Subject'] = 'Objet du message du client Python'
  monemail['From'] = 'prenom.nom@domaine-perso.fr'
  monemail['To'] = 'utilisateur@localhost'

  ctype, encodage = mimetypes.guess_type('./client_messagerie.py')
  if ctype is None or encodage is not None:
      ctype = 'application/octet-stream'
  typeprincipal, soustype = ctype.split('/', 1)
  with open('./client_messagerie.py', 'rb') as fichier:
      monemail.add_attachment(fichier.read(), maintype=typeprincipal, subtype=soustype, filename='client_messagerie.py')

  serveursmtp = smtplib.SMTP('localhost')
  serveursmtp.send_message(monemail)
  serveursmtp.quit()


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./client_messagerie_fichiers.py
  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ mail
  "/var/mail/utilisateur": 1 message 1 nouveau
  >N   1 prenom.nom@domaine lun. nov. 22 16:  37/1573  Objet du message du client Python
  ? 1
  Return-Path: <prenom.nom@domaine-perso.fr>
  X-Original-To: utilisateur@localhost
  Delivered-To: utilisateur@localhost
  Received: from gitlab.domaine-perso.fr (localhost [127.0.0.1])
          by MachineUbuntu.domaine-perso.fr (Postfix) with ESMTP id 0946BC0566
          for <utilisateur@localhost>; Mon, 22 Nov 2021 16:17:26 +0100 (CET)
  MIME-Version: 1.0
  Subject: Objet du message du client Python
  From: prenom.nom@domaine-perso.fr
  To: utilisateur@localhost
  Content-Type: multipart/mixed; boundary="===============6562121151377868138=="
  Message-Id: <20211122151727.0946BC0566@MachineUbuntu.domaine-perso.fr>
  Date: Mon, 22 Nov 2021 16:17:26 +0100 (CET)

  --===============6562121151377868138==
  Content-Type: text/plain; charset="utf-8"
  Content-Transfer-Encoding: 7bit

  Message du client Python

  --===============6562121151377868138==
  Content-Type: text/x-python
  Content-Transfer-Encoding: base64
  Content-Disposition: attachment; filename="client_messagerie.py"
  MIME-Version: 1.0

  IyEgL3Vzci9iaW4vZW52IHB5dGhvbjMKIyAtKi0gY29kaW5nOiB1dGYtOCAtKi0KCmltcG9ydCBz
  bXRwbGliCmZyb20gZW1haWwubWVzc2FnZSBpbXBvcnQgRW1haWxNZXNzYWdlCgptb25lbWFpbCA9
  IEVtYWlsTWVzc2FnZSgpCm1vbmVtYWlsLnNldF9jb250ZW50KCdNZXNzYWdlIGR1IGNsaWVudCBQ
  eXRob24nKQptb25lbWFpbFsnU3ViamVjdCddID0gJ09iamV0IGR1IG1lc3NhZ2UgZHUgY2xpZW50
  IFB5dGhvbicKbW9uZW1haWxbJ0Zyb20nXSA9ICdwcmVub20ubm9tQGRvbWFpbmUtcGVyc28uZnIn
  Cm1vbmVtYWlsWydUbyddID0gJ3V0aWxpc2F0ZXVyQGxvY2FsaG9zdCcKCnNlcnZldXJzbXRwID0g
  c210cGxpYi5TTVRQKCdsb2NhbGhvc3QnKQpzZXJ2ZXVyc210cC5zZW5kX21lc3NhZ2UobW9uZW1h
  aWwpCnNlcnZldXJzbXRwLnF1aXQoKQo=

  --===============6562121151377868138==--


FTP
===

Connexion à un serveur ftp
--------------------------

Pour se connecter à un serveur ftp, nous allons utiliser l'objet :python:`FTP` du module :python:`ftplib`. La méthode :python:`getwelcome()` nous retournera le résultat de cette connection.

Fichier «**repertoire_de_developpement/14_Serveurs/connexion_ftp.py**» :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import ftplib

  with ftplib.FTP('ftp.fr.debian.org') as serveurftp:
      print(serveurftp.getwelcome())


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./connexion_ftp.py
  220 Welcome to french Debian FTP server


Client à un serveur ftp
-----------------------

Pour lire le contenu d'un serveur ftp nous allons utiliser la méthode :python:`dir()` de l'objet :python:`FTP`. Nous verons aussi la propriété :python:`login('nom_utilisateur', 'mot_de_passe')` qui est la connexion effective au serveur ftp.

Fichier «**repertoire_de_developpement/14_Serveurs/client_ftp.py**» :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import ftplib

  with ftplib.FTP('ftp.fr.debian.org') as serveurftp:
      try:
          serveurftp.login()
          fichiers = []
          serveurftp.dir(fichiers.append)
          print(fichiers)
      except ftplib.all_errors as e:
          print('Erreur FTP :', e)


.. code-block:: console

  utilisateur@MachineUbuntu:~/repertoire_de_developpement/14_Serveurs$ ./client_ftp.py
  ['drwxr-xr-x    9 1000     1000         4096 Nov 24 09:33 debian', 'drwxr-xr-x    8 1000     1000         4096 Mar 01  2015 debian-amd64', 'drwxr-sr-x    5 1000     1000          102 Mar 13  2016 debian-backports', 'drwxr-xr-x    6 1000     1000          143 Feb 10  2017 debian-non-US', 'drwxr-xr-x    7 1000     1000          142 Nov 23 22:32 debian-security', 'drwxr-sr-x    5 1000     1000          138 Nov 01  2011 debian-volatile', 'drwxr-xr-x    2 1000     1000            6 Nov 24 00:00 tmp']

Pour passer une commande au serveur ftp, nous allons utiliser la méthode :python:`sendcmd()` pour passer une commande ftp. Nous utiliserons aussi la méthode :python:`ftplib.parse257()` pour retourner uniquement le répertoire de l'information du serveur ftp. Et aussi l'utilisation de la méthode :python:`pwd()` nous permettra de retourner le chemin ftp du serveur. Enfin nous allons changer de répertoire avec la méthode :python:`cwd()`.

Fichier «**repertoire_de_developpement/14_Serveurs/commandes_ftp.py**» :

.. code-block:: python

  #! /usr/bin/env python3
  # -*- coding: utf-8 -*-

  import ftplib

  with ftplib.FTP('ftp.fr.debian.org') as serveurftp:
      try:
          serveurftp.login()

          # Envoie de la commande FTP PWD qui affiche le répertoire courant
          répertoire_courant = serveurftp.sendcmd('PWD')
          print(ftplib.parse257(répertoire_courant))

          # La même chose avec la propriété pwd()
          répertoire_courant = serveurftp.pwd()
          print(répertoire_courant)

          # Change de répertoire
          serveurftp.cwd('debian')
          répertoire_courant = serveurftp.pwd()
          print(répertoire_courant)

      except ftplib.all_errors as e:
          print('Erreur FTP :', e)

De la même façon, nous pouvons créer des répertoires avec la méthode :python:`mkd('nouveau_répertoire')`. Pour afficher le résultat nous pourrons alors utiliser :python:`serveurftp.retrlines('LIST', fichiers.append).`

Pour lire la taille d'un fichier texte c'est :python:`taille = serveurftp.size('debian/README')`. Par contre pour un fichier binaire, il faudra basculer en mode binaire avec la commande :python:`serveurftp.sendcmd('TYPE I')` et faire alors la commande :python:`taille = serveurftp.size('debian/ls-lR.gz')`.

Pour télécharger un fichier, c'est par exemple avec :

.. code-block:: python

  import os
  import ftplib

  with ftplib.FTP('ftp.fr.debian.org') as serveurftp:

      fichier_origine = 'debian/README'
      nom_fichier_téléchargé = 'LISEZMOI'

      try:
          serveurftp.login()

          with open(nom_fichier_téléchargé, 'w') as fichier:
              resultat = serveurftp.retrlines('RETR ' + fichier_origine, serveurftp.write)
              if not resultat.startswith('226 Transfer complete'):
                  print('Télécgargement échoué')
                  if os.path.isfile(nom_fichier_téléchargé):
                      os.remove(nom_fichier_téléchargé)

      except ftplib.all_errors as e:
          print('Erreur FTP :', e)

          if os.path.isfile(nom_fichier_téléchargé):
              os.remove(nom_fichier_téléchargé)


De la même façon pour envoyer un fichier il faudra utiliser la commande :python:`resultat = serveurftp.retrlines('STOR ' + nomdefichierdestination, fichier)`. L'option d':python:`open()` étant biensûr :python:`'rb'`.
