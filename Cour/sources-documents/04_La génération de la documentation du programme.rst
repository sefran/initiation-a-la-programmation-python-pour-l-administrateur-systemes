La génération de la documentation du programme
##############################################

Voir https://www.codeflow.site/fr/article/documenting-python-code

Sphinx est un outil très complet permettant de générer des documentations riches et bien structurées. Il a originellement été créé pour la documentation du langage Python, et a très vite été utilisé pour documenter de nombreux autres projets.

Pour ce qui est de la documentation de code, il est évidemment bien adapté au Python, mais peut aussi être utilisé avec d'autres langages.

Parmi les fonctionnalités que Sphinx propose :

-  la génération automatique de la documentation à partir du code (avec le support de nombreux langages),
-  la possibilité de faire des références entre les pages,
-  le support de plusieurs formats de sortie (HTML, PDF, LaTeX, EPUB, pages de manuel, ...),
-  la gestion d'extensions permettant de l'adapter à toutes les situations et langages.


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 04_01_Configurer Sphinx pour Python.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 04_02_Rédiger la documentation.rst


.. only:: latex

  .. raw:: latex

    \newpage


.. include:: 04_03_Mise en œuvre de la documentation avec Gitlab.rst


.. only:: latex

  .. raw:: latex

    \newpage
