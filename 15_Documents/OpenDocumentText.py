#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from odf.opendocument import OpenDocumentText
from odf.style import PageLayout, MasterPage, Footer, Style, TextProperties, ParagraphProperties, TableProperties, TableColumnProperties
from odf.text import H, P, Span
from odf.table import Table, TableColumn, TableRow, TableCell

document_texte = OpenDocumentText()

# Mise en page
mise_en_page = PageLayout(name='Mise en page')
document_texte.automaticstyles.addElement(mise_en_page)
page_principale = MasterPage(name='Standard', pagelayoutname=mise_en_page)
document_texte.masterstyles.addElement(page_principale)

# Styles
style = document_texte.styles
# Création d'un style
style_de_titre = Style(name='Titre principal', parentstylename='Standard', family='paragraph')
style_de_titre.addElement(TextProperties(attributes={'fontsize':'24pt', 'fontweight':'bold'}))
style.addElement(style_de_titre)

# Un style automatique
style_gras = Style(name='Texte en Gras', family='text')
en_gras = TextProperties(fontweight='bold')
style_gras.addElement(en_gras)
document_texte.automaticstyles.addElement(style_gras)

# Un tableau
contenu_tableau = Style(name='Contenu tableau', family='paragraph')
contenu_tableau.addElement(ParagraphProperties(numberlines='false', linenumber='0'))
style.addElement(contenu_tableau)
# Styles automatiques du tableau
pagination_tableau = Style(name='Pagination tableau', family='table')
pagination_tableau.addElement(TableProperties(width='10cm', align='center'))
document_texte.automaticstyles.addElement(pagination_tableau)
colonne1 = Style(name='Colonne de gauche', family='table-column')
colonne1.addElement(TableColumnProperties(columnwidth='2cm'))
document_texte.automaticstyles.addElement(colonne1)
colonne2 = Style(name='Colonne de droite', family='table-column')
colonne2.addElement(TableColumnProperties(columnwidth='8cm'))
document_texte.automaticstyles.addElement(colonne2)

# Un paragraphe avec un saut de page
saut_de_page = Style(name='Saut de page', parentstylename='Standard', family='paragraph')
saut_de_page.addElement(ParagraphProperties(breakbefore='page'))
document_texte.automaticstyles.addElement(saut_de_page)

# Titre de document
ligne_de_titre = H(outlinelevel=1, stylename=style_de_titre, text="Mon titre de document")
document_texte.text.addElement(ligne_de_titre)

# Texte
paragraphe1 = P(text="Bonjour à tous!")
document_texte.text.addElement(paragraphe1)
paragraphe2 = P(text="")
section_en_gras = Span(stylename=style_gras, text="Ceci est un passage en gras.")
paragraphe2.addElement(section_en_gras)
paragraphe2.addText(" Ceci est après la section en gras.")
document_texte.text.addElement(paragraphe2)

# Tableau
tableau = Table(name='Tableau_Python3', stylename='Pagination tableau')
tableau.addElement(TableColumn(numbercolumnsrepeated=1, stylename='colonne1'))
tableau.addElement(TableColumn(numbercolumnsrepeated=1, stylename='colonne2'))
ligne1_tableau = TableRow()
cellule1 = TableCell(stylename='colonne1')
ligne1_tableau.addElement(cellule1)
cellule2 = TableCell(stylename='colonne2')
ligne1_tableau.addElement(cellule2)
tableau.addElement(ligne1_tableau)
ligne2_tableau = TableRow()
tableau.addElement(ligne2_tableau)
cellule3 = TableCell(stylename='colonne1')
ligne2_tableau.addElement(cellule3)
cellule4 = TableCell(stylename='colonne2')
ligne2_tableau.addElement(cellule4)
cellule1.addElement(P(stylename=contenu_tableau, text="Colonne 1"))
cellule2.addElement(P(stylename=contenu_tableau, text="Colonne 2"))
cellule3.addElement(P(stylename=contenu_tableau, text="Contenu 1"))
cellule4.addElement(P(stylename=contenu_tableau, text="Contenu 2"))
document_texte.text.addElement(tableau)

# Saut de page
paragraphe3 = P(stylename=saut_de_page, text='Texte de deuxième page')
document_texte.text.addElement(paragraphe3)
document_texte.save('monpremierdocument.odt')
