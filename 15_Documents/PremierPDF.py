#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from fpdf import FPDF

# Création du contenu PDF
contenupdf = FPDF()

# Ajout d'une page
contenupdf.add_page()

# Police de carractères
contenupdf.set_font('Helvetica', size=15)

# Création d'un cadre texte
contenupdf.cell(200, 10, txt='Cour Python 3 pour l\'administrateur systèmes', ln=1, align='C')

# Ajout d'un autre cadre de texte
contenupdf.cell(200, 10, txt='Création d\'un PDF.', ln=2, align='C')

# Sauvegarde du PDF dans un fichier
contenupdf.output('PremierPDF.pdf')
