#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, pypfop

format_de_fichier = 'pdf' # 'pdf', 'rtf', 'tiff', 'png', 'pcl', 'ps', 'txt'
donnees = {
    'entete': ['Nom', 'Prénom', 'Age', 'Sexe'],
    'lignes': [
        ('PYTHON', 'Programmeur', 25, 'M'),
        ('ANONYME', 'Personne', 30, 'F'),
        ('INCONNU', 'Utilisateur', 43, 'H')
    ]
}

chemin_document = pypfop.generate_document('tableau.fo.mako', donnees, 'tableau.css', tempdir='.', out_format=format_de_fichier)
os.rename(chemin_document, os.path.join(os.path.dirname(chemin_document), 'tableau-xsl-fo.pdf'))
