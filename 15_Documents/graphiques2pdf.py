#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from pylatex import Command, Package, Document, Section, Subsection, TextColor
from pylatex.utils import italic, bold, NoEscape
from pylatex import Math, TikZ, Figure, Axis, Plot, TikZNode, TikZOptions, TikZCoordinate, TikZDraw, TikZUserPath

options_mise_en_page = {'tmargin': '1cm', 'lmargin': '3cm'}
document = Document(geometry_options=options_mise_en_page, fontenc=None, inputenc=None)

document.documentclass = Command('documentclass', options=['a4paper', '10pt'], arguments=['article'])
document.packages.append(Package('xcolor'))

document.packages.append(Package('babel', 'french'))
#document.packages.append(Package('french'))
document.preamble.append(Command('selectlanguage', 'french'))

document.preamble.append(Command('title', NoEscape(r'\color{red}Des graphiques\color{black}')))
document.preamble.append(Command('author', 'Programmeur PYTHON'))
document.preamble.append(Command('date', NoEscape(r'\today')))
document.append(NoEscape(r'\maketitle'))

with document.create(Section(NoEscape(r'\color{gray}Les graphiques\color{black}'))):
    with document.create(Subsection(NoEscape(r'\color{teal}Une image\color{black}'))):
        with document.create(Figure(position='h!')) as image:
            image.add_image(os.path.abspath('./images/graph1.png'), width='360pt')
            image.add_caption('Une image')
    with document.create(Subsection(NoEscape(r'\color{teal}Une courbe\color{black}'))):
        with document.create(TikZ()):
            options_de_trace = 'height=8cm, width=12cm, grid=major, domain=0.001:10'
            with document.create(Axis(options=options_de_trace)) as graphe:
                graphe.append(Plot(name='Courbe', func='sin(deg(x))/x', options=['samples=200', 'patch type=quadratic spline', 'blue', 'mark=None']))
                coordonees = [(0.0, 1.0), (1.0, 0.85), (2.0, 0.45), (3.0, 0.05), (4.0, -0.2), (5.0, -0.2), (6.0, -0.05), (7.0, 0.1), (8.0, 0.10), (9.0, 0.05), (10.0, -0.05)]
                graphe.append(Plot(name='Mesures', coordinates=coordonees, options=['only marks', 'red', 'mark=x']))
    with document.create(Subsection(NoEscape(r'\color{teal}Un diagramme\color{black}'))):
        with document.create(TikZ()) as diagramme:
            noderond_kwargs = {'draw': 'green!60', 'fill': 'green!5', 'minimum size': '7mm'}
            noeud_rond = TikZOptions('circle','very thick', **noderond_kwargs)
            nodecarre_kwargs = {'draw': 'red!60', 'fill': 'red!5', 'minimum size': '5mm'}
            noeud_carre = TikZOptions('rectangle','very thick', **nodecarre_kwargs)
            position_noeud1 = TikZCoordinate(0, 2)
            noeud1 = TikZNode(text='1', handle='node1', options=noeud_rond, at=position_noeud1)
            position_noeud2 = TikZCoordinate(0, 1)
            noeud2 = TikZNode(text='2', handle='node2', options=noeud_carre, at=position_noeud2)
            position_noeud3 = TikZCoordinate(1, 1)
            noeud3 = TikZNode(text='3', handle='node3', options=noeud_carre, at=position_noeud3)
            position_noeud4 = TikZCoordinate(0, 0)
            noeud4 = TikZNode(text='4', handle='node4', options=noeud_rond, at=position_noeud4)
            diagramme.append(noeud1)
            diagramme.append(noeud2)
            diagramme.append(noeud3)
            diagramme.append(noeud4)
            diagramme.append(TikZDraw([noeud1.south, '--', noeud2.north], options=TikZOptions('->')))
            diagramme.append(TikZDraw([noeud2.east, '--', noeud3.west], options=TikZOptions('->')))
            diagramme.append(TikZDraw([noeud3.south, TikZUserPath('.. controls +(down:7mm) and +(right:7mm) ..'), noeud4.east], options=TikZOptions('->')))
            

document.generate_pdf('graphiques', clean_tex=False, compiler='xelatex')
