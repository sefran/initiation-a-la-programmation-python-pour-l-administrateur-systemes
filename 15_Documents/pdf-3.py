#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import fpdf

# Création du contenu PDF
# Orientation : 'P' pour portrait, 'L' pour paysage.
# format : Format général du document 'A3', 'A4', 'A5', 'letter', 'legal'
# unit : 'mm', 'cm', 'in', 'pt'
contenupdf = fpdf.FPDF(unit='mm')

# Police de carractères
#contenupdf.add_font('Mafonte', '', 'ttf/CooperHewitt-Book.ttf', uni=True)
#contenupdf.add_font('Mafonte', 'I', 'ttf/CooperHewitt-BookItalic.ttf', uni=True)
#contenupdf.add_font('Mafonte', 'B', 'ttf/CooperHewitt-Bold.ttf', uni=True)
#contenupdf.add_font('Mafonte', 'BI', 'ttf/CooperHewitt-BoldItalic.ttf', uni=True)
#contenupdf.add_font('MafonteThin', '', 'ttf/CooperHewitt-Thin.ttf', uni=True)
#contenupdf.add_font('MafonteThin', 'I', 'ttf/CooperHewitt-ThinItalic.ttf', uni=True)
#contenupdf.add_font('MafonteThin', 'B', 'ttf/CooperHewitt-Light.ttf', uni=True)
#contenupdf.add_font('MafonteThin', 'BI', 'ttf/CooperHewitt-LightItalic.ttf', uni=True)
#contenupdf.add_font('MafonteMedium', '', 'ttf/CooperHewitt-Medium.ttf', uni=True)
#contenupdf.add_font('MafonteMedium', 'I', 'ttf/CooperHewitt-MediumItalic.ttf', uni=True)
#contenupdf.add_font('MafonteBold', '', 'ttf/CooperHewitt-Semibold.ttf', uni=True)
#contenupdf.add_font('MafonteBold', 'I', 'ttf/CooperHewitt-SemiboldItalic.ttf', uni=True)
#contenupdf.add_font('MafonteBold', 'B', 'ttf/CooperHewitt-Heavy.ttf', uni=True)
contenupdf.add_font('MafonteBold', 'BI', 'ttf/CooperHewitt-HeavyItalic.ttf', uni=True)

# Options du document
contenupdf.set_compression(True)
contenupdf.set_display_mode('fullpage', 'two')
contenupdf.set_title('Mon PDF de développeur')
contenupdf.set_author('Développeur Python')
contenupdf.set_creator('FPDF Ubuntu')
contenupdf.set_subject('Document de test FPDF généré avec Python')
contenupdf.set_keywords('Python FPDF Ubuntu')

#Pied de page
def footer():
    # À 10 mm du bas
    contenupdf.set_y(-10)
    contenupdf.set_font('Helvetica', 'B', 8)
    contenupdf.cell(0, 10, 'Page ' + str(contenupdf.page_no()) + '/{nb}', 0, 0, 'C')
contenupdf.footer = footer

# Ajout d'une page paysage A5
contenupdf.add_page(orientation='L', format='A5')
# Choix police de caractère
contenupdf.set_font('MafonteBold', 'BIU', 15)
contenupdf.set_text_color(150, 150, 150)
# Création d'un cadre texte
contenupdf.cell(200, 10, txt='Cour Python 3 pour l\'administrateur systèmes', ln=1, align='C')
# Affichage des fontes de carractères
taille_police = 8
for fonte in contenupdf.core_fonts:
    if any([lettre for lettre in fonte if lettre.isupper()]):
        continue
    contenupdf.set_font('MafonteBold', 'BIU', 8)
    contenupdf.cell(0, 10, txt='Fonte {} - {} pts'.format(fonte, taille_police), ln=1, align='C')
    contenupdf.set_font(fonte, size=taille_police)
    contenupdf.cell(0, 10, txt='abcdefghijklmnopqrstuvwxyz', ln=1, align='C')
    contenupdf.cell(0, 10, txt='ABCDEFGHIJKLMNOPQRSTUVWXYZ', ln=1, align='C')
    taille_police += 2
# Ajout nouvelle page portrait A5
contenupdf.add_page(orientation='P', format='A5')
contenupdf.set_font('MafonteBold', 'BIU', 12)
contenupdf.cell(0, 10, txt='UTF-8 : éÉèÈàÀùÙçÇœŒ€±≠¹²³«»…®™←↑→↓', ln=1, align='C')
# Sauvegarde du PDF dans un fichier
contenupdf.output('pdf-3.pdf')
