#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from fpdf import FPDF

# Création du contenu PDF
contenupdf = FPDF()

# Ajout d'une page
contenupdf.add_page()

# Police de carractères
contenupdf.set_font('Helvetica', size=15)

with open('texte.txt', 'r') as fichier:
    # Ajout du texte pour le convertir en PDF
    for ligne in fichier:
        contenupdf.cell(200, 10, txt=ligne, ln=1, align='C')

# Sauvegarde du PDF dans un fichier
contenupdf.output('SecondPDF.pdf')
