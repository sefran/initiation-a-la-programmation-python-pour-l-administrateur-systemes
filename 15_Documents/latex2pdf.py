#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from pylatex import Command, Package, Document, Section, Subsection, Tabular, TextColor
from pylatex.utils import italic, bold, NoEscape
from pylatex import Math

options_mise_en_page = {'tmargin': '1cm', 'lmargin': '10cm'}
document = Document(geometry_options=options_mise_en_page, fontenc=None, inputenc=None)

document.documentclass = Command('documentclass', options=['a4paper', 'landscape', '12pt'], arguments=['article'])

document.packages.append(Package('babel', 'french'))
#document.packages.append(Package('french'))
document.preamble.append(Command('selectlanguage', 'french'))

document.preamble.append(Command('title', NoEscape(r'\color{red} Le titre de mon document \color{black}')))
document.preamble.append(Command('author', 'Programmeur PYTHON'))
document.preamble.append(Command('date', NoEscape(r'\today')))
document.append(NoEscape(r'\maketitle'))

with document.create(Section('La section de mon document')):
    document.append('Une phrase comme ça… \n')
    document.append(italic('Du texte italique.'))
    document.append(bold('Du texte en gras.\n'))
    document.append(TextColor('violet', bold('Gras et en violet.\n')))
    document.append('Des caractères spéciaux : àÀéÉèÈçÇûÛùÙœŒ±≠×÷€$£µ…¿¡§¹²³™©\n')
    document.append('Une phrase "entre guillemets". «ou ici» l\'apostrophe, fi.')
    with document.create(Subsection(NoEscape(r'\color{gray}Les mathématiques \color{black}'))):
        document.append(Math(data=['3*3', '=', 9]))
    with document.create(Subsection(NoEscape(r'\color{gray} Un tableau \color{black}'))):
        with document.create(Tabular('rc|cl')) as tableau:
            tableau.add_hline()
            tableau.add_row((TextColor('teal', 'C1'), TextColor('orange', 'C2'), 'C3', 'C4'))
            tableau.add_hline(1, 2)
            tableau.add_empty_row()
            tableau.add_row(('C5', 'C6', 'C7', 'C8'))

document.generate_pdf('latex', clean_tex=False, compiler='xelatex')
