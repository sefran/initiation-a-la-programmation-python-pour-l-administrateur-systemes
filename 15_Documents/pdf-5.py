#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from fpdf import FPDF, HTMLMixin

class HTMLtoPDF(FPDF, HTMLMixin):
    pass

# Création du contenu PDF
contenupdf = HTMLtoPDF()
# Ajout d'une page
contenupdf.add_page()
# Police de carractères
contenupdf.write_html('''
<!DOCTYPE html>
<html>
<head>
<style>.article {
  background-color: black;
  color: white;
  padding: 20px;
}</style>
</head>
<body>

<h2>Mon site</h2>
<p>Utilisation des styles CSS avec la classe "article" dans un tag HTML :</p>

<div class="artivle">
  <h2>Mon titre</h2>
  <p>Texte de l'article.</p>
  <p>Encore du texte.</p>
</div>

</body>
</html>
''')

# Sauvegarde du PDF dans un fichier
contenupdf.output('HTML.pdf')
