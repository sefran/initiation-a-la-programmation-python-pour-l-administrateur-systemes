#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from odf.opendocument import OpenDocumentSpreadsheet
from odf.style import Style, TextProperties, ParagraphProperties, TableColumnProperties, TableCellProperties, Map
from odf.number import NumberStyle, CurrencyStyle, CurrencySymbol, Number, Text
from odf.text import P
from odf.table import Table, TableColumn, TableRow, TableCell

classeur = OpenDocumentSpreadsheet()

# Création des styles du tableau
style_contenu_classeur = Style(name='argent', family='table-cell')
style_contenu_classeur.addElement(TableCellProperties(textalignsource='fix', repeatcontent='false', verticalalign='middle', border='1.0pt solid #808080'))
style_contenu_classeur.addElement(ParagraphProperties(textalign='center'))
style_contenu_classeur.addElement(TextProperties(fontfamily='Noto Sans', fontsize='15pt'))
classeur.styles.addElement(style_contenu_classeur)
# Style automatiques
# Colonne tableurs
style_colonne = Style(name='colonne1', family='table-column')
style_colonne.addElement(TableColumnProperties(columnwidth='5.0cm', breakbefore='auto'))
classeur.automaticstyles.addElement(style_colonne)

# Style cellules
# Création du style de valeur monétaire financière français euro négative
style_euro_negatif = CurrencyStyle(name='monnaie-euro-negative', volatile='true')
# Change la couleur du texte en rouge
style_euro_negatif.addElement(TextProperties(color='#ff0000'))
# Préfixe le texte avec le symbole négatif
style_euro_negatif.addElement(Text(text=u'-'))
# Met la valeur numérique en forme avec deux décimales après la virgule, avec au minimum 1 digit et en séparant les milliers
style_euro_negatif.addElement(Number(decimalplaces='2', minintegerdigits='1', grouping='true'))
# afficher le synmbole €
style_euro_negatif.addElement(CurrencySymbol(language='fr', country='FR', text=u' €'))
# Ajout du style
classeur.styles.addElement(style_euro_negatif)
# Création du style de valeur monétaire financière français euro
style_euro = CurrencyStyle(name='monnaie-euro')
# Met la valeur numérique en forme avec deux décimales après la virgule, avec au minimum 1 digit et en séparant les milliers
style_euro.addElement(Number(decimalplaces='2', minintegerdigits='1', grouping='true'))
# formatage conditionnel si négatif afficher le style négatif
style_euro.addElement(Map(condition='value()<0', applystylename='monnaie-euro-negative'))
# Afficher le symbole € à la fin
style_euro.addElement(CurrencySymbol(language='fr', country='FR', text=u' €'))
# Ajout du style
classeur.styles.addElement(style_euro)

# Création du style monétaire de cellule
style_monetaire = Style(name="monnaie", family="table-cell", parentstylename=style_contenu_classeur, datastylename="monnaie-euro")
classeur.automaticstyles.addElement(style_monetaire)

# Création d'un tableau de données monétaires
tableau = Table(name='Tableau de valeurs monétaires')
tableau.addElement(TableColumn(stylename=style_colonne, defaultcellstylename='monnaie'))
ligne1 = TableRow()
tableau.addElement(ligne1)
cellule1 = TableCell(valuetype='currency', currency='EUR', value="-1025.25")
ligne1.addElement(cellule1)
ligne2 = TableRow()
tableau.addElement(ligne2)
cellule2 = TableCell(valuetype="currency", currency="EUR", value="5000023.8")
ligne2.addElement(cellule2)
ligne3 = TableRow()
tableau.addElement(ligne3)
cellule3 = TableCell(valuetype='currency', currency='EUR', value="10089")
ligne3.addElement(cellule3)
ligne4 = TableRow()
tableau.addElement(ligne4)
cellule4 = TableCell(valuetype='currency', currency='EUR', value="-10")
ligne4.addElement(cellule4)


classeur.spreadsheet.addElement(tableau)
#print(classeur.contentxml())
classeur.save("monpremiertableur.ods")
