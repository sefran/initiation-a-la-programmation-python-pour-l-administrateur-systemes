#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from fpdf import FPDF

# Création du contenu PDF
# Orientation : 'P' pour portrait, 'L' pour paysage.
contenupdf = FPDF(orientation='L', unit='mm', format='A4')
# Ajout d'une page
contenupdf.add_page()
# Couleur du dessin
contenupdf.set_draw_color(139, 0, 0)
# Épaisseur du trait
contenupdf.set_line_width(1)
# Tracé d'une ligne
contenupdf.line(10, 10, 100, 100)
# Tracé d'un rectangle
contenupdf.set_draw_color(255, 0, 0)
contenupdf.set_fill_color(210, 105, 30)
contenupdf.rect(20, 20, 60, 60, 'F')
# Tracé d'une ellipse
contenupdf.set_draw_color(0, 255, 0)
contenupdf.set_fill_color(255, 140, 0)
contenupdf.ellipse(30, 30, 40, 40, 'F')
# Ajout d'une image
contenupdf.image('images/graph1.png', x=120, y=30, w=150)
# Sauvegarde du PDF dans un fichier
contenupdf.output('pdf-4.pdf')
