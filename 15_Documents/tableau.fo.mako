<%inherit file="A4-portrait.fo.mako" />
<table id="table-principale">
    <table-header>
        <table-row>
            % for nom in entete:
            <table-cell>
                <block>${nom}</block>
            </table-cell>
            % endfor
        </table-row>
    </table-header>
    <table-body>
        % for ligne in lignes:
        <table-row>
            % for cellule in ligne:
            <table-cell>
                <block>${cellule}</block>
            </table-cell>
            % endfor
        </table-row>
        % endfor
    </table-body>
</table>
