"""
.. module:: mon_module
   :platform: Unix, Windows
   :synopsis: Ce module illustre comment écrire votre docstring dans Python.
.. sectionauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Formateur PYTHON <formateur.python@fai.fr>
.. moduleauthor:: Stagiaire ADMINISTRATEUR <stagiaire.administrateur@fai.fr>


"""

__title__ = "Module illustration écriture docstring Python"
__author__ = "Formateur PYTHON"
__license__ = 'GPL-V3'
__version__ = '0.7.3'
__release_life_cycle__ = 'alpha'
# pre-alpha = faisabilité, alpha = développement, beta = test, rc = qualification, prod = production
__docformat__ = 'reStructuredText'


class ClasseExemple():
    """Cette classe docstring montre comment utiliser sphinx et la syntaxe rst.
    La première ligne est une brève explication de la classe avec ses paramètres et ce que renvoi
    l’objet.
    Cela doit être complété par une description plus précise des méthodes et des attributs dans
    le code de la classe dans le code.
    La seule méthode ici est :func:`mafonction`.

    - **paramètres**, **types**, **retour** et **type de retours**::

          :param arg1: description
          :param arg2: description
          :type arg1: type arg1
          :type arg2: type arg2
          :return: description du retour
          :rtype: type du retour

    - Documentez des sections **:Example:** en utilisant la syntaxe des doubles points ``:``

      .. code-block:: rest

        :Example:

          suivi d'une ligne vierge!

      qui apparaît comme suit :

      :Example:

        suivi d'une ligne vierge!

    - Des sections spéciales telles que **Voir aussi**, **Avertissements**, **Notes** avec la
      syntaxe sphinx (*directives de paragraphe*)::

          .. seealso:: blabla
          .. warnings:: blabla
          .. note:: blabla
          .. todo:: blabla

    .. warning::
        Il existe de nombreux autres champs Info mais ils peuvent être redondants:
            * param, parameter, arg, argument, key, keyword: Description d'un paramètre.
            * type: Type de paramètre.
            * raises, raise, except, exception: Quand une exception spécifique est levée.
            * var, ivar, cvar: Description d'une variable.
            * returns, return: Description de la valeur de retour.
            * rtype: Type de retour.

    .. note::
        Il existe de nombreuses autres directives telles que :
        versionadded, versionchanged, rubric, centered, …
        Voir la documentation sphinx pour plus de détails.

    Voici ci-dessous les résultats pour :func:`mafonction` docstring.
    """

    def mafonction(self, arg1, arg2, arg3):
        """Retourne (arg1 / arg2) + arg3

        Ceci est une explication plus précise, qui peut inclure des mathématiques avec la syntaxe
        latex :math:`\\alpha`.
        Ensuite, vous devez fournir une sous-section facultative (juste pour être cohérent et avoir
        une documentation uniforme. Rien ne vous empêche de changer l'ordre):

          - paramètres utilisés ``:param <nom>: <description>``
          - type des paramètres ``:type <nom>: <description>``
          - retours de la méthode ``:returns: <description>``
          - exemples (doctest)
          - utilisation de voir aussi ``.. seealso:: texte``
          - utilisation des notes ``.. note:: texte``
          - utilisation des alertes ``.. warning:: texte``
          - liste des restes à faire ``.. todo:: text``

        **Avantages**:
         - Utilise les balises sphinx.
         - Belle sortie HTML avec les directives Seealso, Note, Warning.


        **Désavantages**:
         - En regardant simplement la docstring, les sections de paramètres, de types et de retours
           n'apparaissent pas bien dans le code.

        :param arg1: la première valeur
        :param arg2: la première valeur
        :param arg3: la première valeur
        :type arg1: int, float,...
        :type arg2: int, float,...
        :type arg3: int, float,...
        :returns: arg1/arg2 +arg3
        :rtype: int, float

        :Example:

        >>> import template
        >>> a = template.ClasseExemple()
        >>> a.mafonction(1,1,1)
        2

        .. note:: il peut être utile de souligner une caractéristique importante
        .. seealso:: :class:`AutreClasseExemple`
        .. warning:: arg2 doit être différent de zéro.
        .. todo:: vérifier que arg2 est non nul.
        """
        return arg1 / arg2 + arg3
