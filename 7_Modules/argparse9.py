#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbose", action="store_true", help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2
if args.verbose:
    print("le carré de {} est égal à {}".format(args.carré, reponse))
else:
    print(reponse) 
