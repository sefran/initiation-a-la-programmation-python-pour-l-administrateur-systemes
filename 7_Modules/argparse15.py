#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
parser.add_argument("-v", "--verbosity", action="count", default=0)
args = parser.parse_args()
reponse = args.x**args.y
if args.verbosity >= 2:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
elif args.verbosity >= 1:
    print("{}^{} = {}".format(args.x, args.y, reponse))
else:
    print(reponse) 
