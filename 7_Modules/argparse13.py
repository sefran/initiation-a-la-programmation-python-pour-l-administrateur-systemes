#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("carré", type=int, help="affiche le carré du nombre passé en argument")
parser.add_argument("-v", "--verbosity", action="count", help="augmente la verbosité de sortie")
args = parser.parse_args()
reponse = args.carré**2

# corection: remplacer == avec >=
if args.verbosity >= 2:
    print("le carré de {} est égal à {}".format(args.carré , reponse))
elif args.verbosity >= 1:
    print("{}² = {}".format(args.carré , reponse))
else:
    print(reponse) 
