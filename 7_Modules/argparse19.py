#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gettext
__TRANSLATIONS = {
    'ambiguous option: %(option)s could match %(matches)s': 'option ambiguë: %(option)s parmi %(matches)s',
    'argument "-" with mode %r': 'argument "-" en mode %r',
    'cannot merge actions - two groups are named %r': 'cannot merge actions - two groups are named %r',
    "can't open '%(filename)s': %(error)s": "can't open '%(filename)s': %(error)s",
    'dest= is required for options like %r': 'dest= is required for options like %r',
    'expected at least one argument': 'au moins un argument est attendu',
    'expected at most one argument': 'au plus un argument est attendu',
    'expected one argument': 'un argument est nécessaire',
    'ignored explicit argument %r': 'ignored explicit argument %r',
    'invalid choice: %(value)r (choose from %(choices)s)': 'choix invalide: %(value)r (parmi %(choices)s)',
    'invalid conflict_resolution value: %r': 'invalid conflict_resolution value: %r',
    'invalid option string %(option)r: must start with a character %(prefix_chars)r':
        'invalid option string %(option)r: must start with a character %(prefix_chars)r',
    'invalid %(type)s value: %(value)r': 'valeur invalide de type %(type)s: %(value)r',
    'mutually exclusive arguments must be optional': 'mutually exclusive arguments must be optional',
    'not allowed with argument %s': "pas permis avec l'argument %s",
    'one of the arguments %s is required': 'au moins un argument requis parmi %s',
    'optional arguments': 'arguments optionnels',
    'positional arguments': 'arguments positionnels',
    "'required' is an invalid argument for positionals": "'required' is an invalid argument for positionals",
    'show this help message and exit': 'affiche ce message d\'aide',
    'unrecognized arguments: %s': 'argument non reconnu: %s',
    'unknown parser %(parser_name)r (choices: %(choices)s)': 'unknown parser %(parser_name)r (choices: %(choices)s)',
    'usage: ': 'utilisation: ',
    '%(prog)s: error: %(message)s\n': '%(prog)s: erreur: %(message)s\n',
    '%r is not callable': '%r is not callable',
}
gettext.gettext = lambda text: __TRANSLATIONS[text] or text

import argparse

parser = argparse.ArgumentParser(description="calcule X à la puissance Y")
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("x", type=int, help="la base")
parser.add_argument("y", type=int, help="l’exposant")
args = parser.parse_args()
reponse = args.x**args.y

if args.quiet:
    print(reponse)
elif args.verbose:
    print("{} à la puissance {} est égal à {}".format(args.x, args.y, reponse))
else:
    print("{}^{} = {}".format(args.x, args.y, reponse)) 
