#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# Module de fichier python3
print("Mon module __name__ est défini à {}".format(__name__))

if __name__ == "__main__":
    print("Fichier exécuté directement")
else:
    print("Fichier exécuté comme importé")
