#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import win32evtlog 
import pprint 
import sys 

# S'abonne aux événements de l'application et les enregistre 
# Pour déclencher manuellement un nouvel événement, ouvrez une console d'administration et tapez: (remplacez 125 par tout autre ID qui vous convient) 
#   eventcreate.exe /L "application" /t warning /id 125 /d "Ceci est un avertissement de test" 

# event_context peut être `None` si ce n'est pas obligatoire, c'est juste pour montrer comment cela fonctionne 
event_context = { "info": "cet objet est toujours passé à votre retour" } 
# Event log source to listen to 
event_source = 'application' 

def new_logs_event_handler(reason, context, evt): 
 """ 
 Appelé lorsque de nouveaux événements sont enregistrés. 

 raison - raison pour laquelle l'événement a été enregistré? 
 contexte - contexte dans lequel le gestionnaire d'événements a été enregistré
 evt - événement handle 
 """ 
 # Imprimez simplement quelques informations sur l'événement 
 print ('raison', raison, 'contexte', contexte, 'événement handle', evt) 

 # Rendre l'événement en XML, il y a peut-être un moyen d'obtenir un objet mais je ne l'ai pas trouvé 
 print('Événement rendu:', win32evtlog.EvtRender(evt, win32evtlog.EvtRenderEventXml)) 

 # ligne vide pour séparer les journaux 
 print(' - ') 

 # Assurez-vous que tout le texte imprimé est réellement imprimé sur la console maintenant 
 sys.stdout.flush() 

 return 0 

# Abonnez-vous aux futurs événements 
subscription = win32evtlog.EvtSubscribe(event_source, win32evtlog.EvtSubscribeToFutureEvents, None, Callback=new_logs_event_handler, Context=event_context, Query=None) 
