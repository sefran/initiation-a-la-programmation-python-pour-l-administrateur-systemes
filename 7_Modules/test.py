#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, getopt

def main(argv):
   fichierentre = ''
   fichiersortie = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print('utilisation : test.py -i <fichier_en_entrée> -o <fichier_de_sortie>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print('utilisation : test.py -i <fichier_en_entrée> -o <fichier_de_sortie>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         fichierentre = arg
      elif opt in ("-o", "--ofile"):
         fichiersortie = arg
   print('Le fichier en entré est', fichierentre)
   print('Le fichier en sortie est', fichiersortie)

if __name__ == "__main__":
   main(sys.argv[1:]) 
