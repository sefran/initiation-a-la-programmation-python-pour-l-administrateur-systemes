if __name__ == '__main__':
    import time
    import systemd.daemon as service

    print('Démarrage de service_python …')
    time.sleep(5)
    print('Démarrage OK')
    service.notify('READY=1')

    while True:
        print('Réponse du service python de démonstration')
        time.sleep(5)
